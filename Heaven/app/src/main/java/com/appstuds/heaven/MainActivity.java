package com.appstuds.heaven;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.LinePageIndicator;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    CustomSwipeAdapter customSwipeAdapter;
    CirclePageIndicator mIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager)findViewById(R.id.view_pager);
        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        customSwipeAdapter = new CustomSwipeAdapter(this);
        viewPager.setAdapter(customSwipeAdapter);
//
//         ViewPager.OnPageChangeListener mListener = new ViewPager.OnPageChangeListener() {
//
//            @Override
//            public void onPageSelected(int arg0) {
//                // TODO Auto-generated method stub
//                selectedIndex = arg0;
//
//            }
//            boolean callHappened;
//            @Override
//            public void onPageScrolled(int arg0, float arg1, int arg2) {
//                // TODO Auto-generated method stub
//                if( mPageEnd && arg0 == selectedIndex && !callHappened)
//                {
//                    Log.d(getClass().getName(), "Okay");
//                    mPageEnd = false;//To avoid multiple calls.
//                    callHappened = true;
//                }else
//                {
//                    mPageEnd = false;
//                }
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int arg0) {
//                // TODO Auto-generated method stub
//                if(selectedIndex == adapter.getCount() - 1)
//                {
//                    mPageEnd = true;
//                }
//            }
//        };
//        ViewPager.setOnPageChangeListener(mListener);

        mIndicator.setViewPager(viewPager);



    }
}
