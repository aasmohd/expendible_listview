package com.appstuds.heaven;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by H.P on 2/24/2018.
 */

public class Tab1 extends Fragment {
    View root;
    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_tab1, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view_tab1);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_Tab1(getActivity());
        recyclerView.setAdapter(adapter);
        return root;
    }




}
