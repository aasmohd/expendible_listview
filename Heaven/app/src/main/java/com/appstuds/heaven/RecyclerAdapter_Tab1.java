package com.appstuds.heaven;


import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class RecyclerAdapter_Tab1 extends RecyclerView.Adapter<RecyclerAdapter_Tab1.RecyclerViewHolder> {

    Context context;
    View view;
    int [] img_res={R.drawable.man,R.drawable.man};




    public RecyclerAdapter_Tab1(Context context) {

        this.context = context;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapterdata1, null);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.chatPersonName.setText("Byrant Twiford");
        holder.chatPersonImage.setImageResource(img_res[0]);
        holder.chatPersonName1.setText("Byrant Twiford");
        holder.chatPersonImage1.setImageResource(img_res[1]);

    }

    @Override
    public int getItemCount() {

        return 1;

    }




    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
          TextView chatPersonName,chatPersonName1;
          ImageView chatPersonImage,chatPersonImage1;

        public RecyclerViewHolder(View View) {
            super(View);
            chatPersonName = (TextView) View.findViewById(R.id.chatpersonname);
            chatPersonImage = (ImageView) View.findViewById(R.id.chatpersonimage);
            chatPersonName1 = (TextView) View.findViewById(R.id.chatpersonname1);
            chatPersonImage1 = (ImageView) View.findViewById(R.id.chatpersonimage1);

        }
    }



}
