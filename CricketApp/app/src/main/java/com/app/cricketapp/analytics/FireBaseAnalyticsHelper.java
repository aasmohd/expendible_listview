package com.app.cricketapp.analytics;

import android.os.Bundle;

import com.app.cricketapp.ApplicationController;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by Rahul on 30/04/17.
 */
public class FireBaseAnalyticsHelper {
    private static FireBaseAnalyticsHelper fireBaseAnalyticsHelper;

    private FireBaseAnalyticsHelper() {
        //private constructor
    }

    public static synchronized FireBaseAnalyticsHelper getSharedInstance() {
        if (fireBaseAnalyticsHelper == null)
            fireBaseAnalyticsHelper = new FireBaseAnalyticsHelper();

        return fireBaseAnalyticsHelper;
    }

    void identify(String userId) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString("user_id", userId);
            ApplicationController.getApplicationInstance().getDefaultTracker()
                    .logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
