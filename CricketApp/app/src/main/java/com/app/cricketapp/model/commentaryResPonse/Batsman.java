
package com.app.cricketapp.model.commentaryResPonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Batsman {

    @SerializedName("runs")
    @Expose
    private Long runs;
    @SerializedName("dotball")
    @Expose
    private Long dotball;
    @SerializedName("six")
    @Expose
    private Long six;
    @SerializedName("four")
    @Expose
    private Long four;
    @SerializedName("ball_count")
    @Expose
    private Long ballCount;
    @SerializedName("key")
    @Expose
    private String key;

    public Long getRuns() {
        return runs;
    }

    public void setRuns(Long runs) {
        this.runs = runs;
    }

    public Long getDotball() {
        return dotball;
    }

    public void setDotball(Long dotball) {
        this.dotball = dotball;
    }

    public Long getSix() {
        return six;
    }

    public void setSix(Long six) {
        this.six = six;
    }

    public Long getFour() {
        return four;
    }

    public void setFour(Long four) {
        this.four = four;
    }

    public Long getBallCount() {
        return ballCount;
    }

    public void setBallCount(Long ballCount) {
        this.ballCount = ballCount;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
