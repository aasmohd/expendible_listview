package com.app.cricketapp.fcm;

import android.content.Intent;
import android.util.Log;

import com.app.cricketapp.service.SaveUserService;
import com.app.cricketapp.utility.AppConstants;
import com.app.cricketapp.utility.Utility;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by rahulgupta on 16/03/17.
 * Firebase instance id generation service
 */

public class MyFireBaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i(MyFireBaseInstanceIDService.class.getSimpleName()
                , "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        Utility.putStringValueInSharedPreference(this, AppConstants.PREFS_DEVICE_TOKEN, refreshedToken);
        startSaveUserService();

    }

    private void startSaveUserService() {
        Intent intent = new Intent(this, SaveUserService.class);
        startService(intent);
    }
}
