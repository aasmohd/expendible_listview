package com.app.cricketapp.msupport

import android.Manifest

/**
 * Created by Rahul on 28 Feb 2017.
 */
object MSupportConstants {
    /**
     * This Constants for SDK needed to be compare with
     */
    val SDK_VERSION = 23
    val RC_GET_ACCOUNTS = 123

    /**
     * marshmallow permission list constants
     */
    val GET_ACCOUNTS = Manifest.permission.GET_ACCOUNTS
}
