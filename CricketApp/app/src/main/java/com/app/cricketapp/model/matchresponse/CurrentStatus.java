package com.app.cricketapp.model.matchresponse;

import android.text.TextUtils;

import com.app.cricketapp.utility.Lg;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rahulgupta on 04/08/17.
 */

public class CurrentStatus implements IMatch {

    @SerializedName("NB")
    @Expose
    public String NB;
    @SerializedName("WB")
    @Expose
    public String WB;
    @SerializedName("RUN")
    @Expose
    public String RUN;
    @SerializedName("WK")
    @Expose
    public String WK;
    @SerializedName("BYE")
    @Expose
    public String BYE;
    @SerializedName("LBYE")
    @Expose
    public String LBYE;
    @SerializedName("MSG")
    @Expose
    public String MSG;
    @SerializedName("OTHER")
    @Expose
    public String OTHER;
    @SerializedName("REVERTED")
    @Expose
    public String REVERTED;

    public String getTvMessage() {
        if (TextUtils.isEmpty(MSG) || MSG.equals("0"))
            return "";
        return MSG;
    }

    public String getOtherText() {
        if (TextUtils.isEmpty(OTHER))
            return "";
        return OTHER;
    }

    public String isLastStatusRollBacked() {
        return REVERTED;
    }

    public String isWicket() {
        if (TextUtils.isEmpty(WK))
            return "0";
        return WK;
    }

    public String isWideBall() {
        if (TextUtils.isEmpty(WB))
            return "0";
        return WB;
    }

    public String isNoBall() {
        if (TextUtils.isEmpty(NB))
            return "0";
        return NB;
    }

    public int getScoredRuns() {
        try {
            return Integer.parseInt(RUN);
        } catch (Exception e) {
            Lg.i("Error", e.getMessage());
            return -1;
        }
    }

    public String getByeRuns() {
        if (TextUtils.isEmpty(BYE))
            return "0";
        return BYE;
    }

    public String getLegBye() {
        if (TextUtils.isEmpty(LBYE))
            return "0";
        return LBYE;
    }

}
