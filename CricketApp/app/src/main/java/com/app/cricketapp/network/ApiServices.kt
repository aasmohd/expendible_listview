package com.app.cricketapp.network


import com.app.cricketapp.model.MyPollAccuracy.MainFeed
import com.app.cricketapp.model.RequestBean
import com.app.cricketapp.model.commentaryResPonse.CommentaryRes
import com.app.cricketapp.model.matches.MatchesResponse
import com.app.cricketapp.model.newRecentMatchResponse.NodeResponseRecent
import com.app.cricketapp.model.pollresponsemodel.PollResponse
import com.app.cricketapp.model.ranking.RankingResponse
import com.app.cricketapp.model.save_user.NodeResponseSignup
import com.app.cricketapp.model.scorecard.ScorecardResponse
import com.app.cricketapp.model.seasonmodels.seasonDetail.SeasonDetailModel
import com.app.cricketapp.model.seasonmodels.seasonsList.SeasonListModel
import com.app.cricketapp.model.upcomingmatchresponse.NodeResponse
import com.app.cricketapp.utility.AppConstants
import com.google.gson.JsonElement
import retrofit2.Call
import retrofit2.http.*

interface ApiServices {

    @get:GET(AppConstants.RECENT_MATCHES_URL)
    val recentMatches: Call<JsonElement>

    @POST(AppConstants.NEW_RECENT_MATCHES_URL)
    fun newRecentMatches(): Call<NodeResponseRecent>

    @get:GET(AppConstants.UPCOMING_URL)
    val upcomingMatches: Call<JsonElement>

    @POST(AppConstants.NEW_UPCOMING_URL)
    fun newUpcomingMatches(): Call<NodeResponse>

    @POST(AppConstants.WATCH_LIVE_API)
    fun liveWatch(): Call<JsonElement>

    @POST(AppConstants.LATEST_NEWS_URL)
    fun latestNews(): Call<JsonElement>

    @GET(AppConstants.MATCHES)
    fun fireBaseMatches(): Call<MatchesResponse>


    @POST(AppConstants.SCORECARD_URL)
    fun scorecard(@Body requestBean: RequestBean): Call<ScorecardResponse>


    @POST(AppConstants.SAVE_USER_URL)
    fun saveUser(@Body requestBean: RequestBean): Call<NodeResponseSignup>

    @POST(AppConstants.POLL_DATA_URL)
    fun pollData(@Body requestBean: RequestBean): Call<PollResponse>

    @POST(AppConstants.POLL_PREDICT)
    fun predictPoll(@Body requestBean: RequestBean): Call<PollResponse>

    @POST(AppConstants.RANKING_URL)
    fun rankingData(): Call<RankingResponse>

    @POST(AppConstants.SEASON_LIST_URL)
    fun seasonList(): Call<SeasonListModel>

    @POST(AppConstants.SEASON_DETAIL_URL)
    fun seasonDetail(@Body requestBean: RequestBean): Call<SeasonDetailModel>

    @POST(AppConstants.BALL_BY_BALL_URL)
    fun getCommentary(@Body requestBean: RequestBean): Call<CommentaryRes>

    @FormUrlEncoded
    @POST("v1/poll/userPoll")
     fun getResults(@Field("appUserID") appUserID: String , @Field("pageNo") pageNo:Int): Call<MainFeed>


}
