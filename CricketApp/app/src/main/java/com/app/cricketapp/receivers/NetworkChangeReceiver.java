package com.app.cricketapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.app.cricketapp.ApplicationController;
import com.app.cricketapp.model.eve.NetworkChangeEvent;
import com.app.cricketapp.utility.Lg;
import com.app.cricketapp.utility.Utility;

import org.greenrobot.eventbus.EventBus;

/**
 * Network change receiver to check network connectivity
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        EventBus.getDefault().post(new NetworkChangeEvent());
        if (Utility.getNetworkState(context)) {
            ApplicationController.getApplicationInstance().setIsNetworkConnected(true);
            Lg.i("Network Receiver", "connected");
        } else {
            ApplicationController.getApplicationInstance().setIsNetworkConnected(false);
            Lg.i("Network Receiver", "disconnected");
        }
    }
}

