package com.app.cricketapp.utility;

import java.util.HashMap;

/**
 * Created by rahulgupta on 28/04/17.
 * List of timezones and their abbreviations
 */

class TimeZoneList {
    private static HashMap<String, String> timeZoneMap = new HashMap<>();

    static {
        timeZoneMap.put("UTC", "GMT");
        timeZoneMap.put("GMT+1:00", "ECT");
        timeZoneMap.put("GMT+01:00", "ECT");
        timeZoneMap.put("GMT+2:00", "EET");
        timeZoneMap.put("GMT+02:00", "EET");
        timeZoneMap.put("GMT+3:00", "EAT");
        timeZoneMap.put("GMT+03:00", "EAT");
        timeZoneMap.put("GMT+3:30", "MET");
        timeZoneMap.put("GMT+03:30", "MET");
        timeZoneMap.put("GMT+4:00", "NET");
        timeZoneMap.put("GMT+04:00", "NET");
        timeZoneMap.put("GMT+5:00", "PLT");
        timeZoneMap.put("GMT+05:00", "PLT");
        timeZoneMap.put("GMT+5:30", "IST");
        timeZoneMap.put("GMT+05:30", "IST");
        timeZoneMap.put("GMT+6:00", "BST");
        timeZoneMap.put("GMT+06:00", "BST");
        timeZoneMap.put("GMT+7:00", "VST");
        timeZoneMap.put("GMT+07:00", "VST");
        timeZoneMap.put("GMT+8:00", "CTT");
        timeZoneMap.put("GMT+08:00", "CTT");
        timeZoneMap.put("GMT+9:00", "JST");
        timeZoneMap.put("GMT+09:00", "JST");
        timeZoneMap.put("GMT+10:00", "AET");
        timeZoneMap.put("GMT+11:00", "SST");
        timeZoneMap.put("GMT+12:00", "NST");
        timeZoneMap.put("GMT-11:00", "MIT");
        timeZoneMap.put("GMT-10:00", "HST");
        timeZoneMap.put("GMT-9:00", "AST");
        timeZoneMap.put("GMT-09:00", "AST");
        timeZoneMap.put("GMT-8:00", "PST");
        timeZoneMap.put("GMT-08:00", "PST");
        timeZoneMap.put("GMT-7:00", "PNT");
        timeZoneMap.put("GMT-07:00", "PNT");
        timeZoneMap.put("GMT-6:00", "CST");
        timeZoneMap.put("GMT-06:00", "CST");
        timeZoneMap.put("GMT-5:00", "EST");
        timeZoneMap.put("GMT-05:00", "EST");
        timeZoneMap.put("GMT-4:00", "PRT");
        timeZoneMap.put("GMT-04:00", "PRT");
        timeZoneMap.put("GMT-3:30", "CNT");
        timeZoneMap.put("GMT-03:30", "CNT");
        timeZoneMap.put("GMT-3:00", "AGT");
        timeZoneMap.put("GMT-03:00", "AGT");
        timeZoneMap.put("GMT-1:00", "CAT");
        timeZoneMap.put("GMT-01:00", "CAT");
    }

    static String getTimezoneShort(String timeZone, Builder builder) {
        String s = timeZoneMap.get(timeZone);
        return s;
    }

    /**
     * Class for customizations
     */
    public static class Builder {

    }
}
