
package com.app.cricketapp.model.ranking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rankings {

    @SerializedName("odiRanking")
    @Expose
    public OdiRanking odiRanking;
    @SerializedName("testRanking")
    @Expose
    public TestRanking testRanking;
    @SerializedName("t20Ranking")
    @Expose
    public T20Ranking t20Ranking;

    public OdiRanking getOdiRanking() {
        return odiRanking;
    }

    public void setOdiRanking(OdiRanking odiRanking) {
        this.odiRanking = odiRanking;
    }

    public TestRanking getTestRanking() {
        return testRanking;
    }

    public void setTestRanking(TestRanking testRanking) {
        this.testRanking = testRanking;
    }

    public T20Ranking getT20Ranking() {
        return t20Ranking;
    }

    public void setT20Ranking(T20Ranking t20Ranking) {
        this.t20Ranking = t20Ranking;
    }

}
