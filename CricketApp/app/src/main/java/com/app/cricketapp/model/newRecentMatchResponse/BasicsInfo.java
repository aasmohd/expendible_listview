
package com.app.cricketapp.model.newRecentMatchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BasicsInfo {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("venue")
    @Expose
    private String venue;
    @SerializedName("ref")
    @Expose
    private String ref;
    @SerializedName("start_date")
    @Expose
    private long startDate;
    @SerializedName("winner_team")
    @Expose
    private String winnerTeam;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public String getWinnerTeam() {
        return winnerTeam;
    }

    public void setWinnerTeam(String winnerTeam) {
        this.winnerTeam = winnerTeam;
    }

}
