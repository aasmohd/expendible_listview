package com.app.cricketapp.customview.tv;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.cricketapp.R;
import com.app.cricketapp.activity.LiveMatchActivity;
import com.app.cricketapp.activity.MatchDetailActivity;
import com.app.cricketapp.databinding.LayoutTvLedBinding;
import com.app.cricketapp.model.matchresponse.CurrentStatus;
import com.app.cricketapp.model.matchresponse.Inning;
import com.app.cricketapp.model.matchresponse.Team;
import com.app.cricketapp.utility.AppConstants;
import com.app.cricketapp.utility.CRICSounds;
import com.app.cricketapp.utility.FireBaseController;
import com.app.cricketapp.utility.Utility;
import com.google.firebase.crash.FirebaseCrash;

import java.io.IOException;

import pl.droidsonroids.gif.GifDrawable;

import static com.app.cricketapp.utility.AppConstants.BALL_TAG;


/**
 * Created by rahulgupta on 26/04/17.
 */

public class MatchLedTvView extends LinearLayout
{
    private LayoutTvLedBinding tvBinding;
    private AnimationDrawable drawable;
    private TvListener tvListener;
    private FireBaseController fireBaseController;
    private boolean isPlaySpeech;
    private int fragmentPos;

    public void setTvListener(TvListener tvListener)
    {
        this.tvListener = tvListener;
    }

    public interface TvListener
    {
        void onActionWicket();

        boolean isScreenVisible();
    }

    public MatchLedTvView(Context context)
    {
        super(context);
        init(null, 0);
    }

    public MatchLedTvView(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        init(attrs, 0);
    }

    public MatchLedTvView(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    public void setFragmentPos(int fragmentPos)
    {
        this.fragmentPos = fragmentPos;
    }

    /**
     * Method to initialize parameters
     *
     * @param attrs        Any custom attributes
     * @param defStyleAttr Default styles
     */
    private void init(AttributeSet attrs, int defStyleAttr)
    {
        tvBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_tv_led, this, true);
        tvBinding.speaker.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                boolean speechFlag = Utility.getBooleanSharedPreference(view.getContext(), AppConstants.SPEECH_KEY);
                Utility.putBooleanValueInSharedPreference(view.getContext(), AppConstants.SPEECH_KEY, !speechFlag);
                checkSpeechFlag();
            }
        });
        tvBinding.matchInfoBtn.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String live_match_key = fireBaseController.getLiveMatchKey();
                Intent intent = new Intent(getContext(), MatchDetailActivity.class);
                intent.putExtra(AppConstants.EXTRA_KEY, live_match_key);
                intent.putExtra("match_category", "live");
                if (live_match_key != null && !live_match_key.equals("0"))
                {
                    getContext().startActivity(intent);
                } else
                {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_scorecard), Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    public void checkSpeechFlag()
    {
        boolean speechFlag = Utility.getBooleanSharedPreference(getContext(), AppConstants.SPEECH_KEY);
        if (speechFlag)
        {
            setAction2Icon(R.drawable.speaker_on);
        } else
        {
            setAction2Icon(R.drawable.speaker_off);
        }
    }

    public void setFireBaseController(FireBaseController fireBaseController)
    {
        this.fireBaseController = fireBaseController;
    }

    public void setTvData(CurrentStatus currentStatus, boolean isPlaySpeech)
    {
        this.isPlaySpeech = isPlaySpeech;
        if (currentStatus != null)
        {
            if (!currentStatus.getTvMessage().equals("Break"))
                tvBinding.tvText.setText(currentStatus.getTvMessage());
            setStatusText(currentStatus);
        }
    }

    public void setAction2Icon(int drawableResId)
    {
        tvBinding.speaker.setImageResource(drawableResId);
    }

    public void setMatchData()
    {
        String matchStatus = fireBaseController.getConfig().matchStatus;
        String currentInningKey = fireBaseController.getValue("currentInning");

        Team battingTeam = fireBaseController.getBattingTeam();
        Team bowlingTeam = fireBaseController.getBowlingTeam();

        /*String liveurl = fireBaseController.getLiveUrlData();
        if (!liveurl.equals("")){
            showLiveUrlBtn(liveurl);
        }*/

        if (matchStatus != null && matchStatus.equalsIgnoreCase("notStarted"))
        {
            tvBinding.battingTeamNameTv.setText("");
            tvBinding.battingTeamOverTv.setText("");
            tvBinding.bowlingTeamNameTv.setText("");
            tvBinding.bowlingTeamOverTv.setText("");
        } else
        {
            if (fireBaseController.getConfig().matchFormat.toLowerCase().equals("test"))
            {
                //For Test Match
                if (currentInningKey.equals("inning1"))
                {
                    //Set only batting team data
                    if (battingTeam != null)
                    {
                        Inning firstInning = fireBaseController.getFirstInning();
                        tvBinding.battingTeamNameTv.setText(battingTeam.name + " " + firstInning.score
                                + "/" + firstInning.wicket);
                        tvBinding.battingTeamOverTv.setText("(" + firstInning.over + ")");

                    } else
                    {
                        tvBinding.battingTeamNameTv.setText("");
                        tvBinding.battingTeamOverTv.setText("");
                    }

                    tvBinding.bowlingTeamNameTv.setText("");
                    tvBinding.bowlingTeamOverTv.setText("");

                }

                if (currentInningKey.equals("inning2"))
                {
                    if (battingTeam != null)
                    {
                        Inning secondInning = fireBaseController.getSecondInning();
                        tvBinding.battingTeamNameTv.setText(battingTeam.name + " " + secondInning.score
                                + "/" + secondInning.wicket);
                        tvBinding.battingTeamOverTv.setText("(" + secondInning.over + ")");
                    }

                    Inning firstInning = fireBaseController.getFirstInning();
                    if (bowlingTeam != null)
                    {
                        tvBinding.bowlingTeamNameTv.setText(bowlingTeam.name + " " + firstInning.score
                                + "/" + firstInning.wicket);
                        tvBinding.bowlingTeamOverTv.setText("(" + firstInning.over + ")");
                    }
                }

                Inning secondInning = fireBaseController.getSecondInning();
                Inning thirdInning = fireBaseController.getThirdInning();
                Inning firstInning = fireBaseController.getFirstInning();
                Inning fourthInning = fireBaseController.getFourthInning();

                if (currentInningKey.equals("inning3"))
                {
                    //Check Follow on scenario


                    if (secondInning.batting.equals(thirdInning.batting))
                    {
                        //if follow on then second inning batting will bat again
                        if (battingTeam != null)
                        {
                            tvBinding.battingTeamNameTv.setText(battingTeam.name + " " + thirdInning.score
                                    + "/" + thirdInning.wicket);
                            tvBinding.battingTeamOverTv.setText("(" + thirdInning.over + ") & " + secondInning.score);
                        }

                        //set bowling team score of first inning batting team
                        if (bowlingTeam != null)
                        {
                            tvBinding.bowlingTeamNameTv.setText(bowlingTeam.name + " " + firstInning.score
                                    + "/" + firstInning.wicket);
                            tvBinding.bowlingTeamOverTv.setText("(" + firstInning.over + ")");
                        }
                    } else
                    {
                        // not follow on
                        if (battingTeam != null)
                        {
                            tvBinding.battingTeamNameTv.setText(battingTeam.name + " " + thirdInning.score
                                    + "/" + thirdInning.wicket);
                            tvBinding.battingTeamOverTv.setText("(" + thirdInning.over + ") & " + firstInning.score);
                        }

                        if (bowlingTeam != null)
                        {
                            tvBinding.bowlingTeamNameTv.setText(bowlingTeam.name + " " + secondInning.score
                                    + "/" + secondInning.wicket);
                            tvBinding.bowlingTeamOverTv.setText("(" + secondInning.over + ")");
                        }
                    }

                }

                if (currentInningKey.equals("inning4"))
                {
                    //Check Previous Follow on scenario
                    if (secondInning.batting.equals(thirdInning.batting))
                    {
                        if (battingTeam != null)
                        {
                            tvBinding.battingTeamNameTv.setText(battingTeam.name + " " + fourthInning.score
                                    + "/" + fourthInning.wicket);
                            tvBinding.battingTeamOverTv.setText("(" + fourthInning.over + ") & " + firstInning.score);
                        }

                        //set bowling team score of first inning batting team
                        if (bowlingTeam != null)
                        {
                            tvBinding.bowlingTeamNameTv.setText(bowlingTeam.name + " " + thirdInning.score
                                    + "/" + thirdInning.wicket);
                            tvBinding.bowlingTeamOverTv.setText("(" + thirdInning.over + ") & " + secondInning.score);
                        }
                    } else
                    {
                        // Not follow on
                        if (battingTeam != null)
                        {
                            tvBinding.battingTeamNameTv.setText(battingTeam.name + " " + fourthInning.score
                                    + "/" + fourthInning.wicket);
                            tvBinding.battingTeamOverTv.setText(" & " + secondInning.score);
                        }

                        if (bowlingTeam != null)
                        {
                            tvBinding.bowlingTeamNameTv.setText(bowlingTeam.name + " " + thirdInning.score);
                            tvBinding.bowlingTeamOverTv.setText(" & " + firstInning.score);
                        }
                    }
                }

            } else
            {
                //For One Day and T20


                Inning currentInnings = fireBaseController.getCurrentInnings();
                if (battingTeam != null)
                {
                    tvBinding.battingTeamNameTv.setText(battingTeam.name + " " + currentInnings.score
                            + "/" + currentInnings.wicket);
                    tvBinding.battingTeamOverTv.setText("(" + currentInnings.over + ")");
                } else
                {
                    tvBinding.battingTeamNameTv.setText("");
                    tvBinding.battingTeamOverTv.setText("");
                }
                if (currentInningKey != null)
                {
                    if (currentInningKey.equals("inning1"))
                    {
                        tvBinding.bowlingTeamNameTv.setText("");
                        tvBinding.bowlingTeamOverTv.setText("");
                    } else
                    {
                        if (battingTeam != null)
                        {
                            tvBinding.battingTeamNameTv.setText(battingTeam.name + " " + currentInnings.score
                                    + "/" + currentInnings.wicket);
                            tvBinding.battingTeamOverTv.setText("(" + currentInnings.over + ")");
                        } else
                        {
                            tvBinding.battingTeamNameTv.setText("");
                            tvBinding.battingTeamOverTv.setText("");
                        }
                        Inning otherInnings = fireBaseController.getOtherInnings();

                        if (bowlingTeam != null)
                        {
                            tvBinding.bowlingTeamNameTv.setText(bowlingTeam.name + " " + otherInnings.score
                                    + "/" + otherInnings.wicket);
                            tvBinding.bowlingTeamOverTv.setText("(" + otherInnings.over + ")");
                        } else
                        {
                            tvBinding.bowlingTeamNameTv.setText("");
                            tvBinding.bowlingTeamOverTv.setText("");
                        }
                    }
                } else
                {
                    tvBinding.bowlingTeamNameTv.setText("");
                    tvBinding.bowlingTeamOverTv.setText("");
                }
            }
        }
    }

    private void setStatusText(CurrentStatus currentStatus)
    {
        handler.removeCallbacks(runnable);

        tvBinding.currentStatus1Tv.setText("");
        tvBinding.currentStatus2Tv.setText("");
        tvBinding.otherTextContainer.setVisibility(GONE);
        tvBinding.upperView.setVisibility(VISIBLE);
        tvBinding.otherActionsImage.setVisibility(GONE);
        tvBinding.matchTvImage.setImageResource(0);
        tvBinding.otherActionsImage.setImageResource(0);
        tvBinding.matchTvImage.setTag(AppConstants.DEFAULT);
        tvBinding.otherActionsImage.setTag(AppConstants.DEFAULT);

        if (checkOtherEnum(currentStatus.getOtherText()) || currentStatus.getTvMessage().equals("Break"))
        {
            tvBinding.otherText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_18));
            tvBinding.otherTextContainer.setVisibility(VISIBLE);
            tvBinding.upperView.setVisibility(GONE);
            tvBinding.otherActionsImage.setVisibility(GONE);
            if (currentStatus.getTvMessage().equals("Break"))
                tvBinding.otherText.setText(currentStatus.getTvMessage());
            else
            {
                if (currentStatus.getOtherText().equals("Bowler Ruka"))
                {
                    boolean isMaleSpeechKey = Utility.getBooleanSharedPreference(getContext(), AppConstants.IS_MALE_SPEECH_KEY);
                    if (isMaleSpeechKey)
                    {
                        tvBinding.otherText.setText(currentStatus.getOtherText());
                    } else
                    {
                        tvBinding.otherText.setText("Bowler Stop");
                    }
                } else
                    tvBinding.otherText.setText(currentStatus.getOtherText());
            }
            tvBinding.otherText.setVisibility(VISIBLE);

            if (currentStatus.getOtherText().equals("3rd Umpire") && tvListener.isScreenVisible())
            {
                if (isPlaySpeech)
                    CRICSounds.THIRD_UMPIRE.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);
            }

        } else if (currentStatus.getOtherText().contains("New Batsman"))
        {
//            tvBinding.currentStatus1Tv.setText("New");
//            tvBinding.currentStatus2Tv.setText("Batsman");
            String[] str = currentStatus.getOtherText().split(" ");
            for (int i = 0; i < str.length; i++)
            {
                if (i == 2)
                {
                    tvBinding.currentStatus1Tv.setText("" + str[i]);
                }
            }

            try
            {
                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.new_batsmen_gif_v2);
                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
            } catch (Exception e)
            {
                FirebaseCrash.report(e);
                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_batsmen);
            }
//            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_batsmen);
        } else if (currentStatus.getOtherText().equals(AppConstants.BALL))
        {
            tvBinding.currentStatus1Tv.setText("Ball");
            boolean isMaleSpeechKey = Utility.getBooleanSharedPreference(getContext(), AppConstants.IS_MALE_SPEECH_KEY);
            if (isMaleSpeechKey)
                tvBinding.currentStatus2Tv.setText("Chalu");
            else
                tvBinding.currentStatus2Tv.setText("Start");

            try
            {
                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.ball_gif);
                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
            } catch (Exception e)
            {
                FirebaseCrash.report(e);
                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_ball);
            }

//            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_ball);
            tvBinding.matchTvImage.setTag(BALL_TAG);

            if (tvListener.isScreenVisible() && isPlaySpeech)
                CRICSounds.BALL_CHALU.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);

        } else if (currentStatus.isWicket().equals("1") && currentStatus.isWideBall().equals("1"))
        {
            tvBinding.currentStatus1Tv.setText("Wide Ball");
            tvBinding.currentStatus2Tv.setText("WICKET");

            try
            {
                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.wicket_gif_v2);
                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
            } catch (Exception e)
            {
                FirebaseCrash.report(e);
                e.printStackTrace();
            }
//            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_wicket);
            tvListener.onActionWicket();
            if (tvListener.isScreenVisible() && isPlaySpeech)
                CRICSounds.WICKET.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);
        } else if (currentStatus.isWicket().equals("1") && currentStatus.isNoBall().equals("1"))
        {
            tvBinding.currentStatus1Tv.setText("No Ball");
            tvBinding.currentStatus2Tv.setText("RUN OUT");


            GifDrawable gifFromResource = null;
            try
            {
                gifFromResource = new GifDrawable(getResources(), R.drawable.wicket_gif_v2);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            tvBinding.matchTvImage.setImageDrawable(gifFromResource);

//            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_wicket);
            tvListener.onActionWicket();
            if (tvListener.isScreenVisible() && isPlaySpeech)
                CRICSounds.WICKET.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);
        } else if (currentStatus.isWicket().equals("1"))
        {

            try
            {
                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.wicket_gif_v2);
                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
            } catch (Exception e)
            {
                FirebaseCrash.report(e);
                e.printStackTrace();
            }
//            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_wicket);
            tvListener.onActionWicket();
            tvBinding.currentStatus1Tv.setText("WICKET");
            if (tvListener.isScreenVisible() && isPlaySpeech)
                CRICSounds.WICKET.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);

            String formattedRuns = getFormattedRuns(currentStatus.getScoredRuns(), false);
            if (!TextUtils.isEmpty(formattedRuns))
            {
                tvBinding.currentStatus2Tv.setText("+\n" + formattedRuns);
            }

        } else if (currentStatus.isWideBall().equals("1"))
        {
            tvBinding.currentStatus1Tv.setText("Wide Ball");
            if (tvListener.isScreenVisible() && isPlaySpeech)
                CRICSounds.WIDE_BALL.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);

            String formattedRuns = getFormattedRuns(currentStatus.getScoredRuns(), false);
            if (!TextUtils.isEmpty(formattedRuns))
            {
                tvBinding.currentStatus2Tv.setText("+\n" + formattedRuns);
            }

            try
            {
                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.wide_gif_v2);
                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
            } catch (Exception e)
            {
                FirebaseCrash.report(e);
                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_wide_ball);
            }
//            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_wide_ball);
        } else if (currentStatus.isNoBall().equals("1"))
        {
            tvBinding.currentStatus1Tv.setText("No Ball");
            if (tvListener.isScreenVisible() && isPlaySpeech)
                CRICSounds.NO_BALL.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);

            String formattedRuns = getFormattedRuns(currentStatus.getScoredRuns(), false);
            if (!TextUtils.isEmpty(formattedRuns))
            {
                tvBinding.currentStatus2Tv.setText("+\n" + formattedRuns);
            }

            try
            {
                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.no_ball_gif_v2);
                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
            } catch (Exception e)
            {
                FirebaseCrash.report(e);
                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_no_ball);
            }
//            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_no_ball);
        } else if (currentStatus.getByeRuns().equals("1"))
        {
            tvBinding.currentStatus1Tv.setText("Bye");

            String formattedRuns = getFormattedRuns(currentStatus.getScoredRuns(), false);
            if (!TextUtils.isEmpty(formattedRuns))
            {
                tvBinding.currentStatus2Tv.setText("+\n" + formattedRuns);
            }

            try
            {
                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.bye_gif_v2);
                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
            } catch (Exception e)
            {
                FirebaseCrash.report(e);
                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_bye);
            }
//            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_bye);

        } else if (currentStatus.getLegBye().equals("1"))
        {
            tvBinding.currentStatus1Tv.setText("Leg Bye");
            String formattedRuns = getFormattedRuns(currentStatus.getScoredRuns(), false);
            if (!TextUtils.isEmpty(formattedRuns))
            {
                tvBinding.currentStatus2Tv.setText("+\n" + formattedRuns);
            }

            try
            {
                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.leg_bye_gif_v2);
                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
            } catch (Exception e)
            {
                FirebaseCrash.report(e);
                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_leg_bye);
            }
//            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_leg_bye);

        } else
        {

            setRuns(currentStatus, true);
        }

        startTvImageAnimation(tvBinding.matchTvImage);

        startBounceAnimation(tvBinding.currentStatus1Tv);
        startBounceAnimation(tvBinding.currentStatus2Tv);

    }

    /**
     * Check other key enum
     *
     * @param other Other key
     * @return Flag
     */
    private boolean checkOtherEnum(String other)
    {
        if (TextUtils.isEmpty(other))
            return false;
        else if (other.equalsIgnoreCase("0") || other.equalsIgnoreCase(AppConstants.BALL)
                || other.contains("New Batsman"))
            return false;
        return true;
    }

    private void setRuns(CurrentStatus currentStatus, boolean isShowZeroRuns)
    {
        tvBinding.otherTextContainer.setVisibility(VISIBLE);
        tvBinding.upperView.setVisibility(GONE);

        tvBinding.otherText.setVisibility(VISIBLE);
        tvBinding.otherActionsImage.setVisibility(GONE);
        int runs = currentStatus.getScoredRuns();
        if (runs >= 0)
        {
            tvBinding.otherText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_25));
            switch (runs)
            {
                case 0:
                    if (isShowZeroRuns)
                    {
                        if (currentStatus.isLastStatusRollBacked().equalsIgnoreCase("1"))
                        {
                            tvBinding.otherText.setText("");
                        } else if (currentStatus.getTvMessage().equalsIgnoreCase("Bowled"))
                        {
                            tvBinding.otherText.setText("");
                        } else
                        {
                            tvBinding.otherText.setText("0");
                            String matchStatus = fireBaseController.getConfig().matchStatus;
                            if (!matchStatus.equals("finished") &&
                                    !currentStatus.getTvMessage().equals("Break"))
                            {
                                if (tvListener.isScreenVisible() && isPlaySpeech)
                                    CRICSounds.DOT_BALL.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);
                            }
                        }
                    } else
                    {
                        tvBinding.otherText.setText("");
                    }
                    break;
                case 1:
                    tvBinding.otherText.setText("1 Run");
                    if (tvListener.isScreenVisible() && isPlaySpeech)
                        CRICSounds.SINGLE.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);
                    break;
                default:
                    if (runs == 2)
                    {
                        if (tvListener.isScreenVisible() && isPlaySpeech)
                            CRICSounds.DOUBLE_RUN.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);
                    } else if (runs == 3)
                    {
                        if (tvListener.isScreenVisible() && isPlaySpeech)
                            CRICSounds.THREE_RUNS.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);
                    }
                    tvBinding.otherText.setText(runs + " Runs");
                    if (runs == 4 || runs == 6)
                    {
                        tvBinding.otherText.setVisibility(GONE);
                        tvBinding.otherActionsImage.setVisibility(VISIBLE);

                        if (runs == 4)
                        {
                            if (tvListener.isScreenVisible() && isPlaySpeech)
                                CRICSounds.FOUR.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);

                            try
                            {
                                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.four_gif_v2);
                                tvBinding.otherActionsImage.setImageDrawable(gifFromResource);
                            } catch (Exception e)
                            {
                                FirebaseCrash.report(e);
                                e.printStackTrace();
                                tvBinding.otherActionsImage.setImageResource(R.drawable.four_fallback);
                            }

//                        tvBinding.otherActionsImage.setImageResource(R.drawable.animation_list_four);
                            tvBinding.otherActionsImage.setTag(AppConstants.FOUR);
                            startTvImageAnimation(tvBinding.otherActionsImage);
                        } else
                        {
                            if (tvListener.isScreenVisible() && isPlaySpeech)
                                CRICSounds.SIX.play(getContext(), currentStatus.isLastStatusRollBacked(), fragmentPos);

                            try
                            {
                                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.six_gif_v2);
                                tvBinding.otherActionsImage.setImageDrawable(gifFromResource);
                            } catch (Exception e)
                            {
                                FirebaseCrash.report(e);
                                e.printStackTrace();
                                tvBinding.otherActionsImage.setImageResource(R.drawable.six_fallback);
                            }
//                        tvBinding.otherActionsImage.setImageResource(R.drawable.animation_list_six);
                            tvBinding.otherActionsImage.setTag(AppConstants.SIX);
                            startTvImageAnimation(tvBinding.otherActionsImage);
                        }
                    }
                    break;
            }
        } else
        {
            tvBinding.otherText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_18));

            tvBinding.otherText.setText("");
        }
    }

    private String getFormattedRuns(int runs2, boolean isShowZeroRuns)
    {
        String runs = "";
        if (runs2 >= 0)
            switch (runs2)
            {
                case 0:
                    if (isShowZeroRuns)
                    {
                        runs = "0";
                    }
                    break;
                case 1:
                    runs = "1 Run";
                    break;
                default:
                    runs = runs2 + " Runs";
                    break;
            }
        return runs;
    }

    private void startTvImageAnimation(ImageView imageView)
    {

        boolean isStartTimer = true;
        if (imageView.getTag() != null && imageView.getTag() instanceof String)
        {
            String tag = imageView.getTag().toString();
            if (tag.equals(BALL_TAG))
            {
                isStartTimer = false;
            }
        }
        if (imageView.getDrawable() != null && imageView.getDrawable() instanceof AnimationDrawable)
        {
            drawable = (AnimationDrawable) imageView.getDrawable();

            if (drawable != null)
            {
                drawable.setOneShot(false);
                drawable.start();
                if (isStartTimer)
                    startAnimationTimer();
            }
        }
    }

    Handler handler = new Handler();
    Runnable runnable = new Runnable()
    {
        @Override
        public void run()
        {
            if (drawable != null && drawable.isRunning())
            {
                drawable.stop();
                if (tvBinding.otherActionsImage.getTag() != null)
                {

                    String tag = tvBinding.otherActionsImage.getTag().toString();
                    switch (tag)
                    {
                        case AppConstants.FOUR:
                            imageViewAnimatedChange(tvBinding.otherActionsImage.getContext(),
                                    tvBinding.otherActionsImage, R.drawable.four16);
                            break;
                        case AppConstants.SIX:
                            imageViewAnimatedChange(tvBinding.otherActionsImage.getContext(),
                                    tvBinding.otherActionsImage, R.drawable.six16);
                            break;
                        default:
                            tvBinding.otherActionsImage.setImageResource(0);
                            break;
                    }
                }
            }
        }
    };

    /**
     * Method to set image with animation
     *
     * @param c     Context
     * @param v     Imageview object
     * @param resId Image resource id to set
     */
    private void imageViewAnimatedChange(Context c, final ImageView v, final int resId)
    {
        final Animation anim_out = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
        final Animation anim_in = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
        anim_out.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                v.setImageDrawable(v.getResources().getDrawable(resId));
                anim_in.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation)
                    {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation)
                    {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {
                    }
                });
                v.startAnimation(anim_in);
            }
        });
        v.startAnimation(anim_out);
    }

    /**
     * Method to stop animation after three seconds
     */
    private void startAnimationTimer()
    {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 4800);
    }

    private void startBounceAnimation(View view)
    {

        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", 0.80f, 1f);
        scaleX.setRepeatMode(ValueAnimator.REVERSE);
        scaleX.setRepeatCount(ValueAnimator.INFINITE);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", 0.80f, 1f);
        scaleY.setRepeatMode(ValueAnimator.REVERSE);
        scaleY.setRepeatCount(ValueAnimator.INFINITE);


        AnimatorSet scaleSet = new AnimatorSet();
        scaleSet.playTogether(scaleX, scaleY);
        scaleSet.setDuration(1000);
        scaleSet.start();

    }

    public void showLiveUrlBtn(final String url){
        if (!url.equals("")){
            tvBinding.liveIndicator.setVisibility(View.VISIBLE);
            tvBinding.liveIndicator.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent i = new Intent(getContext(), LiveMatchActivity.class);
                    i.putExtra("web_url",url);
                    getContext().startActivity(i);
                    if (fireBaseController != null)
                        fireBaseController.destroy();
                }
            });
        }else {
            tvBinding.liveIndicator.setVisibility(View.GONE);

        }
    }




}
