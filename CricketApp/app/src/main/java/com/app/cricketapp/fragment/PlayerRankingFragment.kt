package com.app.cricketapp.fragment

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.adapter.PlayersRankingAdapter
import com.app.cricketapp.model.ranking.RankingResponse

/**
 * Created by Sunaina on 21-08-2017.
 */
class PlayerRankingFragment : BaseFragment() {

    internal var recyclerView: RecyclerView? = null
    internal var tvRatingPoint: TextView? = null
    internal var playersRankingAdapter: PlayersRankingAdapter? = null
    var matchType = "";

    override fun initUi() {
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        tvRatingPoint = findViewById(R.id.tv_rating) as TextView?

    }

    override fun getLayoutById(): Int {
        return R.layout.fragment_players_ranking
    }
    fun refreshList(dataValue:Int , rankingResponse:RankingResponse,tabPosition: Int) {

        if (dataValue == 3){
            tvRatingPoint?.visibility = View.VISIBLE
        }else{
            tvRatingPoint?.visibility = View.GONE
        }
        playersRankingAdapter = PlayersRankingAdapter(context!!, dataValue , rankingResponse,tabPosition)
        val linearLayoutManager: LinearLayoutManager? = LinearLayoutManager(context)

        recyclerView!!.layoutManager = linearLayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = playersRankingAdapter
    }

}