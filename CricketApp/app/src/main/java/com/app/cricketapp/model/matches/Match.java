
package com.app.cricketapp.model.matches;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Match implements Parcelable {

    @SerializedName("match_room")
    @Expose
    public String matchRoom;
    @SerializedName("team1")
    @Expose
    public String team1;
    @SerializedName("team2")
    @Expose
    public String team2;
    @SerializedName("team_1_logo")
    @Expose
    public String team1Logo;
    @SerializedName("team_2_logo")
    @Expose
    public String team2Logo;
    @SerializedName("landing_text")
    @Expose
    public String landingText;
    @SerializedName("match_started")
    @Expose
    public String matchStarted;
    @SerializedName("match_started_time")
    @Expose
    public String matchStartedTime;

    protected Match(Parcel in) {
        matchRoom = in.readString();
        team1 = in.readString();
        team2 = in.readString();
        team1Logo = in.readString();
        team2Logo = in.readString();
        landingText = in.readString();
        matchStarted = in.readString();
        matchStartedTime = in.readString();
    }

    public static final Creator<Match> CREATOR = new Creator<Match>() {
        @Override
        public Match createFromParcel(Parcel in) {
            return new Match(in);
        }

        @Override
        public Match[] newArray(int size) {
            return new Match[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(matchRoom);
        parcel.writeString(team1);
        parcel.writeString(team2);
        parcel.writeString(team1Logo);
        parcel.writeString(team2Logo);
        parcel.writeString(landingText);
        parcel.writeString(matchStarted);
        parcel.writeString(matchStartedTime);
    }
}
