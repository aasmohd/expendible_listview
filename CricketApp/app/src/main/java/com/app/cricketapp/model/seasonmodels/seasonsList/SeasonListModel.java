
package com.app.cricketapp.model.seasonmodels.seasonsList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SeasonListModel implements Serializable
{

    @SerializedName("status")
    @Expose
    private Long status;
    @SerializedName("messages")
    @Expose
    private String messages;

    @SerializedName("response")
    @Expose
    private List<Season> seasons = null;

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }


    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }



}
