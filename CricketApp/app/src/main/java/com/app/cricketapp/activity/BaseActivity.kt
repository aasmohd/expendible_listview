package com.app.cricketapp.activity

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.customview.MaterialProgressDialog
import com.app.cricketapp.utility.Utility
import com.github.rahatarmanahmed.cpv.CircularProgressView
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


abstract class BaseActivity : AppCompatActivity(), View.OnClickListener {
    var nonBlockingProgressBar: CircularProgressView? = null
    var blockingProgressDialog: MaterialProgressDialog? = null
    var mIsActivityVisible = false
    var viewDataBinding: ViewDataBinding? = null
        private set
    var mTitleTv: TextView? = null
    var toolbar: Toolbar? = null
    var action2: ImageView? = null
    var action3: ImageView? = null
    var headerBack: ImageView? = null

    protected abstract fun initUi()

    protected abstract fun getLayoutById(): Int

    fun getActivity(): BaseActivity {
        return this
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView<ViewDataBinding>(this, getLayoutById())
        blockingProgressDialog = Utility.getProgressDialogInstance(this)
        nonBlockingProgressBar = findViewById(R.id.circular_progress_bar) as CircularProgressView?


        if (toolbar != null) {
//            toolbar?.setPadding(0, 0, 0, 0)
//            toolbar?.setContentInsetsAbsolute(0, 0)
        }
        action2 = findViewById(R.id.header_actions_2) as ImageView?
        action3 = findViewById(R.id.header_actions_3) as ImageView?
        headerBack = findViewById(R.id.header_back) as ImageView?

        mTitleTv = findViewById(R.id.title_tv) as TextView?


        if (mTitleTv != null)
            mTitleTv?.text = title.toString().toUpperCase()

        if (action2 != null) {
            action2?.setOnClickListener(this)
        }
        if (action3 != null) {
            action3?.setOnClickListener(this)
        }
        if (headerBack != null) {
            headerBack?.setOnClickListener(this)
        }
        initUi()
    }

    override fun onResume() {
        super.onResume()
        mIsActivityVisible = true
    }

    override fun onPause() {
        super.onPause()
        mIsActivityVisible = false
    }

    public fun isActivityVisible(): Boolean {
        return mIsActivityVisible
    }

    public fun showProgressBar(isShow: Boolean) {
        if (nonBlockingProgressBar != null) {
            if (isShow)
                nonBlockingProgressBar?.visibility = View.VISIBLE
            else
                nonBlockingProgressBar?.visibility = View.GONE
        }
    }

    protected fun toggleAction2Icon(visibility: Int) {
        if (action2 != null) {
            action2?.visibility = visibility
        }
    }

    protected fun toggleAction3Icon(visibility: Int) {
        if (action3 != null) {
            action3?.visibility = visibility
        }
    }

    protected fun setAction2Icon(resId: Int) {
        if (action2 != null) {
            action2?.setImageResource(resId)
        }
    }

    public fun showProgressDialog(isShow: Boolean) {
        if (blockingProgressDialog != null) {
            if (isShow)
                blockingProgressDialog?.show()
            else
                blockingProgressDialog?.dismiss()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            android.R.id.home, R.id.header_back ->
                onHeaderBackClicked()
            R.id.header_actions_2 ->
                onAction2Clicked(v)
            R.id.header_actions_3 ->
                onAction3Clicked(v)
        }
    }

    open fun onAction3Clicked(v: View) {
    }

    open fun onAction2Clicked(v: View) {
    }

    open fun onAction1Clicked(v: View) {
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onHeaderBackClicked()
        }
        return super.onOptionsItemSelected(item)
    }

    protected fun onHeaderBackClicked() {
        finish()
    }

    protected fun setBackButtonEnabled() {
        if (headerBack != null) {
            headerBack?.visibility = View.VISIBLE
        }
    }

    protected fun setTitlePadding(padding: Int) {
        if (mTitleTv != null)
            mTitleTv?.setPadding(padding, 0, 0, 0)
    }

    protected fun setToolbarColor(color: Int) {
        if (toolbar != null)
            toolbar?.setBackgroundColor(color)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

}
