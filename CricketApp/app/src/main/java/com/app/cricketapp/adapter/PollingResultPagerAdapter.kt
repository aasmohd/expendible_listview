package com.app.cricketapp.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.akexorcist.roundcornerprogressbar.TextRoundCornerProgressBar
import com.app.cricketapp.R
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.model.RequestBean
import com.app.cricketapp.model.pollresponsemodel.Poll
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by Prashant on 27-07-2017.
 */
class PollingResultPagerAdapter(var context: Context, var pollList: ArrayList<Poll>) : PagerAdapter() {
    var pollListener: PollListener? = null

    init {
        pollListener = context as PollingResultPagerAdapter.PollListener
    }

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return view == `object`
    }

    interface PollListener {
        fun predictPoll(position: Int, requestBean: RequestBean)
    }

    override fun getCount(): Int {
        return pollList.size
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val poll = pollList[position]


        val itemView = LayoutInflater.from(context).inflate(R.layout.pager_view_polling, null)


        val submit: TextView = itemView.findViewById(R.id.btn_submit)
        submit.setOnClickListener {
            val radioGroup: RadioGroup = itemView.findViewById(R.id.ques_radio_group)
            val checkedRadioButton = radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
            if (checkedRadioButton == null) {
                CustomToast.getInstance(context).showToast(R.string.select_poll_option)
            } else {
                val appUserId: String = Utility.getStringSharedPreference(context, AppConstants.APP_USERID)
                val requestBean = RequestBean()

                requestBean.app_user_id = appUserId
                requestBean.poll_id = poll.pollId

                when (checkedRadioButton.id) {
                    R.id.tv1 -> {
                        requestBean.prediction_type = poll.pollOptions[0].optionId
                        pollListener?.predictPoll(position, requestBean)
                    }
                    R.id.tv2 -> {
                        requestBean.prediction_type = poll.pollOptions[1].optionId
                        pollListener?.predictPoll(position, requestBean)
                    }
                    R.id.tv3 -> {
                        requestBean.prediction_type = poll.pollOptions[2].optionId
                        pollListener?.predictPoll(position, requestBean)
                    }
                }

            }

        }
        val team1: TextView = itemView.findViewById(R.id.team1_name)
        val pollGiveTeam1: TextView = itemView.findViewById(R.id.team1_name1)
        val pollGiveTeam2: TextView = itemView.findViewById(R.id.team2_name2)

        val quesText: TextView = itemView.findViewById(R.id.question_text)
        val pollGiveQuesText: TextView = itemView.findViewById(R.id.question_text2)

        val team2: TextView = itemView.findViewById(R.id.team2_name)

        val logo1: CircleImageView = itemView.findViewById(R.id.match_1_iv)
        val pollGiveLogo1: CircleImageView = itemView.findViewById(R.id.match_1_iv1)

        val logo2: CircleImageView = itemView.findViewById(R.id.match_2_iv)
        val pollGiveLogo2: CircleImageView = itemView.findViewById(R.id.match_2_iv2)

        val progress1: TextRoundCornerProgressBar = itemView.findViewById(R.id.ques1_progress_1)
        val progress2: TextRoundCornerProgressBar = itemView.findViewById(R.id.ques2_progress_2)
        val progress3: TextRoundCornerProgressBar = itemView.findViewById(R.id.ques3_progress_3)
        val ques1Perc: TextView = itemView.findViewById(R.id.ques1_perc)
        val ques2Perc: TextView = itemView.findViewById(R.id.ques2_perc)
        val ques3Perc: TextView = itemView.findViewById(R.id.ques3_perc)

        val view1: LinearLayout = itemView.findViewById(R.id.view_1)
        val view2: RelativeLayout = itemView.findViewById(R.id.view_2)

        val pollGiveQues1: RadioButton = itemView.findViewById(R.id.tv1)
        val pollGiveQues2: RadioButton = itemView.findViewById(R.id.tv2)
        val pollGiveQues3: RadioButton = itemView.findViewById(R.id.tv3)

        val ques1: TextView = itemView.findViewById(R.id.ques1)
        val ques2: TextView = itemView.findViewById(R.id.ques2)
        val ques3: TextView = itemView.findViewById(R.id.ques3)

        val pollGiveMatchType: ImageView = itemView.findViewById(R.id.match_type)
        val pollResultMatchType: ImageView = itemView.findViewById(R.id.result_match_type)

        if (poll.matchFormat == "50") {
            pollGiveMatchType.setImageResource(R.drawable.odi_ribbon)
            pollResultMatchType.setImageResource(R.drawable.odi_ribbon)
        } else if (poll.matchFormat == "Test") {
            pollGiveMatchType.setImageResource(R.drawable.test_ribbon)
            pollResultMatchType.setImageResource(R.drawable.test_ribbon)
        } else {
            pollGiveMatchType.setImageResource(R.drawable.t20_ribbon)
            pollResultMatchType.setImageResource(R.drawable.t20_ribbon)
        }

        val dateTv: TextView = itemView.findViewById(R.id.date_tv)
        val resultsDateTv: TextView = itemView.findViewById(R.id.results_date_tv)
        val localDate = Utility.getLocalDate(poll.matchDate, AppConstants.DATE_FORMAT4)

        dateTv.text = localDate
        resultsDateTv.text = localDate


        val totalVote: TextView = itemView.findViewById(R.id.tv_total_vote)
        val total = poll.totalVotes
        totalVote.text = "Total Votes: $total"

        pollGiveQues1.text = poll.pollOptions[0].optionText
        ques1.text = poll.pollOptions[0].optionText

        pollGiveQues2.text = poll.pollOptions[1].optionText
        ques2.text = poll.pollOptions[1].optionText

        pollGiveQues3.text = poll.pollOptions[2].optionText
        ques3.text = poll.pollOptions[2].optionText



        quesText.text = poll.pollQuestion
        pollGiveQuesText.text = poll.pollQuestion

        if (poll.isPollSubmitted == 1L) {
            pollGiveQuesText.text = context.getString(R.string.predictions)
            quesText.text = context.getString(R.string.predictions)
            view1.visibility = View.VISIBLE
            view2.visibility = View.GONE
        } else {
            view1.visibility = View.GONE
            view2.visibility = View.VISIBLE
        }

        team1.text = poll.team1Info.teamName
        pollGiveTeam1.text = poll.team1Info.teamName
        if (!TextUtils.isEmpty(poll.team1Info.teamLogo)){
            Utility.setImageWithUrl(poll.team1Info.teamLogo, R.drawable.default_image, logo1)
            Utility.setImageWithUrl(poll.team1Info.teamLogo, R.drawable.default_image, pollGiveLogo1)
        }

        team2.text = poll.team2Info.teamName
        pollGiveTeam2.text = poll.team2Info.teamName
        if (!TextUtils.isEmpty(poll.team2Info.teamLogo)){

            Utility.setImageWithUrl(poll.team2Info.teamLogo, R.drawable.default_image, logo2)
            Utility.setImageWithUrl(poll.team2Info.teamLogo, R.drawable.default_image, pollGiveLogo2)
        }

        //Set progress and percentage
        //System.out.printf("%.2f", val);

        val option1Perc = Math.round(calculatePercentage(poll.totalVotes, poll.pollOptions[0].optionVotes))
        val option2Perc = Math.round(calculatePercentage(poll.totalVotes, poll.pollOptions[1].optionVotes))
        val option3Perc = Math.round(calculatePercentage(poll.totalVotes, poll.pollOptions[2].optionVotes))

        progress1.progress = option1Perc.toFloat()
        ques1Perc.text = "$option1Perc%"
        progress1.radius = 15

        progress2.progress = option2Perc.toFloat()
        ques2Perc.text = "$option2Perc%"
        progress2.radius = 15

        progress3.progress = option3Perc.toFloat()
        ques3Perc.text = "$option3Perc%"
        progress3.radius = 15

        container?.addView(itemView)
        return itemView
    }

    override fun getItemPosition(`object`: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }

    private fun calculatePercentage(totalVotes: Long, optionVotes: Long): Float {
        if (totalVotes.toInt() == 0)
            return 0f
        val l = ((optionVotes) / totalVotes.toFloat()) * 100
        return l
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as FrameLayout)
    }

}