package com.app.cricketapp.activity

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.app.cricketapp.R
import com.app.cricketapp.adapter.WatchLiveAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdRequest
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_watch_live.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by bhavya on 12-04-2017.
 */

public class WatchLiveActivity : BaseActivity() {
    var mWatchLiveAdapter: WatchLiveAdapter? = null
    var manager: LinearLayoutManager? = null
    var watchLiveList: ArrayList<Match> = ArrayList()

    override fun initUi() {
        setBackButtonEnabled()
        showProgressBar(true)
        if (Utility.isNetworkAvailable(this)) {
            callGetWatchLiveApi()
        } else {
            showProgressBar(false)
            CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
        }
        mWatchLiveAdapter = WatchLiveAdapter(this,watchLiveList)
        manager = LinearLayoutManager(getActivity())
        recycler_view.layoutManager = manager
        recycler_view.setHasFixedSize(true)
        loadBannerAd()
    }

    override fun getLayoutById(): Int {
        return R.layout.activity_watch_live
    }

    fun callGetWatchLiveApi() {
        val apiService = AppRetrofit.getInstance().apiServices
        val call = apiService.liveWatch()
        call.enqueue(object : Callback <JsonElement> {

            override fun onResponse(call: Call<JsonElement>?, response: Response<JsonElement>?) {
                if (response?.body()!= null){
                    try {
                        showProgressBar(false)
                        val recentResponse = response?.body().toString()
                        val gson = Gson()
                        val listType = object : TypeToken<ArrayList<Match>>() {
                        }.type
                        val recentArrayList: ArrayList<Match> = gson.fromJson(recentResponse, listType)
                        watchLiveList.addAll(recentArrayList)

                        recycler_view.adapter = mWatchLiveAdapter
                        mWatchLiveAdapter?.notifyDataSetChanged()
                        Lg.i("WatchLive", call.toString())
                        /*check if there is no data in the response*/
                        if (watchLiveList.size == 0) {
                            empty_tv.visibility = View.VISIBLE
                        } else {
                            empty_tv.visibility = View.GONE

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<JsonElement>?, t: Throwable?) {
                showProgressBar(false)
            }
        })

    }

    /*load banner ad for admob*/
    fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

}
