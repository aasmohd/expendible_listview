package com.app.cricketapp.model.matchresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rahulgupta on 03/08/17.
 */

public class Inning implements IMatch {
    @SerializedName("batting")
    @Expose
    public String batting;
    @SerializedName("bowling")
    @Expose
    public String bowling;
    @SerializedName("inningOver")
    @Expose
    public String inningOver;
    @SerializedName("ballsRemaining")
    @Expose
    public String ballsRemaining;
    @SerializedName("score")
    @Expose
    public String score;
    @SerializedName("wicket")
    @Expose
    public String wicket;
    @SerializedName("target")
    @Expose
    private String target;

    public String getTarget() {
        try {
            int i = Integer.parseInt(target);
        } catch (Exception e) {
            e.printStackTrace();
            target = "0";
        }
        return target;
    }

    @SerializedName("over")
    @Expose
    public String over;

}
