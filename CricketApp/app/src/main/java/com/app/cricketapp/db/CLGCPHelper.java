package com.app.cricketapp.db;

import android.net.Uri;

import com.app.cricketapp.model.newRecentMatchResponse.NewRecentMatchResponse;
import com.app.cricketapp.model.ranking.RankingResponse;
import com.app.cricketapp.model.scorecard.ScorecardResponse;
import com.app.cricketapp.model.upcomingmatchresponse.NewUpcomingMatchResponse;

import java.util.List;


public class CLGCPHelper {


    public static class Uris {

        public final static Uri URI_RECENT_MATCH_TABLE = Uri.withAppendedPath(CLGContentProviderData.CONTENT_URI,NewRecentMatchResponse.class.getSimpleName());
        public final static Uri URI_SCORECARD_TABLE = Uri.withAppendedPath(CLGContentProviderData.CONTENT_URI,ScorecardResponse.class.getSimpleName());
        public final static Uri URI_LIST_TABLE = Uri.withAppendedPath(CLGContentProviderData.CONTENT_URI,List.class.getSimpleName());
        public final static Uri URI_RANKING_TABLE = Uri.withAppendedPath(CLGContentProviderData.CONTENT_URI,RankingResponse.class.getSimpleName());
        public final static Uri URI_UPCOMING_TABLE = Uri.withAppendedPath(CLGContentProviderData.CONTENT_URI,NewUpcomingMatchResponse.class.getSimpleName());
       }

}
