package com.app.cricketapp.adapter

import android.content.Context
import android.content.Intent
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.activity.MatchDetailActivity
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.model.upcomingmatchresponse.NewUpcomingMatchResponse
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.*

/**
 * Created by bhavya on 12-04-2017.
 */

class UpcomingMatchesAdapter(var context: Context, var upcomingMatchResponse: NewUpcomingMatchResponse, var pagerPosition:Int) :
        RecyclerView.Adapter<UpcomingMatchesAdapter.MyViewHolder>(), ViewPager.OnPageChangeListener {

    var upcomingMatchPagerAdapter: UpcomingMatchPagerAdapter? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.row_pager, parent, false)
        return MyViewHolder(view)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        when (pagerPosition) {
            0 -> {
                val odiMatches = upcomingMatchResponse.getOdiMatches()

                holder.mMatchType.setImageResource(Utility.getDayOfWeek((odiMatches.get(position).
                        basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.TARGET_DATE_FORMAT_2))

                Utility.setImageWithUrl(odiMatches.get(position).teams.b!!.logo, R.drawable.default_image, holder.mTeamName2Iv)
                Utility.setImageWithUrl(odiMatches.get(position).teams.a!!.logo, R.drawable.default_image, holder.mTeamName1Iv)
                val utcTimeFormat = Utility.getLocalDate((odiMatches.get(position).basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.TARGET_DATE_FORMAT_2)

                val localTime2 = Utility.getLocalDate((odiMatches.get(position).basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT)
                val gmtTime = Utility.parseDateTimeUtcToGmt(localTime2)

                val localTime = Utility.getLocalDate((odiMatches.get(position).basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT1)

//        String localTimeZone = Utility.getLocalTimeZone()
//        holder.mTimeTv.setText(utcTimeFormat + " " + localTime + " " + localTimeZone + " / " + gmtTime + " GMT")
                holder?.mTimeTv?.text = "$utcTimeFormat $localTime"
//                holder?.mTimeTv?.text = "$utcTimeFormat $localTime / $gmtTime GMT"

                holder.mTeamName1Tv.setText(odiMatches.get(position).teams.a!!.key!!.toUpperCase())
                holder.mTeamName2Tv.setText(odiMatches.get(position).teams.b!!.key!!.toUpperCase())
                holder.mMatchPositionTv.setText(odiMatches.get(position).names.relatedName)
                val place = odiMatches.get(position).basicsInfo.venue
                val str = place!!.split(",".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                if (str.size > 2) {
                    var s = ""
                    for (i in str.indices) {
                        if (i < 2) {
                            if (s.isEmpty()) {
                                s = s + str[i]
                            } else {
                                s = s + ", " + str[i]
                            }
                        }
                    }

                    holder.mPlaceTv.text = s
                } else {
                    holder.mPlaceTv.setText(place)
                }

//                holder.mleauge.setText(odiMatches.get(position).season.name)
                holder.mleauge.setText(odiMatches.get(position).teams.a!!.name + " VS " +
                        odiMatches.get(position).teams.b!!.name)

                holder.mWinning_team.visibility = View.GONE
                holder.card.setOnClickListener {
                    if (upcomingMatchResponse.getOdiMatches().get(position).results.score_card_available.equals("1", ignoreCase = true)) {
                        val intent = Intent(context, MatchDetailActivity::class.java)

                        intent.putExtra(AppConstants.EXTRA_KEY, upcomingMatchResponse.getOdiMatches().get(position).others.key)
                        intent.putExtra("match_type", 0)
                        context.startActivity(intent)
                    } else {
                        CustomToast.getInstance(context).showToast(R.string.no_scorecard)
                    }
                }
            }
            1 -> {
                val testMaches = upcomingMatchResponse.getTestMatche()
                holder.mMatchType.setImageResource(Utility.getDayOfWeek((testMaches.get(position).
                        basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.TARGET_DATE_FORMAT_2))
                Utility.setImageWithUrl(testMaches.get(position).teams.b!!.logo, R.drawable.default_image, holder.mTeamName2Iv)
                Utility.setImageWithUrl(testMaches.get(position).teams.a!!.logo, R.drawable.default_image, holder.mTeamName1Iv)
                val utcTimeFormat = Utility.getLocalDate((testMaches.get(position).basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.TARGET_DATE_FORMAT_2)

                val localTime2 = Utility.getLocalDate((testMaches.get(position).basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT)
                val gmtTime = Utility.parseDateTimeUtcToGmt(localTime2)

                val localTime = Utility.getLocalDate((testMaches.get(position).basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT1)

//        String localTimeZone = Utility.getLocalTimeZone()
//        holder.mTimeTv.setText(utcTimeFormat + " " + localTime + " " + localTimeZone + " / " + gmtTime + " GMT")
                holder?.mTimeTv?.text = "$utcTimeFormat $localTime"
//                holder?.mTimeTv?.text = "$utcTimeFormat $localTime / $gmtTime GMT"
                holder.mTeamName1Tv.setText(testMaches.get(position).teams.a!!.key!!.toUpperCase())
                holder.mTeamName2Tv.setText(testMaches.get(position).teams.b!!.key!!.toUpperCase())
                holder.mMatchPositionTv.setText(testMaches.get(position).names.relatedName)
                val place1 = testMaches.get(position).basicsInfo.venue
                val str1 = place1!!.split(",".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                if (str1.size > 2) {
                    var s = ""
                    for (i in str1.indices) {
                        if (i < 2) {
                            if (s.isEmpty()) {
                                s = s + str1[i]
                            } else {
                                s = s + ", " + str1[i]
                            }
                        }
                    }

                    holder.mPlaceTv.text = s
                } else {
                    holder.mPlaceTv.setText(place1)
                }

//                holder.mleauge.setText(testMaches.get(position).season.name)
                holder.mleauge.setText(testMaches.get(position).teams.a!!.name + " VS " +
                        testMaches.get(position).teams.b!!.name)
                holder.mWinning_team.visibility = View.GONE
                holder.card.setOnClickListener {
                    if (upcomingMatchResponse.getTestMatche().get(position).results.score_card_available.equals("1", ignoreCase = true)) {
                        val intent = Intent(context, MatchDetailActivity::class.java)

                        intent.putExtra(AppConstants.EXTRA_KEY, upcomingMatchResponse.getTestMatche().get(position).others.key)
                        intent.putExtra("match_type", 0)
                        context.startActivity(intent)
                    } else {
                        CustomToast.getInstance(context).showToast(R.string.no_scorecard)
                    }
                }
            }
            2 -> {
                val t20Matches = upcomingMatchResponse.getT20Matches()
                holder.mMatchType.setImageResource(Utility.getDayOfWeek((t20Matches.get(position).
                        basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.TARGET_DATE_FORMAT_2))
                Utility.setImageWithUrl(t20Matches.get(position).teams.b!!.logo, R.drawable.default_image, holder.mTeamName2Iv)
                Utility.setImageWithUrl(t20Matches.get(position).teams.a!!.logo, R.drawable.default_image, holder.mTeamName1Iv)
                val utcTimeFormat = Utility.getLocalDate((t20Matches.get(position).basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.TARGET_DATE_FORMAT_2)

                val localTime2 = Utility.getLocalDate((t20Matches.get(position).basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT)
                val gmtTime = Utility.parseDateTimeUtcToGmt(localTime2)

                val localTime = Utility.getLocalDate((t20Matches.get(position).basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT1)

//        String localTimeZone = Utility.getLocalTimeZone()
//        holder.mTimeTv.setText(utcTimeFormat + " " + localTime + " " + localTimeZone + " / " + gmtTime + " GMT")
                holder?.mTimeTv?.text = "$utcTimeFormat $localTime"
//                holder?.mTimeTv?.text = "$utcTimeFormat $localTime / $gmtTime GMT"
                holder.mTeamName1Tv.text = t20Matches.get(position).teams.a!!.key!!.toUpperCase()
                holder.mTeamName2Tv.text = t20Matches.get(position).teams.b!!.key!!.toUpperCase()
                holder.mMatchPositionTv.text = t20Matches.get(position).names.relatedName
                val place2 = t20Matches.get(position).basicsInfo.venue
                val str2 = place2!!.split(",".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                if (str2.size > 2) {
                    var s = ""
                    for (i in str2.indices) {
                        if (i < 2) {
                            if (s.isEmpty()) {
                                s = s + str2[i]
                            } else {
                                s = s + ", " + str2[i]
                            }
                        }
                    }

                    holder.mPlaceTv.text = s
                } else {
                    holder.mPlaceTv.setText(place2)
                }

//                holder.mleauge.setText(t20Matches.get(position).season.name)
                holder.mleauge.setText(t20Matches.get(position).teams.a!!.name + " VS " +
                        t20Matches.get(position).teams.b!!.name)

                holder.mWinning_team.visibility = View.GONE
                holder.card.setOnClickListener {
                    if (upcomingMatchResponse.getT20Matches().get(position).results.score_card_available.equals("1", ignoreCase = true)) {
                        val intent = Intent(context, MatchDetailActivity::class.java)

                        intent.putExtra(AppConstants.EXTRA_KEY, upcomingMatchResponse.getT20Matches().get(position).others.key)
                        intent.putExtra("match_type", 0)
                        context.startActivity(intent)
                    } else {
                        CustomToast.getInstance(context).showToast(R.string.no_scorecard)
                    }
                }
            }
        }

        if (position % 4 == 0 && position != 0) {
            holder.adsContainer.visibility = View.VISIBLE
            if (position % 4 == 0 && position != 0) {
                // Set its video options.
                holder.mAdView.videoOptions = VideoOptions.Builder()
                        .setStartMuted(true)
                        .build()

                // The VideoController can be used to get lifecycle events and info about an ad's video
                // asset. One will always be returned by getVideoController, even if the ad has no video
                // asset.
                val mVideoController = holder.mAdView.videoController
                mVideoController.videoLifecycleCallbacks = object : VideoController.VideoLifecycleCallbacks() {
                    override fun onVideoEnd() {
                        Log.d("Video", "Video playback is finished.")
                        super.onVideoEnd()
                    }
                }

                // Set an AdListener for the AdView, so the Activity can take action when an ad has finished
                // loading.
                holder.mAdView.adListener = object : AdListener() {
                    override fun onAdLoaded() {
                        if (mVideoController.hasVideoContent()) {
                            Log.d("Video", "Received an ad that contains a video asset.")
                        } else {
                            Log.d("Video", "Received an ad that does not contain a video asset.")
                        }
                    }
                }

                holder.mAdView.loadAd(AdRequest.Builder()
                        .addTestDevice("C11191FE7944B40640463F2686610772").build())
            }

        } else {
            holder.adsContainer.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {

        when (pagerPosition) {
            0 -> return upcomingMatchResponse.getOdiMatches().size

            1 -> return upcomingMatchResponse.getTestMatche().size

            2 -> return upcomingMatchResponse.getT20Matches().size
            else -> return 0
        }
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        var adsContainer: RelativeLayout
        var mAdView: NativeExpressAdView
        var mMatchPositionTv: TextView
        var mPlaceTv: TextView
        var mTimeTv: TextView
        var mTeamName1Tv: TextView
        var mTeamName2Tv: TextView
        var mleauge: TextView
        var mWinning_team: TextView
        var mTeamName1Iv: ImageView
        var mTeamName2Iv: ImageView
        var mMatchType: ImageView
        var card: FrameLayout

        init {
            mWinning_team = view.findViewById(R.id.winning_team)
            mleauge = view.findViewById(R.id.league)
            mMatchPositionTv = view.findViewById(R.id.match_position)
            mPlaceTv = view.findViewById(R.id.place_tv)
            mTimeTv = view.findViewById(R.id.time_tv)
            mTeamName1Tv = view.findViewById(R.id.team_name_1_tv)
            mTeamName2Tv = view.findViewById(R.id.team_name_2_tv)
            mTeamName1Iv = view.findViewById(R.id.team_name_1_iv)
            mTeamName2Iv = view.findViewById(R.id.team_name_2_iv)
            mMatchType = view.findViewById(R.id.match_type)
            card = view.findViewById(R.id.card)
            adsContainer = view.findViewById(R.id.ads_container)
            mAdView = view.findViewById(R.id.adView)

        }
    }

}
