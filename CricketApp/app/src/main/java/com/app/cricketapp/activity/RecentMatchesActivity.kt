package com.app.cricketapp.activity

import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.View
import com.app.cricketapp.R
import com.app.cricketapp.adapter.RecentMatchesPagerMainAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.db.dao.RecentResultDAO
import com.app.cricketapp.model.newRecentMatchResponse.NewRecentMatchResponse
import com.app.cricketapp.model.newRecentMatchResponse.NodeResponseRecent
import com.app.cricketapp.model.upcomingmatchresponse.UpcomingMatchResponse
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.activity_watch_live.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*




/**
 * Created by bhavya on 12-04-2017.
 */

class RecentMatchesActivity : BaseActivity(){
    var mRecentMatchPagerMainAdapter: RecentMatchesPagerMainAdapter?= null
    var recentMatchList: ArrayList<UpcomingMatchResponse.List> = ArrayList()
    var recentMatchResponse: NewRecentMatchResponse? = null
    var mPagerMain: ViewPager? = null
    var mTabLayout: TabLayout? = null
    override fun initUi() {

        mPagerMain = findViewById(R.id.view_pager) as ViewPager?
        mTabLayout = findViewById(R.id.tab_layout) as TabLayout?
        setBackButtonEnabled()
        toggleAction3Icon(View.GONE)
        showProgressBar(true)

        if (Utility.isNetworkAvailable(this)) {
            callGetRecentMatchesApi()
        } else {
            showProgressBar(false)
            CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
        }

       /* if(Utility.getLongSharedPreference(this,"time_stamp")!= 0L){
            // time difference check logic
            if (Utility.getLongSharedPreference(this,"time_stamp") < Utility.
                    getStringSharedPreference(this, AppConstants.MATCH_API_SERVER_TIME).toLong())
            {
                if (Utility.isNetworkAvailable(this)) {
                    callGetRecentMatchesApi()
                } else {
                    showProgressBar(false)
                    CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
                }
            }else{
                showProgressBar(false)
                recentMatchResponse = RecentResultDAO.getRecentMatches()
                mRecentMatchPagerMainAdapter = RecentMatchesPagerMainAdapter(this@RecentMatchesActivity,recentMatchResponse!!)
                setupViewPager(mPagerMain!!,mTabLayout!!)
            }

        }
        else{
            val time = System.currentTimeMillis()

            Utility.putLongValueInSharedPreference(this,"time_stamp",time)

            if (Utility.isNetworkAvailable(this)) {
                callGetRecentMatchesApi()
            } else {
                showProgressBar(false)
                CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
            }
        }*/
        MobileAds.initialize(applicationContext, getString(R.string.banner_ad_unit_id))

        loadBannerAd()
        initFullScreenAdd()
    }

    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()
                }
            }
            requestNewInterstitial()
        }
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .build()

        mInterstitialAd.loadAd(adRequest)
        mInterstitialAd.show()
    }

    override fun getLayoutById(): Int {

        return R.layout.activity_new_upcoming_match
    }

    fun callGetRecentMatchesApi() {
        val apiService = AppRetrofit.getInstance().apiServices
        val call = apiService.newRecentMatches()
        call.enqueue(object : Callback <NodeResponseRecent> {

            override fun onResponse(call: Call<NodeResponseRecent>?, response: Response<NodeResponseRecent>?) {
                showProgressBar(false)
                if (response?.body() != null){
                    Utility.putLongValueInSharedPreference(this@RecentMatchesActivity,"time_stamp",System.currentTimeMillis())

                    try {

                        RecentResultDAO.delete();
                        RecentResultDAO.insert(response.body().response!!);

                        recentMatchResponse = RecentResultDAO.getRecentMatches()

                        mRecentMatchPagerMainAdapter = RecentMatchesPagerMainAdapter(this@RecentMatchesActivity,recentMatchResponse!!)
                        setupViewPager(mPagerMain!!,mTabLayout!!)



                        if (recentMatchList.size > 6) {
                            recentMatchList[5].showAd = true
                        } else {
                            recentMatchList[recentMatchList.size - 1].showAd = true
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                }


            }

            override fun onFailure(call: Call<NodeResponseRecent>?, t: Throwable?) {
                showProgressBar(false)

            }
        })


    }

    /*load banner ad for admob*/
    fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun setupViewPager(viewPager: ViewPager, tabLayout: TabLayout) {
        viewPager.adapter = mRecentMatchPagerMainAdapter
        tabLayout.setupWithViewPager(viewPager)
    }

}
