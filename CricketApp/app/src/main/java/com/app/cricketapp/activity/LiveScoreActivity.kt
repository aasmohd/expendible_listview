package com.app.cricketapp.activity

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.app.cricketapp.R
import com.app.cricketapp.adapter.LiveScoreAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.model.matches.NodeResponseLive
import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.app.cricketapp.network.ApiServices
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.gson.Gson
import com.google.gson.JsonElement
import kotlinx.android.synthetic.main.activity_watch_live.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by bhavya on 12-04-2017.
 */

public class LiveScoreActivity : BaseActivity() {
    var mWatchLiveAdapter: LiveScoreAdapter? = null
    var manager: LinearLayoutManager? = null
    val watchLiveList: ArrayList<Match> = ArrayList()

    override fun initUi() {
        setBackButtonEnabled()
        showProgressBar(true)

        if (Utility.isNetworkAvailable(this)) {
            callGetWatchLiveApi()
        } else {
            showProgressBar(false)
            CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
        }
        mWatchLiveAdapter = LiveScoreAdapter(watchLiveList)
        manager = LinearLayoutManager(getActivity())
        recycler_view.layoutManager = manager
        recycler_view.setHasFixedSize(true)
        initFullScreenAdd()
        loadBannerAd()
    }

    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()

                }
            }
            requestNewInterstitial()
        }
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .addTestDevice("06CD7F1190497B9ADD39C03F2D21C717")
                .build()

        mInterstitialAd.loadAd(adRequest)
    }

    private fun loadBannerAd() {
        val adRequest: AdRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    override fun getLayoutById(): Int {
        return R.layout.activity_watch_live
    }

    private fun callGetWatchLiveApi() {
        val apiService: ApiServices = AppRetrofit.getInstance().apiServices
        val call: Call<JsonElement> = apiService.liveWatch()

        call.enqueue(object : Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>?, response: Response<JsonElement>?) {
                try {
                    showProgressBar(false)
                    val recentResponse = Gson().fromJson<NodeResponseLive>(response?.body().toString(),NodeResponseLive::class.java)
                   /* val gson = Gson()
                    val listType = object : TypeToken<ArrayList<Match>>() {

                    }.type
                    val recentArrayList: ArrayList<Match> = gson.fromJson(recentResponse, listType)*/
                    val recentArrayList = recentResponse.response
                    watchLiveList.addAll(recentArrayList)

                    if (watchLiveList.size > 6) {
                        recentArrayList[5].showAd = true
                    } else {
                        recentArrayList[recentArrayList.size - 1].showAd = true
                    }

                    recycler_view.adapter = mWatchLiveAdapter
                    mWatchLiveAdapter?.notifyDataSetChanged()
                    Lg.i("WatchLive", call.toString())
                    /*check if there is no data in the response*/
                    if (watchLiveList.size == 0) {
                        empty_tv.visibility = View.VISIBLE
                    } else {
                        empty_tv.visibility = View.GONE
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    empty_tv.visibility = View.VISIBLE
                }

            }

            override fun onFailure(call: Call<JsonElement>?, t: Throwable?) {
                showProgressBar(false)
            }
        })


    }

}
