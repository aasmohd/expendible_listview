package com.app.cricketapp.model.upcomingmatchresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BasicsInfo : Serializable {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("venue")
    @Expose
    var venue: String? = null
    @SerializedName("ref")
    @Expose
    var ref: String? = null
    @SerializedName("start_date")
    @Expose
    var startDate: String? = null
    @SerializedName("winner_team")
    @Expose
    var winnerTeam: String? = null

}
