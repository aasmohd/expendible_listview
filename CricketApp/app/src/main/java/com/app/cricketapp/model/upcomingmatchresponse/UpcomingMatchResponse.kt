package com.app.cricketapp.model.upcomingmatchresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class UpcomingMatchResponse {

    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("status")
    @Expose
    var status: Long = 0
    @SerializedName("result")
    @Expose
    var result: UpcomingMatchResult? = null


    class UpcomingMatchResult {

        @SerializedName("list")
        @Expose
        var list: ArrayList<List>? = null
    }

    class List {

        var showAd = false
        var date: Long = 0

        @SerializedName("day")
        @Expose
        var day: Long = 0
        @SerializedName("matches")
        @Expose
        var matches: ArrayList<Match> = ArrayList()

    }

}



