
package com.app.cricketapp.model.scorecard;

import com.app.cricketapp.utility.AppConstants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Batting implements Serializable, IScore {

    public boolean isBattingView;

    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("dots")
    @Expose
    public Long dots;
    @SerializedName("sixes")
    @Expose
    public Long sixes;
    @SerializedName("runs")
    @Expose
    public Long runs;
    @SerializedName("balls")
    @Expose
    public Long balls;
    @SerializedName("fours")
    @Expose
    public Long fours;
    @SerializedName("strike_rate")
    @Expose
    public Float strikeRate;
    @SerializedName("out_str")
    @Expose
    public String outStr;

    @Override
    public int getViewType() {
        return AppConstants.BATTING;
    }
}
