
package com.app.cricketapp.model.commentaryResPonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Match {

    @SerializedName("runs")
    @Expose
    private Long runs;
    @SerializedName("wicket")
    @Expose
    private Long wicket;

    public Long getRuns() {
        return runs;
    }

    public void setRuns(Long runs) {
        this.runs = runs;
    }

    public Long getWicket() {
        return wicket;
    }

    public void setWicket(Long wicket) {
        this.wicket = wicket;
    }

}
