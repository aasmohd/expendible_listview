package com.app.cricketapp.fragment

import android.content.Context
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.app.cricketapp.R
import com.app.cricketapp.adapter.InfoAdapter
import com.app.cricketapp.interfaces.FragmentDataCallback
import com.app.cricketapp.model.upcomingmatchresponse.Match

/**
 * Created by Prashant on 11-08-2017.
 */

class InfoFragment : BaseFragment() {
    internal var recyclerView: RecyclerView? = null
    internal var infoAdapter: InfoAdapter? = null
    internal var context: Context? = null
    var fragmentDataCallback: FragmentDataCallback? = null
    var matchBean: Match? = null

//    private var mAdView: NativeExpressAdView? = null

    override fun initUi() {
        context = activity
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        val linearLayoutManager: LinearLayoutManager? = LinearLayoutManager(context)
        recyclerView!!.layoutManager = linearLayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()

        fragmentDataCallback = activity as FragmentDataCallback
        matchBean = fragmentDataCallback?.bean
        infoAdapter = InfoAdapter(matchBean)
        recyclerView!!.adapter = infoAdapter


       /* mAdView = findViewById(R.id.adView) as NativeExpressAdView

        // Set its video options.
        mAdView?.videoOptions = VideoOptions.Builder()
                .setStartMuted(true)
                .build()

        // The VideoController can be used to get lifecycle events and info about an ad's video
        // asset. One will always be returned by getVideoController, even if the ad has no video
        // asset.
        val mVideoController = mAdView?.videoController
        mVideoController?.videoLifecycleCallbacks = object : VideoController.VideoLifecycleCallbacks() {
            override fun onVideoEnd() {
                Log.d("Native Ad", "Video playback is finished.")
                super.onVideoEnd()
            }
        }

        // Set an AdListener for the AdView, so the Activity can take action when an ad has finished
        // loading.
        mAdView?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                if (mVideoController?.hasVideoContent()!!) {
                    Log.d("Native Ad", "Received an ad that contains a video asset.")
                } else {
                    Log.d("Native Ad", "Received an ad that does not contain a video asset.")
                }
            }
        }

        mAdView?.loadAd(AdRequest.Builder().addTestDevice("06CD7F1190497B9ADD39C03F2D21C717").build())*/

    }

    override fun getLayoutById(): Int {
        return R.layout.fragment_info
    }
}
