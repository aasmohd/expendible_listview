package com.app.cricketapp.activity

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.app.cricketapp.R
import com.app.cricketapp.adapter.LatestNewsAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.db.dao.NewsListDAO
import com.app.cricketapp.model.latestnewsresponse.LatestNewsResponse
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.gson.Gson
import com.google.gson.JsonElement
import kotlinx.android.synthetic.main.activity_latest_news.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by bhavya on 18-04-2017.
 */

public class LatestNewsActivity : BaseActivity() {
    var latestNewsList: ArrayList<com.app.cricketapp.model.latestnewsresponse.List> = ArrayList()
    var   latestNewsAdapter = LatestNewsAdapter(latestNewsList)
    override fun initUi() {
        setBackButtonEnabled()
        showProgressBar(true)

        if(Utility.getLongSharedPreference(this,"news_time_stamp")!= 0L){
            // time difference check logic
            if (Utility.getLongSharedPreference(this,"news_time_stamp") < Utility.
                    getStringSharedPreference(this,AppConstants.NEWS_SERVER_TIME).toLong())
            {
                if (Utility.isNetworkAvailable(this)) {
                    callLatestNewsApi()
                } else {
                    showProgressBar(false)
                    CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
                }
            }else{
                showProgressBar(false)
                latestNewsList.addAll(NewsListDAO.getRecentMatches())
                if (latestNewsList.size > 6) {
                    latestNewsList[5].showAd = true
                } else {
                    latestNewsList[latestNewsList.size - 1].showAd = true
                }

                latestNewsAdapter.notifyDataSetChanged()


                if (latestNewsList.size == 0) {
                    empty_tv.visibility = View.VISIBLE
                } else {
                    empty_tv.visibility = View.GONE

                }
            }

        }
        else{
            val time = System.currentTimeMillis()

            Utility.putLongValueInSharedPreference(this,"news_time_stamp",time)

            if (Utility.isNetworkAvailable(this)) {
                callLatestNewsApi()
            } else {
                showProgressBar(false)
                CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
            }
        }
        val manager = LinearLayoutManager(getActivity())
        recycler_view.layoutManager = manager
        recycler_view.setHasFixedSize(true)
        recycler_view.adapter = latestNewsAdapter
        loadBannerAd()
        initFullScreenAdd()
    }

    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()
                }
            }
            requestNewInterstitial()
        }
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .build()

        mInterstitialAd.loadAd(adRequest)
    }

    override fun getLayoutById(): Int {
        return R.layout.activity_latest_news

    }

    fun callLatestNewsApi() {
        val apiService = AppRetrofit.getInstance().apiServices
        val call = apiService.latestNews()
        call.enqueue(object : Callback <JsonElement> {

            override fun onFailure(call: Call<JsonElement>?, t: Throwable?) {
                showProgressBar(false)
            }

            override fun onResponse(call: Call<JsonElement>?, response: Response<JsonElement>?) {
                if (response?.body() != null){
                    Utility.putLongValueInSharedPreference(this@LatestNewsActivity,"news_time_stamp",System.currentTimeMillis())

                    try {
                        showProgressBar(false)
                        val recentResponse = response.body().toString()
                        val gson = Gson()
                        val latestNewsResponse = gson.fromJson<LatestNewsResponse>(recentResponse,LatestNewsResponse::class.java)

                        val recentArrayList: ArrayList<com.app.cricketapp.model.latestnewsresponse.List>
                                    = latestNewsResponse.response!!

                        NewsListDAO.delete();
                        NewsListDAO.bulkInsert(recentArrayList);

                        latestNewsList.addAll(NewsListDAO.getRecentMatches())

                        if (latestNewsList.size > 6) {
                            latestNewsList[5].showAd = true
                        } else {
                            latestNewsList[latestNewsList.size - 1].showAd = true
                        }

                        latestNewsAdapter.notifyDataSetChanged()

                        Lg.i("RecentMatch", call.toString())
                        if (recentArrayList.size == 0) {
                            empty_tv.visibility = View.VISIBLE
                        } else {
                            empty_tv.visibility = View.GONE

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        empty_tv.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

}
