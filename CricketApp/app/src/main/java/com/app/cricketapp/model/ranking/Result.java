
package com.app.cricketapp.model.ranking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("rankings")
    @Expose
    public Rankings rankings;

    public Rankings getRankings() {
        return rankings;
    }

    public void setRankings(Rankings rankings) {
        this.rankings = rankings;
    }

}
