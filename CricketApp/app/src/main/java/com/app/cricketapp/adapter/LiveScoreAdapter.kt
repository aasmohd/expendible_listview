package com.app.cricketapp.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.activity.MatchDetailActivity
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.*
import java.util.*

/**
 * Created by bhavya on 12-04-2017.
 */

class LiveScoreAdapter(var watchLiveList: ArrayList<Match>) : RecyclerView.Adapter<LiveScoreAdapter.MyViewHolder>() {
    var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        context = parent?.context
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.row_watch_live, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {

        val match = watchLiveList[position]
        holder?.mTeamName1Tv?.text = match.teams.a?.key
        holder?.mTeamName2Tv?.text = match.teams.b?.key
        holder?.mMatchNameTv?.text = match.msgs.info
        holder?.mDateTv?.text = match.names.relatedName

        try {
            if (match.msgs.info.isEmpty()) {
                holder?.mMatchNameTv?.visibility = View.GONE
            } else {
                holder?.mMatchNameTv?.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            e.printStackTrace()
            holder?.mMatchNameTv?.visibility = View.GONE
        }

        holder?.mPlaceTv?.visibility = View.GONE
        holder?.otherTeamScore1?.visibility = View.GONE
        holder?.otherTeamScore2?.visibility = View.GONE

        holder?.fullname?.setText(match.teams.a!!.name + " VS " +
                match.teams.b!!.name)

        if (match.others.format == "one-day") {
            holder?.matchType?.setImageResource(R.drawable.odi_ribbon)
        } else if (match.others.format == "test") {
            holder?.matchType?.setImageResource(R.drawable.test_ribbon)
        } else {
            holder?.matchType?.setImageResource(R.drawable.t20_ribbon)
        }
        //Show Place name if match not started
        if (match.basicsInfo.status?.toLowerCase() == "notstarted") {
            holder?.mPlaceTv?.visibility = View.VISIBLE
            holder?.mPlaceTv?.text = match.basicsInfo.venue
        } else {
            //else show scores of respective team

            try {
                //Common
                val iniings12 = match.teams.a?.key + " " + match.results.innings.a.runs +
                        "-" + match.results.innings.a.wickets + " (" + match.results.innings.a.overs + ")"
                holder?.otherTeamScore1?.text = iniings12.toUpperCase()
                holder?.otherTeamScore1?.visibility = View.VISIBLE

                val iniings1 = match.teams.b?.key + " " + match.results.innings.b.runs +
                        "-" + match.results.innings.b.wickets + " (" + match.results.innings.b.overs + ")"
                holder?.otherTeamScore2?.text = iniings1.toUpperCase()
                holder?.otherTeamScore2?.visibility = View.VISIBLE


                if (match.now.innings == "2" && match.others.format == "test") {
                    if (match.now.batting_team == "a") {
                        val iniings11 = match.teams.a?.key + " " + match.results.innings.a.runs +
                                " & " + match.results.other_innings.a.runs + "-" + match.results.other_innings.a.wickets
                        holder?.otherTeamScore1?.text = iniings11.toUpperCase()
                        holder?.otherTeamScore1?.visibility = View.VISIBLE

                        val iniings12 = match.teams.b?.key + " " + match.results.innings.b.runs +
                                " & " + match.results.other_innings.b.runs
                        holder?.otherTeamScore2?.text = iniings12.toUpperCase()
                        holder?.otherTeamScore2?.visibility = View.VISIBLE

                    } else if (match.now.batting_team == "b") {
                        val iniings11 = match.teams.a?.key + " " + match.results.innings.a.runs +
                                " & " + match.results.other_innings.a.runs
                        holder?.otherTeamScore1?.text = iniings11.toUpperCase()
                        holder?.otherTeamScore1?.visibility = View.VISIBLE

                        val iniings12 = match.teams.b?.key + " " + match.results.innings.b.runs +
                                " & " + match.results.other_innings.b.runs + "-" + match.results.other_innings.b.wickets
                        holder?.otherTeamScore2?.text = iniings12.toUpperCase()
                        holder?.otherTeamScore2?.visibility = View.VISIBLE
                    } else {
                        val iniings11 = match.teams.a?.key + " " + match.results.innings.a.runs +
                                " & " + match.results.other_innings.a.runs
                        holder?.otherTeamScore1?.text = iniings11.toUpperCase()
                        holder?.otherTeamScore1?.visibility = View.VISIBLE

                        val iniings12 = match.teams.b?.key + " " + match.results.innings.b.runs +
                                " & " + match.results.other_innings.b.runs
                        holder?.otherTeamScore2?.text = iniings12.toUpperCase()
                        holder?.otherTeamScore2?.visibility = View.VISIBLE
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                holder?.mPlaceTv?.text = match.basicsInfo.venue
                holder?.mPlaceTv?.visibility = View.VISIBLE
                holder?.otherTeamScore1?.visibility = View.GONE
                holder?.otherTeamScore2?.visibility = View.GONE
            }

        }

        Utility.setImageWithUrl(match.teams.a?.logo, R.drawable.default_image, holder?.mTeamName1Iv)

        Utility.setImageWithUrl(match.teams.b?.logo, R.drawable.default_image, holder?.mTeamName2Iv)

        val utcTimeFormat = Utility.getLocalDate((match.basicsInfo.startDate)?.toLong() as Long * 1000,
                AppConstants.TARGET_DATE_FORMAT_2)

        val localTime2 = Utility.getLocalDate((match.basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT)
//        val gmtTime = Utility.parseDateTimeUtcToGmt(localTime2)

        val localTime = Utility.getLocalDate((match.basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT1)

//        String localTimeZone = Utility.getLocalTimeZone()
//        holder.mTimeTv.setText(utcTimeFormat + " " + localTime + " " + localTimeZone + " / " + gmtTime + " GMT")
        val day = Utility.getDay( (match.basicsInfo.startDate!!.toLong()*1000) )
        holder?.mTimeTv?.text = "$day, $utcTimeFormat $localTime"

        if (position%6 == 0 && position!=0) {
            holder?.adsContainer?.visibility = View.VISIBLE
            // Set its video options.
            holder?.mAdView?.videoOptions = VideoOptions.Builder()
                    .setStartMuted(true)
                    .build()

            // The VideoController can be used to get lifecycle events and info about an ad's video
            // asset. One will always be returned by getVideoController, even if the ad has no video
            // asset.
            val mVideoController = holder?.mAdView?.videoController
            mVideoController?.videoLifecycleCallbacks = object : VideoController.VideoLifecycleCallbacks() {
                override fun onVideoEnd() {
                    Log.d("Video", "Video playback is finished.")
                    super.onVideoEnd()
                }
            }

            // Set an AdListener for the AdView, so the Activity can take action when an ad has finished
            // loading.
            holder?.mAdView?.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    if (mVideoController?.hasVideoContent()!!) {
                        Log.d("Video", "Received an ad that contains a video asset.")
                    } else {
                        Log.d("Video", "Received an ad that does not contain a video asset.")
                    }
                }
            }

            holder?.mAdView?.loadAd(AdRequest.Builder()
                    .addTestDevice("C11191FE7944B40640463F2686610772").build())
        } else {
            holder?.adsContainer?.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return watchLiveList.size

    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var adsContainer: RelativeLayout = view.findViewById(R.id.ads_container)
        var mAdView: NativeExpressAdView = view.findViewById(R.id.adView)

        var mMatchNameTv: TextView? = view.findViewById(R.id.match_name_tv)
        var mDateTv: TextView? = view.findViewById(R.id.date_tv)
        var mPlaceTv: TextView? = view.findViewById(R.id.place_tv)
        var otherTeamScore1: TextView? = view.findViewById(R.id.other_team_score)
        var otherTeamScore2: TextView? = view.findViewById(R.id.other_team_score2)
        var mTimeTv: TextView? = view.findViewById(R.id.time_tv)
        var mTeamName1Tv: TextView? = view.findViewById(R.id.team_name_1_tv)
        var mTeamName2Tv: TextView? = view.findViewById(R.id.team_name_2_tv)
        var mTeamName1Iv: ImageView = view.findViewById(R.id.team_name_1_iv)
        var mTeamName2Iv: ImageView = view.findViewById(R.id.team_name_2_iv)
        var matchType: ImageView = view.findViewById(R.id.match_type)
        var fullname: TextView = view.findViewById(R.id.fullname_tv)

        init {
            itemView.setOnClickListener { v ->

                val match = watchLiveList[adapterPosition]
                if (match.results.score_card_available == "1") {
                    val intent: Intent = Intent(v.context, MatchDetailActivity::class.java)
                    intent.putExtra(AppConstants.EXTRA_KEY, match.others?.key)
                    intent.putExtra("match_category", "live")
                    v.context.startActivity(intent)
                } else {
                    CustomToast.getInstance(v.context as Activity?).showToast(R.string.no_scorecard)
                }
            }
        }
    }
}
