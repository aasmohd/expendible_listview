package com.app.cricketapp.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.cricketapp.R;
import com.app.cricketapp.customview.MaterialProgressDialog;
import com.app.cricketapp.utility.AppConstants;
import com.app.cricketapp.utility.Utility;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public abstract class BaseFragment extends Fragment {
    protected Context mContext;
    private View view;
    private MaterialProgressDialog mMaterialProgressDialog;
    private CircularProgressView mMaterialProgressBar;
    public ViewDataBinding viewDataBinding;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutById(), container, false);
        if (viewDataBinding != null)
            return viewDataBinding.getRoot();
        else
            return inflater.inflate(getLayoutById(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        mMaterialProgressBar = (CircularProgressView) findViewById(R.id.circular_progress_bar);

        DatabaseReference child = FirebaseDatabase.getInstance().getReference().child(AppConstants.FIREBASE_CHILD);

        mMaterialProgressDialog = Utility.getProgressDialogInstance(mContext);
        initUi();
    }

    protected View findViewById(int resId) {
        return view.findViewById(resId);
    }

    protected abstract void initUi();

    protected abstract int getLayoutById();

    public void showProgressDialog(boolean iShow) {
        if (mMaterialProgressDialog != null) {
            if (iShow)
                mMaterialProgressDialog.show();
            else
                mMaterialProgressDialog.dismiss();
        }
    }

    public void updateSpeechIcon() {

    }

    public void showProgressBar(boolean iShow) {
        if (mMaterialProgressBar != null) {
            if (iShow)
                mMaterialProgressBar.setVisibility(View.VISIBLE);
            else
                mMaterialProgressBar.setVisibility(View.GONE);
        }
    }


    public Context getContext() {
        return mContext;
    }

}
