package com.app.cricketapp.activity

import android.support.design.widget.AppBarLayout
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPager
import android.view.View
import com.app.cricketapp.R
import com.app.cricketapp.adapter.PlayerStatesPagerAdapter
import com.app.cricketapp.databinding.ActivityPlayerStatsBinding
import com.app.cricketapp.fragment.CommentaryFragment
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd

/**
 * Created by Prashant on 05-10-2017.
 */
class PlayerStatsActivity : BaseActivity() , AppBarLayout.OnOffsetChangedListener {

    private val PERCENTAGE_TO_SHOW_IMAGE = 20
    private var mFab: View? = null
    private var mMaxScrollSize: Int = 0
    private var mIsImageHidden: Boolean = false
    var binding:ActivityPlayerStatsBinding? = null

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, i: Int) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout!!.getTotalScrollRange()

        val currentScrollPercentage = Math.abs(i) * 100 / mMaxScrollSize

        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_IMAGE) {
            if (!mIsImageHidden) {
                mIsImageHidden = true

                ViewCompat.animate(mFab).scaleY(0f).scaleX(0f).start()
            }
        }

        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_IMAGE) {
            if (mIsImageHidden) {
                mIsImageHidden = false
                ViewCompat.animate(mFab).scaleY(1f).scaleX(1f).start()
            }
        }
    }

    override fun initUi() {
        binding = viewDataBinding as ActivityPlayerStatsBinding?
        binding?.collapsing?.title = "Virat Kohli"
        binding?.flexibleToolbar?.setNavigationOnClickListener { onBackPressed() }
        binding?.teamImage1Iv?.setImageResource(R.drawable.icon)

        val tabLayout = binding?.tabLayout
        val viewPager = binding?.viewPager

            setupViewPager(viewPager!!, tabLayout!!)


        loadBannerAd()
        initFullScreenAdd()
    }

    override fun getLayoutById(): Int {
       return R.layout.activity_player_stats
    }
    private fun loadBannerAd() {
        val adRequest: AdRequest = AdRequest.Builder().build()
        binding?.adView?.loadAd(adRequest)
    }

    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()
                }
            }
            requestNewInterstitial()
        }
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .build()

        mInterstitialAd.loadAd(adRequest)
    }

    private fun setupViewPager(viewPager: ViewPager, tabLayout: TabLayout) {
        val adapter = PlayerStatesPagerAdapter(supportFragmentManager)
        adapter.addFragment(CommentaryFragment(), "INFO")
        adapter.addFragment(CommentaryFragment(), "BATTING")
        adapter.addFragment(CommentaryFragment(), "BOWLING")

        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> {
                    }
                    1 -> {
                    }
                    2 -> {
                    }
                }
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        tabLayout.setupWithViewPager(viewPager)

    }

}




