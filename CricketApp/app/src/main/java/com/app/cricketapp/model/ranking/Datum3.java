
package com.app.cricketapp.model.ranking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum3 {

    @SerializedName("player_name")
    @Expose
    public String playerName;
    @SerializedName("position")
    @Expose
    public Long position;
    @SerializedName("points")
    @Expose
    public Long points;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

}
