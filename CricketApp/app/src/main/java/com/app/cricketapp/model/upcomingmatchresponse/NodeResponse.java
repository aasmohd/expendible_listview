package com.app.cricketapp.model.upcomingmatchresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Prashant on 15-12-2017.
 */

public class NodeResponse
{
    @SerializedName("status")
    @Expose
    private long status;
    @SerializedName("response")
    @Expose
    private NewUpcomingMatchResponse response;
    @SerializedName("time")
    @Expose
    private long time;

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public NewUpcomingMatchResponse getResponse() {
        return response;
    }

    public void setResponse(NewUpcomingMatchResponse response) {
        this.response = response;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
