
package com.app.cricketapp.model.scorecard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Teams implements Serializable {

    @SerializedName("a")
    @Expose
    public A a;
    @SerializedName("b")
    @Expose
    public A b;

}
