package com.app.cricketapp.model.save_user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Texts {

    @SerializedName("disclaimerParagraphs")
    @Expose
    var disclaimerParagraphs: String? = null

}
