
package com.app.cricketapp.model.commentaryResPonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bowler {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("bowling")
    @Expose
    private Bowling bowling;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Bowling getBowling() {
        return bowling;
    }

    public void setBowling(Bowling bowling) {
        this.bowling = bowling;
    }

}
