
package com.app.cricketapp.model.commentaryResPonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Batting {

    @SerializedName("dots")
    @Expose
    private Long dots;
    @SerializedName("sixes")
    @Expose
    private Long sixes;
    @SerializedName("runs")
    @Expose
    private Long runs;
    @SerializedName("balls")
    @Expose
    private Long balls;
    @SerializedName("fours")
    @Expose
    private Long fours;
    @SerializedName("dismissed")
    @Expose
    private Boolean dismissed = false;

    public Long getDots() {
        return dots;
    }

    public void setDots(Long dots) {
        this.dots = dots;
    }

    public Long getSixes() {
        return sixes;
    }

    public void setSixes(Long sixes) {
        this.sixes = sixes;
    }

    public Long getRuns() {
        return runs;
    }

    public void setRuns(Long runs) {
        this.runs = runs;
    }

    public Long getBalls() {
        return balls;
    }

    public void setBalls(Long balls) {
        this.balls = balls;
    }

    public Long getFours() {
        return fours;
    }

    public void setFours(Long fours) {
        this.fours = fours;
    }

    public Boolean getDismissed()
    {
        return dismissed;
    }

    public void setDismissed(Boolean dismissed)
    {
        this.dismissed = dismissed;
    }
}
