
package com.app.cricketapp.model.matches;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Result implements Parcelable {

    @SerializedName("matches")
    public ArrayList<Match> matches = null;

    public String getRunning_msg() {
        return running_msg;
    }

    public void setRunning_msg(String running_msg) {
        this.running_msg = running_msg;
    }

    @SerializedName("running_msg")
    public String running_msg=null;

    protected Result(Parcel in) {
        matches = in.createTypedArrayList(Match.CREATOR);

    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }



    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(matches);

    }
}
