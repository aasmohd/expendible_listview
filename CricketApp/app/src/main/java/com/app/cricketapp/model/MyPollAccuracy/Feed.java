package com.app.cricketapp.model.MyPollAccuracy;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by osr on 8/2/18.
 */

public class Feed {


    @SerializedName("correctPoll")
    float correctPoll;
    @SerializedName("totalPoll")
    int totalPoll;

    public int getTotalPoll()
    {
        return totalPoll;
    }

    public void setTotalPoll(int totalPoll)
    {
        this.totalPoll = totalPoll;
    }

    public float getCorrectPoll() {
        return correctPoll;
    }

    public void setCorrectPoll(float correctPoll) {
        this.correctPoll = correctPoll;
    }

    public float getIncorrectPoll() {
        return incorrectPoll;
    }

    public void setIncorrectPoll(float incorrectPoll) {
        this.incorrectPoll = incorrectPoll;
    }

    @SerializedName("incorrectPoll")
    float incorrectPoll;
    @SerializedName("pendingPoll")
    float pendingPoll;
    @SerializedName("polls")
    public ArrayList<Polls> polls;




    public float getPendingPoll() {
        return pendingPoll;
    }

    public void setPendingPoll(float pendingPoll) {
        this.pendingPoll = pendingPoll;
    }

    public ArrayList<Polls> getPolls() {
        return polls;
    }

    public void setPolls(ArrayList<Polls> polls) {
        this.polls = polls;
    }


}
