package com.app.cricketapp.adapter

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.model.scorecard.TeamInfo
import com.app.cricketapp.utility.Lg

/**
 * Created by Prashant on 11-08-2017.
 */

class LineupAdapter(var teamAInfo: TeamInfo?,
                    var teamBInfo: TeamInfo?) : RecyclerView.Adapter<LineupAdapter.LineupViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LineupViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_line_up_row, parent, false)
        return LineupViewHolder(view)
    }

    override fun onBindViewHolder(holder: LineupViewHolder, position: Int) {
        if (position == 0) {
            holder.teamLayout.visibility = View.VISIBLE
        } else {
            holder.teamLayout.visibility = View.GONE
        }
        holder.aTeamTv.text = teamAInfo?.name
        holder.bTeamTv.text = teamBInfo?.name

        if (position.rem(2) == 0) {
            holder.teamPlayersLayout.setBackgroundColor(Color.WHITE)
        } else {
            holder.teamPlayersLayout.setBackgroundColor(Color.parseColor("#F7F7F7"))
        }
        holder.aPlayerTv.text = ""
        holder.bPlayerTv.text
        try {
            holder.aPlayerTv.text = teamAInfo?.allPlayers?.get(position)?.fullName
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            holder.bPlayerTv.text = teamBInfo?.allPlayers?.get(position)?.fullName
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        var teamAPlayers = 0
        var teamBPlayers = 0
        try {
            teamAPlayers = teamAInfo?.allPlayers!!.size
        } catch (e: Exception) {
            Lg.e("Error", "Lineup not available")
        }
        try {
            teamBPlayers = teamBInfo?.allPlayers!!.size
        } catch (e: Exception) {
            Lg.e("Error", "Lineup not available")
        }

        if (teamAPlayers == teamBPlayers)
            return teamAPlayers
        else {
            if (teamAPlayers > teamBPlayers)
                return teamAPlayers

        }
        return teamBPlayers
    }

    inner class LineupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var aTeamTv: TextView = itemView.findViewById<TextView>(R.id.a_team)
        internal var bTeamTv: TextView = itemView.findViewById<TextView>(R.id.b_team)

        internal var aPlayerTv: TextView = itemView.findViewById<TextView>(R.id.a_player)
        internal var bPlayerTv: TextView = itemView.findViewById<TextView>(R.id.b_player)
        internal var teamLayout: LinearLayout = itemView.findViewById<LinearLayout>(R.id.team_lv)
        internal var teamPlayersLayout: LinearLayout = itemView.findViewById<LinearLayout>(R.id.team_players_lv)

    }
}
