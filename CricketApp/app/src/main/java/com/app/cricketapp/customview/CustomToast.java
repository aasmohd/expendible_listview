package com.app.cricketapp.customview;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cricketapp.R;


/**
 * Created by Rahul on 10-10-2016.
 */

public class CustomToast {

    private static CustomToast mInstance;
    private Context context;
    private Toast toast;

    private CustomToast(Context context) {
        this.context = context;
    }

    public static synchronized CustomToast getInstance(Context context) {
        synchronized (CustomToast.class) {
            if (mInstance == null) {
                mInstance = new CustomToast(context);
            }
        }
        return mInstance;
    }

    private void initToast() {
        LayoutInflater li = LayoutInflater.from(context);
        View layout = li.inflate(R.layout.layout_custom_toast,null);

        toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 80);
        toast.setView(layout);//setting the view of custom toast layout
    }

    public void showToast(String str) {
        if (toast == null) {
            initToast();
        }
        TextView textView = toast.getView().findViewById(R.id.custom_toast_message);
        textView.setText(str);

        toast.show();
    }

    public void showToast(int str) {
        if (toast == null) {
            initToast();
        }
        TextView textView = (TextView) toast.getView().findViewById(R.id.custom_toast_message);
        textView.setText(str);

        toast.show();
    }
}
