package com.app.cricketapp.activity

import android.support.v7.widget.LinearLayoutManager
import com.app.cricketapp.R
import com.app.cricketapp.adapter.SeasonAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.databinding.ActivitySeasonBinding
import com.app.cricketapp.model.seasonmodels.seasonsList.Season
import com.app.cricketapp.model.seasonmodels.seasonsList.SeasonListModel
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import kotlinx.android.synthetic.main.activity_latest_news.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Prashant on 04-10-2017.
 */
class SeasonActivity:BaseActivity(){
    var binding:ActivitySeasonBinding? = null
    var seasonAdapter: SeasonAdapter? = null
    var seasonsList:MutableList<Season> = ArrayList<Season>()

    override fun getLayoutById(): Int {
        return R.layout.activity_season
    }

    override fun initUi() {
        binding = viewDataBinding as ActivitySeasonBinding?
        setBackButtonEnabled()
        seasonAdapter = SeasonAdapter(this@SeasonActivity, seasonsList )
        if (Utility.isNetworkAvailable(this)) {
            //call Api
            callGetSeasonAPI()
        } else {
            showProgressBar(false)
            CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
        }
        val manager = LinearLayoutManager(getActivity())
        binding?.recyclerView?.layoutManager = manager
        binding?.recyclerView?.setHasFixedSize(true)
        binding?.recyclerView?.adapter = seasonAdapter
        loadBannerAd()
        initFullScreenAdd()
    }

    private fun callGetSeasonAPI() {
        showProgressBar(true)
        val apiService = AppRetrofit.getInstance().apiServices
        val call = apiService.seasonList()
        call.enqueue(object : Callback<SeasonListModel> {
            override fun onFailure(call: Call<SeasonListModel>?, t: Throwable?) {
                showProgressBar(false)
            }

            override fun onResponse(call: Call<SeasonListModel>?, response: Response<SeasonListModel>?) {
                showProgressBar(false)

                val seasonList = response?.body()?.seasons
                val status=response?.body()?.status

                if (status == 1L){
                    this@SeasonActivity.seasonsList.addAll(seasonList!!)
                    seasonAdapter?.notifyDataSetChanged()
                }
            }
        })
    }

    fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()
                }
            }
            requestNewInterstitial()
        }
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .build()

        mInterstitialAd.loadAd(adRequest)
    }

}