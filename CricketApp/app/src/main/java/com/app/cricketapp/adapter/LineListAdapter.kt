package com.app.cricketapp.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.activity.NewMatchRateActivity
import com.app.cricketapp.model.matchresponse.MatchRoom
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility

/**
 * Created by Prashant on 03-02-2018.
 */
class LineListAdapter(var context: Context , private var matchRoomList:MutableList<MatchRoom>):
        RecyclerView.Adapter<LineListAdapter.SeasonVH>()
{
    var matchListN = java.util.ArrayList<MatchRoom>()
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SeasonVH {
        val view = LayoutInflater.from(context).inflate(R.layout.row_fast_line,parent,false)
        return SeasonVH(view)
    }

    override fun getItemCount(): Int {
        return matchRoomList.size
    }

    override fun onBindViewHolder(holder: SeasonVH, position: Int) {
        var match = matchRoomList.get(position)

        Utility.setImageWithUrl(match.team_1_logo, R.drawable.default_image, holder.team1Image)
        Utility.setImageWithUrl(match.team_2_logo, R.drawable.default_image, holder.team2Image)

        holder.team1Name.setText(match.team1)
        holder.team2Name.setText(match.team2)

        holder.matchTv.setText(match.team1_full_name+ " vs "+match.team2_full_name)
        holder.dateTv.setText(match.match_started_time)

        holder.matchType.setText(match.match_number)
        if (!TextUtils.isEmpty(match.landing_text)&& match.landing_text != "0"){
            holder.statusTv.text = match.landing_text
            holder.timerTv.visibility = View.GONE
        }else{
            if (!TextUtils.isEmpty(match.match_started_time)){
                updateTimeRemaining(System.currentTimeMillis(),match,holder.timerTv,holder.statusTv)
            }else{
                val matchStarted = match.match_started.toLowerCase()
                if (matchStarted == "finished") {
                    holder.statusTv.text = context.getString(R.string.finished)
                    holder.timerTv.visibility = View.GONE
                } else if (matchStarted == "playing"
                        || matchStarted == "started") {
                    holder.statusTv.text = context.getString(R.string.playing_now)
                    holder.timerTv.visibility = View.GONE
                } else {
                    holder.statusTv.text = ""
                    holder.timerTv.visibility = View.GONE
                }
            }
            /*if (match.matchStatus.equals("finished",true)){
                holder.matchStatus.text = match.matchStatus.toUpperCase()
            }else if (match.matchStatus.equals("notStarted",true)){
                updateTimeRemaining(System.currentTimeMillis(), match, holder.matchStatus)
            }else{
                holder.matchStatus.text = "PLAYING NOW"
            }*/
        }


        holder.cardLayout?.setOnClickListener{ v ->
            val intent = Intent(v.context, NewMatchRateActivity::class.java)
            matchListN.clear()
            matchListN.add(match)
            intent.putParcelableArrayListExtra(AppConstants.EXTRA_MATCHES_ARRAY, matchListN)
            intent.putExtra(AppConstants.EXTRA_POS, position)
            v.context.startActivity(intent)
        }
    }

    class SeasonVH(view: View) : RecyclerView.ViewHolder(view) {
        var cardLayout = view.findViewById<FrameLayout>(R.id.card)
        var matchType = view.findViewById<TextView>(R.id.match_type)
        var team1Image = view.findViewById<ImageView>(R.id.team_name_1_iv)
        var team2Image = view.findViewById<ImageView>(R.id.team_name_2_iv)
        var team1Name = view.findViewById<TextView>(R.id.team_name_1_tv)
        var team2Name = view.findViewById<TextView>(R.id.team_name_2_tv)
        var timerTv = view.findViewById<TextView>(R.id.time_tv)
        var statusTv = view.findViewById<TextView>(R.id.status_tv)
        var dateTv = view.findViewById<TextView>(R.id.date_tv)
        var matchTv = view.findViewById<TextView>(R.id.fullname_tv)

    }

    private fun updateTimeRemaining(currentTime: Long, match: MatchRoom, timerTv: TextView,statusTv:TextView) {
        val parseDateTimeLocal = Utility.parseDateTimeLocal(match.match_started_time, "dd/MM/yyyy HH:mm")

        timerTv.visibility = View.GONE

        val timeDiff = parseDateTimeLocal.time - currentTime
        if (timeDiff > 0)
        {
            val seconds = timeDiff / 1000
            val hr = seconds / 3600
            val rem = seconds % 3600
            val mn = rem / 60
            val sec = rem % 60

            var hrStr = ""
            if (hr < 10) {
                hrStr = "0" + hr
            } else {
                hrStr = "" + hr
            }

            var mnStr = ""
            if (mn < 10) {
                mnStr = "0" + mn
            } else {
                mnStr = "" + mn
            }

            var secStr = ""
            if (sec < 10) {
                secStr = "0" + sec
            } else {
                secStr = "" + sec
            }

            if (!TextUtils.isEmpty(match.landing_text) && match.landing_text != "0") {
                statusTv.text = match.landing_text
                timerTv.visibility = View.GONE
            } else {
                val matchStarted = match.match_started.toLowerCase()
                if (matchStarted == "finished") {
                    statusTv.text = context.getString(R.string.finished)
                    timerTv.visibility = View.GONE
                } else if (matchStarted == "playing"
                        || matchStarted == "started") {
                    statusTv.text = context.getString(R.string.playing_now)
                    timerTv.visibility = View.GONE
                } else if (matchStarted == "notstarted") {
                    timerTv.visibility= View.VISIBLE
                    timerTv.text = "Match starts in $hrStr:$mnStr:$secStr"
                    statusTv.visibility= View.GONE

                } else {
                    statusTv.text = ""
                    timerTv.visibility = View.GONE
                }
            }
        } else
        {
            val matchStarted = match.match_started.toLowerCase()
            if (matchStarted == "finished") {
                statusTv.text = context.getString(R.string.finished)
                timerTv.visibility = View.GONE
            } else if (matchStarted == "playing"
                    || matchStarted == "started") {
                statusTv.text = context.getString(R.string.playing_now)
                timerTv.visibility = View.GONE
            } else {
                statusTv.text = ""
                timerTv.visibility = View.GONE
            }
        }
    }

}