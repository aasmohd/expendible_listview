
package com.app.cricketapp.model.scorecard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class A implements Serializable {

    @SerializedName("batting")
    @Expose
    public ArrayList<Batting> batting = null;
    @SerializedName("bowling")
    @Expose
    public ArrayList<Bowling> bowling = null;
    @SerializedName("team_info")
    @Expose
    public TeamInfo teamInfo;

}
