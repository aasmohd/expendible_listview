package com.app.cricketapp.utility;

import org.jetbrains.annotations.Nullable;

/**
 * Created on 17-04-2017.
 */

public class AppConstants {

    //LIVE
    public static String FIREBASE_CHILD = "match";
    public static String FIREBASE_CHILD2 = "match";
//  public static String BASE_URL = "http://www.mobiappexpress.com:4005/lineguru/api/";
    public static String BASE_URL = "http://18.221.244.184:4005/lineguru/api/";
    public static final String UPCOMING_URL = "v1/match/upcoming";
    public static final String NEW_UPCOMING_URL = "v1/liveMatch/upcomingMatches";
    public static final String RECENT_MATCHES_URL = "v1/match/recent";
    public static final String WATCH_LIVE_API = "v1/liveMatch/liveMatches";
    public static final String LATEST_NEWS_URL = "v1/liveMatch/getNews";
    public static final String MATCHES = "v1/match/get-matches";
    public static final String SCORECARD_URL = "v1/liveMatch/scorecard";
    public static final String SAVE_USER_URL = "v1/user/signUp";
    public static final String NEW_RECENT_MATCHES_URL = "v1/liveMatch/recentMatches";
    public static final String POLL_DATA_URL = "v1/poll/listPolls";
    public static final String POLL_PREDICT = "v1/poll/givePoll";
    public static final String RANKING_URL = "v1/liveMatch/getRanking";
    public static final String SEASON_LIST_URL = "v1/series/getSeason";
    public static final String SEASON_DETAIL_URL = "v1/series/seasonMatches";
//    public static final String BALL_BY_BALL_URL = "v1/liveMatch/ballSummary ";
    public static final String BALL_BY_BALL_URL = "v1/liveMatch/testMatchBallSummary ";


    //DEV
    /*public static String BASE_URL = "http://cricketdev.apphosthub.com/api/";
    public static final String UPCOMING_URL = "v1/match/upcoming";
    public static final String NEW_UPCOMING_URL = "v1/match/upcomingMatches";
    public static final String RECENT_MATCHES_URL = "v1/match/recent";
    public static final String WATCH_LIVE_API = "v1/match/live";
    public static final String LATEST_NEWS_URL = "v1/match/news";
    public static final String MATCHES = "v1/match/get-matches";
    public static final String SCORECARD_URL = "v1/match/scorecard";
    public static final String SAVE_USER_URL = "v1/match/signup";
    public static final String NEW_RECENT_MATCHES_URL = "v1/match/recentMatches";
    public static final String POLL_DATA_URL = "v1/polls/listPolls";
    public static final String POLL_PREDICT = "v1/polls/predictPolls";
    public static final String RANKING_URL = "v1/match/getRanking";
    public static final String SEASON_LIST_URL = "v2/seasons/getSeason";
    public static final String SEASON_DETAIL_URL = "v2/seasons/seasonMatches";*/

    public static final String MATCH_RATE_BEAN = "match_rate_intent";

    public static final String SERVER_DATE_FORMAT = "MM/dd/yyyy hh:mm:ss a";
    public static final String SERVER_DATE_FORMAT1 = "hh:mm a";
    public static final String SERVER_DATE_FORMAT2 = "dd MMMM, hh:mm a";
    //2017-04-18T14:30+00:00
    public static final String NEA_DATE_FORMAT = "yyyyddmmhhmmss";
    public static final String DATE_FORMAT1 = "yyyy-MM-dd'T'HH:mm:ssS";
    public static final String DATE_FORMAT2 = "yyyy-MM-dd'T'HH:mmXXX";
    // "yyyy-mm-dd't'hh:mm:ss'z'"
    public static final String DATE_FORMAT3 = "dd-MM-yyyy";
    public static final String DATE_FORMAT4 = "dd-MMM-yyyy";

    /*10-04-2017/ 2:30*/
    public static final String TARGET_DATE_FORMAT_1 = "dd-MM-yyyy'/'hh:mm";
    public static final String TARGET_DATE_FORMAT_2 = "dd MMM";
    public static final String TARGET_DATE_FORMAT_3 = "EE MMM dd";
    public static final String LATEST_NEWS_INTENT = "latest_news_intent";
    public static final String SPEECH_KEY = "speech_key";
    public static final String IS_MALE_SPEECH_KEY = "is_male_speech_key";
    public static final String EXTRA_KEY = "extra_key";
    public static final String EXTRA_POS = "extra_pos";
    public static final String EXTRA_MATCHES_ARRAY = "extra_matches_array";
    public static final String EXTRA_MATCHES = "extra_matches";
    public static final String PREFS_IS_FIRST_TIME = "prefs_is_first_time";
    public static final String PREFS_DEVICE_TOKEN = "prefs_device_token";
    public static final String PREFS_MATCH_POS = "prefs_match_pos";
    public static final String PREFS_DISCLAIMER = "prefs_disclaimer";
    public static final String PREFS_ACCESS_TOKEN = "prefs_access_token";
    public static final String TOPIC_MATCH = "match";
    public static final String TOPIC_ANDROID = "topic_android";

    public static final String BALL = "Ball";

    public static final String DEFAULT = "def";
    public static final String FOUR = "four";
    public static final String BALL_TAG = "ball_tag";
    public static final String SIX = "six";

    public static final Integer BATTING = 1;
    public static final Integer BOWLING = 2;
    public static final Integer BATTING_VIEW = 3;
    public static final Integer BOWLING_VIEW = 4;
    @Nullable
    public static final String APP_USERID = "aap_userid";
    public static final String RUNNING_MSG = "running_msg";

    public static final String NOTIFICATION_PREFS = "notification_prefs";
    @Nullable
    public static final String MATCH_API_SERVER_TIME = "match_server_time";
    public static final String RANKING_API_SERVER_TIME = "ranking_server_time";
    public static final String NEWS_SERVER_TIME = "news_server_time";


    public static String CONTENT_PROVIDER_AUTHORITY_NAME() {
        return "com.cricketapp.contentprovider";
    }



}
