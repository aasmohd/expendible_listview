
package com.app.cricketapp.model.commentaryResPonse;

import java.util.List;

public class Response {

    private Long total;

    public List<Balls> data = null;

    private String format;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<Balls> getData() {
        return data;
    }

    public void setData(List<Balls> data) {
        this.data = data;
    }

    public String getFormat()
    {
        return format;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }
}
