
package com.app.cricketapp.model.ranking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class T20Ranking {

    @SerializedName("t20TeamRanking")
    @Expose
    public T20TeamRanking t20TeamRanking;
    @SerializedName("t20BatsmenRanking")
    @Expose
    public T20BatsmenRanking t20BatsmenRanking;
    @SerializedName("t20BowlerRanking")
    @Expose
    public T20BowlerRanking t20BowlerRanking;
    @SerializedName("t20AllRounderRanking")
    @Expose
    public T20AllRounderRanking t20AllRounderRanking;

    public T20TeamRanking getT20TeamRanking() {
        return t20TeamRanking;
    }

    public void setT20TeamRanking(T20TeamRanking t20TeamRanking) {
        this.t20TeamRanking = t20TeamRanking;
    }

    public T20BatsmenRanking getT20BatsmenRanking() {
        return t20BatsmenRanking;
    }

    public void setT20BatsmenRanking(T20BatsmenRanking t20BatsmenRanking) {
        this.t20BatsmenRanking = t20BatsmenRanking;
    }

    public T20BowlerRanking getT20BowlerRanking() {
        return t20BowlerRanking;
    }

    public void setT20BowlerRanking(T20BowlerRanking t20BowlerRanking) {
        this.t20BowlerRanking = t20BowlerRanking;
    }

    public T20AllRounderRanking getT20AllRounderRanking() {
        return t20AllRounderRanking;
    }

    public void setT20AllRounderRanking(T20AllRounderRanking t20AllRounderRanking) {
        this.t20AllRounderRanking = t20AllRounderRanking;
    }

}
