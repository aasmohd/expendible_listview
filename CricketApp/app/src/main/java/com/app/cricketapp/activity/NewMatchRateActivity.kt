package com.app.cricketapp.activity

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.support.v4.view.ViewPager
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.WindowManager
import com.app.cricketapp.R
import com.app.cricketapp.adapter.MatchesPagerAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.model.eve.NetworkChangeEvent
import com.app.cricketapp.model.eve.RefreshLineScreen
import com.app.cricketapp.model.matchresponse.MatchRoom
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import kotlinx.android.synthetic.main.activity_new_match_rate.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 * Created by bhavya on 13-04-2017.
 */

class NewMatchRateActivity : BaseActivity() {

    override fun getLayoutById(): Int {
        return R.layout.activity_new_match_rate
    }

    var matchArrayList:ArrayList<MatchRoom>? = null
    var pos = 0

    override fun initUi() {
        EventBus.getDefault().register(this)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        try {
            matchArrayList = intent.extras.getParcelableArrayList<MatchRoom>(AppConstants.EXTRA_MATCHES_ARRAY)
            pos = intent.extras.getInt(AppConstants.EXTRA_POS)

            pager.adapter = MatchesPagerAdapter(supportFragmentManager, matchArrayList)
            checkForAdd(matchArrayList!!)
            pager.offscreenPageLimit = pager.adapter.count
            tabs.setupWithViewPager(pager)

            if (matchArrayList!!.size > 1)
                tabs.setSelectedTabIndicatorHeight(5)
            else
                tabs.setSelectedTabIndicatorHeight(0)

            pager.currentItem = pos
            Utility.putIntValueInSharedPreference(getActivity(), AppConstants.PREFS_MATCH_POS, pos)
            pager.addOnPageChangeListener(listener)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        setBackButtonEnabled()
        toggleAction3Icon(View.GONE)

        if (!Utility.isNetworkAvailable(this)) {
            showAlertDialog(this, getString(R.string.check_internet_connection))
        }
    }

    var listener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        }

        override fun onPageSelected(position: Int) {
            val matchesPagerAdapter = pager.adapter as MatchesPagerAdapter
            Utility.putIntValueInSharedPreference(getActivity(), AppConstants.PREFS_MATCH_POS, position)
            try {
                matchesPagerAdapter.mFragmentList[position].updateSpeechIcon()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    fun checkForAdd(matchArrayList: ArrayList<MatchRoom>) {
        var count = -1
        if (!matchArrayList.isEmpty()) {
            count = 0
            for (i in matchArrayList.indices) {
                val matchStarted = matchArrayList[i].match_started.toLowerCase()
                if (matchStarted == "notstarted") {
                    count += 1
                }
            }
        }

        if (count != -1 && matchArrayList.size == count) {
            initFullScreenAdd()
        } else {
            //remove add
        }
    }


    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()

                }
            }
            requestNewInterstitial()
        }
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .addTestDevice("06CD7F1190497B9ADD39C03F2D21C717")
                .build()

        mInterstitialAd.loadAd(adRequest)
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onNetworkChangeEvent(event: NetworkChangeEvent) {
        if (!Utility.isNetworkAvailable(this)) {
            showAlertDialog(this, getString(R.string.check_internet_connection))
        }/*else{
            pager.adapter = MatchesPagerAdapter(supportFragmentManager, matchArrayList)
            pager.offscreenPageLimit = pager.adapter.count
            tabs.setupWithViewPager(pager)

            if (matchArrayList!!.size > 1)
                tabs.setSelectedTabIndicatorHeight(5)
            else
                tabs.setSelectedTabIndicatorHeight(0)

            pager.currentItem = pos
            Utility.putIntValueInSharedPreference(getActivity(), AppConstants.PREFS_MATCH_POS, pos)
            pager.addOnPageChangeListener(listener)
        }*/
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onMatchUpdateEvent(event: RefreshLineScreen) {
        val intent2 = Intent(getActivity(), NewMatchRateActivity::class.java)
        intent2.putParcelableArrayListExtra(AppConstants.EXTRA_MATCHES_ARRAY, event.matchArrayList)
        intent2.putExtra(AppConstants.EXTRA_POS, 0)

        intent = intent2

        val isReCreate = checkReCreate(event.matchArrayList)

        if (event.matchArrayList.isEmpty()) {
            CustomToast.getInstance(getActivity()).showToast(R.string.stay_tuned_with_us_for_upcoming_matches)
            finish()
        } else {
            if (isReCreate) {
                pager.clearOnPageChangeListeners()
                pager.adapter = null
                recreate()
            }
        }
    }

    fun checkReCreate(matchArrayList: ArrayList<MatchRoom>): Boolean {

        if (pager.adapter == null)
            return true

        if (matchArrayList.size != pager.adapter.count)
            return true

        val adapter = pager.adapter as MatchesPagerAdapter
        for (i in matchArrayList.indices) {
            val match = matchArrayList[i]

            val match1 = adapter.matchArrayList[i]

            if ((match.match_room == match1.match_room) &&
                    (match.match_started == match1.match_started) &&
                    (match.team1 == match1.team1) && (match.team2 == match1.team2)) {
                Lg.i("MatchRate", "Every key is same")
            } else {
                Lg.i("MatchRate", "Key is different.")
                return true
            }
        }
        return false

    }

    private var mAlertDialog: AlertDialog? = null

    fun showAlertDialog(mContext: Context, text: String?) {
        if (mAlertDialog == null) {
            mAlertDialog = AlertDialog.Builder(mContext).setMessage(text)
                    .setTitle(mContext.getString(R.string.app_name))
                    .setIcon(R.drawable.ic_launcher)
                    .setCancelable(false)
                    .setPositiveButton(mContext.getString(R.string.ok)) { _, _ -> mAlertDialog?.dismiss() }.create()

            val button = mAlertDialog?.getButton(DialogInterface.BUTTON_POSITIVE)
            button?.setTextColor(mContext.resources.getColor(R.color.color_accent))
        }

        if (!mAlertDialog?.isShowing!!) {
            mAlertDialog?.show()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

}