
package com.app.cricketapp.model.pollresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollOption {

    @SerializedName("option_id")
    @Expose
    private long optionId;
    @SerializedName("option_text")
    @Expose
    private String optionText;
    @SerializedName("option_votes")
    @Expose
    private long optionVotes;

    public long getOptionId() {
        return optionId;
    }

    public void setOptionId(long optionId) {
        this.optionId = optionId;
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    public long getOptionVotes() {
        return optionVotes;
    }

    public void setOptionVotes(long optionVotes) {
        this.optionVotes = optionVotes;
    }

}
