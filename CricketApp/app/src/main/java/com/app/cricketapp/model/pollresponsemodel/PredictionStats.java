
package com.app.cricketapp.model.pollresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PredictionStats {

    @SerializedName("total_polls")
    @Expose
    private long totalPolls;
    @SerializedName("correct_count")
    @Expose
    private long correctCount;
    @SerializedName("incorrect_count")
    @Expose
    private long incorrectCount;

    public long getTotalPolls() {
        return totalPolls;
    }

    public void setTotalPolls(long totalPolls) {
        this.totalPolls = totalPolls;
    }

    public long getCorrectCount() {
        return correctCount;
    }

    public void setCorrectCount(long correctCount) {
        this.correctCount = correctCount;
    }

    public long getIncorrectCount() {
        return incorrectCount;
    }

    public void setIncorrectCount(long incorrectCount) {
        this.incorrectCount = incorrectCount;
    }

}
