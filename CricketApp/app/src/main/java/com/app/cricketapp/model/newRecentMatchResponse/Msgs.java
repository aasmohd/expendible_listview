
package com.app.cricketapp.model.newRecentMatchResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Msgs {

    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("completed")
    @Expose
    private String completed;
    @SerializedName("others")
    @Expose
    private List<Object> others = null;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public List<Object> getOthers() {
        return others;
    }

    public void setOthers(List<Object> others) {
        this.others = others;
    }

}
