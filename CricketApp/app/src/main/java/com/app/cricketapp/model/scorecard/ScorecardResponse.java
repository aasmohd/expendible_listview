
package com.app.cricketapp.model.scorecard;

import com.app.cricketapp.db.dbhelper.DBClass;
import com.app.cricketapp.model.upcomingmatchresponse.Match;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ScorecardResponse implements DBClass {

    public int status;
    @SerializedName("response")
    @Expose
    public Result result;

    public String key;

    public static class Result {
        public ArrayList<ScoreCard> scorecard;
        @SerializedName("match_summary")
        @Expose
        public Match match_summary;

        public Match getMatch_summary() {
            return match_summary;
        }

        public void setMatch_summary(Match match_summary) {
            this.match_summary = match_summary;
        }
    }

    public static class ScoreCard implements Serializable {
        @SerializedName("inning_info")
        @Expose
        public InningInfo inningInfo;
        @SerializedName("teams")
        @Expose
        public Teams teams;
    }


}
