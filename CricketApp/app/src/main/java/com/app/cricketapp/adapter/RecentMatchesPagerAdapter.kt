package com.app.cricketapp.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.activity.MatchDetailActivity
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.crashlytics.android.Crashlytics
import java.util.*

class RecentMatchesPagerAdapter(var mContext: Context, var matches: ArrayList<Match>, var localDate: String) : PagerAdapter() {


    override fun getCount(): Int {
        return matches.size
    }

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val itemView = LayoutInflater.from(mContext).inflate(R.layout.row_recent_matches, container, false)

        val dateTv: TextView = itemView.findViewById(R.id.date_tv)


        val mTeamName1Tv: TextView = itemView.findViewById(R.id.team_country)
        val mTeamName2Tv: TextView = itemView.findViewById(R.id.team_2_country)

        val team1Score1Tv: TextView = itemView.findViewById(R.id.team1_score)
        val team1Score2Tv: TextView = itemView.findViewById(R.id.team1_score2)

        val team2Score1Tv: TextView = itemView.findViewById(R.id.team2_score)
        val team2Score2Tv: TextView = itemView.findViewById(R.id.team2_score2)

        val mTeamName1Iv: ImageView = itemView.findViewById(R.id.match_1_iv)
        val mTeamName2Iv: ImageView = itemView.findViewById(R.id.match_2_iv)

        val mWonMatchTv: TextView = itemView.findViewById(R.id.won_match_tv)
        val matchType: ImageView = itemView.findViewById(R.id.match_type)

        val match = matches[position]


        if (match.others.format == "one-day") {
            matchType.setImageResource(R.drawable.odi_ribbon)
        } else if (match.others.format == "test") {
            matchType.setImageResource(R.drawable.test_ribbon)
        } else {
            matchType.setImageResource(R.drawable.t20_ribbon)
        }

        dateTv.text = localDate
        mTeamName1Tv.text = match.teams.a?.key
        mTeamName2Tv.text = match.teams.b?.key
        Utility.setImageWithUrl(match.teams.a?.logo, R.drawable.default_image, mTeamName1Iv)

        Utility.setImageWithUrl(match.teams.b?.logo, R.drawable.default_image, mTeamName2Iv)

        mWonMatchTv.text = match.msgs.info

        team1Score2Tv.visibility = View.GONE
        team2Score2Tv.visibility = View.GONE
        //Set Team 1 score
        run {
            val a = match.results.innings.a
            team1Score1Tv.text = "${a.runs}/${a.wickets}"
        }
        run {
            if (match.results.other_innings != null) {
                try {
                    val a = match.results.other_innings.a
                    team1Score2Tv.text = "& ${a.runs}/${a.wickets}"
                    team1Score2Tv.visibility = View.VISIBLE
                } catch (e: Exception) {
                    Crashlytics.getInstance().core.log("")
                    Lg.e("Error", e.message)
                }
            }
        }

        //Set Team 2 score
        run {
            val b = match.results.innings.b
            team2Score1Tv.text = "${b.runs}/${b.wickets}"
        }

        run {
            if (match.results.other_innings != null) {
                try {
                    val b = match.results.other_innings.b
                    team2Score2Tv.text = "& ${b.runs}/${b.wickets}"
                    team2Score2Tv.visibility = View.VISIBLE
                } catch (e: Exception) {
                    Crashlytics.getInstance().core.log("")
                    Lg.e("Error", e.message)
                }
            }
        }
        if (!match.others.format.equals("test")) {
            val a = match.results.innings.a.overs
            team1Score2Tv.text = a + " ovr"
            team1Score2Tv.visibility = View.VISIBLE

            val b = match.results.innings.b.overs
            team2Score2Tv.text = b + " ovr"
            team2Score2Tv.visibility = View.VISIBLE
        }

        container?.addView(itemView)

        itemView.setOnClickListener { v ->
            if (match.results.score_card_available == "1") {
                val intent: Intent = Intent(v.context, MatchDetailActivity::class.java)
                intent.putExtra(AppConstants.EXTRA_KEY, match.others?.key)
                v.context.startActivity(intent)
            } else {
                CustomToast.getInstance(v.context as Activity?).showToast(R.string.no_scorecard)
            }
        }
        return itemView

    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as FrameLayout)
    }

    override fun getItemPosition(`object`: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }

}