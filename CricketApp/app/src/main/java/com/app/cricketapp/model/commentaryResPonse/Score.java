package com.app.cricketapp.model.commentaryResPonse;

/**
 * Created by Prashant on 09-01-2018.
 */

public class Score
{
    String team;
    Long runs;
    String overs;
    Long wickets;

    public Long getRuns()
    {
        return runs;
    }

    public void setRuns(Long runs)
    {
        this.runs = runs;
    }

    public String getOvers()
    {
        return overs;
    }

    public void setOvers(String overs)
    {
        this.overs = overs;
    }

    public Long getWickets()
    {
        return wickets;
    }

    public void setWickets(Long wickets)
    {
        this.wickets = wickets;
    }

    public String getTeam()
    {
        return team;
    }

    public void setTeam(String team)
    {
        this.team = team;
    }
}
