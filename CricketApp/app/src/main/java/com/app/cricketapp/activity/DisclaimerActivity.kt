package com.app.cricketapp.activity

import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import org.json.JSONArray

/**
 * Created by bhavya on 02-06-2017.
 */
class DisclaimerActivity : BaseActivity() {

    override fun initUi() {
        val disclaimerTv: TextView = findViewById(R.id.disclaimer_tv) as TextView
        val okayTv: TextView = findViewById(R.id.okay_tv) as TextView

        val disclaimer = Utility.getStringSharedPreference(getActivity(), AppConstants.PREFS_DISCLAIMER)
        try {
            val jsonArray = JSONArray(disclaimer)
            val stringBuilder = StringBuilder()
            (0..jsonArray.length() - 1)
                    .map { jsonArray[it] }
                    .forEach { stringBuilder.append(it).append(" ") }
            disclaimerTv.text = stringBuilder.trim()
        }
        catch (e : Exception){
            e.printStackTrace()
            disclaimerTv.text = " No information available at this time"
        }


        okayTv.setOnClickListener { finish() }
    }

    override fun getLayoutById(): Int {
        return R.layout.activity_disclaimer
    }
}