
package com.app.cricketapp.model.ranking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OdiRanking {

    @SerializedName("odiTeamRanking")
    @Expose
    public OdiTeamRanking odiTeamRanking;
    @SerializedName("odiBatsmenRanking")
    @Expose
    public OdiBatsmenRanking odiBatsmenRanking;
    @SerializedName("odiBowlerRanking")
    @Expose
    public OdiBowlerRanking odiBowlerRanking;
    @SerializedName("odiAllRounderRanking")
    @Expose
    public OdiAllRounderRanking odiAllRounderRanking;

    public OdiTeamRanking getOdiTeamRanking() {
        return odiTeamRanking;
    }

    public void setOdiTeamRanking(OdiTeamRanking odiTeamRanking) {
        this.odiTeamRanking = odiTeamRanking;
    }

    public OdiBatsmenRanking getOdiBatsmenRanking() {
        return odiBatsmenRanking;
    }

    public void setOdiBatsmenRanking(OdiBatsmenRanking odiBatsmenRanking) {
        this.odiBatsmenRanking = odiBatsmenRanking;
    }

    public OdiBowlerRanking getOdiBowlerRanking() {
        return odiBowlerRanking;
    }

    public void setOdiBowlerRanking(OdiBowlerRanking odiBowlerRanking) {
        this.odiBowlerRanking = odiBowlerRanking;
    }

    public OdiAllRounderRanking getOdiAllRounderRanking() {
        return odiAllRounderRanking;
    }

    public void setOdiAllRounderRanking(OdiAllRounderRanking odiAllRounderRanking) {
        this.odiAllRounderRanking = odiAllRounderRanking;
    }

}
