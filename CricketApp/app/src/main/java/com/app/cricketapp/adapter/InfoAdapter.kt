package com.app.cricketapp.adapter

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility

/**
 * Created by Prashant on 11-08-2017.
 */

class InfoAdapter(var matchBean: Match?)
    : RecyclerView.Adapter<InfoAdapter.MyInfoHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyInfoHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_info_row, parent, false)
        return MyInfoHolder(view)
    }

    override fun onBindViewHolder(holder: MyInfoHolder, position: Int) {

        if (position.rem(2) == 0) {
            holder.itemView.setBackgroundColor(Color.WHITE)
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#F7F7F7"))
        }

        when (position) {
            0 -> {
                holder.infoType.text = "Season"
                holder.infoValue.text = matchBean?.season?.name
            }
            1 -> {
                holder.infoType.text = "Match"
                holder.infoValue.text = matchBean?.names?.title
            }
            2 -> {
                holder.infoType.text = "Venue"
                holder.infoValue.text = matchBean?.basicsInfo?.venue
            }
            3 -> {
                holder.infoType.text = "Date"

                val localDate = Utility.getLocalDate2(matchBean?.basicsInfo?.startDate?.toLong()!!
                        * 1000, AppConstants.SERVER_DATE_FORMAT2)
                holder.infoValue.text = localDate
            }
            4 -> {
                holder.infoType.text = "Toss"
                holder.infoValue.text = matchBean?.results?.toss?.str
            }
            5 -> {
                holder.infoType.text = "Status"
                if (TextUtils.isEmpty(matchBean?.msgs?.info)) {
                    holder.infoValue.text = "Not Available"
                } else
                    holder.infoValue.text = matchBean?.msgs?.info
            }
        }
    }

    override fun getItemCount(): Int {
        return 6
    }

    inner class MyInfoHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var infoType: TextView = itemView.findViewById<TextView>(R.id.info_type)
        internal var infoValue: TextView = itemView.findViewById<TextView>(R.id.info_value)

    }
}
