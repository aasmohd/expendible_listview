package com.app.cricketapp.model.matchresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rahulgupta on 03/08/17.
 */

public class Config implements IMatch {

    @SerializedName("matchStartTime")
    @Expose
    public String matchStartTime;
    @SerializedName("matchStatus")
    @Expose
    public String matchStatus;
    @SerializedName("matchFormat")
    @Expose
    public String matchFormat;
    @SerializedName("landingText")
    @Expose
    public String landingText;

}
