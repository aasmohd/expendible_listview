package com.app.cricketapp.activity

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.View
import com.app.cricketapp.R
import com.app.cricketapp.adapter.ScoreCardPagerAdapter
import com.app.cricketapp.databinding.ActivityMatchDetailBinding
import com.app.cricketapp.fragment.CommentaryFragment
import com.app.cricketapp.fragment.InfoFragment
import com.app.cricketapp.fragment.LineupFragment
import com.app.cricketapp.fragment.ScoreCardPagerFragment
import com.app.cricketapp.interfaces.FragmentDataCallback
import com.app.cricketapp.model.RequestBean
import com.app.cricketapp.model.scorecard.ScorecardResponse
import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Prashant on 11-08-2017.
 */

class MatchDetailActivity : BaseActivity(), FragmentDataCallback {
    override fun getMyResponse(): ScorecardResponse = scoreCardResponse!!

    override fun getBean(): Match = matchBean!!

    var dataBinding: ActivityMatchDetailBinding? = null
    var matchBean: Match? = null
    var scoreCardResponse: ScorecardResponse? = null
    var match_category:String? = null

    override fun initUi() {
        dataBinding = viewDataBinding as ActivityMatchDetailBinding?

        setBackButtonEnabled()
        toggleAction3Icon(View.GONE)

        callGetScorecard(intent.getStringExtra(AppConstants.EXTRA_KEY))
        match_category = intent.getStringExtra("match_category")

        loadBannerAd()
        initFullScreenAdd()
    }

    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()
                }
            }
            requestNewInterstitial()
        }
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .build()

        mInterstitialAd.loadAd(adRequest)
    }

    override fun getLayoutById(): Int = R.layout.activity_match_detail

    private fun setupViewPager(viewPager: ViewPager, tabLayout: TabLayout) {
        val adapter = ScoreCardPagerAdapter(supportFragmentManager)
        adapter.addFragment(ScoreCardPagerFragment(), "SCORECARD")
        var commentryfrag = CommentaryFragment()
        var bundle = Bundle()
        bundle.putString("category",match_category)
        commentryfrag.arguments = bundle
        adapter.addFragment(commentryfrag,"COMMENTARY")
        adapter.addFragment(InfoFragment(), "INFO")
        adapter.addFragment(LineupFragment(), "SQUADS")

        viewPager.adapter = adapter
        viewPager.setCurrentItem(1)
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> {
                    }
                    1 -> {
                    }
                    2 -> {
                    }
                    3->{
                    }

                }
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        tabLayout.setupWithViewPager(viewPager)


    }

    private fun loadBannerAd() {
        val adRequest: AdRequest = AdRequest.Builder().build()
        dataBinding?.adView?.loadAd(adRequest)
    }

    private fun callGetScorecard(key: String?) {
        showProgressBar(true)

        val apiService = AppRetrofit.getInstance().apiServices
        val request = RequestBean()
        request.key = key
        val call = apiService.scorecard(request)
        call.enqueue(object : Callback<ScorecardResponse> {

            override fun onResponse(call: Call<ScorecardResponse>?, response: Response<ScorecardResponse>?) {
                try {
                    showProgressBar(false)

                    scoreCardResponse = response?.body()
                    scoreCardResponse?.key = intent.getStringExtra(AppConstants.EXTRA_KEY)
                    matchBean = scoreCardResponse?.result?.match_summary
                    mTitleTv?.text = matchBean?.names?.name

                    val tabLayout = dataBinding?.tabLayout
                    val viewPager = dataBinding?.viewPager

                    if (matchBean != null)
                        setupViewPager(viewPager!!, tabLayout!!)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ScorecardResponse>?, t: Throwable?) {
                showProgressBar(false)
            }

        })
    }
}
