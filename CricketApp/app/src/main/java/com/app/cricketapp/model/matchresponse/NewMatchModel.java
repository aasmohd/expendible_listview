package com.app.cricketapp.model.matchresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rahulgupta on 04/08/17.
 */

public class NewMatchModel {
    public Inning inning1, inning2, inning3, inning4;
    public Session session;
    public Market market;
    public Lambi lambi;
    public Team firstTeam, secondTeam;
    public CurrentStatus currentStatus;
    public Config config;

    @SerializedName("previousBall")
    @Expose
    public String previousBall;
    @SerializedName("currentInning")
    @Expose
    public String currentInning;
    @SerializedName("ballsRemaining")
    @Expose
    public String ballsRemaining;
    @SerializedName("androidAdFlag")
    @Expose
    public String androidAdFlag;
    @SerializedName("iosAdFlag")
    @Expose
    public String iosAdFlag;
    @SerializedName("localDescription")
    @Expose
    public String localDescription;
    @SerializedName("landingText")
    @Expose
    public String landingText;
    @SerializedName("batsmanOneStatus")
    @Expose
    public String batsmanOneStatus;
    @SerializedName("batsmanTwoStatus")
    @Expose
    public String batsmanTwoStatus;
    @SerializedName("favouriteTeam")
    @Expose
    public String favouriteTeam;
    @SerializedName("lambiShow")
    @Expose
    public String lambiShow;
    @SerializedName("sessionSuspend")
    @Expose
    public String sessionSuspend;
    @SerializedName("bowlerStatus")
    @Expose
    public String bowlerStatus;
    @SerializedName("matchDay")
    @Expose
    public String matchDay;

    @SerializedName("showRunningMsg")
    @Expose
    public String showRunningMsg;

    @SerializedName("liveMatchKey")
    @Expose
    public String liveMatchKey;

    @SerializedName("liveMatchUrl")
    @Expose
    public String liveMatchUrl;
}
