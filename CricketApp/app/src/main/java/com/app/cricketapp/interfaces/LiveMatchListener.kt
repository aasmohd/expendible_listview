package com.app.cricketapp.interfaces

import com.app.cricketapp.model.matchresponse.MatchRoom

/**
 * Created by Prashant on 03-10-2017.
 */
interface LiveMatchListener {

    fun matchList(matchRoomList: java.util.ArrayList<MatchRoom>)
    fun runningMsgData(runningMsg:String)
    fun isMaintenance(maintenanceKey:String)

}