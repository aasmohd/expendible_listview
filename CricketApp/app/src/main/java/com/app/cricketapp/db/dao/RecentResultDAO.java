package com.app.cricketapp.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.app.cricketapp.db.CLGCPHelper;
import com.app.cricketapp.db.DBUtil;
import com.app.cricketapp.model.newRecentMatchResponse.NewRecentMatchResponse;

import java.util.List;

/**
 * Created by Prashant on 26-09-2017.
 */

public class RecentResultDAO {
    private static Context mContext;

    public static void init(Context context) {
        mContext = context;
    }

    public static int insert(NewRecentMatchResponse brand) {
        ContentValues values;
        values = DBUtil.getContentValues(brand);
        Uri uri = mContext.getContentResolver().insert(CLGCPHelper.Uris.URI_RECENT_MATCH_TABLE, values);
        int localIndexId = 0;
        if (uri != null) {
            localIndexId = Integer.parseInt(uri.getLastPathSegment());
        }
        return localIndexId;
    }

    public static void update(NewRecentMatchResponse brand) {
        ContentValues values;
        values = DBUtil.getContentValues(brand);
        mContext.getContentResolver().update(CLGCPHelper.Uris.URI_RECENT_MATCH_TABLE, values, null, null);
    }

    public static Loader<Cursor> getCursorLoader(String id) {
        return new CursorLoader(mContext, CLGCPHelper.Uris.URI_RECENT_MATCH_TABLE, null, "catId = '" + id + "'", null, null);
    }

    public static void delete() {
        mContext.getContentResolver().delete(CLGCPHelper.Uris.URI_RECENT_MATCH_TABLE, null, null);
    }


    public static NewRecentMatchResponse getRecentMatches() {
        Cursor cursor = mContext.getContentResolver().query(CLGCPHelper.Uris.URI_RECENT_MATCH_TABLE, null, null, null, null);
        NewRecentMatchResponse newRecentMatchResponse = new NewRecentMatchResponse();
        if (cursor != null && cursor.moveToNext()) {
            newRecentMatchResponse = DBUtil.getFromContentValue(cursor, NewRecentMatchResponse.class);
        }
        return newRecentMatchResponse;
    }

    public static void bulkInsert(List<NewRecentMatchResponse> appModelList) {
        ContentValues[] contentValues = DBUtil.getContentValuesList(appModelList);
        mContext.getContentResolver().bulkInsert(CLGCPHelper.Uris.URI_RECENT_MATCH_TABLE, contentValues);
    }
}
