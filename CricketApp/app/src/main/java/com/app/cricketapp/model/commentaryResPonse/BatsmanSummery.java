
package com.app.cricketapp.model.commentaryResPonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BatsmanSummery
{

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("batting")
    @Expose
    private Batting batting;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Batting getBatting() {
        return batting;
    }

    public void setBatting(Batting batting) {
        this.batting = batting;
    }

}
