package com.app.cricketapp.fcm

import android.app.ActivityManager
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.widget.RemoteViews
import com.app.cricketapp.R
import com.app.cricketapp.activity.HomeActivity
import com.app.cricketapp.activity.NewMatchRateActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*



/**
 * Created by rahulgupta on 16/03/17.
 * Firebase messaging service to handle incoming notifications
 */

public class MyFireBaseMessagingService : FirebaseMessagingService() {

    val TAG = MyFireBaseMessagingService::class.java.simpleName
    var notificationManager: NotificationManager? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        if (notificationManager == null)
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

        try {
            val body = remoteMessage?.notification?.body
            val title = getString(R.string.app_name)
            val type = remoteMessage?.data?.get("type")


            val am = application.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val activePackages = getActivePackages(am)
            val cn = am.getRunningTasks(1).get(0).topActivity
            if (!activePackages.get(0).equals("com.app.cricketapp")){
                if (type == "2") {
                    if (cn.className.toString().equals("com.app.cricketapp.activity.NewMatchRateActivity")) {

                    }else{
                        val intent = Intent(this, NewMatchRateActivity::class.java)
                        createNotificationIntent(intent, title, body)
                    }
                } else {
                    val intent = Intent(this, HomeActivity::class.java)
                    createNotificationIntent(intent, title, body)
                }
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    /**
     * Method to create notification intent
     *
     * @param notificationIntent Notification intent
     * @param title              Title of notification
     * @param message            Message
     */
    fun createNotificationIntent(notificationIntent: Intent, title: String, message: String?) {

        val contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val mBuilder = NotificationCompat.Builder(this)
                .setPriority(NotificationCompat.PRIORITY_MAX) //must give priority to High, Max which will considered as heads-up notification
                .setSmallIcon(R.drawable.icon)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_launcher))
                .setContentTitle(title)
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setStyle(NotificationCompat.BigTextStyle().bigText(message))

        mBuilder.setContentText(message)
        mBuilder.setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_VIBRATE)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.color = resources.getColor(R.color.colorPrimary)
        }

        notificationManager?.notify(Random().nextInt(), mBuilder.build())
    }

    fun CustomNotification(notificationIntent: Intent, title: String, message: String?) {
        // Using RemoteViews to bind custom layouts into Notification
        val remoteViews = RemoteViews(packageName,
                R.layout.custom_notification)

        val pIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(this)
                .setPriority(NotificationCompat.PRIORITY_MAX) //must give priority to High, Max which will considered as heads-up notification
                .setSmallIcon(R.drawable.icon)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_launcher))
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .setContent(remoteViews)

        // Locate and set the Image into customnotificationtext.xml ImageViews
        remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_launcher)

        // Locate and set the Text into customnotificationtext.xml TextViews
        remoteViews.setTextViewText(R.id.title, "IND")
        remoteViews.setTextViewText(R.id.text, "200 / 12")

        builder.setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_VIBRATE)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.color = resources.getColor(R.color.colorPrimary)
        }

        notificationManager?.notify(Random().nextInt(), builder.build())

    }

    private fun getActivePackages(mActivityManager: ActivityManager): Array<String> {
        val activePackages = HashSet<String>()
        val processInfos = mActivityManager.runningAppProcesses
        for (processInfo in processInfos) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                activePackages.addAll(processInfo.pkgList)
            }
        }
        return activePackages.toTypedArray()
    }


}
