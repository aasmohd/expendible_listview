
package com.app.cricketapp.model.newRecentMatchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Results {

    @SerializedName("winner_team")
    @Expose
    private String winnerTeam;
    @SerializedName("toss")
    @Expose
    private Toss toss;
    @SerializedName("score_card_available")
    @Expose
    private long scoreCardAvailable;
    @SerializedName("innings")
    @Expose
    private Innings innings;

    public String getWinnerTeam() {
        return winnerTeam;
    }

    public void setWinnerTeam(String winnerTeam) {
        this.winnerTeam = winnerTeam;
    }

    public Toss getToss() {
        return toss;
    }

    public void setToss(Toss toss) {
        this.toss = toss;
    }

    public long getScoreCardAvailable() {
        return scoreCardAvailable;
    }

    public void setScoreCardAvailable(long scoreCardAvailable) {
        this.scoreCardAvailable = scoreCardAvailable;
    }

    public Innings getInnings() {
        return innings;
    }

    public void setInnings(Innings innings) {
        this.innings = innings;
    }

}
