package com.app.cricketapp.adapter

import android.content.Context
import android.content.Intent
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.activity.NewsDetailActivity
import com.app.cricketapp.model.latestnewsresponse.List
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.*
import java.util.*

/**
 * Created by bhavya on 12-04-2017.
 */
class LatestNewsAdapter(var latestNewsList: ArrayList<List>) :
        RecyclerView.Adapter<LatestNewsAdapter.MyViewHolder>(), ViewPager.OnPageChangeListener {
    var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        context = parent?.context
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.row_latest_news
                , parent, false)
        return MyViewHolder(view)

    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        val list = latestNewsList[position]
        if(list.pubDate!= null){
/*            val utcTimeFormat = Utility.getUtcDate(list.pubDate!!.toLong() * 1000, AppConstants.DATE_FORMAT1)
            val timeValue = Utility.getTimeAgo(utcTimeFormat, context, AppConstants.DATE_FORMAT1)

            holder?.mDateTv?.text = timeValue*/
        }

        holder?.mTitleTv?.text = list.title

        Utility.setImageWithUrl(list.thumburl, R.drawable.news_placeholder, holder?.mNewsIv)
        holder?.mMainContainer?.setOnClickListener {
            val intent = Intent(context, NewsDetailActivity::class.java)
            intent.putExtra(AppConstants.LATEST_NEWS_INTENT, list)
            context?.startActivity(intent)
        }

        if (list.showAd) {
            holder?.adsContainer?.visibility = View.VISIBLE
            // Set its video options.
            holder?.mAdView?.videoOptions = VideoOptions.Builder()
                    .setStartMuted(true)
                    .build()

            // The VideoController can be used to get lifecycle events and info about an ad's video
            // asset. One will always be returned by getVideoController, even if the ad has no video
            // asset.
            val mVideoController = holder?.mAdView?.videoController
            mVideoController?.videoLifecycleCallbacks = object : VideoController.VideoLifecycleCallbacks() {
                override fun onVideoEnd() {
                    Log.d("Video", "Video playback is finished.")
                    super.onVideoEnd()
                }
            }

            // Set an AdListener for the AdView, so the Activity can take action when an ad has finished
            // loading.
            holder?.mAdView?.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    if (mVideoController?.hasVideoContent()!!) {
                        Log.d("Video", "Received an ad that contains a video asset.")
                    } else {
                        Log.d("Video", "Received an ad that does not contain a video asset.")
                    }
                }
            }

            holder?.mAdView?.loadAd(AdRequest.Builder()
                    .addTestDevice("C11191FE7944B40640463F2686610772").build())
        } else {
            holder?.adsContainer?.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return latestNewsList.size

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {


    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageSelected(position: Int) {

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var adsContainer: RelativeLayout = itemView.findViewById(R.id.ads_container)
        var mAdView: NativeExpressAdView = itemView.findViewById(R.id.adView)

        var mDateTv: TextView? = itemView.findViewById(R.id.date_tv)
        var mTitleTv: TextView? = itemView.findViewById(R.id.news_title_tv)
        var mNewsIv: ImageView? = itemView.findViewById(R.id.latest_news_iv)
        var mMainContainer: LinearLayout? = itemView.findViewById(R.id.main_container)

    }
}
