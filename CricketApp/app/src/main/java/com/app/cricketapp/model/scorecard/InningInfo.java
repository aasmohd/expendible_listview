
package com.app.cricketapp.model.scorecard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InningInfo implements Serializable {

    @SerializedName("inning_number")
    @Expose
    public Long inningNumber;


    @SerializedName("inning_started_by")
    @Expose
    public String inningStartedBy;


}
