package com.app.cricketapp.fragment

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.View
import com.app.cricketapp.BuildConfig
import com.app.cricketapp.R
import com.app.cricketapp.activity.DisclaimerActivity
import com.app.cricketapp.activity.HomeActivity
import com.app.cricketapp.activity.LatestNewsActivity
import com.app.cricketapp.activity.RankingActivity
import com.app.cricketapp.databinding.FragmentNavigationBinding
import com.app.cricketapp.interfaces.NavDrawerEnum
import com.app.cricketapp.model.save_user.SaveUserResponse
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.fragment_navigation.*

/**
 * Created by Sunaina on 12-08-2017.
 */
class NavigationFragment : BaseFragment(), View.OnClickListener {


    private var binding: FragmentNavigationBinding? = null
    private var navDrawerEnum: NavDrawerEnum? = null
    private var drawerLayout: DrawerLayout? = null
    private var response: SaveUserResponse? = null
    private var mDrawerToggle: ActionBarDrawerToggle? = null


    override fun initUi() {
        binding = viewDataBinding as FragmentNavigationBinding
        initObjects()
    }

    private fun initObjects() {
        binding?.tvDisclaimer?.setOnClickListener(this)
        binding?.tvSeasons?.setOnClickListener(this)
        binding?.tvRankings?.setOnClickListener(this)
        binding?.tvRateUs?.setOnClickListener(this)
        binding?.tvFeedback?.setOnClickListener(this)
        binding?.tvShareApp?.setOnClickListener(this)
        binding?.tvPlayerStats?.setOnClickListener(this)
        binding?.tvCountries?.setOnClickListener(this)
        binding?.tvCountries?.setOnClickListener(this)
        update.setOnClickListener(this)
        version.text = "Version ${BuildConfig.VERSION_NAME}"
        val isMaleSpeechKey = Utility.getBooleanSharedPreference(context, AppConstants.IS_MALE_SPEECH_KEY)
        if (isMaleSpeechKey)
            binding?.radioButtonHindi?.isChecked = true
        else
            binding?.radioButtonEnglish?.isChecked = true

        binding?.radioGroupLang?.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == binding?.radioButtonHindi?.id) {
                Utility.putBooleanValueInSharedPreference(context, AppConstants.IS_MALE_SPEECH_KEY, true)
            } else {
                Utility.putBooleanValueInSharedPreference(context, AppConstants.IS_MALE_SPEECH_KEY, false)
            }
        }

        val notification_prefs = Utility.getStringSharedPreference(context, AppConstants.NOTIFICATION_PREFS)
        if (notification_prefs.equals("on"))
            binding?.radioButtonOn?.isChecked = true
        else
            binding?.radioButtonOf?.isChecked = true

        binding?.radioGroupNotification?.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == binding?.radioButtonOn?.id) {
                FirebaseMessaging.getInstance().subscribeToTopic(AppConstants.TOPIC_MATCH)
                FirebaseMessaging.getInstance().subscribeToTopic(AppConstants.TOPIC_ANDROID)
                Utility.putStringValueInSharedPreference(context, AppConstants.NOTIFICATION_PREFS, "on")
            } else {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(AppConstants.TOPIC_MATCH)
                FirebaseMessaging.getInstance().unsubscribeFromTopic(AppConstants.TOPIC_ANDROID)
                Utility.putStringValueInSharedPreference(context, AppConstants.NOTIFICATION_PREFS, "off")
            }
        }
    }

    override fun getLayoutById(): Int {
        return R.layout.fragment_navigation
    }

    fun setUp(fragmentId: Int, drawerLayout: DrawerLayout,
              mToolbar: Toolbar?) {
        this.drawerLayout = drawerLayout
        mDrawerToggle = object : ActionBarDrawerToggle(activity, drawerLayout,
                null, R.string.drawer_open,
                R.string.drawer_open) {
            override fun onDrawerClosed(drawerView: View?) {
                super.onDrawerClosed(drawerView)
                if (!isAdded) {
                    return
                }
                Utility.hideKeyboard(context)

                when (navDrawerEnum) {
                    NavDrawerEnum.DISCLAIMER -> {
                        val intent = Intent(activity, DisclaimerActivity::class.java)
                        startActivity(intent)
                    }
                    NavDrawerEnum.RATE_US -> {
                        openPlayStoreLink()
                    }
                    NavDrawerEnum.FEEDBACK -> {
                        openEmailIntent()
                    }
                    NavDrawerEnum.SHARE -> {
                        openSharingTool()
                    }
                    NavDrawerEnum.RANKING->{
                        val intent = Intent(activity, RankingActivity::class.java)
                        startActivity(intent)
                    }
                    NavDrawerEnum.SEASONS->{
                        val intent = Intent(activity,LatestNewsActivity::class.java)
                        startActivity(intent)
                    }
                }
                navDrawerEnum = null
            }

            override fun onDrawerOpened(drawerView: View?) {
                super.onDrawerOpened(drawerView)
                if (!isAdded) {
                    return
                }
                response = (activity as HomeActivity).response
                try {
                    checkAppVersion()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                Utility.hideKeyboard(context)
            }
        }
        // Defer code dependent on restoration of previous instance state.
        drawerLayout.post { (mDrawerToggle as ActionBarDrawerToggle).syncState() }

        drawerLayout.addDrawerListener(mDrawerToggle as ActionBarDrawerToggle)
    }

    fun checkAppVersion() {
        val appCurrentVersion: Int = BuildConfig.VERSION_CODE
        val versionInfo = response?.result?.versionInfo
        val latestVersion: Double = versionInfo?.latestVersion as Double

        if (appCurrentVersion >= latestVersion) {
            //No show dialog
            update.visibility = View.GONE
        } else {
            BuildConfig.VERSION_NAME
            update.visibility = View.VISIBLE
        }
    }


    override fun onClick(view: View) {

        when (view.id) {
            R.id.update -> {
                navDrawerEnum = NavDrawerEnum.RATE_US
                drawerLayout?.closeDrawers()
            }
            R.id.tv_disclaimer -> {
                navDrawerEnum = NavDrawerEnum.DISCLAIMER
                drawerLayout?.closeDrawers()
            }
            R.id.tv_seasons -> {
                navDrawerEnum = NavDrawerEnum.SEASONS
                drawerLayout?.closeDrawers()
            }
            R.id.tv_rankings -> {
                navDrawerEnum = NavDrawerEnum.RANKING
                drawerLayout?.closeDrawers()
            }
            R.id.tv_rate_us -> {
                navDrawerEnum = NavDrawerEnum.RATE_US
                drawerLayout?.closeDrawers()
            }

            R.id.tv_feedback -> {
                navDrawerEnum = NavDrawerEnum.FEEDBACK
                drawerLayout?.closeDrawers()
            }
            R.id.tv_share_app -> {
                navDrawerEnum = NavDrawerEnum.SHARE
                drawerLayout?.closeDrawers()
            }
            R.id.tv_player_stats -> {
                navDrawerEnum = NavDrawerEnum.PLAYER_STATS
                drawerLayout?.closeDrawers()
            }
            R.id.tv_countries -> {
                navDrawerEnum = NavDrawerEnum.COUNTRIES
                drawerLayout?.closeDrawers()
            }
        }

    }

    private fun openSharingTool() {

        val packageName = context.packageName
        val sharingIntent: Intent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        try {
            sharingIntent.putExtra(Intent.EXTRA_TEXT, response?.result?.invitation?.message)
            startActivity(Intent.createChooser(sharingIntent, "Share using"))

        } catch (e: Exception) {
            e.printStackTrace()
            try {
                val playStoreUrl: String = (Uri.parse(getString(R.string.play_store_base_url) +
                        packageName).toString())
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                        "Hi," + "\n" + getString(R.string.share_test) + "\n\n" + playStoreUrl)
                startActivity(Intent.createChooser(sharingIntent, "Share using"))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    private fun openEmailIntent() {

        try {

            val intent = Intent(Intent.ACTION_SENDTO) // it's not ACTION_SEND
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_SUBJECT, response?.result?.feedback?.subject)
            intent.putExtra(Intent.EXTRA_TEXT, response?.result?.feedback?.message)
            intent.data = Uri.parse("mailto:" + response?.result?.feedback?.email) // or just "mailto:" for blank
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) // this will make such that when user returns to your app, your app is displayed, instead of the email app.
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun openPlayStoreLink() {

        val appPackageName = context.packageName // getPackageName() from Context or Activity object
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)))
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.play_store_base_url) + appPackageName)))
        }

    }

    fun overLay(s:String){
        if (s.equals("1")) {
            binding?.suspendedTv?.visibility = View.GONE
        } else if(!s.equals("0"))
        {
            binding?.suspendedTv?.visibility = View.VISIBLE
        }
    }
}



