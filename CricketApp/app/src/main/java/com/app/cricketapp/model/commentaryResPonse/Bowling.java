
package com.app.cricketapp.model.commentaryResPonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bowling {

    @SerializedName("runs")
    @Expose
    private Long runs;
    @SerializedName("balls")
    @Expose
    private Long balls;
    @SerializedName("wickets")
    @Expose
    private Long wickets;
    @SerializedName("extras")
    @Expose
    private Long extras;
    @SerializedName("overs")
    @Expose
    private String overs;

    public Long getRuns() {
        return runs;
    }

    public void setRuns(Long runs) {
        this.runs = runs;
    }

    public Long getBalls() {
        return balls;
    }

    public void setBalls(Long balls) {
        this.balls = balls;
    }

    public Long getWickets() {
        return wickets;
    }

    public void setWickets(Long wickets) {
        this.wickets = wickets;
    }

    public Long getExtras() {
        return extras;
    }

    public void setExtras(Long extras) {
        this.extras = extras;
    }

    public String getOvers() {
        return overs;
    }

    public void setOvers(String overs) {
        this.overs = overs;
    }

}
