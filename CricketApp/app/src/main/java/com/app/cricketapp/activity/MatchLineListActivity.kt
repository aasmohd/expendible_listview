package com.app.cricketapp.activity

import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import com.app.cricketapp.R
import com.app.cricketapp.adapter.LineListAdapter
import com.app.cricketapp.databinding.ActivityMatchLineListBinding
import com.app.cricketapp.model.matchresponse.MatchRoom
import com.app.cricketapp.utility.AppConstants
import java.util.*

/**
 * Created by Prashant on 03-02-2018.
 */
class MatchLineListActivity: BaseActivity() {
    var binding:ActivityMatchLineListBinding? = null
    var lineListAdapter: LineListAdapter? = null
    var matchArrayList:ArrayList<MatchRoom>? = null

    override fun initUi() {
        binding = viewDataBinding as ActivityMatchLineListBinding?
        startUpdateTimer()
        setBackButtonEnabled()
        matchArrayList = intent.extras.getParcelableArrayList<MatchRoom>(AppConstants.EXTRA_MATCHES_ARRAY)
        lineListAdapter = LineListAdapter(this,matchArrayList!!)
        val manager = LinearLayoutManager(getActivity())
        binding?.recyclerView?.layoutManager = manager
        binding?.recyclerView?.setHasFixedSize(true)
        binding?.recyclerView?.adapter = lineListAdapter
    }

    override fun getLayoutById(): Int {
        return R.layout.activity_match_line_list
    }

    private fun startUpdateTimer() {
        val tmr = Timer()
        tmr.schedule(object : TimerTask() {
            override fun run() {
                mHandler.post(updateRemainingTimeRunnable)
            }
        }, 1000, 1000)
    }

    private val mHandler = Handler()
    private val updateRemainingTimeRunnable = Runnable {
        synchronized(lineListAdapter!!) {
            lineListAdapter?.notifyDataSetChanged()

        }
    }
}