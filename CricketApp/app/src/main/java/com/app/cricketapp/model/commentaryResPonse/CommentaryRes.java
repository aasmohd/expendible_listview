
package com.app.cricketapp.model.commentaryResPonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentaryRes{

    @SerializedName("status")
    @Expose
    private Long status;
    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("time")
    @Expose
    private Long time;



    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

}
