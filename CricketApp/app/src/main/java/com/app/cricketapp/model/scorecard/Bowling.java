
package com.app.cricketapp.model.scorecard;

import com.app.cricketapp.utility.AppConstants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Bowling implements Serializable, IScore {

    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("dots")
    @Expose
    public long dots;
    @SerializedName("runs")
    @Expose
    public long runs;
    @SerializedName("balls")
    @Expose
    public long balls;
    @SerializedName("maiden_overs")
    @Expose
    public long maidenOvers;
    @SerializedName("wickets")
    @Expose
    public long wickets;
    @SerializedName("extras")
    @Expose
    public long extras;
    @SerializedName("overs")
    @Expose
    public String overs;
    @SerializedName("economy")
    @Expose
    public double economy;
    public boolean isBowlingView;

    @Override
    public int getViewType() {
        return AppConstants.BOWLING;
    }
}
