package com.app.cricketapp.activity

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.view.View
import android.widget.Toast
import com.app.cricketapp.BuildConfig
import com.app.cricketapp.R
import com.app.cricketapp.adapter.HomeMatchAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.databinding.ActivityHomeNewBinding
import com.app.cricketapp.fragment.NavigationFragment
import com.app.cricketapp.interfaces.LiveMatchListener
import com.app.cricketapp.interfaces.OkCancelCallback
import com.app.cricketapp.model.eve.RefreshLineScreen
import com.app.cricketapp.model.matches.MatchesResponse
import com.app.cricketapp.model.matchresponse.MatchRoom
import com.app.cricketapp.model.save_user.SaveUserResponse
import com.app.cricketapp.service.SaveUserService
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.FireBaseController
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.activity_home_new.*
import kotlinx.android.synthetic.main.layout_home_header.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Call
import java.util.*


/**
 * Created by bhavya on 12-04-2017.
 */

class HomeActivity : BaseActivity(), LiveMatchListener {
    override fun isMaintenance(maintenanceKey: String) {
        if (maintenanceKey.equals("1")) {
            dataBinding?.suspendedTv?.visibility = View.GONE
            mNavigationHomeFragment?.overLay(maintenanceKey)
            getMatchesFB()
            canCallService = true
        } else if (!maintenanceKey.equals("0")) {
            dataBinding?.pager?.visibility = View.INVISIBLE
            dataBinding?.indicator?.visibility = View.GONE
            dataBinding?.stayTuned?.visibility = View.VISIBLE
            dataBinding?.stayTuned?.text = maintenanceKey
            dataBinding?.suspendedTv?.visibility = View.VISIBLE
            canCallService = false
            mNavigationHomeFragment?.overLay(maintenanceKey)

        }
    }

    override fun runningMsgData(runningMsg: String) {
        massage = runningMsg
        Utility.putStringValueInSharedPreference(this@HomeActivity, AppConstants.RUNNING_MSG, massage)
    }

    override fun matchList(matchRoomList: ArrayList<MatchRoom>) {
        this.matchRoomList = matchRoomList
        canCallService = true
        adapter?.notifyDataSetChanged()

    }

    var matchRoomList: java.util.ArrayList<MatchRoom>? = null

    var massage = ""

    var dataBinding: ActivityHomeNewBinding? = null
    var mAdView: AdView? = null


    var mNavigationHomeFragment: NavigationFragment? = null

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
        fireBaseController?.destroy()
    }

    override fun onStop() {
        super.onStop()

    }

    override fun onStart() {
        super.onStart()

    }


    override fun initUi() {
        EventBus.getDefault().register(this)
        fireBaseController?.setLiveMatchListener(this)
        fireBaseController?.initMatchRoom()
        stay_tuned.visibility = View.GONE
        mAdView = findViewById(R.id.adView) as AdView?
        setToolbarColor(resources.getColor(R.color.colorPrimaryDark))
        dataBinding = super.viewDataBinding as ActivityHomeNewBinding?

        dataBinding?.upcomingMatchTv?.setOnClickListener(this)
        dataBinding?.matchRateTv?.setOnClickListener(this)
        dataBinding?.liveScoreTv?.setOnClickListener(this)
        dataBinding?.recentMatchTv?.setOnClickListener(this)
        dataBinding?.latestNewsTv?.setOnClickListener(this)
        dataBinding?.pollTv?.setOnClickListener(this)

        showProgressBar(true)
//        code  for drawer layout
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer_iv.setOnClickListener {
            drawer.openDrawer(GravityCompat.START)
        }

        mNavigationHomeFragment = supportFragmentManager
                .findFragmentById(R.id.navigation_drawer_left) as NavigationFragment
        mNavigationHomeFragment?.setUp(R.id.navigation_drawer_left,
                drawer, toolbar)

        toggleAction3Icon(View.VISIBLE)

        MobileAds.initialize(applicationContext, getString(R.string.banner_ad_unit_id))

        // Set the dimensions of the sign-in button.

        loadBannerAd()
        checkAction()



        if (Utility.isNetworkAvailable(this)) {

            startSaveUserService()
        } else {
            Toast.makeText(this, R.string.check_internet_connection, Toast.LENGTH_LONG).show()
        }

//        canCallService = true

        fireBaseController?.initMatchUpdateKey {
            setHandler()
        }

        fireBaseController?.getRunningMsg()

    }

    var canCallService = false
    val handler = Handler()
    private var runnable: Runnable = Runnable {
        if (call != null)
            call?.cancel()
        if (canCallService) {
            getMatchesFB()
        } else {
            setHandler()
//            showProgressBar(false)
        }
    }

    fun setHandler() {
        if (Utility.isNetworkAvailable(this)) {

            handler.removeCallbacks(runnable)
            handler.postDelayed(runnable, 100)
        } else {
            Toast.makeText(this, R.string.check_internet_connection, Toast.LENGTH_LONG).show()
        }

    }

    var fireBaseController = FireBaseController.getInstance(getActivity())

    private fun startSaveUserService() {
        val intent = Intent(this, SaveUserService::class.java)
        startService(intent)
    }

    var response: SaveUserResponse? = null

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSaveUserEvent(response: SaveUserResponse?) {
        this.response = response
        checkAppVersion()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun isPagerLoaded(b: Boolean?) {
        showProgressBar(false)
        canCallService = true
    }


    /**
     * Check Action
     */
    fun checkAction() {
        try {
            if (intent.action == "matchscreen") {
                val notificationIntent: Intent = Intent(this, NewMatchRateActivity::class.java)
                startActivity(notificationIntent)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun checkAppVersion() {
        val appCurrentVersion: Int = BuildConfig.VERSION_CODE
        val versionInfo = response?.result?.versionInfo
        val latestVersion: Double = versionInfo?.latestVersion as Double
        val forceUpdate: Int = versionInfo.forceUpdate as Int

        if (forceUpdate != 0) {
            Utility.showAlertDialogWithOkButton(getActivity(),
                    getString(R.string.new_version_available), object : OkCancelCallback {

                override fun onOkClicked() {
                    onCancelClicked()
                }

                override fun onCancelClicked() {
                    openPlayStoreLink()
                    finish()
                }

            }, getString(R.string.update))
        } else {
            val isShown = Utility.getBooleanSharedPreference(getActivity(), "prefs_version_" + latestVersion)
            if (versionInfo.updateAvailable == 0) {

            } else {
                Utility.putBooleanValueInSharedPreference(getActivity(), "prefs_version_" + latestVersion, true)
                Utility.showAlertDialogWithOkCancelButton(getActivity(),
                        getString(R.string.new_version_available), object : OkCancelCallback {

                    override fun onOkClicked() {
                        openPlayStoreLink()
                    }

                    override fun onCancelClicked() {
                    }

                }, getString(R.string.update), getString(R.string.cancel))
            }
        }

    }


    fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        mAdView?.loadAd(adRequest)
    }

    override fun getLayoutById(): Int {
        return R.layout.activity_home_new

    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.upcoming_match_tv ->
                startActivity(Intent(this, UpcomingMatchesActivity::class.java))

            R.id.match_rate_tv -> {
                if (adapter?.matchList?.size!=0) {


                    val adapter = dataBinding?.pager?.adapter as HomeMatchAdapter
                    if (adapter.matchList.size == 1) {
                        try {
                            val intent = Intent(v.context, NewMatchRateActivity::class.java)
                            intent.putParcelableArrayListExtra(AppConstants.EXTRA_MATCHES_ARRAY, adapter.matchList)
                            intent.putExtra(AppConstants.EXTRA_POS, 0)
                            v.context.startActivity(intent)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            CustomToast.getInstance(getActivity()).showToast(R.string.stay_tuned_with_us_for_upcoming_matches)
                        }
                    } else {
                        try {

                            val intent = Intent(v.context, MatchLineListActivity::class.java)
                            intent.putParcelableArrayListExtra(AppConstants.EXTRA_MATCHES_ARRAY, adapter.matchList)
                            v.context.startActivity(intent)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            CustomToast.getInstance(getActivity()).showToast(R.string.stay_tuned_with_us_for_upcoming_matches)
                        }

                    }
                }

            }

            R.id.live_score_tv ->
                startActivity(Intent(this, LiveScoreActivity::class.java))

            R.id.recent_match_tv ->
                startActivity(Intent(this, RecentMatchesActivity::class.java))

            R.id.latest_news_tv ->
                startActivity(Intent(this, SeasonActivity::class.java))

            R.id.poll_tv ->
                startActivity(Intent(this, PollingActivity::class.java))

        }
    }

    fun openPlayStoreLink() {
        val appPackageName = packageName // getPackageName() from Context or Activity object
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)))
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.play_store_base_url) + appPackageName)))
        }
    }

    override fun onResume() {
        super.onResume()
        if (call != null)
            call?.cancel()
    }

    var call: Call<MatchesResponse>? = null

    var adapter:HomeMatchAdapter? = null

    fun getMatchesFB() {
        var matches = matchRoomList
        showProgressBar(false)
        if (matches != null) {
            dataBinding?.pager?.visibility = View.VISIBLE
            dataBinding?.indicator?.visibility = View.GONE
            if (matches?.size == 0) {
                stay_tuned.visibility = View.VISIBLE
                dataBinding?.pager?.adapter = null
                try {
                    dataBinding?.indicator?.setViewPager(dataBinding?.pager)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                EventBus.getDefault().post(RefreshLineScreen(matches))
            } else {
                EventBus.getDefault().post(RefreshLineScreen(matches))

                adapter = HomeMatchAdapter(getActivity(), matches)
                dataBinding?.pager?.adapter = adapter

                if (dataBinding?.pager?.adapter?.count!! > 1) {
                    dataBinding?.indicator?.visibility = View.VISIBLE
                } else {
                    dataBinding?.indicator?.visibility = View.GONE
                }
                try {
                    dataBinding?.indicator?.setViewPager(dataBinding?.pager)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                stay_tuned.visibility = View.GONE
            }

        }
//        Handler().postDelayed({showProgressBar(false)},1500)
    }


    /*fun getMatches() {
        showProgressBar(true)
        val apiService = AppRetrofit.getInstance().apiServices
        call = apiService.fireBaseMatches()
        stay_tuned.visibility = View.GONE
        if (call != null)
            call?.enqueue(object : Callback<MatchesResponse> {

                override fun onFailure(call: Call<MatchesResponse>?, t: Throwable?) {
                    showProgressBar(false)
                }

                override fun onResponse(call: Call<MatchesResponse>?, response: Response<MatchesResponse>?) {
                    showProgressBar(false)
                    try {
                        if (response != null && response.body() != null) {
                            val massage = response.body().result.running_msg
                            Utility.putStringValueInSharedPreference(this@HomeActivity, AppConstants.RUNNING_MSG, massage)
                            val matches = response.body().result.matches
                            dataBinding?.indicator?.visibility = View.GONE
                            if (matches.isEmpty()) {
                                stay_tuned.visibility = View.VISIBLE
                                dataBinding?.pager?.adapter = null
                                try {
                                    dataBinding?.indicator?.setViewPager(dataBinding?.pager)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                EventBus.getDefault().post(RefreshLineScreen(matches))
                            } else {
                                EventBus.getDefault().post(RefreshLineScreen(matches))

                                val adapter = HomeMatchAdapter(getActivity(), matches)
                                dataBinding?.pager?.adapter = adapter

                                if (dataBinding?.pager?.adapter?.count!! > 1) {
                                    dataBinding?.indicator?.visibility = View.VISIBLE
                                } else {
                                    dataBinding?.indicator?.visibility = View.GONE
                                }
                                try {
                                    dataBinding?.indicator?.setViewPager(dataBinding?.pager)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                stay_tuned.visibility = View.GONE
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        stay_tuned.visibility = View.VISIBLE

                    }


                }
            })
    }*/

    override fun onAction3Clicked(v: View) {
        openSharingTool()
    }

    private fun openSharingTool() {

        val packageName = this.packageName
        val sharingIntent: Intent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        try {
            sharingIntent.putExtra(Intent.EXTRA_TEXT, response?.result?.invitation?.message)
            startActivity(Intent.createChooser(sharingIntent, "Share using"))

        } catch (e: Exception) {
            e.printStackTrace()
            try {
                val playStoreUrl: String = (Uri.parse(getString(R.string.play_store_base_url) +
                        packageName).toString())
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                        "Hi," + "\n" + getString(R.string.share_test) + "\n\n" + playStoreUrl)
                startActivity(Intent.createChooser(sharingIntent, "Share using"))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }


}

