package com.app.cricketapp.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import java.util.*

/**
 * Created by bhavya on 12-04-2017.
 */

public class WatchLiveAdapter(var context: Context, var watchLiveList: ArrayList<Match>) : RecyclerView.Adapter<WatchLiveAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        
        val view= LayoutInflater.from(context).inflate(R.layout.row_watch_live, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {

        val match = watchLiveList[position]
        holder?.mTeamName1Tv?.text = match.teams.a?.key
        holder?.mTeamName2Tv?.text = match.teams.b?.key
        holder?.mMatchNameTv?.text = match.others.format
        holder?.mDateTv?.text = match.names.relatedName
        holder?.mPlaceTv?.text = match.basicsInfo.venue

        /*Glide.with(context).load(match.teams.a.logo).placeholder(R.drawable.default_image).into((holder.mTeamName1Iv))
        Glide.with(context).load(match.teams.b.logo).placeholder(R.drawable.default_image).into((holder.mTeamName2Iv))
*/
        val utcTimeFormat = Utility.getLocalDate((match.basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.TARGET_DATE_FORMAT_2)

        val localTime2 = Utility.getLocalDate((match.basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT)
        val gmtTime = Utility.parseDateTimeUtcToGmt(localTime2)

        val localTime = Utility.getLocalDate((match.basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT1)

//        String localTimeZone = Utility.getLocalTimeZone()
//        holder.mTimeTv.setText(utcTimeFormat + " " + localTime + " " + localTimeZone + " / " + gmtTime + " GMT")
        holder?.mTimeTv?.text = "$utcTimeFormat $localTime / $gmtTime GMT"
    }

    override fun getItemCount(): Int {
        
        return watchLiveList.size
    }

     class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
         var mMatchNameTv:TextView= view.findViewById(R.id.match_name_tv)
         var  mDateTv: TextView= view.findViewById(R.id.date_tv)
         var mPlaceTv: TextView= view.findViewById(R.id.place_tv)
         var mTimeTv : TextView= view.findViewById(R.id.time_tv)
         var mTeamName1Tv : TextView= view.findViewById(R.id.team_name_1_tv)
         var  mTeamName2Tv: TextView= view.findViewById(R.id.team_name_2_tv)
         var  mTeamName1Iv: ImageView= view.findViewById(R.id.team_name_1_iv)
         var mTeamName2Iv: ImageView= view.findViewById(R.id.team_name_2_iv)
    }
}
