package com.app.cricketapp.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.app.cricketapp.db.CLGCPHelper;
import com.app.cricketapp.db.DBUtil;
import com.app.cricketapp.model.ranking.RankingResponse;

import java.util.List;

/**
 * Created by Prashant on 27-09-2017.
 */

public class RankingResponseDAO {
    private static Context mContext;

    public static void init(Context context) {
        mContext = context;
    }

    public static int insert(RankingResponse brand) {
        ContentValues values;
        values = DBUtil.getContentValues(brand);
        Uri uri = mContext.getContentResolver().insert(CLGCPHelper.Uris.URI_RANKING_TABLE, values);
        int localIndexId = 0;
        if (uri != null) {
            localIndexId = Integer.parseInt(uri.getLastPathSegment());
        }
        return localIndexId;
    }

    public static void update(RankingResponse brand) {
        ContentValues values;
        values = DBUtil.getContentValues(brand);
        mContext.getContentResolver().update(CLGCPHelper.Uris.URI_RANKING_TABLE, values, null, null);
    }

    public static Loader<Cursor> getCursorLoader(String id) {
        return new CursorLoader(mContext, CLGCPHelper.Uris.URI_RANKING_TABLE, null, null, null, null);
    }

    public static void delete() {
        mContext.getContentResolver().delete(CLGCPHelper.Uris.URI_RANKING_TABLE, null, null);
    }


    public static RankingResponse getRecentMatches() {
        Cursor cursor = mContext.getContentResolver().query(CLGCPHelper.Uris.URI_RANKING_TABLE, null, null, null, null);
        RankingResponse RankingResponse = new RankingResponse();
        if (cursor != null && cursor.moveToNext()) {
            RankingResponse = DBUtil.getFromContentValue(cursor, RankingResponse.class);
        }
        return RankingResponse;
    }

    public static void bulkInsert(List<RankingResponse> appModelList) {
        ContentValues[] contentValues = DBUtil.getContentValuesList(appModelList);
        mContext.getContentResolver().bulkInsert(CLGCPHelper.Uris.URI_RANKING_TABLE, contentValues);
    }
}
