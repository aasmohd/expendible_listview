package com.app.cricketapp.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.app.cricketapp.db.CLGCPHelper;
import com.app.cricketapp.db.DBUtil;
import com.app.cricketapp.model.scorecard.ScorecardResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Prashant on 26-09-2017.
 */

public class ScorecardResponseDAO {
    private static Context mContext;

    public static void init(Context context) {
        mContext = context;
    }

    public static int insert(ScorecardResponse brand) {
        ContentValues values;
        values = DBUtil.getContentValues(brand);
        Uri uri = mContext.getContentResolver().insert(CLGCPHelper.Uris.URI_SCORECARD_TABLE, values);
        int localIndexId = 0;
        if (uri != null) {
            localIndexId = Integer.parseInt(uri.getLastPathSegment());
        }
        return localIndexId;
    }

    public static void update(ScorecardResponse brand) {
        ContentValues values;
        values = DBUtil.getContentValues(brand);
        mContext.getContentResolver().update(CLGCPHelper.Uris.URI_SCORECARD_TABLE, values, null, null);
    }

    public static Loader<Cursor> getCursorLoader(String id) {
        return new CursorLoader(mContext, CLGCPHelper.Uris.URI_SCORECARD_TABLE, null,"catId = '"+id+"'", null,null);
    }

    public static List<ScorecardResponse> getMatchList() {
        Cursor cursor = mContext.getContentResolver().query(CLGCPHelper.Uris.URI_SCORECARD_TABLE, null, null, null,null);
        List<ScorecardResponse> matchList = new ArrayList<>();
        if(cursor != null)
        {
            matchList = DBUtil.getListFromCursor(cursor, ScorecardResponse.class);
        }
        return matchList;
    }


    public static void delete() {
        mContext.getContentResolver().delete(CLGCPHelper.Uris.URI_SCORECARD_TABLE, null, null);
    }

    public static void bulkInsert(List<ScorecardResponse> appModelList) {
        ContentValues [] contentValues = DBUtil.getContentValuesList(appModelList);
        mContext.getContentResolver().bulkInsert(CLGCPHelper.Uris.URI_SCORECARD_TABLE, contentValues);
    }
}
