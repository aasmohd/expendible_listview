
package com.app.cricketapp.model.newRecentMatchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Innings {

    @SerializedName("a")
    @Expose
    private A_ a;
    @SerializedName("b")
    @Expose
    private B_ b;

    public A_ getA() {
        return a;
    }

    public void setA(A_ a) {
        this.a = a;
    }

    public B_ getB() {
        return b;
    }

    public void setB(B_ b) {
        this.b = b;
    }

}
