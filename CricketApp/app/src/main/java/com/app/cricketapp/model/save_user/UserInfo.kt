package com.app.cricketapp.model.save_user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserInfo {

    @SerializedName("appUserID")
    @Expose
    var appUserID: Long = 0

}
