package com.app.cricketapp.model.matchresponse;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Prashant on 03-10-2017.
 */

public class MatchRoom implements Parcelable,Comparable<MatchRoom>
{


    private int app_order;

    private String landing_text;

    private String match_room;

    private String match_started;

    private String match_started_time;

    private String status;

    private String match_number;

    private String team1_full_name;

    public String getMatch_number()
    {
        return match_number;
    }

    public void setMatch_number(String match_number)
    {
        this.match_number = match_number;
    }

    public String getTeam1_full_name()
    {
        return team1_full_name;
    }

    public void setTeam1_full_name(String team1_full_name)
    {
        this.team1_full_name = team1_full_name;
    }

    public String getTeam2_full_name()
    {
        return team2_full_name;
    }

    public void setTeam2_full_name(String team2_full_name)
    {
        this.team2_full_name = team2_full_name;
    }

    private String team2_full_name;

    private String team1;

    private String team2;

    private String team_1_logo;

    private String team_2_logo;
    public final static Parcelable.Creator<MatchRoom> CREATOR = new Creator<MatchRoom>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MatchRoom createFromParcel(Parcel in) {
            return new MatchRoom(in);
        }

        public MatchRoom[] newArray(int size) {
            return (new MatchRoom[size]);
        }

    }
            ;

    protected MatchRoom(Parcel in) {
        this.app_order = ((int) in.readValue((int.class.getClassLoader())));
        this.landing_text = ((String) in.readValue((String.class.getClassLoader())));
        this.match_room = ((String) in.readValue((String.class.getClassLoader())));
        this.match_started = ((String) in.readValue((String.class.getClassLoader())));
        this.match_started_time = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.match_number = ((String) in.readValue((String.class.getClassLoader())));
        this.team1_full_name = ((String) in.readValue((String.class.getClassLoader())));
        this.team2_full_name = ((String) in.readValue((String.class.getClassLoader())));
        this.team1 = ((String) in.readValue((String.class.getClassLoader())));
        this.team2 = ((String) in.readValue((String.class.getClassLoader())));
        this.team_1_logo = ((String) in.readValue((String.class.getClassLoader())));
        this.team_2_logo = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MatchRoom() {
    }

    public int getApp_order() {
        return app_order;
    }

    public void setApp_order(int app_order) {
        this.app_order = app_order;
    }

    public String getLanding_text() {
        return landing_text;
    }

    public void setLanding_text(String landing_text) {
        this.landing_text = landing_text;
    }

    public String getMatch_room() {
        return match_room;
    }

    public void setMatch_room(String match_room) {
        this.match_room = match_room;
    }

    public String getMatch_started() {
        return match_started;
    }

    public void setMatch_started(String match_started) {
        this.match_started = match_started;
    }

    public String getMatch_started_time() {
        return match_started_time;
    }

    public void setMatch_started_time(String match_started_time) {
        this.match_started_time = match_started_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getTeam_1_logo() {
        return team_1_logo;
    }

    public void setTeam_1_logo(String team_1_logo) {
        this.team_1_logo = team_1_logo;
    }

    public String getTeam_2_logo() {
        return team_2_logo;
    }

    public void setTeam_2_logo(String team_2_logo) {
        this.team_2_logo = team_2_logo;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(app_order);
        dest.writeValue(landing_text);
        dest.writeValue(match_room);
        dest.writeValue(match_started);
        dest.writeValue(match_started_time);
        dest.writeValue(status);
        dest.writeValue(match_number);
        dest.writeValue(team1_full_name);
        dest.writeValue(team2_full_name);
        dest.writeValue(team1);
        dest.writeValue(team2);
        dest.writeValue(team_1_logo);
        dest.writeValue(team_2_logo);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public int compareTo(MatchRoom matchRoom) {
        int compare_order = ((MatchRoom)matchRoom).getApp_order();
          /* For Ascending order*/
        return this.app_order-compare_order;
    }

    @Override
    public String toString()
    {
        return "MatchRoom{" +
                "app_order=" + app_order +
                ", landing_text='" + landing_text + '\'' +
                ", match_room='" + match_room + '\'' +
                ", match_started='" + match_started + '\'' +
                ", match_started_time='" + match_started_time + '\'' +
                ", status='" + status + '\'' +
                ", match_number='" + match_number + '\'' +
                ", team1_full_name='" + team1_full_name + '\'' +
                ", team2_full_name='" + team2_full_name + '\'' +
                ", team1='" + team1 + '\'' +
                ", team2='" + team2 + '\'' +
                ", team_1_logo='" + team_1_logo + '\'' +
                ", team_2_logo='" + team_2_logo + '\'' +
                '}';
    }
}