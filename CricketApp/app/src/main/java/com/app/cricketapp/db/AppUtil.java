package com.app.cricketapp.db;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;

/**
 * Created by tsingh on 21/1/15.
 */
public class AppUtil {
    private static final String LOG_TAG = AppUtil.class.getSimpleName();

    public static <T> T parseJson(String json, Class<T> tClass) {
        return new Gson().fromJson(json, tClass);
    }

    public static <T> T parseJson(JsonElement result, Class<T> tClass) {
        return new Gson().fromJson(result, tClass);
    }

    public static <T> T parseJson(String json, Type type) {
        return new Gson().fromJson(json, type);
    }

    public static JsonObject parseJson(String response) {
        JsonObject jo = null;
        JsonElement e = null;
        return new JsonParser().parse(response).getAsJsonObject();
    }

    public static JsonArray getJsonElement(String s) {
        return new JsonParser().parse(s).getAsJsonArray();
    }

    public static String getExt(String fileName) {
        String fileFormat = "";
        fileFormat = (fileName.split("[.]")[1]);
        return fileFormat;
    }

    public static String getJson(Object model) {
        return new Gson().toJson(model);
    }

    public static void showToast(Context mContext, String message) {
        if (mContext != null) {
            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
        }
    }

}