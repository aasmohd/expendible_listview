package com.app.cricketapp.model.latestnewsresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class LatestNewsResponse : Serializable {

    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("status")
    @Expose
    var status: Long = 0
    @SerializedName("response")
    @Expose
    var response: ArrayList<List>? = null


    class LatestNewsResult : Serializable {

        @SerializedName("list")
        @Expose
        var list: ArrayList<List>? = null

    }




}



