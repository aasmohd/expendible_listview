package com.app.cricketapp.customview.tv;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.cricketapp.R;
import com.app.cricketapp.databinding.LayoutTvLedBinding;
import com.app.cricketapp.model.matchresponse.MatchBean;
import com.app.cricketapp.utility.AppConstants;
import com.app.cricketapp.utility.CRICSounds;
import com.app.cricketapp.utility.Utility;
import com.google.firebase.crash.FirebaseCrash;

import pl.droidsonroids.gif.GifDrawable;

import static com.app.cricketapp.utility.AppConstants.BALL_TAG;

/**
 * Created by rahulgupta on 26/04/17.
 */

public class MatchTvView extends LinearLayout {
    public MatchTvView(Context context) {
        super(context);
    }

    public MatchTvView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MatchTvView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MatchTvView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
//    private LayoutTvLedBinding tvBinding;
//    private AnimationDrawable drawable;
//    private MatchBean matchBean;
//    private TvListener tvListener;
//
//    public void setTvListener(TvListener tvListener) {
//        this.tvListener = tvListener;
//    }
//
//    public interface TvListener {
//        void onActionWicket();
//    }
//
//    public MatchTvView(Context context) {
//        super(context);
//        init(null, 0);
//    }
//
//    public MatchTvView(Context context, @Nullable AttributeSet attrs) {
//        super(context, attrs);
//        init(attrs, 0);
//    }
//
//    public MatchTvView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        init(attrs, defStyleAttr);
//    }
//
//    /**
//     * Method to initialize parameters
//     *
//     * @param attrs        Any custom attributes
//     * @param defStyleAttr Default styles
//     */
//    private void init(AttributeSet attrs, int defStyleAttr) {
//        tvBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
//                R.layout.layout_tv_led, this, true);
//        tvBinding.speaker.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                boolean speechFlag = Utility.getBooleanSharedPreference(view.getContext(), AppConstants.SPEECH_KEY);
//                Utility.putBooleanValueInSharedPreference(view.getContext(), AppConstants.SPEECH_KEY, !speechFlag);
//                checkSpeechFlag();
//            }
//        });
//    }
//
//    void checkSpeechFlag() {
//        boolean speechFlag = Utility.getBooleanSharedPreference(getContext(), AppConstants.SPEECH_KEY);
//        if (speechFlag) {
//            setAction2Icon(R.drawable.speaker_on);
//        } else {
//            setAction2Icon(R.drawable.speaker_off);
//        }
//    }
//
//    public void setTvData(MatchBean matchBean) {
//        this.matchBean = matchBean;
//        MatchBean.CurrentStatus currentStatus = matchBean.getCurrentStatus();
//        if (currentStatus != null) {
//            if (!currentStatus.getMsg().equals("Break"))
//                tvBinding.tvText.setText(currentStatus.getMsg());
//            setStatusText(currentStatus);
//        }
//    }
//
//    public void setAction2Icon(int drawableResId) {
//        tvBinding.speaker.setImageResource(drawableResId);
//    }
//
//    public void setMatchName(MatchBean matchBean) {
//        if (matchBean.getMatchStatus() != null && matchBean.getMatchStatus().equalsIgnoreCase("notStarted")) {
//            tvBinding.battingTeamNameTv.setText("");
//            tvBinding.battingTeamOverTv.setText("");
//            tvBinding.bowlingTeamNameTv.setText("");
//            tvBinding.bowlingTeamOverTv.setText("");
//        } else {
//            tvBinding.battingTeamNameTv.setText(matchBean.getBattingTeam() + " " + matchBean.getBattingScore()
//                    + "/" + matchBean.getBattingWickets());
//            tvBinding.battingTeamOverTv.setText("(" + matchBean.getBattingOver() + ")");
//
//            if (matchBean.getInning() != null) {
//                if (matchBean.getInning().equals("1")) {
//                    tvBinding.bowlingTeamNameTv.setText("");
//                    tvBinding.bowlingTeamOverTv.setText("");
//                } else if (matchBean.getInning().equals("2")) {
//                    tvBinding.bowlingTeamNameTv.setText(matchBean.getBowlingTeam() + " " + matchBean.getBowlingScore()
//                            + "/" + matchBean.getBowlingWickets());
//                    tvBinding.bowlingTeamOverTv.setText("(" + matchBean.getBowlingOver() + ")");
//                }
//            } else {
//                tvBinding.bowlingTeamNameTv.setText("");
//                tvBinding.bowlingTeamOverTv.setText("");
//            }
//        }
//    }
//
//    private void setStatusText(MatchBean.CurrentStatus currentStatus) {
//        handler.removeCallbacks(runnable);
//
//        tvBinding.currentStatus1Tv.setText("");
//        tvBinding.currentStatus2Tv.setText("");
//        tvBinding.otherTextContainer.setVisibility(GONE);
//        tvBinding.upperView.setVisibility(VISIBLE);
//        tvBinding.otherActionsImage.setVisibility(GONE);
//        tvBinding.matchTvImage.setImageResource(0);
//        tvBinding.otherActionsImage.setImageResource(0);
//        tvBinding.matchTvImage.setTag(AppConstants.DEFAULT);
//        tvBinding.otherActionsImage.setTag(AppConstants.DEFAULT);
//
//        if (checkOtherEnum(currentStatus.getOther()) || currentStatus.getMsg().equals("Break")) {
//            tvBinding.otherTextContainer.setVisibility(VISIBLE);
//            tvBinding.upperView.setVisibility(GONE);
//            tvBinding.otherActionsImage.setVisibility(GONE);
//            if (currentStatus.getMsg().equals("Break"))
//                tvBinding.otherText.setText(currentStatus.getMsg());
//            else
//                tvBinding.otherText.setText(currentStatus.getOther());
//
//            tvBinding.otherText.setVisibility(VISIBLE);
//
//            if (currentStatus.getOther().equals("3rd Umpire")) {
//                CRICSounds.THIRD_UMPIRE.play(getContext(), currentStatus.getREVERTED(), 0);
//            }
//
//        } else if (currentStatus.getOther().equals("New Batsman")) {
//            tvBinding.currentStatus1Tv.setText("New");
//            tvBinding.currentStatus2Tv.setText("Batsman");
//
//            try {
//                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.new_batsmen_gif_v2);
//                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
//            } catch (Exception e) {
//                FirebaseCrash.report(e);
//                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_batsmen);
//            }
////            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_batsmen);
//        } else if (currentStatus.getOther().equals(AppConstants.BALL)) {
//            tvBinding.currentStatus1Tv.setText("Ball");
//            tvBinding.currentStatus2Tv.setText("Chalu");
//
//            try {
//                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.ball_gif);
//                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
//            } catch (Exception e) {
//                FirebaseCrash.report(e);
//                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_ball);
//            }
//
////            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_ball);
//            tvBinding.matchTvImage.setTag(AppConstants.BALL_TAG);
//            CRICSounds.BALL_CHALU.play(getContext(), currentStatus.getREVERTED(), 0);
//
//        } else if (currentStatus.getWk().equals("1") && currentStatus.getWb().equals("1")) {
//            tvBinding.currentStatus1Tv.setText("Wide Ball");
//            tvBinding.currentStatus2Tv.setText("WICKET");
//
//            try {
//                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.wicket_gif_v2);
//                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
//            } catch (Exception e) {
//                FirebaseCrash.report(e);
//                e.printStackTrace();
//            }
////            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_wicket);
//            tvListener.onActionWicket();
//            CRICSounds.WICKET.play(getContext(), currentStatus.getREVERTED(), 0);
//        } else if (currentStatus.getWk().equals("1") && currentStatus.getNb().equals("1")) {
//            tvBinding.currentStatus1Tv.setText("No Ball");
//            tvBinding.currentStatus2Tv.setText("RUN OUT");
//
//            try {
//                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.wicket_gif_v2);
//                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
//            } catch (Exception e) {
//                FirebaseCrash.report(e);
//                e.printStackTrace();
//            }
////            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_wicket);
//            tvListener.onActionWicket();
//            CRICSounds.WICKET.play(getContext(), currentStatus.getREVERTED(), 0);
//        } else if (currentStatus.getWk().equals("1")) {
//
//            try {
//                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.wicket_gif_v2);
//                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
//            } catch (Exception e) {
//                FirebaseCrash.report(e);
//                e.printStackTrace();
//            }
////            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_wicket);
//            tvListener.onActionWicket();
//            tvBinding.currentStatus1Tv.setText("WICKET");
//            CRICSounds.WICKET.play(getContext(), currentStatus.getREVERTED(), 0);
//
//            String formattedRuns = getFormattedRuns(currentStatus.getRun(), false);
//            if (!TextUtils.isEmpty(formattedRuns)) {
//                tvBinding.currentStatus2Tv.setText("+\n" + formattedRuns);
//            }
//
//        } else if (currentStatus.getWb().equals("1")) {
//            tvBinding.currentStatus1Tv.setText("Wide Ball");
//
//            String formattedRuns = getFormattedRuns(currentStatus.getRun(), false);
//            if (!TextUtils.isEmpty(formattedRuns)) {
//                tvBinding.currentStatus2Tv.setText("+\n" + formattedRuns);
//            }
//
//            try {
//                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.wide_gif_v2);
//                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
//            } catch (Exception e) {
//                FirebaseCrash.report(e);
//                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_wide_ball);
//            }
////            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_wide_ball);
//        } else if (currentStatus.getNb().equals("1")) {
//            tvBinding.currentStatus1Tv.setText("No Ball");
//
//            String formattedRuns = getFormattedRuns(currentStatus.getRun(), false);
//            if (!TextUtils.isEmpty(formattedRuns)) {
//                tvBinding.currentStatus2Tv.setText("+\n" + formattedRuns);
//            }
//
//            try {
//                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.no_ball_gif_v2);
//                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
//            } catch (Exception e) {
//                FirebaseCrash.report(e);
//                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_no_ball);
//            }
////            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_no_ball);
//        } else if (currentStatus.getBye().equals("1")) {
//            tvBinding.currentStatus1Tv.setText("Bye");
//
//            String formattedRuns = getFormattedRuns(currentStatus.getRun(), false);
//            if (!TextUtils.isEmpty(formattedRuns)) {
//                tvBinding.currentStatus2Tv.setText("+\n" + formattedRuns);
//            }
//
//            try {
//                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.bye_gif_v2);
//                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
//            } catch (Exception e) {
//                FirebaseCrash.report(e);
//                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_bye);
//            }
////            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_bye);
//
//        } else if (currentStatus.getLbye().equals("1")) {
//            tvBinding.currentStatus1Tv.setText("Leg Bye");
//            String formattedRuns = getFormattedRuns(currentStatus.getRun(), false);
//            if (!TextUtils.isEmpty(formattedRuns)) {
//                tvBinding.currentStatus2Tv.setText("+\n" + formattedRuns);
//            }
//
//            try {
//                GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.leg_bye_gif_v2);
//                tvBinding.matchTvImage.setImageDrawable(gifFromResource);
//            } catch (Exception e) {
//                FirebaseCrash.report(e);
//                e.printStackTrace();
//                tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_leg_bye);
//            }
////            tvBinding.matchTvImage.setImageResource(R.drawable.animation_list_leg_bye);
//
//        } else {
//            setRuns(currentStatus, true);
//        }
//
//        startTvImageAnimation(tvBinding.matchTvImage);
//
//        startBounceAnimation(tvBinding.currentStatus1Tv);
//        startBounceAnimation(tvBinding.currentStatus2Tv);
//
//    }
//
//    /**
//     * Check other key enum
//     *
//     * @param other Other key
//     * @return Flag
//     */
//    private boolean checkOtherEnum(String other) {
//        if (TextUtils.isEmpty(other))
//            return false;
//        else if (other.equalsIgnoreCase("0") || other.equalsIgnoreCase(AppConstants.BALL)
//                || other.equalsIgnoreCase("New Batsman"))
//            return false;
//        return true;
//    }
//
//    private void setRuns(MatchBean.CurrentStatus currentStatus, boolean isShowZeroRuns) {
//        tvBinding.otherTextContainer.setVisibility(VISIBLE);
//        tvBinding.upperView.setVisibility(GONE);
//
//        tvBinding.otherText.setVisibility(VISIBLE);
//        tvBinding.otherActionsImage.setVisibility(GONE);
//        int runs = currentStatus.getRun();
//        switch (runs) {
//            case 0:
//                if (isShowZeroRuns) {
//                    if (currentStatus.getREVERTED().equalsIgnoreCase("1")) {
//                        tvBinding.otherText.setText("");
//                    } else if (currentStatus.getMsg().equalsIgnoreCase("Bowled")) {
//                        tvBinding.otherText.setText("");
//                    } else {
//                        tvBinding.otherText.setText("0");
//                        if (!matchBean.getMatchStatus().equals("finished") &&
//                                !currentStatus.getMsg().equals("Break")) {
//                            CRICSounds.DOT_BALL.play(getContext(), currentStatus.getREVERTED(), 0);
//                        }
//                    }
//                } else {
//                    tvBinding.otherText.setText("");
//                }
//                break;
//            case 1:
//                tvBinding.otherText.setText("1 Run");
//                CRICSounds.SINGLE.play(getContext(), currentStatus.getREVERTED(), 0);
//                break;
//            default:
//                if (runs == 2) {
//                    CRICSounds.DOUBLE_RUN.play(getContext(), currentStatus.getREVERTED(), 0);
//                } else if (runs == 3) {
//                    CRICSounds.THREE_RUNS.play(getContext(), currentStatus.getREVERTED(), 0);
//                }
//                tvBinding.otherText.setText(runs + " Runs");
//                if (runs == 4 || runs == 6) {
//                    tvBinding.otherText.setVisibility(GONE);
//                    tvBinding.otherActionsImage.setVisibility(VISIBLE);
//
//                    if (runs == 4) {
//                        CRICSounds.FOUR.play(getContext(), currentStatus.getREVERTED(), 0);
//
//                        try {
//                            GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.four_gif_v2);
//                            tvBinding.otherActionsImage.setImageDrawable(gifFromResource);
//                        } catch (Exception e) {
//                            FirebaseCrash.report(e);
//                            e.printStackTrace();
//                            tvBinding.otherActionsImage.setImageResource(R.drawable.four_fallback);
//                        }
//
////                        tvBinding.otherActionsImage.setImageResource(R.drawable.animation_list_four);
//                        tvBinding.otherActionsImage.setTag(AppConstants.FOUR);
//                        startTvImageAnimation(tvBinding.otherActionsImage);
//                    } else {
//                        CRICSounds.SIX.play(getContext(), currentStatus.getREVERTED(), 0);
//
//                        try {
//                            GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.six_gif_v2);
//                            tvBinding.otherActionsImage.setImageDrawable(gifFromResource);
//                        } catch (Exception e) {
//                            FirebaseCrash.report(e);
//                            e.printStackTrace();
//                            tvBinding.otherActionsImage.setImageResource(R.drawable.six_fallback);
//                        }
////                        tvBinding.otherActionsImage.setImageResource(R.drawable.animation_list_six);
//                        tvBinding.otherActionsImage.setTag(AppConstants.SIX);
//                        startTvImageAnimation(tvBinding.otherActionsImage);
//                    }
//                }
//                break;
//        }
//    }
//
//    private String getFormattedRuns(int runs2, boolean isShowZeroRuns) {
//        String runs = "";
//        switch (runs2) {
//            case 0:
//                if (isShowZeroRuns) {
//                    runs = "0";
//                }
//                break;
//            case 1:
//                runs = "1 Run";
//                break;
//            default:
//                runs = runs2 + " Runs";
//                break;
//        }
//        return runs;
//    }
//
//    private void startTvImageAnimation(ImageView imageView) {
//
//        boolean isStartTimer = true;
//        if (imageView.getTag() != null && imageView.getTag() instanceof String) {
//            String tag = imageView.getTag().toString();
//            if (tag.equals(BALL_TAG)) {
//                isStartTimer = false;
//            }
//        }
//        if (imageView.getDrawable() != null && imageView.getDrawable() instanceof AnimationDrawable) {
//            drawable = (AnimationDrawable) imageView.getDrawable();
//
//            if (drawable != null) {
//                drawable.setOneShot(false);
//                drawable.start();
//                if (isStartTimer)
//                    startAnimationTimer();
//            }
//        }
//    }
//
//    Handler handler = new Handler();
//    Runnable runnable = new Runnable() {
//        @Override
//        public void run() {
//            if (drawable != null && drawable.isRunning()) {
//                drawable.stop();
//                if (tvBinding.otherActionsImage.getTag() != null) {
//
//                    String tag = tvBinding.otherActionsImage.getTag().toString();
//                    switch (tag) {
//                        case AppConstants.FOUR:
//                            imageViewAnimatedChange(tvBinding.otherActionsImage.getContext(),
//                                    tvBinding.otherActionsImage, R.drawable.four16);
//                            break;
//                        case AppConstants.SIX:
//                            imageViewAnimatedChange(tvBinding.otherActionsImage.getContext(),
//                                    tvBinding.otherActionsImage, R.drawable.six16);
//                            break;
//                        default:
//                            tvBinding.otherActionsImage.setImageResource(0);
//                            break;
//                    }
//                }
//            }
//        }
//    };
//
//    /**
//     * Method to set image with animation
//     *
//     * @param c     Context
//     * @param v     Imageview object
//     * @param resId Image resource id to set
//     */
//    private void imageViewAnimatedChange(Context c, final ImageView v, final int resId) {
//        final Animation anim_out = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
//        final Animation anim_in = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
//        anim_out.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                v.setImageDrawable(v.getResources().getDrawable(resId));
//                anim_in.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                    }
//                });
//                v.startAnimation(anim_in);
//            }
//        });
//        v.startAnimation(anim_out);
//    }
//
//    /**
//     * Method to stop animation after three seconds
//     */
//    private void startAnimationTimer() {
//        handler.removeCallbacks(runnable);
//        handler.postDelayed(runnable, 4800);
//    }
//
//    private void startBounceAnimation(View view) {
//
//        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", 0.80f, 1f);
//        scaleX.setRepeatMode(ValueAnimator.REVERSE);
//        scaleX.setRepeatCount(ValueAnimator.INFINITE);
//        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", 0.80f, 1f);
//        scaleY.setRepeatMode(ValueAnimator.REVERSE);
//        scaleY.setRepeatCount(ValueAnimator.INFINITE);
//
//
//        AnimatorSet scaleSet = new AnimatorSet();
//        scaleSet.playTogether(scaleX, scaleY);
//        scaleSet.setDuration(1000);
//        scaleSet.start();
//
//    }
//
//
}
