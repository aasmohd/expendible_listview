package com.app.cricketapp.model.upcomingmatchresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Others : Serializable {

    @SerializedName("format")
    @Expose
    var format: String? = null
    @SerializedName("key")
    @Expose
    var key: String? = null

}
