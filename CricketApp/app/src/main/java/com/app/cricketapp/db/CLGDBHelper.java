package com.app.cricketapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.app.cricketapp.model.latestnewsresponse.List;
import com.app.cricketapp.model.newRecentMatchResponse.NewRecentMatchResponse;
import com.app.cricketapp.model.ranking.RankingResponse;
import com.app.cricketapp.model.scorecard.ScorecardResponse;
import com.app.cricketapp.model.upcomingmatchresponse.NewUpcomingMatchResponse;

/**
 * DB Manager class manage the table structure and db upgrade facility.
 *
 * @author Ashutosh
 */
public class CLGDBHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database
    // version.
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "CLG.db";
    private static SQLiteOpenHelper dbManager;

    public CLGDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should
     * happen.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // execute create table query
        String CREATE_TABLE_NEW_RECENT = DBUtil.getCreateTableStatement(NewRecentMatchResponse.class);
        String CREATE_TABLE_SCORECARD = DBUtil.getCreateTableStatement(ScorecardResponse.class);
        String CREATE_TABLE_LIST = DBUtil.getCreateTableStatement(List.class);
        String CREATE_TABLE_RANKING = DBUtil.getCreateTableStatement(RankingResponse.class);
        String CREATE_TABLE_UPCOMING = DBUtil.getCreateTableStatement(NewUpcomingMatchResponse.class);

        db.execSQL(CREATE_TABLE_NEW_RECENT);
        db.execSQL(CREATE_TABLE_SCORECARD);
        db.execSQL(CREATE_TABLE_LIST);
        db.execSQL(CREATE_TABLE_RANKING);
        db.execSQL(CREATE_TABLE_UPCOMING);
    }

    /**
     * Called when the database needs to be upgraded. The implementation should
     * use this method to drop tables, add tables, or do anything else it needs
     * to upgrade to the new schema version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        resetDB(db);
    }

    public void resetDB(SQLiteDatabase db) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + NewRecentMatchResponse.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + ScorecardResponse.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + List.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + RankingResponse.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + NewUpcomingMatchResponse.class.getSimpleName());
        // Create tables again
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        resetDB(db);
    }

    /**
     * @param context
     * @return SQLiteDatabase instance.
     */
    public static SQLiteDatabase getdbInstance(Context context) {
        if (dbManager == null) {
            dbManager = new CLGDBHelper(context);
        }
        return dbManager.getWritableDatabase();
    }

    public static SQLiteDatabase getdbInstance() {
        return dbManager.getWritableDatabase();
    }

}
