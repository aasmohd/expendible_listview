package com.app.cricketapp.model.watchliveresponse

import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class WatchLiveResponse {

    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("status")
    @Expose
    var status: Long = 0
    @SerializedName("result")
    @Expose
    var result: UpcomingMatchResult? = null


    class UpcomingMatchResult {

        @SerializedName("list")
        @Expose
        var list: ArrayList<Match>? = null
    }


    class List {

        @SerializedName("day")
        @Expose
        var day: Long = 0
        @SerializedName("matches")
        @Expose
        var matches: ArrayList<Match>? = null

    }


}



