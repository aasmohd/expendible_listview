package com.app.cricketapp.db;

import android.net.Uri;

import com.app.cricketapp.utility.AppConstants;


public class CLGContentProviderData {

    public static final String AUTHORITY = AppConstants.CONTENT_PROVIDER_AUTHORITY_NAME();

    // The URI scheme used for content URIs
    public static final String SCHEME = "content";

    /**
     * The DataProvider content URI
     */
    public static final Uri CONTENT_URI = Uri.parse(SCHEME + "://" + AUTHORITY);

    public static final int ID_APP_TABLE = 1;
    public static final int ID_CATEGORY_TABLE = 2;
    public static final int ID_LIST_TABLE = 3;
    public static final int ID_RANKING_TABLE = 4;
    public static final int ID_UPCOMING_TABLE = 5;

}
