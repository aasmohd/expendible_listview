package com.app.cricketapp.activity

import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.View
import com.app.cricketapp.R
import com.app.cricketapp.adapter.UpcomingMatchPagerMainAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.db.dao.UpcomingResultDAO
import com.app.cricketapp.model.upcomingmatchresponse.NodeResponse
import com.app.cricketapp.model.upcomingmatchresponse.UpcomingMatchResponse
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import kotlinx.android.synthetic.main.activity_watch_live.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by bhavya on 12-04-2017.
 */

class UpcomingMatchesActivity : BaseActivity() {
    var mUpcomingMatchPagermainAdapter:UpcomingMatchPagerMainAdapter? = null
    var upcomingMatchList: ArrayList<UpcomingMatchResponse.List> = ArrayList()
    var mPagerMain:ViewPager? = null
    var mTabLayout:TabLayout? = null

    override fun initUi() {
        showProgressBar(true)
        setBackButtonEnabled()
        toggleAction3Icon(View.GONE)
        mPagerMain = findViewById(R.id.view_pager) as ViewPager?
        mTabLayout = findViewById(R.id.tab_layout) as TabLayout?

        if(Utility.getLongSharedPreference(this,"upcoming_time_stamp")!= 0L){
            // time difference check logic
            if (Utility.getLongSharedPreference(this,"upcoming_time_stamp") < Utility.
                    getStringSharedPreference(this,AppConstants.MATCH_API_SERVER_TIME).toLong())
            {

                if (Utility.isNetworkAvailable(this)) {
                    callGetUpcomingMatchesApi()
                } else {
                    showProgressBar(false)
                    CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
                }
            }else{
                showProgressBar(false)
                var upcomingResponse = UpcomingResultDAO.getRecentMatches()
                mUpcomingMatchPagermainAdapter = UpcomingMatchPagerMainAdapter(this@UpcomingMatchesActivity,upcomingResponse!!)
                setupViewPager(mPagerMain!!,mTabLayout!!)
            }

        }
        else{
            val time = System.currentTimeMillis()

            Utility.putLongValueInSharedPreference(this,"upcoming_time_stamp",time)

            if (Utility.isNetworkAvailable(this)) {
                callGetUpcomingMatchesApi()
            } else {
                showProgressBar(false)
                CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
            }
        }

        loadBannerAd()
        initFullScreenAdd()
    }

    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()

                }
            }
            requestNewInterstitial()
        }
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .build()

        mInterstitialAd.loadAd(adRequest)
        mInterstitialAd.show()
    }

    fun callGetUpcomingMatchesApi() {
        val apiService = AppRetrofit.getInstance().apiServices
        val call = apiService.newUpcomingMatches()
        call.enqueue(object : Callback <NodeResponse> {

            override fun onResponse(call: Call<NodeResponse>?, response: Response<NodeResponse>?) {
                if (response?.body() != null){
                    Utility.putLongValueInSharedPreference(this@UpcomingMatchesActivity,"upcoming_time_stamp",System.currentTimeMillis())
                    try {
                        showProgressBar(false)

                        UpcomingResultDAO.delete();
                        UpcomingResultDAO.insert(response.body().response!!);

                        var upcomingResponse = UpcomingResultDAO.getRecentMatches()
                        mUpcomingMatchPagermainAdapter = UpcomingMatchPagerMainAdapter(this@UpcomingMatchesActivity,upcomingResponse!!)
                        setupViewPager(mPagerMain!!,mTabLayout!!)

                        if (upcomingMatchList.size > 6) {
                            upcomingMatchList[5].showAd = true
                        } else {
                            upcomingMatchList[upcomingMatchList.size - 1].showAd = true
                        }

                        Lg.i("UpcomingMatch", call.toString())
                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                }
            }

            override fun onFailure(call: Call<NodeResponse>?, t: Throwable?) {
                showProgressBar(false)
            }
        })


    }

    override fun getLayoutById(): Int {
        return R.layout.activity_new_upcoming_match
    }

    /*load banner ad for admob*/
    fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun setupViewPager(viewPager: ViewPager, tabLayout: TabLayout) {
        viewPager.adapter = mUpcomingMatchPagermainAdapter
        tabLayout.setupWithViewPager(viewPager)
    }
}
