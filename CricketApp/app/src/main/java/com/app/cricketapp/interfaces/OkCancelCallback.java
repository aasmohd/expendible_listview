package com.app.cricketapp.interfaces;

/**
 * Created by rahul on 28/8/15.
 */
public interface OkCancelCallback {

    void onOkClicked();

    void onCancelClicked();

}
