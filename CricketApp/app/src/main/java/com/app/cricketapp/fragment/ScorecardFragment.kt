package com.app.cricketapp.fragment

import android.support.v7.widget.LinearLayoutManager
import com.app.cricketapp.R
import com.app.cricketapp.adapter.ScoreCardAdapter
import com.app.cricketapp.model.scorecard.Batting
import com.app.cricketapp.model.scorecard.Bowling
import com.app.cricketapp.model.scorecard.IScore
import com.app.cricketapp.model.scorecard.ScorecardResponse
import com.app.cricketapp.utility.AppConstants
import kotlinx.android.synthetic.main.fragment_score_card.*

/**
 * Created by rahulgupta on 07/06/17.
 */

class ScorecardFragment : BaseFragment() {

    override fun initUi() {

        val scoreCard = arguments.getSerializable(AppConstants.EXTRA_KEY) as ScorecardResponse.ScoreCard
        val inningType = arguments.getString(AppConstants.EXTRA_POS)
        val commonArrayList = ArrayList<IScore>()

        //Add Batting Header
        val battingHeader = Batting()
        battingHeader.isBattingView = true
        commonArrayList.add(battingHeader)

        if (inningType == "a") {
            //Team a
            val teamA = scoreCard.teams?.a

            //Add Batting List
            teamA?.batting?.let {
                commonArrayList.addAll(it) }

            //Add Bowling Header
            val bowlingHeader = Bowling()
            bowlingHeader.isBowlingView = true
            commonArrayList.add(bowlingHeader)

            //Add Bowling List
            teamA?.bowling?.let { commonArrayList.addAll(it) }

            bowlingScorecardRv.layoutManager = LinearLayoutManager(mContext)
            bowlingScorecardRv.adapter = ScoreCardAdapter(mContext, commonArrayList)
        } else {
            //Team a
            val teamB = scoreCard.teams?.b

            //Add Batting List
            teamB?.batting?.let { commonArrayList.addAll(it) }

            //Add Bowling Header
            val bowlingHeader = Bowling()
            bowlingHeader.isBowlingView = true
            commonArrayList.add(bowlingHeader)

            //Add Bowling List
            teamB?.bowling?.let { commonArrayList.addAll(it) }


            bowlingScorecardRv.layoutManager = LinearLayoutManager(mContext)
            bowlingScorecardRv.adapter = ScoreCardAdapter(mContext, commonArrayList)
        }
    }

    override fun getLayoutById(): Int {
        return R.layout.fragment_score_card
    }
}
