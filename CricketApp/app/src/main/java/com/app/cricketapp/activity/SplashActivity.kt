package com.app.cricketapp.activity

import android.content.Intent
import android.os.Handler
import android.preference.PreferenceManager
import android.text.TextUtils
import com.app.cricketapp.R
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import com.google.firebase.messaging.FirebaseMessaging

class SplashActivity : BaseActivity() {

    override fun initUi() {
        Utility.generateFBKeyHash(this)
        if(TextUtils.isEmpty(Utility.getStringSharedPreference(this,AppConstants.NOTIFICATION_PREFS))){
            FirebaseMessaging.getInstance().subscribeToTopic(AppConstants.TOPIC_MATCH)
            FirebaseMessaging.getInstance().subscribeToTopic(AppConstants.TOPIC_ANDROID)
            Utility.putStringValueInSharedPreference(this,AppConstants.NOTIFICATION_PREFS,"on")
        }else if (Utility.getStringSharedPreference(this,AppConstants.NOTIFICATION_PREFS).equals("on")){
            FirebaseMessaging.getInstance().subscribeToTopic(AppConstants.TOPIC_MATCH)
            FirebaseMessaging.getInstance().subscribeToTopic(AppConstants.TOPIC_ANDROID)
        }else{
            FirebaseMessaging.getInstance().unsubscribeFromTopic(AppConstants.TOPIC_MATCH)
            FirebaseMessaging.getInstance().unsubscribeFromTopic(AppConstants.TOPIC_ANDROID)
        }


        startHomeActivity()
    }

    override fun getLayoutById(): Int {
        return R.layout.activity_splash
    }

    fun startHomeActivity() {

        var splashTime = 3000.toLong()
        val preferences = PreferenceManager.getDefaultSharedPreferences(getActivity())
        val isFirstTime = preferences.getBoolean(AppConstants.PREFS_IS_FIRST_TIME, true)
        if (isFirstTime) {
            Utility.putBooleanValueInSharedPreference(getActivity(), AppConstants.IS_MALE_SPEECH_KEY, false)
            preferences.edit().putBoolean(AppConstants.SPEECH_KEY, true).apply()
            preferences.edit().putBoolean(AppConstants.PREFS_IS_FIRST_TIME, false).apply()
        } else {
            splashTime = 1000
        }
        //When launched first time, this will be false
        Handler().postDelayed({
            startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
            finish()
        }, splashTime)
    }
}
