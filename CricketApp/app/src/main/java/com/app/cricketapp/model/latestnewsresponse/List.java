package com.app.cricketapp.model.latestnewsresponse;

import com.app.cricketapp.db.dbhelper.DBClass;

import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

/**
 * Created by Prashant on 27-09-2017.
 */

public class List implements Serializable, DBClass {


    public String _id;
    @Nullable
    public String author;
    @Nullable
    public String link;
    @Nullable
    public String pubDate;
    @Nullable
    public String title;
    @Nullable
    public String description;
    @Nullable
    public String thumburl;
    @Nullable
    public String newsid;
    public boolean showAd;

    @Nullable
    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(@Nullable String var1) {
        this.author = var1;
    }

    @Nullable
    public String getLink() {
        return this.link;
    }

    public void setLink(@Nullable String var1) {
        this.link = var1;
    }

    @Nullable
    public String getPubDate() {
        return this.pubDate;
    }

    public void setPubDate(@Nullable String var1) {
        this.pubDate = var1;
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    public void setTitle(@Nullable String var1) {
        this.title = var1;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    public void setDescription(@Nullable String var1) {
        this.description = var1;
    }

    @Nullable
    public String getThumburl() {
        return this.thumburl;
    }

    public void setThumburl(@Nullable String var1) {
        this.thumburl = var1;
    }

    @Nullable
    public String getNewsid() {
        return this.newsid;
    }

    public void setNewsid(@Nullable String var1) {
        this.newsid = var1;
    }

    public boolean getShowAd() {
        return this.showAd;
    }

    public void setShowAd(boolean var1) {
        this.showAd = var1;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}


