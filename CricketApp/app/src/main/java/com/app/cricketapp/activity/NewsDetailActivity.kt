package com.app.cricketapp.activity

import com.app.cricketapp.R
import com.app.cricketapp.databinding.ActivityNewsDetailBinding
import com.app.cricketapp.model.latestnewsresponse.List
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.activity_news_detail.*

/**
 * Created by bhavya on 18-04-2017.
 */

class NewsDetailActivity : BaseActivity() {
    var dataBinding: ActivityNewsDetailBinding? = null

    override fun initUi() {
        setBackButtonEnabled()
        val newsData = intent.getSerializableExtra(AppConstants.LATEST_NEWS_INTENT) as List
        dataBinding = viewDataBinding as ActivityNewsDetailBinding?
        setData(newsData)
        loadBannerAd()
    }

    override fun getLayoutById(): Int {
        return R.layout.activity_news_detail
    }

    fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        adView3.loadAd(adRequest)
    }

    private fun setData(newsData: List) {
        dataBinding?.newsTitleTv?.text = newsData.title

        /*val utcTimeFormat = Utility.getUtcDate(newsData.pubDate!!.toLong() * 1000, AppConstants.DATE_FORMAT1)
        val timeValue = Utility.getTimeAgo(utcTimeFormat, getActivity(), AppConstants.DATE_FORMAT1)

        dataBinding?.newsDateTv?.text = timeValue*/
        Utility.setImageWithUrl(newsData.thumburl, R.drawable.news_placeholder, dataBinding?.latestNewsIv)

        dataBinding?.newsDescriptionTv?.text = newsData.description

    }
}
