package com.app.cricketapp.model.seasonmodels.seasonDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Prashant on 10-10-2017.
 */

public class TeamDetail implements Serializable
{
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("logo")
    @Expose
    private String logo;
    private String short_name;
    private final static long serialVersionUID = -4830693660187957545L;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLogo()
    {
        return logo;
    }

    public void setLogo(String logo)
    {
        this.logo = logo;
    }

    public String getShort_name()
    {
        return short_name;
    }

    public void setShort_name(String short_name)
    {
        this.short_name = short_name;
    }
}
