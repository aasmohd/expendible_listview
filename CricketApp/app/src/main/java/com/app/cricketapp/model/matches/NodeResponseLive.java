package com.app.cricketapp.model.matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Prashant on 15-12-2017.
 */

public class NodeResponseLive
{
    @SerializedName("status")
    @Expose
    private long status;
    @SerializedName("response")
    @Expose
    private List<com.app.cricketapp.model.upcomingmatchresponse.Match> response = null;
    @SerializedName("time")
    @Expose
    private long time;

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public List<com.app.cricketapp.model.upcomingmatchresponse.Match> getResponse() {
        return response;
    }

    public void setResponse(List<com.app.cricketapp.model.upcomingmatchresponse.Match> response) {
        this.response = response;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
