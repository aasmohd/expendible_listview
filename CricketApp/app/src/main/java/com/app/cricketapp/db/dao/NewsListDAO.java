package com.app.cricketapp.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.app.cricketapp.db.CLGCPHelper;
import com.app.cricketapp.db.DBUtil;
import com.app.cricketapp.model.latestnewsresponse.List;

import java.util.ArrayList;


/**
 * Created by Prashant on 27-09-2017.
 */

public class NewsListDAO {
    private static Context mContext;

    public static void init(Context context) {
        mContext = context;
    }

    public static int insert(List brand) {
        ContentValues values;
        values = DBUtil.getContentValues(brand);
        Uri uri = mContext.getContentResolver().insert(CLGCPHelper.Uris.URI_LIST_TABLE, values);
        int localIndexId = 0;
        if (uri != null) {
            localIndexId = Integer.parseInt(uri.getLastPathSegment());
        }
        return localIndexId;
    }

    public static void update(List brand) {
        ContentValues values;
        values = DBUtil.getContentValues(brand);
        mContext.getContentResolver().update(CLGCPHelper.Uris.URI_LIST_TABLE, values, null, null);
    }

    public static Loader<Cursor> getCursorLoader() {
        return new CursorLoader(mContext, CLGCPHelper.Uris.URI_LIST_TABLE, null, null, null, null);
    }

    public static void delete() {
        mContext.getContentResolver().delete(CLGCPHelper.Uris.URI_LIST_TABLE, null, null);
    }


    public static java.util.List<List> getRecentMatches() {
        Cursor cursor = mContext.getContentResolver().query(CLGCPHelper.Uris.URI_LIST_TABLE, null, null, null, null);
        java.util.List<List> list = new ArrayList();
        if (cursor != null && cursor.moveToNext()) {
            list = DBUtil.getListFromCursor(cursor, List.class);
        }
        return list;
    }

    public static void bulkInsert(java.util.List<List> appModelList) {
        ContentValues[] contentValues = DBUtil.getContentValuesList(appModelList);
        mContext.getContentResolver().bulkInsert(CLGCPHelper.Uris.URI_LIST_TABLE, contentValues);
    }
}
