package com.app.cricketapp.fragment

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.app.cricketapp.R
import com.app.cricketapp.adapter.LineupAdapter
import com.app.cricketapp.interfaces.FragmentDataCallback
import com.app.cricketapp.model.scorecard.ScorecardResponse

/**
 * Created by Prashant on 11-08-2017.
 */

class LineupFragment : BaseFragment() {
    internal var recyclerView: RecyclerView? = null
    internal var lineupAdapter: LineupAdapter? = null
    var fragmentDataCallback: FragmentDataCallback? = null
    var scoreCardResponse: ScorecardResponse? = null

    override fun initUi() {
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        val linearLayoutManager: LinearLayoutManager? = LinearLayoutManager(mContext)
        recyclerView!!.layoutManager = linearLayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()

        fragmentDataCallback = activity as FragmentDataCallback
        scoreCardResponse = fragmentDataCallback?.myResponse

        val teamAPlayers = scoreCardResponse?.result?.scorecard?.get(0)?.teams?.a?.teamInfo
        val teamBPlayers = scoreCardResponse?.result?.scorecard?.get(0)?.teams?.b?.teamInfo

        lineupAdapter = LineupAdapter(teamAPlayers, teamBPlayers)
        recyclerView!!.adapter = lineupAdapter

    }

    override fun getLayoutById(): Int = R.layout.fragment_lineup
}
