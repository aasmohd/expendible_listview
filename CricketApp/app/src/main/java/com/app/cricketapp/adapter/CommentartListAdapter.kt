package com.app.cricketapp.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.model.commentaryResPonse.Balls
import com.app.cricketapp.model.commentaryResPonse.BatsmanSummery
import com.app.cricketapp.model.scorecard.ScorecardResponse

/**
 * Created by Prashant on 11-08-2017.
 */

class CommentartListAdapter(internal var context: Context, internal var itemList: List<Balls>,
                            internal var scorecardResponse: ScorecardResponse,internal var overMap: HashMap<String,String>) : RecyclerView.Adapter<CommentartListAdapter.CommentaryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentaryViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_commentary, parent, false)
        return CommentaryViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CommentaryViewHolder, position: Int)
    {
        if (!TextUtils.isEmpty(itemList.get(position).comment)){
            holder.data.text = Html.fromHtml(itemList.get(position).comment)
        }else{
            if (!TextUtils.isEmpty(itemList.get(position).wicket)){
                holder.data.text = Html.fromHtml(itemList.get(position).bowler.key + " to " +
                        itemList.get(position).striker +": "+itemList.get(position).runs)
            }else{
                holder.data.text = Html.fromHtml(itemList.get(position).bowler.key + " to " +
                        itemList.get(position).striker +". "+itemList.get(position).wicket + " "
                + itemList.get(position).wicketType+": "+itemList.get(position).runs
                )
            }
        }
        holder.overBall.text = itemList.get(position).overStr.toString()
        if (TextUtils.isEmpty(itemList.get(position).wicket))
        {
            setRuns(itemList.get(position).runs,itemList.get(position).ballType,holder)
        }else{
            if (itemList.get(position).runs.toInt()>0){
                setRuns("W"+itemList.get(position).runs, itemList.get(position).ballType,holder)

            }else{
                setRuns("W", itemList.get(position).ballType,holder)
            }

        }


        if (itemList.get(position).summary!= null){
            val batsmanL:ArrayList<BatsmanSummery>? = ArrayList()
            for (batsman in itemList.get(position).summary.batsmanSummeries){

                    if (batsman.batting!= null && !batsman.batting.dismissed)
                    batsmanL?.add(batsman)

            }
            if(batsmanL?.size ==1){
                holder.batsman1.text = batsmanL.get(0).key
                holder.batsman1Run.text =  batsmanL.get(0).batting?.runs.toString()+" (" + batsmanL.get(0).batting?.balls + ")"
                holder.batsman2.text =" "
                holder.batsman2Run.text = " "
            }else{
                holder.batsman1.text = batsmanL?.get(0)?.key
                holder.batsman1Run.text =  batsmanL?.get(0)?.batting?.runs.toString()+" (" + batsmanL?.get(0)?.batting?.balls + ")"
                holder.batsman2.text = batsmanL?.get(1)?.key
                holder.batsman2Run.text = batsmanL?.get(1)?.batting?.runs.toString() +
                        " (" + batsmanL?.get(1)?.batting?.balls + ")"
            }


            holder.bowler.text = itemList.get(position).summary.bowler.get(0).key
            holder.bowlerBall.text = itemList.get(position).summary.bowler.get(0).bowling.overs + "-" +
                    itemList.get(position).summary.bowler.get(0).bowling.extras + "-" +
                    itemList.get(position).summary.bowler.get(0).bowling.runs + "-" +
                    itemList.get(position).summary.bowler.get(0).bowling.wickets


            if (itemList.get(position).battingTeam.equals("a",true)){
                holder.teamName.text = scorecardResponse.result.match_summary.teams.a?.name?.toUpperCase()
            }else{
                holder.teamName.text = scorecardResponse.result.match_summary.teams.b?.name?.toUpperCase()

            }

            holder.summeryOvr.text = "Over "+itemList.get(position).summary.over.toString()

            holder.summeryRuns.text = itemList.get(position).summary.match.runs.toString() + " - " +
                    itemList.get(position).summary.match.wicket.toString()

            val s = overMap.get(itemList.get(position).summary.over.toString() +
                    itemList.get(position).battingTeam+itemList.get(position).innings)+ "  (<b>" + itemList.get(position).summary.runs + " Runs</b>) "
            holder.summeryOverRuns.text = Html.fromHtml(s)


            holder.summeryLay.visibility = View.VISIBLE
        }else{
            holder.summeryLay.visibility = View.GONE
        }

    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class CommentaryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var data: TextView
        var summeryLay: LinearLayout
        var overBall: TextView
        var overRuns: TextView
        var summeryOvr:TextView
        var batsman1:TextView
        var batsman2:TextView
        var bowler:TextView
        var batsman1Run:TextView
        var batsman2Run:TextView
        var bowlerBall:TextView
        var teamName:TextView
        var summeryRuns:TextView
        var summeryOverRuns:TextView

        init {
            data = itemView.findViewById<TextView>(R.id.commentary_data)
            summeryLay = itemView.findViewById(R.id.summery_lay)
            overBall = itemView.findViewById(R.id.over_data)
            overRuns = itemView.findViewById(R.id.run_ball)

            summeryOvr = itemView.findViewById(R.id.over)
            batsman1 = itemView.findViewById(R.id.batsman1)
            batsman2 = itemView.findViewById(R.id.batsman2)
            bowler = itemView.findViewById(R.id.bowler)
            teamName = itemView.findViewById(R.id.team)
            summeryRuns = itemView.findViewById(R.id.runs)
            summeryOverRuns = itemView.findViewById(R.id.over_run)

            batsman1Run = itemView.findViewById(R.id.batsman1_run)
            batsman2Run = itemView.findViewById(R.id.batsman2_run)
            bowlerBall = itemView.findViewById(R.id.bowler_run)
        }
    }
    
    fun setRuns(run:String , ballType:String , holder: CommentaryViewHolder){


//        holder.overRuns.setBackgroundResource(R.drawable.shape_circle_green)

        if (!ballType.equals("normal",true)){

            if (ballType.toLowerCase().contains("no")) {
                holder.overRuns.setBackgroundResource(R.drawable.shape_circle_extra)
                holder.overRuns.setTextColor(holder.overRuns.resources.getColor(R.color.white_feffff))
                val temp = run.toInt()-1
                holder.overRuns.text = "N"+ temp
            }else if (ballType.toLowerCase().contains("wide")) {
                holder.overRuns.setBackgroundResource(R.drawable.shape_circle_extra)
                holder.overRuns.setTextColor(holder.overRuns.resources.getColor(R.color.white_feffff))
                if (run.toInt()>1){
                    val temp = run.toInt()-1
                    holder.overRuns.text = "wd"+temp
                }else{
                    holder.overRuns.text = "wd"
                }

                holder.overRuns.textSize = 12f

            }else if(ballType.toLowerCase().contains("leg")){
                holder.overRuns.setBackgroundResource(R.drawable.shape_circle_extra)
                holder.overRuns.setTextColor(holder.overRuns.resources.getColor(R.color.white_feffff))
                if (run.toInt()>0){
                    holder.overRuns.text = "L"+run
                }else{
                    holder.overRuns.text = "L"
                }
//                holder.overRuns.text = run
            }else{
                holder.overRuns.setBackgroundResource(R.drawable.shape_circle_extra)
                holder.overRuns.setTextColor(holder.overRuns.resources.getColor(R.color.white_feffff))
                holder.overRuns.text = run
            }

        }else{
            if (run.equals("6")){
                holder.overRuns.setTextColor(holder.overRuns.resources.getColor(R.color.white_feffff))
                holder.overRuns.setBackgroundResource(R.drawable.shape_circle_six)
                holder.overRuns.text = run
            }else if (run.equals("4") ) {
                holder.overRuns.setTextColor(holder.overRuns.resources.getColor(R.color.white_feffff))
                holder.overRuns.setBackgroundResource(R.drawable.shape_circle_blue)
                holder.overRuns.text = run

            } else if (run.equals("0")){
                holder.overRuns.setTextColor(holder.overRuns.resources.getColor(R.color.white_feffff))
                holder.overRuns.setBackgroundResource(R.drawable.shape_circle_gray)
                holder.overRuns.text = run

            }else if (run.contains("W",true)){
                holder.overRuns.setBackgroundResource(R.drawable.shape_circle_red)
                holder.overRuns.setTextColor(holder.overRuns.resources.getColor(R.color.white_feffff))
                holder.overRuns.text = run
            }else{
                holder.overRuns.setBackgroundResource(R.drawable.shape_circle_green)
                holder.overRuns.setTextColor(holder.overRuns.resources.getColor(R.color.white_feffff))
                holder.overRuns.text = run

            }
        }



        val dimension = holder?.overRuns?.context?.resources?.getDimension(R.dimen.margin_3)?.toInt()
        if (dimension != null) {
            holder.overRuns.setPadding(dimension, dimension, dimension, dimension)
        } 
    }
}
