package com.app.cricketapp.fragment

import android.content.Context
import android.os.Handler
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.adapter.CommentartListAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.interfaces.FragmentDataCallback
import com.app.cricketapp.model.RequestBean
import com.app.cricketapp.model.commentaryResPonse.Balls
import com.app.cricketapp.model.commentaryResPonse.CommentaryRes
import com.app.cricketapp.model.scorecard.ScorecardResponse
import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.PaginationScrollListener
import com.app.cricketapp.utility.Utility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap


/**
 * Created by Prashant on 11-08-2017.
 */

class CommentaryFragment : BaseFragment() {
    internal var recyclerView: RecyclerView? = null
    internal var refreshLay: SwipeRefreshLayout? = null
    internal var noCommentary: TextView? = null
    internal var commentartListAdapter: CommentartListAdapter? = null
    internal var context: Context? = null

    private var isLoading = true
    private var isLastPage = false

    private val TOTAL_PAGES = 5

    var itemList: ArrayList<Balls>? = null
    var matchcategory: String? = null
    var total: Long? = null
    var matchBean: Match? = null
    var fragmentDataCallback: FragmentDataCallback? = null
    var scoreCardResponse: ScorecardResponse? = null
    var overMap: HashMap<String, String>? = null

    override fun initUi() {
        context = activity
        overMap = HashMap()
        matchcategory = arguments.getString("category")
        fragmentDataCallback = activity as FragmentDataCallback
        matchBean = fragmentDataCallback?.bean
        scoreCardResponse = fragmentDataCallback?.myResponse

        startUpdateTimer()

        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        refreshLay = findViewById(R.id.refresh_lay) as SwipeRefreshLayout
        noCommentary = findViewById(R.id.no_commentary_tv) as TextView

        itemList = ArrayList<Balls>()


        val linearLayoutManager: LinearLayoutManager? = LinearLayoutManager(context)
        recyclerView!!.layoutManager = linearLayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()

        recyclerView!!.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {

            override fun loadMoreItems() {
                // make API call with query
                val index = (itemList?.size?.minus(1))
                if (itemList?.get(index!!)?.ballCount!! == 0L) {
                    this@CommentaryFragment.isLastPage = true
                } else {
                    this@CommentaryFragment.isLastPage = false
                    this@CommentaryFragment.isLoading = true

                    if (Utility.isNetworkAvailable(context)) {
                        callCommentaryApi(itemList?.get(index!!)?.ballCount!! - 30, itemList?.get(index!!)?.ballCount!! - 1, "down")
                        this@CommentaryFragment.isLoading = true
                        showProgressBar(true)
                    } else {
                        showProgressBar(false)
                        CustomToast.getInstance(context).showToast(getString(R.string.check_internet_connection))
                    }
                }

            }

            override fun getTotalPageCount(): Int {
                return TOTAL_PAGES
            }

            override fun isLastPage(): Boolean {
                return this@CommentaryFragment.isLastPage
            }

            override fun isLoading(): Boolean {
                return this@CommentaryFragment.isLoading
            }

        })

        commentartListAdapter = CommentartListAdapter(context!!, itemList!!, scoreCardResponse!!, overMap!!)
        recyclerView!!.adapter = commentartListAdapter

        refreshLay?.setOnRefreshListener {
            mHandler.post(updateRunnable)
        }

    }

    fun callCommentaryApi(min: Long, max: Long, format: String) {

        val request = RequestBean()
        request.key = scoreCardResponse?.key
        request.minBall = min
        request.maxBall = max

        val apiService = AppRetrofit.getInstance().apiServices
        val call = apiService.getCommentary(request)
        call.enqueue(object : Callback<CommentaryRes> {
            override fun onFailure(call: Call<CommentaryRes>?, t: Throwable?) {
                showProgressBar(false)
            }

            override fun onResponse(call: Call<CommentaryRes>?, response: Response<CommentaryRes>?) {
                if (response?.body()?.response != null && response.body()?.response?.data != null) {
                    showProgressBar(false)
                    refreshLay?.isRefreshing = false
                    val dataResponse = response.body()?.response?.data!!
                    val team1Score: TextView = findViewById(R.id.tv_team1_score) as TextView
                    val team2Score: TextView = findViewById(R.id.tv_team2_score) as TextView
                    val teamStatus: TextView = findViewById(R.id.tv_team_status) as TextView
                    val team1Name: TextView = findViewById(R.id.tv_team1) as TextView
                    val team2Name: TextView = findViewById(R.id.tv_team2) as TextView
                    val team1Lay: LinearLayout = findViewById(R.id.team1_lay) as LinearLayout
                    val team2Lay: LinearLayout = findViewById(R.id.team2_lay) as LinearLayout

                    if (format.equals("first")) {
                        if (dataResponse != null) {

                            if (dataResponse.isEmpty()) {
                                noCommentary?.visibility = View.VISIBLE
                            } else {
                                noCommentary?.visibility = View.GONE

                                if (!dataResponse.get(0).score.isEmpty()) {
                                    /*for (score in dataResponse.get(0).score){
                                    if(score.team.equals(scoreCardResponse?.result?.scorecard?.get(0)
                                            ?.inningInfo?.inningStartedBy,true))
                                    {
                                        team1Lay.visibility = View.VISIBLE
                                        team1Score.text = ""+score?.runs + " - "+ score?.wickets + " ("+ score?.overs + ") "
                                        team1Name.text = matchBean?.teams?.b?.name?.toUpperCase()

                                        //

                                        team2Lay.visibility = View.VISIBLE
                                        team2Score.text = ""+matchBean?.results?.innings?.a?.runs +
                                                " - "+ matchBean?.results?.innings?.a?.wickets +
                                                " ("+matchBean?.results?.innings?.a?.overs + ") "
                                        team2Name.text = matchBean?.teams?.a?.name?.toUpperCase()


                                        break;

                                    }else{
                                        team2Lay.visibility = View.VISIBLE
                                        team2Score.text = ""+matchBean?.results?.innings?.b?.runs +
                                                " - "+ matchBean?.results?.innings?.b?.wickets +
                                                " ("+matchBean?.results?.innings?.b?.overs + ") "
                                        team2Name.text = matchBean?.teams?.b?.name?.toUpperCase()

                                        //

                                        team1Lay.visibility = View.VISIBLE
                                        team1Score.text = ""+score?.runs + " - "+ score?.wickets + " ("+ score?.overs + ") "
                                        team1Name.text = matchBean?.teams?.a?.name?.toUpperCase()

                                        break;
                                    }
                                }*/
                                    var a = 0;
                                    var b = 0;
                                    for (score in dataResponse.get(0).score) {
                                        if (response.body()?.response?.format?.equals("test", true)!!) {

                                            if (score.team.equals("a", true)) {
                                                team1Lay.visibility = View.VISIBLE
                                                if (a == 0) {
                                                    team1Score.append("" + score?.runs + " & ")
                                                    a++
                                                } else {
                                                    team1Score.append("" + score?.runs)
                                                    a++
                                                }

                                                team1Name.text = matchBean?.teams?.a?.name?.toUpperCase()
                                            } else {
                                                team2Lay.visibility = View.VISIBLE
                                                if (b == 0) {
                                                    team2Score.append("" + score?.runs + " & ")
                                                    b++
                                                } else {
                                                    team2Score.append("" + score?.runs)
                                                    b++
                                                }

                                                team2Name.text = matchBean?.teams?.b?.name?.toUpperCase()
                                            }

                                        } else {
                                            if (score.team.equals("a", true)) {
                                                team1Lay.visibility = View.VISIBLE
                                                team1Score.text = "" + score?.runs + " - " + score?.wickets + " (" + score?.overs + ") "
                                                team1Name.text = matchBean?.teams?.a?.name?.toUpperCase()
                                            } else {
                                                team2Lay.visibility = View.VISIBLE
                                                team2Score.text = "" + score?.runs + " - " + score?.wickets + " (" + score?.overs + ") "
                                                team2Name.text = matchBean?.teams?.b?.name?.toUpperCase()
                                            }
                                        }
                                    }

                                    if (!TextUtils.isEmpty(scoreCardResponse?.result?.match_summary?.msgs?.result)) {
                                        teamStatus.visibility = View.VISIBLE
                                        teamStatus.text = scoreCardResponse?.result?.match_summary?.msgs?.result
                                    }
                                }

                                itemList?.addAll(dataResponse)
                                for (data in dataResponse) {
                                    addOverData(data)
                                }
                                commentartListAdapter?.notifyDataSetChanged()
                                total = response.body()?.response?.total
                                isLoading = false
                            }
                        }

                    } else if (format.equals("down")) {
                        for (r in dataResponse) {
                            itemList?.add(r)
                            addOverData(r)
                        }
                        total = response.body()?.response?.total

                        commentartListAdapter?.notifyDataSetChanged()
                        isLoading = false
                        showProgressBar(false)
                    } else {
                        if (dataResponse.isEmpty()) {

                        } else {
/*
                        for (score in dataResponse.get(0).score){
                            if(score.team.equals(scoreCardResponse?.result?.scorecard?.get(0)
                                    ?.inningInfo?.inningStartedBy,true))
                            {
                                team1Lay.visibility = View.VISIBLE
                                team1Score.text = ""+score?.runs + " - "+ score?.wickets + " ("+ score?.overs + ") "
                                team1Name.text = matchBean?.teams?.b?.name?.toUpperCase()

                                //

                                team2Lay.visibility = View.VISIBLE
                                team2Score.text = ""+matchBean?.results?.innings?.a?.runs +
                                        " - "+ matchBean?.results?.innings?.a?.wickets +
                                        " ("+matchBean?.results?.innings?.a?.overs + ") "
                                team2Name.text = matchBean?.teams?.a?.name?.toUpperCase()


                                break;

                            }else{
                                team2Lay.visibility = View.VISIBLE
                                team2Score.text = ""+matchBean?.results?.innings?.b?.runs +
                                        " - "+ matchBean?.results?.innings?.b?.wickets +
                                        " ("+matchBean?.results?.innings?.b?.overs + ") "
                                team2Name.text = matchBean?.teams?.b?.name?.toUpperCase()

                                //

                                team1Lay.visibility = View.VISIBLE
                                team1Score.text = ""+score?.runs + " - "+ score?.wickets + " ("+ score?.overs + ") "
                                team1Name.text = matchBean?.teams?.a?.name?.toUpperCase()

                                break;
                            }
                        }
*/
                            for (score in dataResponse.get(0).score) {
                                if (score.team.equals("a", true)) {
                                    team1Lay.visibility = View.VISIBLE
                                    team1Score.text = "" + score?.runs + " - " + score?.wickets + " (" + score?.overs + ") "
                                    team1Name.text = matchBean?.teams?.a?.name?.toUpperCase()
                                } else {
                                    team2Lay.visibility = View.VISIBLE
                                    team2Score.text = "" + score?.runs + " - " + score?.wickets + " (" + score?.overs + ") "
                                    team2Name.text = matchBean?.teams?.b?.name?.toUpperCase()
                                }
                            }

                            if (!TextUtils.isEmpty(scoreCardResponse?.result?.match_summary?.msgs?.completed)) {
                                teamStatus.visibility = View.VISIBLE
                                teamStatus.text = scoreCardResponse?.result?.match_summary?.msgs?.completed
                            }

                            for (r in dataResponse.size.minus(1) downTo 0) {
                                itemList?.add(0, dataResponse.get(r))
                                addOverData(dataResponse.get(r))
                            }
                            total = response.body()?.response?.total

                            commentartListAdapter?.notifyDataSetChanged()
                        }


                        isLoading = false


                    }

                }
            }

        })
    }

    override fun getLayoutById(): Int {
        return R.layout.fragment_commentary
    }

    private fun startUpdateTimer() {
        if (Utility.isNetworkAvailable(context)) {
            callCommentaryApi(0, 0, "first")
            isLoading = true
        } else {
            showProgressBar(false)
            CustomToast.getInstance(context).showToast(getString(R.string.check_internet_connection))
        }
        if (matchcategory?.equals("live")!!) {
            val tmr = Timer()
            tmr.schedule(object : TimerTask() {
                override fun run() {
                    mHandler.post(updateRunnable)
                }
            }, 60000, 60000)
        }

    }

    private val mHandler = Handler()
    private val updateRunnable = Runnable {
        if (!isLoading) {
            if (total?.minus(1) == itemList?.get(0)?.ballCount!!) {
                if (Utility.isNetworkAvailable(context)) {
                    callCommentaryApi(total!!, itemList?.get(0)?.ballCount!!.plus(30), "up")
                    isLoading = true
                } else {
                    showProgressBar(false)
                    CustomToast.getInstance(context).showToast(getString(R.string.check_internet_connection))
                }

            } else {
                if (Utility.isNetworkAvailable(context)) {
                    callCommentaryApi(itemList?.get(0)?.ballCount!!, itemList?.get(0)?.ballCount!!.plus(30), "up")
                    isLoading = true
                } else {
                    showProgressBar(false)
                    CustomToast.getInstance(context).showToast(getString(R.string.check_internet_connection))
                }

            }

        } else {
            refreshLay?.isRefreshing = false
        }


    }

    fun addOverData(data: Balls) {
        if (TextUtils.isEmpty(overMap?.get(data.over.toString() + data.battingTeam+data.innings))) {
            if (TextUtils.isEmpty(data.wicket)) {


                if (data.ballType.equals("wide", true)) {
                    if (data.runs.toInt() > 1) {
                        overMap?.put(data.over.toString() + data.battingTeam+data.innings, "wd" + data.runs.toInt().minus(1));
                    } else {
                        overMap?.put(data.over.toString() + data.battingTeam+data.innings, "wd");
                    }

                } else if(data.ballType.contains("leg", true)){
                    if (data.runs.toInt() > 0) {
                        overMap?.put(data.over.toString() + data.battingTeam+data.innings, "L" + data.runs);
                    } else {
                        overMap?.put(data.over.toString() + data.battingTeam+data.innings, "L");
                    }
                }else {
                    overMap?.put(data.over.toString() + data.battingTeam+data.innings, data.runs)
                }
            } else {
                overMap?.put(data.over.toString() + data.battingTeam+data.innings, "W")
            }


        } else {
            if (TextUtils.isEmpty(data.wicket)) {


                if (data.ballType.equals("wide", true)) {

                    if (data.runs.toInt() > 1) {
                        var run = "wd"+data.runs.toInt().minus(1).toString() + " " + overMap?.get(data.over.toString() + data.battingTeam+data.innings)
                        overMap?.put(data.over.toString() + data.battingTeam+data.innings, run);
                    } else {
                        var run = "wd"+ " " + overMap?.get(data.over.toString() + data.battingTeam+data.innings)
                        overMap?.put(data.over.toString() + data.battingTeam+data.innings, run);
                    }
                } else if(data.ballType.contains("leg", true)){
                    if (data.runs.toInt() > 0) {
                        var run = "L"+data.runs.toString() + " " + overMap?.get(data.over.toString() + data.battingTeam+data.innings)
                        overMap?.put(data.over.toString() + data.battingTeam+data.innings, run);
                    } else {
                        var run = "L"+ " " + overMap?.get(data.over.toString() + data.battingTeam+data.innings)
                        overMap?.put(data.over.toString() + data.battingTeam+data.innings, run);
                    }
                }else {
                    var run = data.runs + " " + overMap?.get(data.over.toString() + data.battingTeam+data.innings)
                    overMap?.put(data.over.toString() + data.battingTeam+data.innings, run)
                }


            } else {
                var run = "W" + " " + overMap?.get(data.over.toString() + data.battingTeam+data.innings)
                overMap?.put(data.over.toString() + data.battingTeam+data.innings, run)
            }

        }
    }
}
