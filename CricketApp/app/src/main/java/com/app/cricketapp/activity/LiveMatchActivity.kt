package com.app.cricketapp.activity

import android.view.Window
import android.view.WindowManager
import android.webkit.WebView
import com.app.cricketapp.R
import com.app.cricketapp.databinding.ActivityLiveMatchBinding
import com.google.android.gms.ads.AdRequest
import tcking.github.com.giraffeplayer.GiraffePlayer

/**
 * Created on 12-12-2017.
 */
class LiveMatchActivity : BaseActivity() {
    var webview: WebView? = null
    var binding: ActivityLiveMatchBinding? = null
    lateinit var player: GiraffePlayer

    override fun getLayoutById(): Int {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_live_match
    }

    override fun initUi() {
        binding = viewDataBinding as ActivityLiveMatchBinding?
        val url = intent.getStringExtra("web_url")
//        webview = binding?.myWebView
//        webview!!.visibility = View.GONE
//        webview!!.clearHistory()
//        webview!!.getSettings().javaScriptEnabled = true
//        webview!!.getSettings().setPluginState(WebSettings.PluginState.ON)
//        webview!!.loadUrl(url)
//        webview!!.webChromeClient = WebChromeClient()
//        showProgressBar(true)
//        webview!!.webViewClient = object : WebViewClient() {
//
//            override fun onPageFinished(view: WebView, url: String) {
//                webview!!.visibility = View.VISIBLE
//                showProgressBar(false)
//            }
//        }
        player = GiraffePlayer(this)
        player.play(url)
//        player.play(url.replace("\'","))
        loadBannerAd()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (player != null) {
            player.stop();
        }
    }

    override fun onResume() {
        super.onResume()
        if (player != null && player.isPlaying) {
            player.pause()
        }
    }

    override fun onStop() {
        super.onStop()
        if (player != null) {
            player.stop()
        }
    }

    private fun loadBannerAd() {
        val adRequest: AdRequest = AdRequest.Builder().build()
        binding?.adView?.loadAd(adRequest)

    }

}