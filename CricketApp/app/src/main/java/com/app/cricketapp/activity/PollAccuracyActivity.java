package com.app.cricketapp.activity;

import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.app.cricketapp.R;
import com.app.cricketapp.customview.Formatt;
import com.app.cricketapp.model.MyPollAccuracy.MainFeed;
import com.app.cricketapp.model.MyPollAccuracy.Polls;
import com.app.cricketapp.network.ApiServices;
import com.app.cricketapp.network.AppRetrofit;
import com.app.cricketapp.utility.AppConstants;
import com.app.cricketapp.utility.PaginationScrollListener;
import com.app.cricketapp.utility.Utility;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Prashant on 10-02-2018.
 */

public class PollAccuracyActivity extends BaseActivity {
    private List<Polls> datalist;
    PieChart pieChart;
    int colorgreen = Color.parseColor("#2dcc70");
    int colorred = Color.parseColor("#EC7148");
    int coloryellow = Color.parseColor("#f1c40f");
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;

    private boolean isLoading = true;
    private boolean isLastPage = false;
    private int totalPage = 1;
    private int currentPage =1;

    @Override
    protected void initUi() {
        // Log.d(TAG, "onCreate: start");
        datalist = new ArrayList<Polls>();
        pieChart = (PieChart) findViewById(R.id.pchart);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setDescription(null);
        pieChart.setHoleRadius(0);
        pieChart.setUsePercentValues(true);
        pieChart.setTouchEnabled(false);
        pieChart.setDrawEntryLabels(true);
        pieChart.setRotationEnabled(false);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(this);

        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager){

            @Override
            protected void loadMoreItems()
            {
                currentPage++;
                if(currentPage == totalPage || totalPage ==1){
                    isLastPage = true;
                }else {
                    isLoading = true;
                    getResults(currentPage);
                }

            }

            @Override
            public int getTotalPageCount()
            {
                return totalPage;
            }

            @Override
            public boolean isLastPage()
            {
                return isLastPage;
            }

            @Override
            public boolean isLoading()
            {
                return isLoading;
            }
        });

        if (isLoading)
        getResults(currentPage);
        //  addDataset();
    }

    private void getResults(final int PageNo) {


        ApiServices apiServices = AppRetrofit.getInstance().getApiServices();
        Call<MainFeed> mainFeedCall = apiServices.getResults( Utility.getStringSharedPreference(this,AppConstants.APP_USERID),PageNo);
        mainFeedCall.enqueue(new Callback<MainFeed>() {
            @Override
            public void onResponse(Call<MainFeed> call, Response<MainFeed> response) {
                isLoading = false;
                if (response.body().getStatus() == 1) {
                    if (response.body().getResponse().getTotalPoll()>15){
                        totalPage = response.body().getResponse().getTotalPoll()/15;
                    }

                    datalist.addAll(response.body().getResponse().getPolls());
                    List<Float> ydata= new ArrayList<>();
                    //Log.d("connnn",datalist.toString());
                    //if (response.body().getResponse().getCorrectPoll()!= 0 )
                        ydata.add(response.body().getResponse().getCorrectPoll());
                    //if (response.body().getResponse().getIncorrectPoll() != 0)
                        ydata.add(response.body().getResponse().getIncorrectPoll());
                    //if (response.body().getResponse().getPendingPoll() != 0)
                        ydata.add(response.body().getResponse().getPendingPoll());
                    addDataset(ydata);
                    RecyclerAdapter adapter = new RecyclerAdapter(getApplicationContext(), datalist);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<MainFeed> call, Throwable t) {
                Log.d("not", t.toString());
                isLoading = false;

            }
        });

    }


    private void addDataset(List<Float> ydata) {
        boolean red = false ,green = false ,yellow = false;
        int combination = -1;
        ArrayList<PieEntry> yEntrys = new ArrayList<>();

        for (int i = 0; i < ydata.size(); i++) {
            if (ydata.get(i).equals(new Float(0.0))){
                Log.d("DEBUG", "addDataset: "+ydata.get(i));
                switch (i){
                    case 0 :
                        green = true;
                        break;
                    case 1 :
                        red = true;
                        break;
                    case 2 :
                        yellow = true;
                        break;
                }
            }
            if (ydata.get(i)!= 0 )
            yEntrys.add(new PieEntry(ydata.get(i), i));

        }

        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
        pieDataSet.setSelectionShift(6f);
        pieDataSet.setValueLinePart1OffsetPercentage(60.f);
        pieDataSet.setValueLinePart1Length(1f);
        pieDataSet.setValueLinePart2Length(2f);
        pieDataSet.setValueTextSize(15f);
        pieDataSet.setValueLineColor(Color.WHITE);
        pieDataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);


        ArrayList<Integer> colors = new ArrayList<>();
        if (green && !red && !yellow){
            combination = 0;
        }
        if (red && !green && !yellow){
            combination = 1;
        }
        if (yellow && !red && !green){
            combination = 2;
        }
        if (green && red &&!yellow){
            combination = 3;
        }
        if (red && yellow && !green){
            combination = 4;
        }
        if (green && yellow && !red){
            combination = 5;
        }
        if (green && red && yellow){
            combination = 6;
        }
        if (!green && !red && !yellow){
            combination = 7;
        }
        /*if (!green && !red && !yellow){
            combination = 7;
        } else if (green && yellow){
            combination = 5;
        } else if (red && yellow){
            combination = 4;
        }else if (green && red){
            combination = 3;
        } else if (yellow){
            combination = 2;
        }
*/
        switch (combination){
            case 0 :
                colors.add(colorred);
                colors.add(coloryellow);
                break;
            case 1 :
                colors.add(colorgreen);
                colors.add(coloryellow);
                break;
            case 2 :
                colors.add(colorgreen);
                colors.add(colorred);
                break;
            case 3 :
                colors.add(coloryellow);
                break;
            case 4 :
                colors.add(colorgreen);
                break;
            case 5 :
                colors.add(colorred);
                break;
            case 6 :

                break;
            case 7 :
                colors.add(colorgreen);
                colors.add(colorred);
                colors.add(coloryellow);
                break;
            default:
                break;
        }

        pieDataSet.setColors(colors);


        Legend legend = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.NONE);
        legend.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);

        PieData pieData = new PieData(pieDataSet);
        pieData.setValueTextColors(colors);
        pieData.setValueFormatter(new Formatt());
        pieChart.setData(pieData);
        pieChart.invalidate();
    }


    @Override
    protected int getLayoutById() {
        return R.layout.activity_poll_accuracy;
    }
}
