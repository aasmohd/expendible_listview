
package com.app.cricketapp.model.commentaryResPonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Summary {

    @SerializedName("runs")
    @Expose
    private Long runs;
    @SerializedName("over")
    @Expose
    private Long over;
    @SerializedName("wicket")
    @Expose
    private Long wicket;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("batsman")
    @Expose
    private List<BatsmanSummery> batsmanSummeries = null;
    @SerializedName("bowler")
    @Expose
    private List<Bowler> bowler = null;
    @SerializedName("match")
    @Expose
    private Match match;

    public Long getRuns() {
        return runs;
    }

    public void setRuns(Long runs) {
        this.runs = runs;
    }

    public Long getOver() {
        return over;
    }

    public void setOver(Long over) {
        this.over = over;
    }

    public Long getWicket() {
        return wicket;
    }

    public void setWicket(Long wicket) {
        this.wicket = wicket;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<BatsmanSummery> getBatsmanSummeries() {
        return batsmanSummeries;
    }

    public void setBatsmanSummeries(List<BatsmanSummery> batsmanSummeries) {
        this.batsmanSummeries = batsmanSummeries;
    }

    public List<Bowler> getBowler() {
        return bowler;
    }

    public void setBowler(List<Bowler> bowler) {
        this.bowler = bowler;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

}
