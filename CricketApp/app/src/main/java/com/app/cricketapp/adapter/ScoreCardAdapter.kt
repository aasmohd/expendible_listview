package com.app.cricketapp.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.model.scorecard.Batting
import com.app.cricketapp.model.scorecard.Bowling
import com.app.cricketapp.model.scorecard.IScore
import com.app.cricketapp.utility.AppConstants
import java.util.*

/**
 * Created by bhavya on 12-04-2017.
 */

class ScoreCardAdapter(var context: Context, var watchLiveList: ArrayList<IScore>?)
    : RecyclerView.Adapter<ScoreCardAdapter.ScoreCardVH>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ScoreCardAdapter.ScoreCardVH? {

        when (viewType) {
            AppConstants.BATTING_VIEW -> {
                val view = LayoutInflater.from(context).inflate(R.layout.layout_batting_header, parent, false)
                return BattingHeaderVH(view)
            }
            AppConstants.BATTING -> {
                val view = LayoutInflater.from(context).inflate(R.layout.row_live_score_batting_team, parent, false)
                return BattingVH(view)
            }
            AppConstants.BOWLING_VIEW -> {
                val view = LayoutInflater.from(context).inflate(R.layout.layout_bowling_header, parent, false)
                return BowlingHeaderVH(view)
            }
            AppConstants.BOWLING -> {
                val view = LayoutInflater.from(context).inflate(R.layout.row_live_score_bowling, parent, false)
                return BowlingVH(view)
            }
        }
        return null
    }

    override fun onBindViewHolder(holder: ScoreCardVH?, position: Int) {
        val match = watchLiveList?.get(position)

        when (getItemViewType(position)) {
            AppConstants.BATTING_VIEW -> {
            }
            AppConstants.BATTING -> {
                val battingVh = holder as BattingVH
                /*if (position.rem(2) == 0) {
                    battingVh.itemView?.setBackgroundColor(Color.WHITE)
                } else {
                    battingVh.itemView?.setBackgroundColor(Color.parseColor("#F6F7F8"))
                }*/
                if (match is Batting) {
                    battingVh.name.text = match.fullname
                    battingVh.status.text = match.outStr
                    battingVh.runs.text = match.runs.toString()
                    battingVh.balls.text = match.balls.toString()
                    battingVh.fours.text = match.fours.toString()
                    battingVh.sixs.text = match.sixes.toString()
                    battingVh.strikeRate.text = match.strikeRate.toString()
                }
            }
            AppConstants.BOWLING_VIEW -> {

            }
            AppConstants.BOWLING -> {
                val bowlingVh = holder as BowlingVH
                /*if (position.rem(2) == 0) {
                    bowlingVh.itemView?.setBackgroundColor(Color.WHITE)
                } else {
                    bowlingVh.itemView?.setBackgroundColor(Color.parseColor("#F6F7F8"))
                }*/
                if (match is Bowling) {
                    bowlingVh.name.text = match?.fullname
                    bowlingVh.overs.text = match?.overs
                    bowlingVh.maidenOvers.text = match?.maidenOvers.toString()
                    bowlingVh.runs.text = match?.runs.toString()
                    bowlingVh.wicket.text = match?.wickets.toString()
                    bowlingVh.nb.text = match?.extras.toString()
                    bowlingVh.wb.text = match?.extras.toString()
                    bowlingVh.er.text = match?.economy.toString()
                }
            }
        }


    }

    override fun getItemCount(): Int {
        val size = watchLiveList?.size
        return size as Int
    }

    override fun getItemViewType(position: Int): Int {
        return ViewType.getViewType(watchLiveList?.get(position))
    }

    /**
     * View type class holding constants
     */
    object ViewType {

        /**
         * Method to get view type from api

         * @param str View type returned by api
         * *
         * @return Integer representation of view type
         */
        fun getViewType(match: IScore?): Int {

            if (match is Batting && match.viewType == AppConstants.BATTING) {
                if (match.isBattingView)
                    return AppConstants.BATTING_VIEW
                else {
                    return AppConstants.BATTING
                }
            } else if (match is Bowling) {
                if (match.isBowlingView)
                    return AppConstants.BOWLING_VIEW
                else {
                    return AppConstants.BOWLING
                }
            }
            return -1

        }
    }

    open class ScoreCardVH(view: View) : RecyclerView.ViewHolder(view)

    class BattingHeaderVH(view: View) : ScoreCardVH(view)

    class BowlingHeaderVH(view: View) : ScoreCardVH(view)

    class BattingVH(view: View) : ScoreCardVH(view) {
        var name: TextView = view.findViewById(R.id.name)
        var status: TextView = view.findViewById(R.id.status)
        var runs: TextView = view.findViewById(R.id.runs)
        var balls: TextView = view.findViewById(R.id.balls)
        var fours: TextView = view.findViewById(R.id.fours)
        var sixs: TextView = view.findViewById(R.id.sixs)
        var strikeRate: TextView = view.findViewById(R.id.strike_rate)
    }

    class BowlingVH(view: View) : ScoreCardVH(view) {
        var name: TextView = view.findViewById(R.id.name)
        var overs: TextView = view.findViewById(R.id.overs)
        var maidenOvers: TextView = view.findViewById(R.id.maiden)
        var runs: TextView = view.findViewById(R.id.runs)
        var wicket: TextView = view.findViewById(R.id.wicket)

        var nb: TextView = view.findViewById(R.id.no_balls)
        var wb: TextView = view.findViewById(R.id.wide_balls)
        var er: TextView = view.findViewById(R.id.economy_rate)

    }
}
