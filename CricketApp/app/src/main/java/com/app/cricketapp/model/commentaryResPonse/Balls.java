
package com.app.cricketapp.model.commentaryResPonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Balls
{

    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("wicket_type")
    @Expose
    private String wicketType;
    @SerializedName("over")
    @Expose
    private Long over;
    @SerializedName("ball")
    @Expose
    private String ball;
    @SerializedName("innings")
    @Expose
    private String innings;
   /* @SerializedName("wagon_position")
    @Expose
    private String wagonPosition;*/
    @SerializedName("wicket")
    @Expose
    private String wicket;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("ball_type")
    @Expose
    private String ballType;
    @SerializedName("striker")
    @Expose
    private String striker;
    @SerializedName("batting_team")
    @Expose
    private String battingTeam;
    @SerializedName("runs")
    @Expose
    private String runs;
    @SerializedName("over_str")
    @Expose
    private Double overStr;
    @SerializedName("extras")
    @Expose
    private String extras;
    @SerializedName("ballId")
    @Expose
    private String ballId;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("batsman")
    @Expose
    private Batsman batsman;
    @SerializedName("bowler")
    @Expose
    private Batsman bowler;
    @SerializedName("ballCount")
    @Expose
    private Long ballCount;
    @SerializedName("summary")
    @Expose
    private Summary summary;

    @SerializedName("score")
    @Expose
    private List<Score> score;


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getWicketType() {
        return wicketType;
    }

    public void setWicketType(String wicketType) {
        this.wicketType = wicketType;
    }

    public Long getOver() {
        return over;
    }

    public void setOver(Long over) {
        this.over = over;
    }

    public String getBall() {
        return ball;
    }

    public void setBall(String ball) {
        this.ball = ball;
    }

    public String getInnings() {
        return innings;
    }

    public void setInnings(String innings) {
        this.innings = innings;
    }

    /*public String getWagonPosition() {
        return wagonPosition;
    }

    public void setWagonPosition(String wagonPosition) {
        this.wagonPosition = wagonPosition;
    }*/

    public String getWicket() {
        return wicket;
    }

    public void setWicket(String wicket) {
        this.wicket = wicket;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBallType() {
        return ballType;
    }

    public void setBallType(String ballType) {
        this.ballType = ballType;
    }

    public String getStriker() {
        return striker;
    }

    public void setStriker(String striker) {
        this.striker = striker;
    }

    public String getBattingTeam() {
        return battingTeam;
    }

    public void setBattingTeam(String battingTeam) {
        this.battingTeam = battingTeam;
    }

    public String getRuns() {
        return runs;
    }

    public void setRuns(String runs) {
        this.runs = runs;
    }

    public Double getOverStr() {
        return overStr;
    }

    public void setOverStr(Double overStr) {
        this.overStr = overStr;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getBallId() {
        return ballId;
    }

    public void setBallId(String ballId) {
        this.ballId = ballId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public Batsman getBatsman() {
        return batsman;
    }

    public void setBatsman(Batsman batsman) {
        this.batsman = batsman;
    }
    public Long getBallCount() {
        return ballCount;
    }

    public void setBallCount(Long ballCount) {
        this.ballCount = ballCount;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }


    public List<Score> getScore()
    {
        return score;
    }

    public void setScore(List<Score> score)
    {
        this.score = score;
    }

    public Batsman getBowler()
    {
        return bowler;
    }

    public void setBowler(Batsman bowler)
    {
        this.bowler = bowler;
    }
}
