package com.app.cricketapp.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import java.util.*


class UpcomingMatchPagerAdapter(var context: Context, var matches: ArrayList<Match>, var localDate: String) : PagerAdapter() {

    override fun getCount(): Int {

        return matches.size
    }

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {

        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val match = matches.get(position)

        val itemView = LayoutInflater.from(context).inflate(R.layout.row_upcoming_matches_pager1, container, false)

        val mTeamName1Tv: TextView = itemView.findViewById(R.id.team_name_1_tv)
        val mTeamName2Tv: TextView = itemView.findViewById(R.id.team_name_2_tv)
        mTeamName1Tv.text = match.teams.a?.key
        mTeamName2Tv.text = match.teams.b?.key

        val mTeamName1Iv: ImageView = itemView.findViewById(R.id.team_name_1_iv)
        val mTeamName2Iv: ImageView = itemView.findViewById(R.id.team_name_2_iv)

        Utility.setImageWithUrl(match.teams.a?.logo, R.drawable.default_image, mTeamName1Iv)

        Utility.setImageWithUrl(match.teams.b?.logo, R.drawable.default_image, mTeamName2Iv)

        val mStartDateTv: TextView = itemView.findViewById(R.id.time_tv)

        val localTime = Utility.getLocalDate((match.basicsInfo.startDate)?.toLong() as Long * 1000, AppConstants.SERVER_DATE_FORMAT2)

        mStartDateTv.text = localTime

        val matchType: ImageView = itemView.findViewById(R.id.match_type)
        if (match.others.format == "one-day") {
            matchType.setImageResource(R.drawable.odi_ribbon)
        } else if (match.others.format == "test") {
            matchType.setImageResource(R.drawable.test_ribbon)
        } else {
            matchType.setImageResource(R.drawable.t20_ribbon)
        }


        val leagueTv: TextView = itemView.findViewById(R.id.league)
        leagueTv.text = match.season.name

        val locationTv: TextView = itemView.findViewById(R.id.place_tv)
        var s = match.basicsInfo.venue.toString()
        locationTv.setText(s)

        val match_number_tv: TextView = itemView.findViewById(R.id.date_tv)
        match_number_tv.text = match.names.relatedName
        container?.addView(itemView)

        return itemView

    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as LinearLayout)
    }

    override fun getItemPosition(`object`: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }

}