package com.app.cricketapp.utility;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.app.cricketapp.customview.CustomToast;
import com.app.cricketapp.interfaces.LiveMatchListener;
import com.app.cricketapp.model.matchresponse.Config;
import com.app.cricketapp.model.matchresponse.CurrentStatus;
import com.app.cricketapp.model.matchresponse.IMatch;
import com.app.cricketapp.model.matchresponse.Inning;
import com.app.cricketapp.model.matchresponse.Lambi;
import com.app.cricketapp.model.matchresponse.Market;
import com.app.cricketapp.model.matchresponse.MatchRoom;
import com.app.cricketapp.model.matchresponse.NewMatchModel;
import com.app.cricketapp.model.matchresponse.Session;
import com.app.cricketapp.model.matchresponse.Team;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahulgupta on 03/08/17.
 */

public class FireBaseController {
    private HashMap<String, IMatch> matchDataMap = new HashMap<>();
    private HashMap<String, String> matchStringMap = new HashMap<>();

    private static final String KEY_LIVE_URL = "liveMatchUrl";
    private static final String KEY_CONFIG = "config";
    private static final String KEY_FIRST_TEAM = "firstTeam";
    private static final String KEY_SECOND_TEAM = "secondTeam";
    private static final String KEY_INNING_1 = "inning1";
    private static final String KEY_INNING_2 = "inning2";
    private static final String KEY_INNING_3 = "inning3";
    private static final String KEY_INNING_4 = "inning4";
    private static final String KEY_CURRENT_STATUS = "currentStatus";
    private static final String KEY_MARKET = "market";
    private static final String KEY_SESSION = "session";
    private static final String KEY_LAMBI = "lambi";
    private static final String KEY_RUNNING_MSG = "showRunningMsg";
    private MatchListener matchListener;
    private LiveMatchListener liveMatchListener;
    private DatabaseReference mDatabase;
    private String fireBaseChild;
    private ChildEventListener childEventListener;
    private Context context;
    private boolean isFirstTime = true;
    ArrayList<MatchRoom> mMatchList;
    int nowConnected = 0;

    private FireBaseController(Context context) {
//        init();
        this.context = context;
    }

    public static FireBaseController getInstance(Context context) {
//        if (fireBaseController == null)
        FireBaseController fireBaseController = new FireBaseController(context);
        return fireBaseController;
    }


    public void initMatchUpdateKey(final MatchUpdateListener listener) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("getMatchUpdate").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Lg.i("getMatchUpdate", dataSnapshot.getValue().toString());
                if (listener != null)
                    listener.onMatchUpdate(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void initMatchRoom(){
        mMatchList = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("match-rooms").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> matchChildren = dataSnapshot.getChildren();
                mMatchList.clear();
                for (DataSnapshot match : matchChildren){
                    MatchRoom match_ = match.getValue(MatchRoom.class);
                    Lg.i("FireBaseController", new Gson().toJson(match_));
                    if (match_.getStatus().equals("1") )
                        mMatchList.add(match_);
                }
                Collections.sort(mMatchList);

                Lg.i("matchListSize", ""+ mMatchList.size());
                liveMatchListener.matchList(mMatchList);
//                mDatabase.child("match-rooms").removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getRunningMsg(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("runningMessage").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String runningMsg = dataSnapshot.getValue().toString();
                liveMatchListener.runningMsgData(runningMsg);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mDatabase.child("underMaintenance").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String maintenanceKey = dataSnapshot.getValue().toString();
                liveMatchListener.isMaintenance(maintenanceKey);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    public void init(final String fireBaseChild) {
        nowConnected = 1;
        this.fireBaseChild = fireBaseChild;
        isFirstTime = true;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(fireBaseChild).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    NewMatchModel newMatchModel = dataSnapshot.getValue(NewMatchModel.class);
                    Lg.i("FireBaseController", new Gson().toJson(newMatchModel));
                    mDatabase.child(fireBaseChild).removeEventListener(this);


                    Config config = newMatchModel.config;
                    matchDataMap.put(KEY_CONFIG, config);

                    Team firstTeam = newMatchModel.firstTeam;
                    matchDataMap.put(KEY_FIRST_TEAM, firstTeam);


                    Team secondTeam = newMatchModel.secondTeam;
                    matchDataMap.put(KEY_SECOND_TEAM, secondTeam);


                    Inning inning1 = newMatchModel.inning1;
                    matchDataMap.put(KEY_INNING_1, inning1);

                    Inning inning2 = newMatchModel.inning2;
                    matchDataMap.put(KEY_INNING_2, inning2);

                    Inning inning3 = newMatchModel.inning3;
                    matchDataMap.put(KEY_INNING_3, inning3);

                    Inning inning4 = newMatchModel.inning4;
                    matchDataMap.put(KEY_INNING_4, inning4);

                    CurrentStatus currentStatus = newMatchModel.currentStatus;
                    matchDataMap.put(KEY_CURRENT_STATUS, currentStatus);

                    Market market = newMatchModel.market;
                    matchDataMap.put(KEY_MARKET, market);

                    Session session = newMatchModel.session;
                    matchDataMap.put(KEY_SESSION, session);

                    Lambi lambi = newMatchModel.lambi;
                    matchDataMap.put(KEY_LAMBI, lambi);

                    matchStringMap.put("currentInning", newMatchModel.currentInning);
                    matchStringMap.put("ballsRemaining", newMatchModel.ballsRemaining);
                    matchStringMap.put("favouriteTeam", newMatchModel.favouriteTeam);
                    matchStringMap.put("batsmanOneStatus", newMatchModel.batsmanOneStatus);
                    matchStringMap.put("batsmanTwoStatus", newMatchModel.batsmanTwoStatus);
                    matchStringMap.put("bowlerStatus", newMatchModel.bowlerStatus);
                    matchStringMap.put("localDescription", newMatchModel.localDescription);
                    matchStringMap.put("previousBall", newMatchModel.previousBall);
                    matchStringMap.put("sessionSuspend", newMatchModel.sessionSuspend);
                    matchStringMap.put("lambiShow", newMatchModel.lambiShow);
                    matchStringMap.put("matchDay", newMatchModel.matchDay);
                    matchStringMap.put(AppConstants.RUNNING_MSG,newMatchModel.showRunningMsg);
                    matchStringMap.put("liveKey",newMatchModel.liveMatchKey);

                    if (matchListener != null) {
                        matchListener.onConfigReceived(KEY_CONFIG, config);
                    }
                    if (matchListener != null) {
                        matchListener.onLiveMatchUrlRecieved(newMatchModel.liveMatchUrl);
                    }
                    if (matchListener != null) {
                        matchListener.onTeamDataReceived(KEY_FIRST_TEAM, firstTeam);
                    }
                    if (matchListener != null) {
                        matchListener.onTeamDataReceived(KEY_SECOND_TEAM, secondTeam);
                    }
                    if (matchListener != null) {
                        matchListener.onCurrentStatusReceived(currentStatus, false);
                    }

                    if (matchListener != null && inning1 != null) {
                        matchListener.onInningDataReceived(KEY_INNING_1, inning1);
                    }

                    if (matchListener != null && inning2 != null) {
                        matchListener.onInningDataReceived(KEY_INNING_2, inning2);
                    }

                    if (matchListener != null && inning3 != null) {
                        matchListener.onInningDataReceived(KEY_INNING_3, inning3);
                    }

                    if (matchListener != null && inning4 != null) {
                        matchListener.onInningDataReceived(KEY_INNING_4, inning4);
                    }

                    if (matchListener != null) {
                        matchListener.updateMarket(market);
                    }

                    if (matchListener != null) {
                        matchListener.updateSession(session);
                    }

                    if (matchListener != null) {
                        matchListener.updateLambi(lambi);
                    }

                    setOtherListeners();

                    if (matchListener != null) {
                        matchListener.updateAll();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomToast.getInstance(context).showToast(e.getMessage());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        connectedRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                boolean connected = dataSnapshot.getValue(Boolean.class);

                if (connected) {

                    if (nowConnected==0){
                        init(fireBaseChild);
                        nowConnected++;
                    }
//                   init(fireBaseChild);

                }else {
//                    init(fireBaseChild);
                    nowConnected = 0;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    public void destroy() {

        for (Map.Entry<String, ValueEventListener> entry : valueEventListeners.entrySet()) {
            String key = entry.getKey();
            ValueEventListener value = entry.getValue();
            mDatabase.child(fireBaseChild).child(key).removeEventListener(value);
        }
        if (childEventListener != null) {
            mDatabase.child(fireBaseChild).removeEventListener(childEventListener);
        }

    }

    private HashMap<String, ValueEventListener> valueEventListeners = new HashMap<>();

    private void setOtherListeners() {
        ValueEventListener valueEventListener = mDatabase.child(fireBaseChild)
                .child(KEY_CONFIG).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Config config = dataSnapshot.getValue(Config.class);
                        matchDataMap.put(KEY_CONFIG, config);
                        if (matchListener != null) {
                            matchListener.onConfigReceived(KEY_CONFIG, config);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_CONFIG, valueEventListener);


        ValueEventListener valueEventListener1 = mDatabase.child(fireBaseChild)
                .child(KEY_FIRST_TEAM).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Team config = dataSnapshot.getValue(Team.class);
                        matchDataMap.put(KEY_FIRST_TEAM, config);
                        if (matchListener != null) {
                            matchListener.onTeamDataReceived(KEY_FIRST_TEAM, config);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_FIRST_TEAM, valueEventListener1);


        ValueEventListener valueEventListener2 = mDatabase.child(fireBaseChild)
                .child(KEY_SECOND_TEAM).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Team config = dataSnapshot.getValue(Team.class);
                        matchDataMap.put(KEY_SECOND_TEAM, config);
                        if (matchListener != null) {
                            matchListener.onTeamDataReceived(KEY_SECOND_TEAM, config);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_SECOND_TEAM, valueEventListener2);


        ValueEventListener valueEventListener3 = mDatabase.child(fireBaseChild)
                .child(KEY_INNING_1).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Inning config = dataSnapshot.getValue(Inning.class);
                        matchDataMap.put(KEY_INNING_1, config);
                        if (matchListener != null) {
                            matchListener.onInningDataReceived(KEY_INNING_1, config);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_INNING_1, valueEventListener3);


        ValueEventListener valueEventListener4 = mDatabase.child(fireBaseChild)
                .child(KEY_INNING_2).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Inning config = dataSnapshot.getValue(Inning.class);
                        matchDataMap.put(KEY_INNING_2, config);
                        if (matchListener != null) {
                            matchListener.onInningDataReceived(KEY_INNING_2, config);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_INNING_2, valueEventListener4);


        ValueEventListener valueEventListenerInning3 = mDatabase.child(fireBaseChild)
                .child(KEY_INNING_3).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Inning config = dataSnapshot.getValue(Inning.class);
                        matchDataMap.put(KEY_INNING_3, config);
                        if (matchListener != null) {
                            matchListener.onInningDataReceived(KEY_INNING_3, config);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_INNING_3, valueEventListenerInning3);


        ValueEventListener valueEventListenerInning4 = mDatabase.child(fireBaseChild)
                .child(KEY_INNING_4).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Inning config = dataSnapshot.getValue(Inning.class);
                        matchDataMap.put(KEY_INNING_4, config);
                        if (matchListener != null) {
                            matchListener.onInningDataReceived(KEY_INNING_4, config);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_INNING_4, valueEventListenerInning4);


        ValueEventListener valueEventListener5 = mDatabase.child(fireBaseChild)
                .child(KEY_CURRENT_STATUS).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        CurrentStatus currentStatus = dataSnapshot.getValue(CurrentStatus.class);
                        matchDataMap.put(KEY_CURRENT_STATUS, currentStatus);
                        if (matchListener != null) {
                            if (isFirstTime) {
                                isFirstTime = false;
                                matchListener.onCurrentStatusReceived(currentStatus, false);
                            } else {
                                matchListener.onCurrentStatusReceived(currentStatus, true);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_CURRENT_STATUS, valueEventListener5);

        ValueEventListener valueEventListener6 = mDatabase.child(fireBaseChild)
                .child(KEY_MARKET).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Market market = dataSnapshot.getValue(Market.class);
                        matchDataMap.put(KEY_MARKET, market);
                        if (matchListener != null) {
                            matchListener.updateMarket(market);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_MARKET, valueEventListener6);


        ValueEventListener valueEventListener7 = mDatabase.child(fireBaseChild)
                .child(KEY_SESSION).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Session market = dataSnapshot.getValue(Session.class);
                        matchDataMap.put(KEY_SESSION, market);
                        if (matchListener != null) {
                            matchListener.updateSession(market);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_SESSION, valueEventListener7);


        ValueEventListener valueEventListener8 = mDatabase.child(fireBaseChild)
                .child(KEY_LAMBI).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Lambi market = dataSnapshot.getValue(Lambi.class);
                        matchDataMap.put(KEY_LAMBI, market);
                        if (matchListener != null) {
                            matchListener.updateLambi(market);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_LAMBI, valueEventListener8);

        ValueEventListener valueEventListener9 = mDatabase.child(fireBaseChild)
                .child(KEY_RUNNING_MSG).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                       if (matchListener!= null && dataSnapshot.getValue()!=null){
                           matchListener.onMsgKeyReceived(dataSnapshot.getValue().toString());
                       }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put(KEY_RUNNING_MSG, valueEventListener9);

        ValueEventListener valueEventListener10 = mDatabase.child(fireBaseChild)
                .child(KEY_LIVE_URL).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (matchListener!= null && dataSnapshot.getValue()!=null){
                            matchListener.onLiveMatchUrlRecieved(dataSnapshot.getValue().toString());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        valueEventListeners.put("liveMatchUrlKey", valueEventListener10);


        childEventListener = mDatabase.child(fireBaseChild).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String dontUseThis) {
                switch (dataSnapshot.getKey()) {
                    case "currentInning":
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null) {
                            matchListener.onInningDataReceived(KEY_INNING_1, null);
                        }
                        break;
                    case "ballsRemaining":
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null)
                            matchListener.refreshTargetView();
                        break;
                    case "favouriteTeam":
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null) {
                            matchListener.updateData(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        }
                        break;
                    case "batsmanOneStatus":
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null) {
                            matchListener.updateData(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        }
                        break;
                    case "batsmanTwoStatus":
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null) {
                            matchListener.updateData(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        }
                        break;
                    case "bowlerStatus":
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null) {
                            matchListener.updateData(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        }
                        break;
                    case "localDescription":
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null) {
                            matchListener.updateData(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        }
                        break;
                    case "previousBall":
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null) {
                            matchListener.updateData(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        }
                        break;
                    case "androidAdFlag":
                        if (matchListener != null) {
                            matchListener.updateData(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        }
                        break;
                    case "sessionSuspend": {
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null) {
                            matchListener.updateData(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        }
                    }
                    case "lambiShow": {
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null) {
                            matchListener.updateData(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        }
                        break;
                    }
                    case "matchDay": {
                        matchStringMap.put(dataSnapshot.getKey(), dataSnapshot.getValue().toString());
                        if (matchListener != null)
                            matchListener.refreshTargetView();
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public String getMatchDay() {
        String matchDay = matchStringMap.get("matchDay");
        if (TextUtils.isEmpty(matchDay))
            return "-";
        return "" + matchDay;
    }

    public String getRunningMsgData(){
        return matchDataMap.get(AppConstants.RUNNING_MSG).toString();
    }

    private Session getCurrentSession() {
        Session session = (Session) matchDataMap.get(KEY_SESSION);
        return session;
    }

    public String getSessionRuns() {
        int sessionMaxScore = Integer.parseInt(getCurrentSession().sessionStatusTwo);
        int runs = Integer.parseInt(getCurrentInnings().score);

        String ballValue1 = String.valueOf(sessionMaxScore - runs);
        if (ballValue1.contains("-")) {
            ballValue1 = "0";
        }
        return ballValue1;
    }

    public String getSessionBalls() {

        int sessionOverBalls = Integer.parseInt(getCurrentSession().sessionOver) * 6;
        int currentBalls = getCurrentBalls();

        String ballValue2 = String.valueOf(sessionOverBalls - currentBalls);
        if (ballValue2.contains("-")) {
            ballValue2 = "0";
        }
        return ballValue2;
    }

    public String getLiveMatchKey(){
        return matchStringMap.get("liveKey");
    }

    public int getRemainingBalls() {
        String ballsRemaining = matchStringMap.get("ballsRemaining");
        try {
            return Integer.parseInt(ballsRemaining);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public double getCurrentRunRate() {
        int battingScore = Integer.parseInt(getCurrentInnings().score);
        double currentBalls = Double.parseDouble(String.valueOf(getCurrentBalls()));
        double curr1 = battingScore / currentBalls;
        double currentRunRate = (curr1 * 6);
        return currentRunRate;

    }

    public int getCurrentBalls() {
        String[] arr = String.valueOf(getCurrentInnings().over).split("\\.");
        if (arr.length > 1) {
            int overs = Integer.parseInt(arr[0]); // 1
            int oversBalls = Integer.parseInt(arr[1]); // 9

            double currentBalls = (overs * 6) + oversBalls;
            try {
                return (int) currentBalls;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            int overs = Integer.parseInt(arr[0]); // 1
            double currentBalls = (overs * 6);
            try {
                return (int) currentBalls;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }
    }

    public Inning getCurrentInnings() {
        String inningKey = matchStringMap.get("currentInning");

        Inning currentInning = (Inning) matchDataMap.get(inningKey);
        return currentInning;
    }

    public Inning getFirstInning() {
        return (Inning) matchDataMap.get("inning1");
    }

    public Inning getSecondInning() {
        return (Inning) matchDataMap.get("inning2");
    }

    public Inning getThirdInning() {
        return (Inning) matchDataMap.get("inning3");
    }

    public Inning getFourthInning() {
        return (Inning) matchDataMap.get("inning4");
    }

    public Inning getOtherInnings() {
        String inningKey = matchStringMap.get("currentInning");
        Inning currentInning;
        if (inningKey.equals("inning1"))
            currentInning = (Inning) matchDataMap.get("inning2");
        else
            currentInning = (Inning) matchDataMap.get("inning1");
        return currentInning;
    }

    public String getValue(String key) {
        String value = matchStringMap.get(key);
        return value;
    }

    public Team getBattingTeam() {
        String inningKey = matchStringMap.get("currentInning");

        Inning currentInning = (Inning) matchDataMap.get(inningKey);
        String battingKey = currentInning.batting;

        Team battingTeam = (Team) matchDataMap.get(battingKey);
        return battingTeam;
    }

    public Team getBowlingTeam() {
        String inningKey = matchStringMap.get("currentInning");

        Inning currentInning = (Inning) matchDataMap.get(inningKey);
        String bowlingKey = currentInning.bowling;

        Team bowlingTeam = (Team) matchDataMap.get(bowlingKey);
        return bowlingTeam;
    }

    public void notifyList(@Nullable final RecyclerView previousBallsRv) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (previousBallsRv != null) {
                        previousBallsRv.smoothScrollToPosition(previousBallsRv.getAdapter().getItemCount() - 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 200);
    }

    public interface MatchUpdateListener {
        void onMatchUpdate(String timestamp);
    }

    public interface MatchListener {
        void onConfigReceived(String keyConfig, Config config);

        void onMsgKeyReceived(String s);

        void onTeamDataReceived(String keyConfig, Team team);

        void onInningDataReceived(String keyConfig, Inning team);

        void refreshTargetView();

        void updateData(String key, String value);

        void onCurrentStatusReceived(CurrentStatus currentStatus, boolean isPlaySpeech);

        void updateMarket(Market market);

        void updateAll();

        void updateSession(Session session);

        void updateLambi(Lambi session);

        void onLiveMatchUrlRecieved(String s);
    }

    public Config getConfig() {
        Config config = (Config) matchDataMap.get(KEY_CONFIG);
        return config;
    }

    public void setCallBack(MatchListener matchListener) {
        this.matchListener = matchListener;
    }
    public void setLiveMatchListener(LiveMatchListener livematchListener) {
        this.liveMatchListener = livematchListener;
    }

    final DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");

    public void goOfline(){
        FirebaseDatabase.getInstance().goOffline();
    }

    public void goOnline(){
        FirebaseDatabase.getInstance().goOnline();
    }

}
