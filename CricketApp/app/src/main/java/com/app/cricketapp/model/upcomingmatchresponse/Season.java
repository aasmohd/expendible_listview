
package com.app.cricketapp.model.upcomingmatchresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Season implements Serializable {

    @SerializedName("name")
    @Expose
    public String name;

}
