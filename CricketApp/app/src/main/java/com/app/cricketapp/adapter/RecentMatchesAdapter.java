package com.app.cricketapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.cricketapp.R;
import com.app.cricketapp.activity.MatchDetailActivity;
import com.app.cricketapp.customview.CustomToast;
import com.app.cricketapp.model.newRecentMatchResponse.NewRecentMatchResponse;
import com.app.cricketapp.model.upcomingmatchresponse.Match;
import com.app.cricketapp.utility.AppConstants;
import com.app.cricketapp.utility.Utility;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;

import java.util.List;

/**
 * Created by rahulgupta on 27/08/17.
 */

public class RecentMatchesAdapter extends RecyclerView.Adapter<RecentMatchesAdapter.MyViewHolder> {

    private final Context context;
    private final NewRecentMatchResponse recentMatchResponse;
    private final int pagerPosition;

    public RecentMatchesAdapter(Context context,
                                NewRecentMatchResponse recentMatchResponse, int pagerPosition) {
        this.context = context;
        this.recentMatchResponse = recentMatchResponse;
        this.pagerPosition = pagerPosition;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pager, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        switch (pagerPosition) {
            case 0:
                List<Match> odiMatches = recentMatchResponse.getOdiMatches();
                holder.mMatchType.setImageResource(Utility.getDayOfWeek(Long.parseLong((odiMatches.
                        get(position).basicsInfo.getStartDate())) * 1000, AppConstants.TARGET_DATE_FORMAT_2));
                Utility.setImageWithUrl(odiMatches.get(position).teams.getB().getLogo(), R.drawable.default_image, holder.mTeamName2Iv);
                Utility.setImageWithUrl(odiMatches.get(position).teams.getA().getLogo(), R.drawable.default_image, holder.mTeamName1Iv);
                String utcTimeFormat = Utility.getLocalDate(Long.parseLong((odiMatches.get(position).basicsInfo.getStartDate())) * 1000, AppConstants.TARGET_DATE_FORMAT_2);

                String localTime2 = Utility.getLocalDate(Long.parseLong((odiMatches.get(position).basicsInfo.getStartDate())) * 1000, AppConstants.SERVER_DATE_FORMAT);
                String gmtTime = Utility.parseDateTimeUtcToGmt(localTime2);

                String localTime = Utility.getLocalDate(Long.parseLong((odiMatches.get(position).basicsInfo.getStartDate())) * 1000, AppConstants.SERVER_DATE_FORMAT1);

                String localTimeZone = Utility.getLocalTimeZone();
//                holder.mTimeTv.setText(utcTimeFormat + " " + localTime + " / " + gmtTime + " GMT");
                holder.mTimeTv.setText(utcTimeFormat + " " + localTime);

                holder.mTeamName1Tv.setText(odiMatches.get(position).teams.getA().getKey().toUpperCase());
                holder.mTeamName2Tv.setText(odiMatches.get(position).teams.getB().getKey().toUpperCase());
                holder.mMatchPositionTv.setText(odiMatches.get(position).names.getRelatedName());
                String place = odiMatches.get(position).basicsInfo.getVenue();
                String[] str = place.split(",");
                if (str.length > 2) {
                    String s = "";
                    for (int i = 0; i < str.length; i++) {
                        if (i < 2) {
                            if (s.isEmpty()) {
                                s = s + str[i];
                            } else {
                                s = s + ", " + str[i];
                            }
                        }
                    }

                    holder.mPlaceTv.setText(s);
                } else {
                    holder.mPlaceTv.setText(place);
                }

//                holder.mleauge.setText(odiMatches.get(position).season.name);
                holder.mleauge.setText(odiMatches.get(position).teams.getA().getName()+ " VS " +
                        odiMatches.get(position).teams.getB().getName());
                if (odiMatches.get(position).msgs.result!= null){
                    holder.mWinning_team.setVisibility(View.VISIBLE);
                    holder.mWinning_team.setText(odiMatches.get(position).msgs.result);
                }

                holder.card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (recentMatchResponse.getOdiMatches().get(position).results.score_card_available.equalsIgnoreCase("1")) {
                            Intent intent = new Intent(context, MatchDetailActivity.class);

                            intent.putExtra(AppConstants.EXTRA_KEY, recentMatchResponse.getOdiMatches().get(position).others.getKey());
                            intent.putExtra("match_type", 0);
                            intent.putExtra("match_category","recent");
                            context.startActivity(intent);
                        } else {
                            CustomToast.getInstance(context).showToast(R.string.no_scorecard);
                        }

                    }
                });
                break;
            case 1:
                List<Match> testMaches = recentMatchResponse.getTestMatche();
                holder.mMatchType.setImageResource(Utility.getDayOfWeek(Long.parseLong((testMaches.
                        get(position).basicsInfo.getStartDate())) * 1000, AppConstants.TARGET_DATE_FORMAT_2));
                Utility.setImageWithUrl(testMaches.get(position).teams.getB().getLogo(), R.drawable.default_image, holder.mTeamName2Iv);
                Utility.setImageWithUrl(testMaches.get(position).teams.getA().getLogo(), R.drawable.default_image, holder.mTeamName1Iv);
                utcTimeFormat = Utility.getLocalDate(Long.parseLong((testMaches.get(position).basicsInfo.getStartDate())) * 1000, AppConstants.TARGET_DATE_FORMAT_2);

                localTime2 = Utility.getLocalDate(Long.parseLong((testMaches.get(position).basicsInfo.getStartDate())) * 1000, AppConstants.SERVER_DATE_FORMAT);
                gmtTime = Utility.parseDateTimeUtcToGmt(localTime2);

                localTime = Utility.getLocalDate(Long.parseLong((testMaches.get(position).basicsInfo.getStartDate())) * 1000, AppConstants.SERVER_DATE_FORMAT1);

                localTimeZone = Utility.getLocalTimeZone();
//                holder.mTimeTv.setText(utcTimeFormat + " " + localTime + " / " + gmtTime + " GMT");
                holder.mTimeTv.setText(utcTimeFormat + " " + localTime );


                holder.mTeamName1Tv.setText(testMaches.get(position).teams.getA().getKey().toUpperCase());
                holder.mTeamName2Tv.setText(testMaches.get(position).teams.getB().getKey().toUpperCase());
                holder.mMatchPositionTv.setText(testMaches.get(position).names.getRelatedName());
                String place1 = testMaches.get(position).basicsInfo.getVenue();
                String[] str1 = place1.split(",");
                if (str1.length > 2) {
                    String s = "";
                    for (int i = 0; i < str1.length; i++) {
                        if (i < 2) {
                            if (s.isEmpty()) {
                                s = s + str1[i];
                            } else {
                                s = s + ", " + str1[i];
                            }
                        }
                    }

                    holder.mPlaceTv.setText(s);
                } else {
                    holder.mPlaceTv.setText(place1);
                }

//                holder.mleauge.setText(testMaches.get(position).season.name);
                holder.mleauge.setText(testMaches.get(position).teams.getA().getName()+ " VS " +
                        testMaches.get(position).teams.getB().getName());

                if (testMaches.get(position).msgs.result!= null){
                    holder.mWinning_team.setVisibility(View.VISIBLE);
                    holder.mWinning_team.setText(testMaches.get(position).msgs.result);
                }
                holder.card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (recentMatchResponse.getTestMatche().get(position).results.score_card_available.equalsIgnoreCase("1")) {
                            Intent intent = new Intent(context, MatchDetailActivity.class);

                            intent.putExtra(AppConstants.EXTRA_KEY, recentMatchResponse.getTestMatche().get(position).others.getKey());
                            intent.putExtra("match_type", 0);
                            intent.putExtra("match_category","recent");
                            context.startActivity(intent);
                        } else {
                            CustomToast.getInstance(context).showToast(R.string.no_scorecard);
                        }

                    }
                });

                break;
            case 2:

                List<Match> t20Matches = recentMatchResponse.getT20Matches();
                holder.mMatchType.setImageResource(Utility.getDayOfWeek(Long.parseLong((t20Matches.
                        get(position).basicsInfo.getStartDate())) * 1000, AppConstants.TARGET_DATE_FORMAT_2));
                Utility.setImageWithUrl(t20Matches.get(position).teams.getB().getLogo(), R.drawable.default_image, holder.mTeamName2Iv);
                Utility.setImageWithUrl(t20Matches.get(position).teams.getA().getLogo(), R.drawable.default_image, holder.mTeamName1Iv);
                utcTimeFormat = Utility.getLocalDate(Long.parseLong((t20Matches.get(position).basicsInfo.getStartDate())) * 1000, AppConstants.TARGET_DATE_FORMAT_2);

                localTime2 = Utility.getLocalDate(Long.parseLong((t20Matches.get(position).basicsInfo.getStartDate())) * 1000, AppConstants.SERVER_DATE_FORMAT);
                gmtTime = Utility.parseDateTimeUtcToGmt(localTime2);

                localTime = Utility.getLocalDate(Long.parseLong((t20Matches.get(position).basicsInfo.getStartDate())) * 1000, AppConstants.SERVER_DATE_FORMAT1);

                localTimeZone = Utility.getLocalTimeZone();
//                holder.mTimeTv.setText(utcTimeFormat + " " + localTime + " / " + gmtTime + " GMT");
                holder.mTimeTv.setText(utcTimeFormat + " " + localTime );

                holder.mTeamName1Tv.setText(t20Matches.get(position).teams.getA().getKey().toUpperCase());
                holder.mTeamName2Tv.setText(t20Matches.get(position).teams.getB().getKey().toUpperCase());
                holder.mMatchPositionTv.setText(t20Matches.get(position).names.getRelatedName());
                String place2 = t20Matches.get(position).basicsInfo.getVenue();
                String[] str2 = place2.split(",");
                if (str2.length > 2) {
                    String s = "";
                    for (int i = 0; i < str2.length; i++) {
                        if (i < 2) {
                            if (s.isEmpty()) {
                                s = s + str2[i];
                            } else {
                                s = s + ", " + str2[i];
                            }
                        }
                    }

                    holder.mPlaceTv.setText(s);
                } else {
                    holder.mPlaceTv.setText(place2);
                }

//                holder.mleauge.setText(t20Matches.get(position).season.name);
                holder.mleauge.setText(t20Matches.get(position).teams.getA().getName()+ " VS " +
                        t20Matches.get(position).teams.getB().getName());

                if (t20Matches.get(position).msgs.result!= null){
                    holder.mWinning_team.setVisibility(View.VISIBLE);
                    holder.mWinning_team.setText(t20Matches.get(position).msgs.result);
                }
                holder.card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (recentMatchResponse.getT20Matches().get(position).results.score_card_available.equalsIgnoreCase("1")) {
                            Intent intent = new Intent(context, MatchDetailActivity.class);

                            intent.putExtra(AppConstants.EXTRA_KEY, recentMatchResponse.getT20Matches().get(position).others.getKey());
                            intent.putExtra("match_type", 0);
                            intent.putExtra("match_category","recent");
                            context.startActivity(intent);
                        } else {
                            CustomToast.getInstance(context).showToast(R.string.no_scorecard);
                        }

                    }
                });

                break;
        }

        if (position % 4 == 0 && position != 0)
        {
            holder.adsContainer.setVisibility(View.VISIBLE);
            if (position % 4 == 0) {
                // Set its video options.
                holder.mAdView.setVideoOptions(new VideoOptions.Builder()
                        .setStartMuted(true)
                        .build());

                // The VideoController can be used to get lifecycle events and info about an ad's video
                // asset. One will always be returned by getVideoController, even if the ad has no video
                // asset.
                final VideoController mVideoController = holder.mAdView.getVideoController();
                mVideoController.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                    @Override
                    public void onVideoEnd() {
                        Log.d("Video", "Video playback is finished.");
                        super.onVideoEnd();
                    }
                });

                // Set an AdListener for the AdView, so the Activity can take action when an ad has finished
                // loading.
                holder.mAdView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        if (mVideoController.hasVideoContent()) {
                            Log.d("Video", "Received an ad that contains a video asset.");
                        } else {
                            Log.d("Video", "Received an ad that does not contain a video asset.");
                        }
                    }
                });

                holder.mAdView.loadAd(new AdRequest.Builder()
                        .addTestDevice("C11191FE7944B40640463F2686610772").build());
            }

        } else {
            holder.adsContainer.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        switch (pagerPosition) {
            case 0:
                return recentMatchResponse.getOdiMatches().size();

            case 1:
                return recentMatchResponse.getTestMatche().size();

            case 2:
                return recentMatchResponse.getT20Matches().size();
            default:
                return 0;
        }

    }

    class MyViewHolder extends RecyclerView.ViewHolder {


        RelativeLayout adsContainer;
        NativeExpressAdView mAdView;
        TextView mMatchPositionTv, mPlaceTv, mTimeTv, mTeamName1Tv, mTeamName2Tv, mleauge, mWinning_team;
        ImageView mTeamName1Iv, mTeamName2Iv, mMatchType;
        FrameLayout card;

        public MyViewHolder(View view) {
            super(view);
            mWinning_team = view.findViewById(R.id.winning_team);
            mleauge = view.findViewById(R.id.league);
            mMatchPositionTv = view.findViewById(R.id.match_position);
            mPlaceTv = view.findViewById(R.id.place_tv);
            mTimeTv = view.findViewById(R.id.time_tv);
            mTeamName1Tv = view.findViewById(R.id.team_name_1_tv);
            mTeamName2Tv = view.findViewById(R.id.team_name_2_tv);
            mTeamName1Iv = view.findViewById(R.id.team_name_1_iv);
            mTeamName2Iv = view.findViewById(R.id.team_name_2_iv);
            mMatchType = view.findViewById(R.id.match_type);
            card = view.findViewById(R.id.card);
            adsContainer = view.findViewById(R.id.ads_container);
            mAdView = view.findViewById(R.id.adView);

        }
    }
}
