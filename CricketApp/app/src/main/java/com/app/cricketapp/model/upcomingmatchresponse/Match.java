
package com.app.cricketapp.model.upcomingmatchresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Match implements Serializable {

    @SerializedName("basics_info")
    @Expose
    public BasicsInfo basicsInfo;
    @SerializedName("names")
    @Expose
    public Names names;
    @SerializedName("season")
    @Expose
    public Season season;
    @SerializedName("teams")
    @Expose
    public Teams teams;
    @SerializedName("others")
    @Expose
    public Others others;
    @SerializedName("msgs")
    @Expose
    public Msgs msgs;

    public Now now;
    public Results results;
    public Boolean showAd;


    public BasicsInfo getBasicsInfo() {
        return basicsInfo;
    }

    public void setBasicsInfo(BasicsInfo basicsInfo) {
        this.basicsInfo = basicsInfo;
    }

    public Names getNames() {
        return names;
    }

    public void setNames(Names names) {
        this.names = names;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Teams getTeams() {
        return teams;
    }

    public void setTeams(Teams teams) {
        this.teams = teams;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }

    public Msgs getMsgs() {
        return msgs;
    }

    public void setMsgs(Msgs msgs) {
        this.msgs = msgs;
    }

    public Now getNow() {
        return now;
    }

    public void setNow(Now now) {
        this.now = now;
    }

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

    public Boolean getShowAd() {
        return showAd;
    }

    public void setShowAd(Boolean showAd) {
        this.showAd = showAd;
    }

    public static class Now implements Serializable {

        public String innings, batting_team;
    }

    public static class Msgs implements Serializable {

        public String info;
        public String completed;
        public String result;
    }


    public static class Toss implements Serializable {
        public String won, str;
        public String decision;
    }

    public static class Results implements Serializable {
        public String winner_team, score_card_available;
        public Innings innings, other_innings;

        public Toss toss;
    }

    public static class Innings implements Serializable {
        public A a;
        public B b;

        public static class A implements Serializable {
            public String runs, balls, overs, wickets;


        }

        public static class B implements Serializable {
            public String runs, balls, overs, wickets;
        }
    }
}
