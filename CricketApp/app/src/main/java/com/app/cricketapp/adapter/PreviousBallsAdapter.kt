package com.app.cricketapp.adapter

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.cricketapp.R

/**
 * Created by rahul on 20-07-2017.
 */

class PreviousBallsAdapter(var watchLiveList: ArrayList<String>?)
    : RecyclerView.Adapter<PreviousBallsAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PreviousBallsAdapter.MyViewHolder {

        val view = LayoutInflater.from(parent?.context).inflate(R.layout.layout_previous_balls_row, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        var match = watchLiveList?.get(position)
        if (TextUtils.isEmpty(match))
            match = "-"

        holder?.runTv?.text = match
        holder?.runTv?.setBackgroundResource(R.drawable.shape_circle_green)

        if (match == "6"){
            holder?.runTv?.setTextColor(holder.runTv.resources.getColor(R.color.white_feffff))
            holder?.runTv?.setBackgroundResource(R.drawable.shape_circle_six)
        }else if (match == "4" ) {
            holder?.runTv?.setTextColor(holder.runTv.resources.getColor(R.color.white_feffff))
            holder?.runTv?.setBackgroundResource(R.drawable.shape_circle_blue)
        } else if (match == "0"){
            holder?.runTv?.setTextColor(holder.runTv.resources.getColor(R.color.white_feffff))
            holder?.runTv?.setBackgroundResource(R.drawable.shape_circle_gray)
        }else if (match != null) {
            if (match.toLowerCase().contains("w")) {
                holder?.runTv?.setBackgroundResource(R.drawable.shape_circle_red)
                holder?.runTv?.setTextColor(holder.runTv.resources.getColor(R.color.white_feffff))
            }
            if (match.toLowerCase().contains("n")) {
                holder?.runTv?.setBackgroundResource(R.drawable.shape_circle_extra)
                holder?.runTv?.setTextColor(holder.runTv.resources.getColor(R.color.white_feffff))
            }
            if (match.toLowerCase().contains("wd")) {
                holder?.runTv?.setBackgroundResource(R.drawable.shape_circle_extra)
                holder?.runTv?.setTextColor(holder.runTv.resources.getColor(R.color.white_feffff))
            }

        }else{

            holder?.runTv?.setBackgroundResource(R.drawable.shape_circle_green)
            holder?.runTv?.setTextColor(holder.runTv.resources.getColor(R.color.white_feffff))
        }

        val dimension = holder?.runTv?.context?.resources?.getDimension(R.dimen.margin_3)?.toInt()
        if (dimension != null) {
            holder.runTv.setPadding(dimension, dimension, dimension, dimension)
        }
    }

    override fun getItemCount(): Int {
        val size = watchLiveList?.size
        return size as Int
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var runTv: TextView = view.findViewById(R.id.run_tv)
    }
}
