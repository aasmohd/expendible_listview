
package com.app.cricketapp.model.scorecard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class TeamInfo implements Serializable {

    @SerializedName("key")
    @Expose
    public String key;
    @SerializedName("team_key")
    @Expose
    public String teamKey;
    @SerializedName("short_name")
    @Expose
    public String shortName;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("all_players")
    @Expose
    public ArrayList<Players> allPlayers = null;

}
