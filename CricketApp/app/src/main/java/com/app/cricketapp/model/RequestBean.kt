package com.app.cricketapp.model

/**
 * Created by bhavya on 02-06-2017.
 */
class RequestBean {
    var app_user_id: String? = null
    var deviceID: String? = null
    var deviceToken: String? = null
    var deviceTypeID: String? = null
    var currentVersion: String? = null
    var prediction_type: Long = 0
    var poll_id: String? = null
    var matchFormat: String? = null
    var playerType: String? = null
    var isTeam: String? = null
    var clientID: String? = null
    var clientSecret: String? = null
    var key: String? = null
    var appUserID:String? = null
    var batting_team: String? = null
    var over: String? = null
    var oder: String? = null
    var minBall: Long = 0
    var maxBall: Long = 0
}