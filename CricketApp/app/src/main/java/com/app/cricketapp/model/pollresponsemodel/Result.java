
package com.app.cricketapp.model.pollresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @SerializedName("prediction_stats")
    @Expose
    private PredictionStats predictionStats;
    @SerializedName("polls")
    @Expose
    private List<Poll> polls = null;

    public PredictionStats getPredictionStats() {
        return predictionStats;
    }

    public void setPredictionStats(PredictionStats predictionStats) {
        this.predictionStats = predictionStats;
    }

    public List<Poll> getPolls() {
        return polls;
    }

    public void setPolls(List<Poll> polls) {
        this.polls = polls;
    }

}