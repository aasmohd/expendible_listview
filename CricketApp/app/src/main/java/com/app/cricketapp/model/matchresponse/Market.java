package com.app.cricketapp.model.matchresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rahulgupta on 04/08/17.
 */

public class Market implements IMatch {
    @SerializedName("marketRateOne")
    @Expose
    public String marketRateOne;
    @SerializedName("marketRateTwo")
    @Expose
    public String marketRateTwo;
}
