
package com.app.cricketapp.model.ranking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestRanking {

    @SerializedName("testTeamRanking")
    @Expose
    public TestTeamRanking testTeamRanking;
    @SerializedName("testBatsmenRanking")
    @Expose
    public TestBatsmenRanking testBatsmenRanking;
    @SerializedName("testBowlerRanking")
    @Expose
    public TestBowlerRanking testBowlerRanking;
    @SerializedName("testAllRounderRanking")
    @Expose
    public TestAllRounderRanking testAllRounderRanking;

    public TestTeamRanking getTestTeamRanking() {
        return testTeamRanking;
    }

    public void setTestTeamRanking(TestTeamRanking testTeamRanking) {
        this.testTeamRanking = testTeamRanking;
    }

    public TestBatsmenRanking getTestBatsmenRanking() {
        return testBatsmenRanking;
    }

    public void setTestBatsmenRanking(TestBatsmenRanking testBatsmenRanking) {
        this.testBatsmenRanking = testBatsmenRanking;
    }

    public TestBowlerRanking getTestBowlerRanking() {
        return testBowlerRanking;
    }

    public void setTestBowlerRanking(TestBowlerRanking testBowlerRanking) {
        this.testBowlerRanking = testBowlerRanking;
    }

    public TestAllRounderRanking getTestAllRounderRanking() {
        return testAllRounderRanking;
    }

    public void setTestAllRounderRanking(TestAllRounderRanking testAllRounderRanking) {
        this.testAllRounderRanking = testAllRounderRanking;
    }

}
