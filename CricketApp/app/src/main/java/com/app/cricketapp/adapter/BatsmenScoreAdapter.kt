package com.app.cricketapp.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.model.scorecard.Batting
import java.util.*

/**
 * Created by bhavya on 12-04-2017.
 */

class BatsmenScoreAdapter(var context: Context, var watchLiveList: ArrayList<Batting>?)
    : RecyclerView.Adapter<BatsmenScoreAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BatsmenScoreAdapter.MyViewHolder {

        val view = LayoutInflater.from(context).inflate(R.layout.row_live_score_batting_team, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {

        if (position.rem(2) == 0) {
            holder?.itemView?.setBackgroundColor(Color.WHITE)
        } else {
            holder?.itemView?.setBackgroundColor(Color.parseColor("#F6F7F8"))
        }
        val match = watchLiveList?.get(position)
        holder?.name?.text = match?.fullname
        holder?.status?.text = match?.outStr
        holder?.runs?.text = match?.runs.toString()
        holder?.balls?.text = match?.balls.toString()
        holder?.fours?.text = match?.fours.toString()
        holder?.sixs?.text = match?.sixes.toString()
        holder?.strikeRate?.text = match?.strikeRate.toString()

    }

    override fun getItemCount(): Int {
        val size = watchLiveList?.size
        return size as Int
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.name)
        var status: TextView = view.findViewById(R.id.status)
        var runs: TextView = view.findViewById(R.id.runs)
        var balls: TextView = view.findViewById(R.id.balls)
        var fours: TextView = view.findViewById(R.id.fours)
        var sixs: TextView = view.findViewById(R.id.sixs)
        var strikeRate: TextView = view.findViewById(R.id.strike_rate)

    }
}
