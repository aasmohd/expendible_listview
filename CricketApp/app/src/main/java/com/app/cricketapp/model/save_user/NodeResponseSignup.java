package com.app.cricketapp.model.save_user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Prashant on 15-12-2017.
 */

public class NodeResponseSignup
{
    @SerializedName("status")
    @Expose
    private long status;
    @SerializedName("response")
    @Expose
    private SaveUserResponse response;
    @SerializedName("time")
    @Expose
    private long time;

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public SaveUserResponse getResponse() {
        return response;
    }

    public void setResponse(SaveUserResponse response) {
        this.response = response;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
