package com.app.cricketapp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.app.cricketapp.activity.BaseActivity;
import com.app.cricketapp.db.CLGDBHelper;
import com.app.cricketapp.db.dao.NewsListDAO;
import com.app.cricketapp.db.dao.RankingResponseDAO;
import com.app.cricketapp.db.dao.RecentResultDAO;
import com.app.cricketapp.db.dao.ScorecardResponseDAO;
import com.app.cricketapp.db.dao.UpcomingResultDAO;
import com.app.cricketapp.utility.Utility;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ApplicationController extends MultiDexApplication implements Application.ActivityLifecycleCallbacks {
    private static ApplicationController mApplicationInstance;
    private boolean mIsNetworkConnected;
    private Activity activity;
    private Typeface latoBold;
    private int randomNumberForAd = 3;

    public static ApplicationController getApplicationInstance() {
        if (mApplicationInstance == null)
            mApplicationInstance = new ApplicationController();
        return mApplicationInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        initializeDBDao();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Lato-Regular.ttf").setFontAttrId(R.attr.fontPath).build());//Roboto Condensed
        registerActivityLifecycleCallbacks(this);
        mApplicationInstance = this;
        mIsNetworkConnected = Utility.getNetworkState(this);
        getDefaultTracker();
    }

    public boolean isActivityVisible() {
        if (activity instanceof BaseActivity)
            return ((BaseActivity) activity).isActivityVisible();
        else
            return false;
    }

    public boolean isNetworkConnected() {
        return mIsNetworkConnected;
    }

    public void setIsNetworkConnected(boolean mIsNetworkConnected) {
        this.mIsNetworkConnected = mIsNetworkConnected;
    }

    public Typeface getLatoBold() {
        if (latoBold == null)
            latoBold = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");

        return latoBold;
    }

    synchronized public FirebaseAnalytics getDefaultTracker() {
        return FirebaseAnalytics.getInstance(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    public int getRandomNumberForAd() {
        return randomNumberForAd;
    }

    public void setRandomNumberForAd(int randomNumberForAd) {
        this.randomNumberForAd = randomNumberForAd;
    }
    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(getApplicationContext());
    }

    private void initializeDBDao() {
        CLGDBHelper.getdbInstance(getApplicationContext());
        RecentResultDAO.init(getApplicationContext());
        ScorecardResponseDAO.init(getApplicationContext());
        UpcomingResultDAO.init(getApplicationContext());
        RankingResponseDAO.init(getApplicationContext());
        NewsListDAO.init(getApplicationContext());

    }
}