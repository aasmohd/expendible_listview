package com.app.cricketapp.msupport

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import com.app.cricketapp.R
import java.util.*

/**
 * Class to handle M support logic
 * Created by Rahul on 28 Feb 2017.
 */

object MSupport {

    /**
     * @return true in case of M Device,
     * * false in case of below M devices
     */
    fun isMSupportDevice(ctx: Context): Boolean {
        return Build.VERSION.SDK_INT >= MSupportConstants.SDK_VERSION
    }


    /**
     * Method to check single permission with rationale

     * @param mActivity   Calling activity context
     * *
     * @param fragment    Calling fragment instance
     * *
     * @param permission  Permission to check
     * *
     * @param requestCode request code
     * *
     * @return true in case of permission is granted or pre marshmallow
     * * false in case of permission is not granted
     * * in case of false we have to request that permission
     */

    @TargetApi(23)
    fun checkPermissionWithRationale(mActivity: Activity, fragment: Fragment,
                                     permission: String, requestCode: Int): Boolean {

        if (MSupport.isMSupportDevice(mActivity)) {

            val permissions = ArrayList<String>()
            val hasPermission = mActivity.checkSelfPermission(permission)

            if (hasPermission != PackageManager.PERMISSION_GRANTED)
                if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission)) {

                    var msg = ""
                    if (requestCode == MSupportConstants.RC_GET_ACCOUNTS) {
                        msg = mActivity.getString(R.string.get_account_permission_rationale)
                    }

                    showMessageOKCancel(mActivity, msg,
                            DialogInterface.OnClickListener { dialog, which ->
                                permissions.add(permission)
                                requestPermissions(permissions, fragment, mActivity, requestCode)
                            })
                    return false

                } else {
                    permissions.add(permission)
                    requestPermissions(permissions, fragment, mActivity, requestCode)
                    return false
                }
            else
                return true

        }
        return true
    }

    /**
     * Method to request permission for activity or fragment

     * @param permissions Permissions to ask for
     * *
     * @param fragment    Fragment instance if fragment is requesting a permission
     * *
     * @param mActivity   Activity instance if activity is requesting a permission
     * *
     * @param requestCode Request code for asked permission
     * *
     * @return Flag if permission is already allowed
     */
    @TargetApi(23)
    private fun requestPermissions(permissions: List<String>, fragment: Fragment?,
                                   mActivity: Activity, requestCode: Int): Boolean {
        if (!permissions.isEmpty()) {
            if (fragment != null)
                fragment.requestPermissions(permissions.toTypedArray(),
                        requestCode)
            else
                mActivity.requestPermissions(permissions.toTypedArray(),
                        requestCode)
            return false
        } else
            return true
    }

    /**
     * Method to show rationale message in a dialog

     * @param activity   Activity object
     * *
     * @param message    Message to show
     * *
     * @param okListener Callback listener
     */
    private fun showMessageOKCancel(activity: Activity, message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(activity.getString(R.string.ok), okListener)
                .create()
                .show()
    }


}