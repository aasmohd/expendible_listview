package com.app.cricketapp.utility

import android.content.Context
import android.media.MediaPlayer
import android.text.TextUtils
import com.app.cricketapp.ApplicationController

import com.app.cricketapp.R

/**
 * Created by rahulgupta on 03/06/17.
 */

enum class CRICSounds {

    BALL_CHALU, DOT_BALL, DOUBLE_RUN, FOUR, SINGLE, SIX, THIRD_UMPIRE, THREE_RUNS, WICKET, WIDE_BALL, NO_BALL;

    private var mp: MediaPlayer? = null

    fun play(context: Context, reverted: String, fragmentPos: Int) {
        val isReverted: String
        if (TextUtils.isEmpty(reverted))
            isReverted = "0"
        else {
            isReverted = reverted
        }
        val speechFlag = Utility.getBooleanSharedPreference(context, AppConstants.SPEECH_KEY)
        val isMaleSpeechKey = Utility.getBooleanSharedPreference(context, AppConstants.IS_MALE_SPEECH_KEY)

        val currentFragmentPos = Utility.getIntFromSharedPreference(context, AppConstants.PREFS_MATCH_POS)
        if (currentFragmentPos == fragmentPos)
            if (speechFlag && isReverted == "0"
                    && ApplicationController.getApplicationInstance().isActivityVisible) {
                reset()
                when (this) {
                //TODO Free Hit scenario
                //TODO No Ball scenario
                    BALL_CHALU -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.chalu)
                        else
                            mp = MediaPlayer.create(context, R.raw.ballstart_female)
                        mp?.start()
                    }
                    DOT_BALL -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.khali)
                        else
                            mp = MediaPlayer.create(context, R.raw.dot_female)
                        mp?.start()
                    }
                    DOUBLE_RUN -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.double_run)
                        else
                            mp = MediaPlayer.create(context, R.raw.double_female)
                        mp?.start()
                    }
                    FOUR -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.chauka)
                        else
                            mp = MediaPlayer.create(context, R.raw.four_female)
                        mp?.start()
                    }
                    SINGLE -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.single)
                        else
                            mp = MediaPlayer.create(context, R.raw.single_female)
                        mp?.start()
                    }
                    SIX -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.six)
                        else
                            mp = MediaPlayer.create(context, R.raw.six_female)
                        mp?.start()
                    }
                    THIRD_UMPIRE -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.thirdumpire)
                        else
                            mp = MediaPlayer.create(context, R.raw.thirdumpire_female)
                        mp?.start()
                    }
                    THREE_RUNS -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.teen)
                        else
                            mp = MediaPlayer.create(context, R.raw.triple_female)
                        mp?.start()
                    }
                    WICKET -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.wicket)
                        else
                            mp = MediaPlayer.create(context, R.raw.wicket_female)
                        mp?.start()
                    }
                    WIDE_BALL -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.wide)
                        else
                            mp = MediaPlayer.create(context, R.raw.wide_female)
                        mp?.start()

                    }
                    NO_BALL -> {
                        if (isMaleSpeechKey)
                            mp = MediaPlayer.create(context, R.raw.noball)
                        else
                            mp = MediaPlayer.create(context, R.raw.noball_female)
                        mp?.start()
                    }
                }
            }
    }

    private fun reset() {
        if (mp != null) {
            mp?.reset()
        }
    }
}
