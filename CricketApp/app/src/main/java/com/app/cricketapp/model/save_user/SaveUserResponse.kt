package com.app.cricketapp.model.save_user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SaveUserResponse {

    @SerializedName("status")
    @Expose
    var status: Long = 0
    @SerializedName("messages")
    @Expose
    var messages: String? = null
    @SerializedName("result")
    @Expose
    var result: Result? = null

}
