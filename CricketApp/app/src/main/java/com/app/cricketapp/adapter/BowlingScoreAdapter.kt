package com.app.cricketapp.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.model.scorecard.Bowling
import java.util.*

/**
 * Created by bhavya on 12-04-2017.
 */

class BowlingScoreAdapter(var context: Context, var watchLiveList: ArrayList<Bowling>?)
    : RecyclerView.Adapter<BowlingScoreAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BowlingScoreAdapter.MyViewHolder {

        val view = LayoutInflater.from(context).inflate(R.layout.row_live_score_bowling, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {

        if (position.rem(2) == 0) {
            holder?.itemView?.setBackgroundColor(Color.WHITE)
        } else {
            holder?.itemView?.setBackgroundColor(Color.parseColor("#F6F7F8"))
        }
        val match = watchLiveList?.get(position)
        holder?.name?.text = match?.fullname
        holder?.overs?.text = match?.overs
        holder?.maidenOvers?.text = match?.maidenOvers.toString()
        holder?.runs?.text = match?.runs.toString()
        holder?.wicket?.text = match?.wickets.toString()

        holder?.nb?.text = match?.extras.toString()
        holder?.wb?.text = match?.extras.toString()
        holder?.er?.text = match?.economy.toString()

    }

    override fun getItemCount(): Int {
        val size = watchLiveList?.size
        return size as Int
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.name)
        var overs: TextView = view.findViewById(R.id.overs)
        var maidenOvers: TextView = view.findViewById(R.id.maiden)
        var runs: TextView = view.findViewById(R.id.runs)
        var wicket: TextView = view.findViewById(R.id.wicket)

        var nb: TextView = view.findViewById(R.id.no_balls)
        var wb: TextView = view.findViewById(R.id.wide_balls)
        var er: TextView = view.findViewById(R.id.economy_rate)

    }
}
