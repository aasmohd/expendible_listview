package com.app.cricketapp.activity

import android.support.v7.widget.LinearLayoutManager
import com.app.cricketapp.R
import com.app.cricketapp.adapter.SeasonDetailAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.model.RequestBean
import com.app.cricketapp.model.seasonmodels.seasonDetail.SeasonDetailModel
import com.app.cricketapp.model.seasonmodels.seasonDetail.SeasonMatch
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import kotlinx.android.synthetic.main.activity_watch_live.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by Prashant on 06-10-2017.
 */
class SeasonDetailActivity : BaseActivity() {

    var mSeasonAdapter: SeasonDetailAdapter? = null
    var manager: LinearLayoutManager? = null
    val seasonMatchList: MutableList<SeasonMatch> = ArrayList<SeasonMatch>()


    override fun initUi() {
        setBackButtonEnabled()
        mSeasonAdapter = SeasonDetailAdapter(seasonMatchList,this)
        var matchKey = intent.getStringExtra(AppConstants.EXTRA_KEY)
        mTitleTv?.text = intent.getStringExtra("title")
        if (Utility.isNetworkAvailable(this)) {
            //call Api
            callSeasonDetailAPI(matchKey)
        } else {
            showProgressBar(false)
            CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
        }
        manager = LinearLayoutManager(getActivity())
        recycler_view.layoutManager = manager
        recycler_view.setHasFixedSize(true)
        recycler_view.adapter = mSeasonAdapter
        initFullScreenAdd()
        loadBannerAd()
    }

    private fun callSeasonDetailAPI(matchKey: String) {
        showProgressBar(true)
        val apiService = AppRetrofit.getInstance().apiServices
        val request = RequestBean()
        request.key = matchKey
        val call = apiService.seasonDetail(request)
        call.enqueue(object : Callback<SeasonDetailModel> {
            override fun onResponse(call: Call<SeasonDetailModel>?, response: Response<SeasonDetailModel>?) {
                showProgressBar(false)
                var detailResponse = response?.body()
                if (detailResponse?.status == 1L) {
                    seasonMatchList.addAll(detailResponse.result?.matches!!)
                    mSeasonAdapter?.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<SeasonDetailModel>?, t: Throwable?) {
                showProgressBar(false)
            }

        })
    }

    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()

                }
            }
            requestNewInterstitial()
        }
    }

    private fun loadBannerAd() {
        val adRequest: AdRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .addTestDevice("06CD7F1190497B9ADD39C03F2D21C717")
                .build()

        mInterstitialAd.loadAd(adRequest)
    }


    override fun getLayoutById(): Int {
        return R.layout.activity_watch_live
    }
}
