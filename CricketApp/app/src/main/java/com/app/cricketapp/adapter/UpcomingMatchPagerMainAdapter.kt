package com.app.cricketapp.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.app.cricketapp.R
import com.app.cricketapp.model.upcomingmatchresponse.NewUpcomingMatchResponse
import kotlinx.android.synthetic.main.row_upcoming_matches_new.view.*

/**
 * Created by Prashant on 11-09-2017.
 */
class UpcomingMatchPagerMainAdapter(var context: Context,var  upcomingMatchResponse: NewUpcomingMatchResponse):PagerAdapter() {
    var mWatchLiveAdapter: UpcomingMatchesAdapter? = null
    var manager: LinearLayoutManager? = null

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val itemView = LayoutInflater.from(context).inflate(R.layout.row_upcoming_matches_new, container, false)
        when(position){
            0->{
                if(upcomingMatchResponse.odiMatches != null){
                    mWatchLiveAdapter = UpcomingMatchesAdapter(context, upcomingMatchResponse,0)
                    mWatchLiveAdapter?.notifyDataSetChanged()
                    if (upcomingMatchResponse.odiMatches.size == 0) {
                        itemView.empty_tv.visibility = View.VISIBLE
                    } else {
                        itemView.empty_tv.visibility = View.GONE

                    }
                }

            }
            1->{
                if (upcomingMatchResponse.testMatche!= null){
                    mWatchLiveAdapter = UpcomingMatchesAdapter(context, upcomingMatchResponse,1)
                    mWatchLiveAdapter?.notifyDataSetChanged()

                    if (upcomingMatchResponse.testMatche.size == 0) {
                        itemView.empty_tv.visibility = View.VISIBLE
                    } else {
                        itemView.empty_tv.visibility = View.GONE

                    }
                }

            }
            2->{
                if (upcomingMatchResponse.odiMatches!= null){
                    mWatchLiveAdapter = UpcomingMatchesAdapter(context, upcomingMatchResponse,2)
                    mWatchLiveAdapter?.notifyDataSetChanged()

                    if (upcomingMatchResponse.t20Matches.size == 0) {
                        itemView.empty_tv.visibility = View.VISIBLE
                    } else {
                        itemView.empty_tv.visibility = View.GONE

                    }
                }

            }
        }
        manager = LinearLayoutManager(context)
        itemView.recycler_view.layoutManager = manager
        itemView.recycler_view.setHasFixedSize(true)
        itemView.recycler_view.adapter = mWatchLiveAdapter


        container?.addView(itemView)
        return itemView
    }

    override fun getCount(): Int {
       return 3
    }
    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as LinearLayout)
    }

    override fun getItemPosition(`object`: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence {
        when(position){
            0->{
                return "ODI"
            }
            1->{
                return "Test"
            }
            2->{
                return "T20"
            }
        }
        return super.getPageTitle(position)
    }
}