package com.app.cricketapp.fragment

import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.app.cricketapp.ApplicationController
import com.app.cricketapp.R
import com.app.cricketapp.adapter.PreviousBallsAdapter
import com.app.cricketapp.databinding.FragmentMatchLineBinding
import com.app.cricketapp.model.matchresponse.MatchBean
import com.app.cricketapp.shake.Techniques
import com.app.cricketapp.shake.YoYo
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.firebase.database.*
import com.google.gson.Gson


/**
 * Created by bhavya on 13-04-2017.
 */

//open class MatchLineFragment : BaseFragment() {
//    var dataBinding: FragmentMatchLineBinding? = null
//    var matchBean: MatchBean? = null
//    val mDatabase: DatabaseReference = FirebaseDatabase.getInstance().reference
//    var mInterstitialAd: InterstitialAd? = null
//
//    override fun getLayoutById(): Int {
//        return R.layout.fragment_match_line
//    }
//
//
//    override fun initUi() {
//        mInterstitialAd = InterstitialAd(context)
//        dataBinding = viewDataBinding as FragmentMatchLineBinding?
//        try {
//            matchBean = arguments.getSerializable(AppConstants.MATCH_RATE_BEAN) as MatchBean?
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//
//        mInterstitialAd?.adUnitId = getString(R.string.interstitial_ad_unit_id)
//
//        mInterstitialAd?.adListener = object : AdListener() {
//
//            override fun onAdLeftApplication() {
//                super.onAdLeftApplication()
//                Lg.i("Add", "add left application")
//            }
//
//            override fun onAdFailedToLoad(i: Int) {
//                super.onAdFailedToLoad(i)
//                requestNewInterstitial()
//                Lg.i("Add", "failed :- " + i)
//            }
//
//            override fun onAdClosed() {
//                super.onAdClosed()
//                requestNewInterstitial()
//                Lg.i("Add", "closed")
//            }
//
//            override fun onAdOpened() {
//                super.onAdOpened()
//                Lg.i("Add", "opened")
//            }
//
//            override fun onAdLoaded() {
//                super.onAdLoaded()
//                Lg.i("Add", "loaded")
//
//            }
//        }
//        requestNewInterstitial()
//        mDatabase.child(AppConstants.FIREBASE_CHILD2).addChildEventListener(childEventListener)
//        mDatabase.child(AppConstants.FIREBASE_CHILD2).addValueEventListener(valueEventListener)
//
//        dataBinding?.previousBallsRv?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//
//        dataBinding?.tv?.setTvListener {}
//
//        /*val width = (resources.displayMetrics.widthPixels * 0.25) / 2
//        dataBinding?.targetTopLeft?.setPadding(width.toInt(), 0, 0, 0)
//        dataBinding?.targetTopRight?.setPadding(width.toInt(), 0, 0, 0)
//        dataBinding?.targetBottomLeft?.setPadding(width.toInt(), 0, 0, 0)
//        dataBinding?.targetBottomRight?.setPadding(width.toInt(), 0, 0, 0)*/
//
//        setData(matchBean, true)
//
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        mDatabase.child(AppConstants.FIREBASE_CHILD2)?.removeEventListener(childEventListener)
//        mDatabase.child(AppConstants.FIREBASE_CHILD2)?.removeEventListener(valueEventListener)
//    }
//
//    override fun onStop() {
//        super.onStop()
//        mDatabase.child(AppConstants.FIREBASE_CHILD2)?.removeEventListener(childEventListener)
//        mDatabase.child(AppConstants.FIREBASE_CHILD2)?.removeEventListener(valueEventListener)
//    }
//    val childEventListener = object : ChildEventListener {
//        override fun onCancelled(p0: DatabaseError?) {
//        }
//
//        override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
//        }
//
//        override fun onChildChanged(dataSnapshot: DataSnapshot?, p1: String?) {
//            Lg.i("Child", "DataSnapshot :" + dataSnapshot.toString())
//            when (dataSnapshot?.key) {
//                "sessionSuspend" -> {
//                    val suspended = dataSnapshot.value.toString()
//                    if (suspended == "1") {
//                        dataBinding?.suspendedTv?.visibility = View.VISIBLE
//                    } else {
//                        dataBinding?.suspendedTv?.visibility = View.GONE
//                    }
//                }
//                "favouriteTeam" -> {
//                    dataBinding?.teamNameTv?.text = dataSnapshot.value.toString()
//                    if (dataSnapshot.value.toString() != "-")
//                        YoYo.with(Techniques.Swing).playOn(dataBinding?.teamNameTv)
//                }
//                "markerRateOne" -> {
//                    setMarketRateOne(dataSnapshot.value.toString())
//                }
//
//                "markerRateTwo" -> {
//                    matchBean?.markerRateTwo = dataSnapshot.value.toString()
//                    setMarketRateTwo(dataSnapshot.value.toString(), true)
//                }
//
//                "lambiN" -> {
//                    dataBinding?.lambi1Iv?.text = dataSnapshot.value.toString()
//                }
//                "lambiY" -> {
//                    matchBean?.lambiY = dataSnapshot.value.toString()
//                    dataBinding?.lambi2Iv?.text = matchBean?.lambiY.toString()
//                }
//
//                "sessionStatusOne" -> {
//                    dataBinding?.runValue1Iv?.text = dataSnapshot.value.toString()
//                }
//                "sessionStatusTwo" -> {
//                    matchBean?.sessionStatusTwo = dataSnapshot.value.toString()
//                    dataBinding?.runValue2Iv?.text = matchBean?.sessionStatusTwo2.toString()
//                }
//
//                "androidAdFlag" -> {
//                    if (ApplicationController.getApplicationInstance().isActivityVisible)
//                        showFullScreenAdd(dataSnapshot.value.toString())
//                }
//                "matchStatus" -> {
//                    val matchStatus = dataSnapshot.value.toString()
//                    Log.i("matchStatus", matchStatus)
//                    matchBean?.matchStatus = matchStatus
//                }
//                "currentStatus" -> {
//                    val toJson = Gson().toJson(dataSnapshot.value)
//                    val currentStatus = Gson().fromJson(toJson, MatchBean.CurrentStatus::class.java)
//                    matchBean?.currentStatus = currentStatus
//                    dataBinding?.tv?.setTvData(matchBean)
//                }
//                "lambiShow" -> {
//                    checkLambiShow(dataSnapshot.value.toString())
//                }
//                "previousBall" -> {
//
//                }
//            }
//        }
//
//        override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
//        }
//
//        override fun onChildRemoved(p0: DataSnapshot?) {
//        }
//    }
//
//    val valueEventListener = object : ValueEventListener {
//        override fun onCancelled(p0: DatabaseError?) {
//        }
//
//        override fun onDataChange(dataSnapshot: DataSnapshot?) {
//            try {
//                matchBean = dataSnapshot?.getValue(MatchBean::class.java)
//                Lg.i("HomeScreen", Gson().toJson(matchBean))
//                setData(matchBean, false)
//
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }
//
//    }
//
//    fun requestNewInterstitial() {
//        val adRequest = AdRequest.Builder()
//                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
//                .build()
//
//        mInterstitialAd?.loadAd(adRequest)
//    }
//
//    fun setMarketRateOne(value: String?) {
//        dataBinding?.marketRate1Iv?.text = value
//    }
//
//    fun setMarketRateTwo(value: String?, isVibrate: Boolean) {
//        dataBinding?.marketRate2Iv?.text = value
//        if (isVibrate) {
//            YoYo.with(Techniques.Pulse).playOn(dataBinding?.marketRate1Iv)
//            YoYo.with(Techniques.Pulse).playOn(dataBinding?.marketRate2Iv)
//        }
//    }
//
//    override fun onResume() {
//        super.onResume()
//        checkSpeechFlag()
//    }
//
//    fun checkSpeechFlag() {
//        val speechFlag = Utility.getBooleanSharedPreference(activity, AppConstants.SPEECH_KEY)
//        if (speechFlag) {
//            dataBinding?.tv?.setAction2Icon(R.drawable.speaker_on)
//        } else {
//            dataBinding?.tv?.setAction2Icon(R.drawable.speaker_off)
//        }
//    }
//
//
//    fun setData(matchBean: MatchBean?, isInitial: Boolean) {
//        if (matchBean != null) {
//
//            checkLambiShow(matchBean.lambiShow)
//            dataBinding?.tv?.setMatchName(matchBean)
//
//            setTargetView()
//
//            if (isInitial) {
//                val speechSetting = Utility.getBooleanSharedPreference(activity, AppConstants.SPEECH_KEY)
//                Utility.putBooleanValueInSharedPreference(activity, AppConstants.SPEECH_KEY, false)
//                dataBinding?.tv?.setTvData(matchBean)
//                Utility.putBooleanValueInSharedPreference(activity, AppConstants.SPEECH_KEY, speechSetting)
//
//                if (matchBean.sessionSuspend == "1") {
//                    dataBinding?.suspendedTv?.visibility = View.VISIBLE
//                } else {
//                    dataBinding?.suspendedTv?.visibility = View.GONE
//                }
//            }
//            setMarketRateTwo(matchBean.markerRateTwo, false)
//
//            dataBinding?.lambi1Iv?.text = matchBean.lambiN.toString()
//            dataBinding?.lambi2Iv?.text = matchBean.lambiY.toString()
//
//            dataBinding?.teamNameTv?.text = matchBean.favouriteTeam
//
//
//            dataBinding?.overValueTv?.text = matchBean.getSessionOver().toString()
//
//            dataBinding?.runValue1Iv?.text = matchBean.sessionStatusOne
//            dataBinding?.runValue2Iv?.text = matchBean.sessionStatusTwo2.toString()
//
//            setSessionBalls()
//
//            dataBinding?.batmenNameTv?.text = matchBean.batsmanOneStatus
//            dataBinding?.batmenName2Tv?.text = matchBean.batsmanTwoStatus
//            dataBinding?.bowlerNameTv?.text = matchBean.bowlerStatus
//            if (TextUtils.isEmpty(matchBean.localDescription)) {
//                dataBinding?.descriptionTv?.visibility = View.GONE
//            } else
//                dataBinding?.descriptionTv?.text = matchBean.localDescription
//
//            setPreviousRuns()
//            val prevRunsList = ArrayList<String>()
//
//            val split = matchBean.previousBall?.split(",")
//
//            if (matchBean.matchStatus?.toLowerCase() == "notstarted") {
//                prevRunsList.add("-")
//            } else {
//                if (split != null && split.isNotEmpty()) {
//                    split.indices.mapTo(prevRunsList) { split[it] }
//                } else {
//                    prevRunsList.add(matchBean.previousBall.toString())
//                }
//            }
//            dataBinding?.previousBallsRv?.adapter = PreviousBallsAdapter(prevRunsList)
//
//            loadBannerAd()
//        }
//
//    }
//
//    private fun setPreviousRuns() {
//
//
//    }
//
//    private fun checkLambiShow(lambiShow: String?) {
//        if (!TextUtils.isEmpty(lambiShow)) {
//            if (lambiShow == "1") {
//                dataBinding?.lambiLv?.visibility = View.VISIBLE
//                dataBinding?.lambiSeparator?.visibility = View.VISIBLE
//            } else {
//                dataBinding?.lambiLv?.visibility = View.GONE
//                dataBinding?.lambiSeparator?.visibility = View.GONE
//            }
//        } else {
//            dataBinding?.lambiLv?.visibility = View.GONE
//            dataBinding?.lambiSeparator?.visibility = View.GONE
//        }
//
//    }
//
//    fun showFullScreenAdd(androidFlag: String) {
//        if (androidFlag == "1") {
//            if (mInterstitialAd?.isLoaded!!) {
//                mInterstitialAd?.show()
//            }
//        }
//    }
//
//    fun setSessionBalls() {
//        //Runs
//        val sessionMaxScore = matchBean?.sessionStatusTwo2 as Int
//        val runs = matchBean?.getRuns() as Int
//        var ballValue1 = sessionMaxScore.minus(runs).toString()
//        if (ballValue1.contains("-")) {
//            ballValue1 = "0"
//        }
//        dataBinding?.ballValue1Iv?.text = ballValue1
//
//        //Balls
//        val sessionOverBalls = matchBean?.getSessionOver()?.times(6) as Int
//        val currentBalls = matchBean?.getCurrentBalls() as Int
//        var ballValue2 = sessionOverBalls.minus(currentBalls).toString()
//        if (ballValue2.contains("-")) {
//            ballValue2 = "0"
//        }
//        dataBinding?.ballValue2Iv?.text = ballValue2
//    }
//
//    /**
//     * Method to set data to target score
//     */
//    fun setTargetView() {
//        if (matchBean?.getTarget() == 0) {
//            dataBinding?.targetParent?.visibility = View.GONE
//            dataBinding?.targetSeparator?.visibility = View.GONE
//
//            dataBinding?.runRateLabelTv?.setText(R.string.run_rate)
//            //( total runs / total balls ) * 6
//            val currentRunRate: Double
//            if (matchBean?.getCurrentBalls() == 0) {
//                currentRunRate = 0.0
//            } else {
//                val battingScore = matchBean?.getBattingScore() as Int
//                val currentBalls = (matchBean?.getCurrentBalls() as Int).toDouble()
//                val curr1 = (battingScore.div(currentBalls))
//                currentRunRate = (curr1 * 6)
//            }
//            if (currentRunRate > 0)
//                dataBinding?.runRateValueTv?.text = String.format("%.2f", currentRunRate)
//            else
//                dataBinding?.runRateValueTv?.text = "-"
//
//        } else {
//            dataBinding?.runRateLabelTv?.setText(R.string.r_r_r)
//            dataBinding?.targetParent?.visibility = View.VISIBLE
//            dataBinding?.targetSeparator?.visibility = View.VISIBLE
//
//            dataBinding?.targetValueTv?.text = matchBean?.getTarget().toString()
//            val target = matchBean?.getTarget() as Int
//            val bowlingScore = matchBean?.getBowlingScore() as Int
//            val remainingRuns = target - bowlingScore
//
//            if (remainingRuns < 1)
//                dataBinding?.runsValueTv?.text = "-"
//            else
//                dataBinding?.runsValueTv?.text = remainingRuns.toString()
//
//            val remainingBalls = matchBean?.getBallsRemaining() as Int
//
//            if (remainingBalls == 0) {
//                dataBinding?.runRateValueTv?.text = "-"
//            } else {
//                if (remainingRuns < 1) {
//                    dataBinding?.runRateValueTv?.text = "-"
//                } else {
//                    val curr1 = remainingRuns.div(remainingBalls.toDouble())
//                    val currentRunRate = (curr1 * 6)
//                    dataBinding?.runRateValueTv?.text = String.format("%.2f", currentRunRate)
//                }
//            }
//
//        }
//        if (matchBean?.getBallsRemaining().toString() == "0")
//            dataBinding?.ballsValueTv?.text = "-"
//        else
//            dataBinding?.ballsValueTv?.text = matchBean?.getBallsRemaining().toString()
//    }
//
//    fun loadBannerAd() {
//        val adRequest = AdRequest.Builder().build()
//        dataBinding?.adView?.loadAd(adRequest)
//    }
//}