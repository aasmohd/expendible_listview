package com.app.cricketapp.adapter

import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.support.v4.view.PagerAdapter
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.activity.NewMatchRateActivity
import com.app.cricketapp.model.matchresponse.MatchRoom
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import de.hdodenhof.circleimageview.CircleImageView
import org.greenrobot.eventbus.EventBus

/**
 * Created by Rahul on 27-07-2017.
 */
class HomeMatchAdapter(var context: Context, var matchList: java.util.ArrayList<MatchRoom>) : PagerAdapter() {

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return matchList.size
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val match = matchList[position]
        val itemView = LayoutInflater.from(context).inflate(R.layout.layout_home_match_row, null)
        var matchListN = java.util.ArrayList<MatchRoom>()


        val team1Name: TextView = itemView.findViewById(R.id.team_name_1_tv)
        val team1Logo: CircleImageView = itemView.findViewById(R.id.team_image_1_iv)

        val team2Name: TextView = itemView.findViewById(R.id.team_name_2_tv)
        val team2Logo: CircleImageView = itemView.findViewById(R.id.team_image_2_iv)

        val status: TextView = itemView.findViewById(R.id.status_tv)
        val matchTimer: TextView = itemView.findViewById(R.id.seconds_tv)


        team1Name.text = match.team1
        Utility.setImageWithUrl(match.team_1_logo, R.drawable.default_image, team1Logo)

        team2Name.text = match.team2
        Utility.setImageWithUrl(match.team_2_logo, R.drawable.default_image, team2Logo)

        startTimer(status, matchTimer, match)

        container?.addView(itemView)

        itemView.setOnClickListener { v ->
            val intent = Intent(v.context, NewMatchRateActivity::class.java)
            matchListN.clear()
            matchListN.add(match)
            intent.putParcelableArrayListExtra(AppConstants.EXTRA_MATCHES_ARRAY, matchListN)
            intent.putExtra(AppConstants.EXTRA_POS, position)
            v.context.startActivity(intent)
        }

        EventBus.getDefault().post(true)

        return itemView
    }

    override fun getItemPosition(`object`: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as LinearLayout)
    }

    var mCountDownTimer: CountDownTimer? = null
    private fun startTimer(statusTv: TextView, secondsTv: TextView,
                           matchBean: MatchRoom) {
        statusTv.text = ""
        if (!TextUtils.isEmpty(matchBean.landing_text) && matchBean.landing_text != "0") {
            statusTv.text = matchBean.landing_text
            secondsTv.visibility = View.GONE
        } else {

            if (!TextUtils.isEmpty(matchBean.match_started_time)) {
                val parseDateTimeLocal = Utility.parseDateTimeLocal(matchBean.match_started_time, "dd/MM/yyyy HH:mm")

                secondsTv.visibility = View.GONE
                val currentMillis = System.currentTimeMillis()
                val futureMillis = parseDateTimeLocal.time - currentMillis

                mCountDownTimer = object : CountDownTimer(futureMillis, 1000) {
                    override fun onFinish() {
                    }

                    override fun onTick(millisUntilFinished: Long) {

                        val seconds = millisUntilFinished / 1000
                        val hr = seconds / 3600
                        val rem = seconds % 3600
                        val mn = rem / 60
                        val sec = rem % 60

                        var hrStr = ""
                        if (hr < 10) {
                            hrStr = "0" + hr
                        } else {
                            hrStr = "" + hr
                        }

                        var mnStr = ""
                        if (mn < 10) {
                            mnStr = "0" + mn
                        } else {
                            mnStr = "" + mn
                        }

                        var secStr = ""
                        if (sec < 10) {
                            secStr = "0" + sec
                        } else {
                            secStr = "" + sec
                        }
                        if (!TextUtils.isEmpty(matchBean.landing_text) && matchBean.landing_text != "0") {
                            statusTv.text = matchBean.landing_text
                            secondsTv.visibility = View.GONE
                        } else {
                            val matchStarted = matchBean.match_started.toLowerCase()
                            if (matchStarted == "finished") {
                                statusTv.text = context.getString(R.string.finished)
                                secondsTv.visibility = View.GONE
                            } else if (matchStarted == "playing"
                                    || matchStarted == "started") {
                                statusTv.text = context.getString(R.string.playing_now)
                                secondsTv.visibility = View.GONE
                            } else if (matchStarted == "notstarted") {
                                secondsTv.visibility = View.VISIBLE
                                statusTv.text = "Match starts in $hrStr:$mnStr"
                                secondsTv.text = ":$secStr"
                            } else {
                                statusTv.text = ""
                                secondsTv.visibility = View.GONE
                            }
                        }

                    }

                }
                mCountDownTimer?.start()
            } else {
                val matchStarted = matchBean.match_started.toLowerCase()
                if (matchStarted == "finished") {
                    statusTv.text = context.getString(R.string.finished)
                    secondsTv.visibility = View.GONE
                } else if (matchStarted == "playing"
                        || matchStarted == "started") {
                    statusTv.text = context.getString(R.string.playing_now)
                    secondsTv.visibility = View.GONE
                } else {
                    statusTv.text = ""
                    secondsTv.visibility = View.GONE
                }
            }

        }
    }

}