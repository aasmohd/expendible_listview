package com.app.cricketapp.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.TextRoundCornerProgressBar;
import com.app.cricketapp.R;
import com.app.cricketapp.model.pollresponsemodel.PredictionStats;
import com.app.cricketapp.utility.Utility;

/**
 * Created by rahulgupta on 28/07/17.
 */

public class PollAnalysisDialog extends Dialog {
    private final PredictionStats predictionStats;

    public PollAnalysisDialog(Context context, PredictionStats predictionStats) {
        super(context);
        this.predictionStats = predictionStats;
    }

    private Float calculatePercentage(Long totalVotes, Long optionVotes) {
        if (totalVotes == 0)
            return 0f;
        float l = ((optionVotes) / totalVotes.floatValue()) * 100;
        return l;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.my_prediction_history);
        Utility.modifyDialogBoundsToFill(this);

        findViewById(R.id.root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        findViewById(R.id.view_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        setCancelable(true);
        setCanceledOnTouchOutside(true);
        ((TextView) findViewById(R.id.question_text)).setText("You have taken part in polls " + predictionStats.getTotalPolls() + " times. This is how you performed.");

        Float correctPercentage = calculatePercentage(predictionStats.getTotalPolls(), predictionStats.getCorrectCount());
        Float inCorrectPercentage = calculatePercentage(predictionStats.getTotalPolls(), predictionStats.getCorrectCount());

        TextRoundCornerProgressBar progress1 = findViewById(R.id.progress_1);
        TextRoundCornerProgressBar progress2 = findViewById(R.id.progress_2);
        TextRoundCornerProgressBar progress3 = findViewById(R.id.progress_3);

        TextView ques1Perc = findViewById(R.id.progress_text_1);
        TextView ques2Perc = findViewById(R.id.progress_text_2);
        TextView ques3Perc = findViewById(R.id.progress_text_3);


        progress1.setProgress(correctPercentage);
        ques1Perc.setText(Math.round(correctPercentage) + "%");
        progress1.setRadius(15);


        progress2.setProgress(inCorrectPercentage);
        ques2Perc.setText(Math.round(inCorrectPercentage) + "%");
        progress2.setRadius(15);

        if (100 - (correctPercentage + inCorrectPercentage) > 0) {
            progress3.setProgress(100 - (correctPercentage + inCorrectPercentage));
            ques3Perc.setText(Math.round(100 - (correctPercentage + inCorrectPercentage)) + "%");
            progress3.setRadius(15);
        } else {
            findViewById(R.id.pending).setVisibility(View.GONE);
        }

    }
}
