package com.app.cricketapp.model.upcomingmatchresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Teams : Serializable {

    @SerializedName("a")
    @Expose
    var a: A? = null
    @SerializedName("b")
    @Expose
    var b: B? = null

    inner class A : Serializable {

        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("key")
        @Expose
        var key: String? = null
        @SerializedName("logo")
        @Expose
        var logo: String? = null


    }

    inner class B : Serializable {

        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("key")
        @Expose
        var key: String? = null
        @SerializedName("logo")
        @Expose
        var logo: String? = null

    }

}
