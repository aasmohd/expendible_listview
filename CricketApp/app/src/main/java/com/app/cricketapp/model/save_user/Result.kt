package com.app.cricketapp.model.save_user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Result {

    @SerializedName("userInfo")
    @Expose
    var userInfo: UserInfo? = null
    @SerializedName("texts")
    @Expose
    var texts: Texts? = null

    var tokenInfo: TokenInfo? = null
    var feedback: FeedBack? = null
    var invitation: Invitation? = null
    var versionInfo: VersionInfo? = null
    var apiInfo: ApiInfo? = null
}

class TokenInfo {
    var token_type: String? = null
    var access_token: String? = null
    var refresh_token: String? = null
}

class FeedBack {
    var email: String? = null
    var subject: String? = null
    var message: String? = null
}

class Invitation {
    var message: String? = null
    var downloadUrl: String? = null
}


class VersionInfo {
    var latestVersion: Double? = 0.0
    var forceUpdate: Int? = 0
    var updateAvailable:Int? = 0
}
class ApiInfo{
    @SerializedName("_id")
    @Expose
    var id: String? = null
    var news: Long = 0
    var season: Long = 0
    var match: Long = 0
    var ranking: Long = 0
}
