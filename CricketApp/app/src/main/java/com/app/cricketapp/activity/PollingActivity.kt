package com.app.cricketapp.activity

import android.content.Intent
import android.support.v4.view.ViewPager
import android.view.View
import com.app.cricketapp.R
import com.app.cricketapp.adapter.PollingResultPagerAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.databinding.ActivityPollingBinding
import com.app.cricketapp.dialogs.PollAnalysisDialog
import com.app.cricketapp.model.RequestBean
import com.app.cricketapp.model.pollresponsemodel.Poll
import com.app.cricketapp.model.pollresponsemodel.PollResponse
import com.app.cricketapp.model.pollresponsemodel.PredictionStats
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import kotlinx.android.synthetic.main.activity_polling.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Prashant on 27-07-2017.
 */
class PollingActivity : BaseActivity(), PollingResultPagerAdapter.PollListener {
    var activityPollingBinding: ActivityPollingBinding? = null
    var pollingResultAdapter: PollingResultPagerAdapter? = null
    var predictionStats: PredictionStats? = null

    override fun initUi() {
        activityPollingBinding = viewDataBinding as ActivityPollingBinding?
        setBackButtonEnabled()
        mTitleTv?.text="POLLS / PREDICTION"

        if (Utility.isNetworkAvailable(this)) {
            showProgressBar(true)
            getPollData()
        } else {
            showProgressBar(false)
            CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
        }
        activityPollingBinding?.pollAccuracy?.visibility = View.GONE
        activityPollingBinding?.pollAccuracy?.setOnClickListener {
//            if (predictionStats != null) {
//                if (predictionStats!!.totalPolls > 0) {
//                    val dialog = PollAnalysisDialog(getActivity(), predictionStats)
//                    dialog.show()
//                } else {
//                    CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
//                }
//            } else {
//                if (Utility.isNetworkAvailable(this)) {
//                    showProgressBar(true)
//                    getPollData()
//                } else {
//                    showProgressBar(false)
//                    CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
//                }
//            }

            startActivity(Intent(this@PollingActivity,PollAccuracyActivity::class.java))
        }
        loadBannerAd()
        initFullScreenAdd()
    }

    fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        adView2.loadAd(adRequest)
    }

    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()

                }
            }
            requestNewInterstitial()
        }
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .addTestDevice("06CD7F1190497B9ADD39C03F2D21C717")
                .build()

        mInterstitialAd.loadAd(adRequest)
    }

    private fun getPollData() {
        val appUserId: String = Utility.getStringSharedPreference(this, AppConstants.APP_USERID)
        var request = RequestBean()
        request.appUserID = appUserId
        val call: Call<PollResponse> = AppRetrofit.getInstance().apiServices.pollData(request)
        call.enqueue(object : Callback <PollResponse> {
            override fun onFailure(call: Call<PollResponse>?, t: Throwable?) {
                showProgressBar(false)
            }

            override fun onResponse(call: Call<PollResponse>?, response: Response<PollResponse>?) {
                showProgressBar(false)

                predictionStats = response?.body()?.result?.predictionStats
                try {
                    val pollList: ArrayList<Poll> = response?.body()?.result?.polls as ArrayList<Poll>
                    if (pollList.isEmpty()) {
                        activityPollingBinding?.emptyTv?.visibility = View.VISIBLE
                    } else {
                        activityPollingBinding?.emptyTv?.visibility = View.GONE
                    }

                    if (predictionStats!!.totalPolls > 0) {
                        activityPollingBinding?.pollAccuracy?.visibility = View.VISIBLE
                    } else {
                        activityPollingBinding?.pollAccuracy?.visibility = View.GONE
                    }

                    if (pollList.size > 1) {
                        activityPollingBinding?.pollingIndicator?.visibility = View.VISIBLE
                    } else {
                        activityPollingBinding?.pollingIndicator?.visibility = View.GONE
                    }

                    pollingResultAdapter = PollingResultPagerAdapter(getActivity(), pollList)
                    activityPollingBinding?.pollingViewPager?.adapter = pollingResultAdapter
                    activityPollingBinding?.pollingIndicator?.setViewPager(activityPollingBinding?.pollingViewPager)

                    activityPollingBinding?.pollingViewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                        override fun onPageScrollStateChanged(state: Int) {
                        }

                        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                        }

                        override fun onPageSelected(position: Int) {
                            activityPollingBinding?.pollingViewPager?.reMeasureCurrentPage(position)
                        }

                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                    activityPollingBinding?.emptyTv?.visibility = View.VISIBLE
                }

            }
        })
    }

    override fun getLayoutById(): Int {
        return R.layout.activity_polling
    }

    override fun predictPoll(position: Int, requestBean: RequestBean) {
        if (Utility.isNetworkAvailable(this)) {
            showProgressDialog(true)
            val apiService = AppRetrofit.getInstance().apiServices
            val call = apiService.predictPoll(requestBean)
            call.enqueue(object : Callback<PollResponse> {

                override fun onResponse(call: Call<PollResponse>?, response: Response<PollResponse>?) {
                    showProgressDialog(false)
                    val userResponse = response!!.body()
//                    val userResponse = Gson().fromJson<PollResponse>(response.toString(),PollResponse::class.java)
                    predictionStats = userResponse.result.predictionStats
                    pollingResultAdapter?.pollList?.set(position, userResponse.result?.polls!![0])
                    pollingResultAdapter?.notifyDataSetChanged()


                    if (predictionStats!!.totalPolls > 0) {
                        activityPollingBinding?.pollAccuracy?.visibility = View.VISIBLE
                    } else {
                        activityPollingBinding?.pollAccuracy?.visibility = View.GONE
                    }
                }

                override fun onFailure(call: Call<PollResponse>?, t: Throwable?) {
                    showProgressDialog(false)
                }
            })
        } else {
            showProgressDialog(false)
            CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
        }


    }

}