package com.app.cricketapp.utility;


import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;

/**
 * Created by bhavya on 19-04-2017.
 */

public class CustomTextToSpeech extends TextToSpeech {
    public CustomTextToSpeech(Context context, OnInitListener listener) {
        super(context, listener);
    }

    public CustomTextToSpeech(Context context, OnInitListener listener, String engine) {
        super(context, listener, engine);
    }

    public int speakCustom(CharSequence text, int queueMode, Bundle params, String utteranceId) {
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return super.speak(text, queueMode, params, utteranceId);
        } else {
            return super.speak(text.toString(), TextToSpeech.QUEUE_ADD, null);
        }*/
        return 0;
    }
}
