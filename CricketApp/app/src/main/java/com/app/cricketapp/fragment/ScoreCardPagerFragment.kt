package com.app.cricketapp.fragment

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.databinding.FragmentScoreCardPagerBinding
import com.app.cricketapp.interfaces.FragmentDataCallback
import com.app.cricketapp.model.scorecard.ScorecardResponse
import com.app.cricketapp.model.upcomingmatchresponse.Match
import com.app.cricketapp.utility.AppConstants

/**
 * Created by bhavya on 12-04-2017.
 */

class ScoreCardPagerFragment : BaseFragment() {

    var matchBean: Match? = null
    var tabs: TabLayout? = null
    var pager: ViewPager? = null
    var fragmentDataCallback: FragmentDataCallback? = null
    var binding: FragmentScoreCardPagerBinding? = null
    var scoreCardResponse: ScorecardResponse? = null

    override fun initUi() {
        binding = viewDataBinding as FragmentScoreCardPagerBinding?
        tabs = binding?.tabLayout
        pager = binding?.viewPager

        fragmentDataCallback = activity as FragmentDataCallback
        matchBean = fragmentDataCallback?.bean
        scoreCardResponse = fragmentDataCallback?.myResponse
        parseScoreCard(scoreCardResponse)
    }


    private inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<BaseFragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment = mFragmentList[position]

        override fun getCount(): Int = mFragmentList.size


        fun addFragments(fragment: ArrayList<BaseFragment>, title: ArrayList<String>) {
            mFragmentList.addAll(fragment)
            mFragmentTitleList.addAll(title)
        }

        override fun getPageTitle(position: Int): CharSequence = mFragmentTitleList[position]

    }

    fun parseScoreCard(scorecardResponse: ScorecardResponse?) {

        try {


            val fragmentList = ArrayList<BaseFragment>()
            val fragmentTabsList = ArrayList<String>()

            val scorecardArrayList = scorecardResponse?.result?.scorecard
            if (scorecardArrayList != null) {
                for (i in scorecardArrayList.indices) {
                    val scoreCard = scorecardArrayList[i]
                    if (i == 0) {
                        //First innings
                        if (scoreCard.inningInfo.inningStartedBy == "a") {
                            //Team a batting first
                            val a = matchBean?.results?.innings?.a
                            val teamAScore = matchBean?.teams?.a?.key + "(" + a?.runs + "/" + a?.wickets + ")"

                            fragmentTabsList.add(teamAScore)

                            var bundle = Bundle()
                            bundle.putSerializable(AppConstants.EXTRA_KEY, scoreCard)
                            var scorecardFragment = ScorecardFragment()
                            bundle.putSerializable(AppConstants.EXTRA_POS, "a")
                            scorecardFragment.arguments = bundle

                            fragmentList.add(scorecardFragment)


                            //Team b batting second
                            val b = matchBean?.results?.innings?.b
                            val teamBScore = matchBean?.teams?.b?.key + "(" + b?.runs + "/" + b?.wickets + ")"
                            fragmentTabsList.add(teamBScore)

                            bundle = Bundle()
                            bundle.putSerializable(AppConstants.EXTRA_KEY, scoreCard)
                            bundle.putSerializable(AppConstants.EXTRA_POS, "b")
                            scorecardFragment = ScorecardFragment()
                            scorecardFragment.arguments = bundle

                            fragmentList.add(scorecardFragment)

                        } else {
                            //Team b batting first
                            val b = matchBean?.results?.innings?.b
                            val teamBScore = matchBean?.teams?.b?.key + "(" + b?.runs + "/" + b?.wickets + ")"
                            fragmentTabsList.add(teamBScore)

                            var bundle = Bundle()
                            bundle.putSerializable(AppConstants.EXTRA_KEY, scoreCard)
                            bundle.putSerializable(AppConstants.EXTRA_POS, "b")
                            var scorecardFragment = ScorecardFragment()
                            scorecardFragment.arguments = bundle

                            fragmentList.add(scorecardFragment)


                            //Team a batting second
                            val a = matchBean?.results?.innings?.a
                            val teamAScore = matchBean?.teams?.a?.key + "(" + a?.runs + "/" + a?.wickets + ")"

                            fragmentTabsList.add(teamAScore)

                            bundle = Bundle()
                            bundle.putSerializable(AppConstants.EXTRA_KEY, scoreCard)
                            bundle.putSerializable(AppConstants.EXTRA_POS, "a")
                            scorecardFragment = ScorecardFragment()
                            scorecardFragment.arguments = bundle

                            fragmentList.add(scorecardFragment)
                        }
                    } else if (i == 1) {
                        //Second innings. Probably a test match
                        if (scoreCard.teams != null) {
                            if (scoreCard.inningInfo.inningStartedBy == "a") {
                                //Team a batting first
                                var bundle = Bundle()
                                var scorecardFragment = ScorecardFragment()

                                if (scoreCard.teams.a.batting!= null && !scoreCard.teams.a.batting.isEmpty()) {
                                    val a = matchBean?.results?.other_innings?.a
                                    val teamAScore = matchBean?.teams?.a?.key + "(" + a?.runs + "/" + a?.wickets + ")"

                                    fragmentTabsList.add(teamAScore)

                                    bundle.putSerializable(AppConstants.EXTRA_KEY, scoreCard)
                                    bundle.putSerializable(AppConstants.EXTRA_POS, "a")

                                    scorecardFragment.arguments = bundle

                                    fragmentList.add(scorecardFragment)
                                }

                                //Team b batting second
                                if (scoreCard.teams.b.batting!= null && !scoreCard.teams.b.batting.isEmpty()) {


                                    val b = matchBean?.results?.other_innings?.b
                                    val teamBScore = matchBean?.teams?.b?.key + "(" + b?.runs + "/" + b?.wickets + ")"
                                    fragmentTabsList.add(teamBScore)
                                    bundle = Bundle()
                                    bundle.putSerializable(AppConstants.EXTRA_KEY, scoreCard)
                                    bundle.putSerializable(AppConstants.EXTRA_POS, "b")
                                    scorecardFragment = ScorecardFragment()
                                    scorecardFragment.arguments = bundle

                                    fragmentList.add(scorecardFragment)
                                }
                            } else {
                                //Team a batting second
                                var bundle = Bundle()
                                var scorecardFragment = ScorecardFragment()
                                if (scoreCard.teams.b.batting!= null && !scoreCard.teams.b.batting.isEmpty()) {

                                    val b = matchBean?.results?.other_innings?.b
                                    val teamBScore = matchBean?.teams?.b?.key + "(" + b?.runs + "/" + b?.wickets + ")"
                                    fragmentTabsList.add(teamBScore)


                                    bundle.putSerializable(AppConstants.EXTRA_KEY, scoreCard)
                                    bundle.putSerializable(AppConstants.EXTRA_POS, "b")

                                    scorecardFragment.arguments = bundle

                                    fragmentList.add(scorecardFragment)

                                }
                                //Team a batting first
                                if (scoreCard.teams.a.batting!= null && !scoreCard.teams.a.batting.isEmpty()) {
                                    val a = matchBean?.results?.other_innings?.a
                                    if (a != null) {
                                        val teamAScore = matchBean?.teams?.a?.key + "(" + a.runs + "/" + a.wickets + ")"

                                        fragmentTabsList.add(teamAScore)

                                        bundle = Bundle()
                                        bundle.putSerializable(AppConstants.EXTRA_KEY, scoreCard)
                                        bundle.putSerializable(AppConstants.EXTRA_POS, "a")
                                        scorecardFragment = ScorecardFragment()
                                        scorecardFragment.arguments = bundle

                                        fragmentList.add(scorecardFragment)
                                    }

                                }
                            }

                        }

                    }
                }

            }


            val mViewPagerAdapter = ViewPagerAdapter(childFragmentManager)

            mViewPagerAdapter.addFragments(fragmentList, fragmentTabsList)

            if (matchBean?.others?.format == "test") {
                tabs?.tabMode = TabLayout.MODE_SCROLLABLE
            } else {
                tabs?.tabMode = TabLayout./*MODE_FIXED*/MODE_SCROLLABLE
            }
            pager?.adapter = mViewPagerAdapter
            tabs?.setupWithViewPager(pager)
            applyFontedTab(pager, tabs)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun applyFontedTab(pager: ViewPager?, tabLayout: TabLayout?) {

        val count = pager?.adapter?.count?.minus(1) as Int
        for (i in 0..count) {
            val v = LayoutInflater.from(mContext).inflate(R.layout.layout_scorecard_tab, null)
            val iv = v.findViewById<TextView>(R.id.tab_tv)
            val pageTitle = pager.adapter?.getPageTitle(i)
            iv.text = pageTitle
            if (i == 0)
                v.isSelected = true
            tabLayout?.getTabAt(i)!!.customView = v
        }

    }


    override fun getLayoutById(): Int = R.layout.fragment_score_card_pager

}
