
package com.app.cricketapp.model.seasonmodels.seasonDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Result implements Serializable
{

    @SerializedName("matches")
    @Expose
    private List<SeasonMatch> matches = null;

    public List<SeasonMatch> getMatches() {
        return matches;
    }

    public void setMatches(List<SeasonMatch> matches) {
        this.matches = matches;
    }

}
