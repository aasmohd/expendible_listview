
package com.app.cricketapp.model.newRecentMatchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class A_ {

    @SerializedName("runs")
    @Expose
    private long runs;
    @SerializedName("balls")
    @Expose
    private long balls;
    @SerializedName("overs")
    @Expose
    private String overs;
    @SerializedName("wickets")
    @Expose
    private long wickets;

    public long getRuns() {
        return runs;
    }

    public void setRuns(long runs) {
        this.runs = runs;
    }

    public long getBalls() {
        return balls;
    }

    public void setBalls(long balls) {
        this.balls = balls;
    }

    public String getOvers() {
        return overs;
    }

    public void setOvers(String overs) {
        this.overs = overs;
    }

    public long getWickets() {
        return wickets;
    }

    public void setWickets(long wickets) {
        this.wickets = wickets;
    }

}
