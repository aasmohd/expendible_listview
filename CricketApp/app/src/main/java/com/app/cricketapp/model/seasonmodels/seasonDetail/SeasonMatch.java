package com.app.cricketapp.model.seasonmodels.seasonDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Prashant on 10-10-2017.
 */

public class SeasonMatch {

    @SerializedName("match_key")
    @Expose
    private String matchKey;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("related_name")
    @Expose
    private String relatedName;
    @SerializedName("short_name")
    @Expose
    private String shortName;
    @SerializedName("format")
    @Expose
    private String format;
    @SerializedName("venue")
    @Expose
    private String venue;
    @SerializedName("team_a")
    @Expose
    private TeamDetail teamA;
    @SerializedName("team_b")
    @Expose
    private TeamDetail teamB;
    @SerializedName("winner_team")
    @Expose
    private TeamDetail winnerTeam;
    @SerializedName("summaryAvailabel")
    @Expose
    private int summaryAvailabel;
    @SerializedName("start_date")
    @Expose
    private Long startDate;

    @SerializedName("description")
    @Expose
    private String description;

    public String getMatchKey() {
        return matchKey;
    }

    public void setMatchKey(String matchKey) {
        this.matchKey = matchKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelatedName() {
        return relatedName;
    }

    public void setRelatedName(String relatedName) {
        this.relatedName = relatedName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public TeamDetail getTeamA() {
        return teamA;
    }

    public void TeamDetail(TeamDetail teamA) {
        this.teamA = teamA;
    }

    public TeamDetail getTeamB() {
        return teamB;
    }

    public void setTeamB(TeamDetail teamB) {
        this.teamB = teamB;
    }

    public TeamDetail getWinnerTeam() {
        return winnerTeam;
    }

    public void setWinnerTeam(TeamDetail winnerTeam) {
        this.winnerTeam = winnerTeam;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public int getSummaryAvailabel()
    {
        return summaryAvailabel;
    }

    public void setSummaryAvailabel(int summaryAvailabel)
    {
        this.summaryAvailabel = summaryAvailabel;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
