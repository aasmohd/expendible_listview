package com.app.cricketapp.model.matchresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rahulgupta on 04/08/17.
 */

public class Session implements IMatch {
    @SerializedName("sessionOver")
    @Expose
    public String sessionOver;
    @SerializedName("sessionStatusOne")
    @Expose
    public String sessionStatusOne;
    @SerializedName("sessionStatusTwo")
    @Expose
    public String sessionStatusTwo;
}
