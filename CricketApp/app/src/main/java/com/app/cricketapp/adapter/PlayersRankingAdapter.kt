package com.app.cricketapp.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.model.ranking.RankingResponse

/**
 * Created by Sunaina on 21-08-2017.
 */

class PlayersRankingAdapter(internal var context: Context, internal var dataValue: Int,
                            internal var rankingResponse: RankingResponse, internal var tabPosition: Int)
    : RecyclerView.Adapter<PlayersRankingAdapter.PlayerRankHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PlayerRankHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_players_rank_list,
                parent, false)
        return PlayerRankHolder(view)
    }

    override fun onBindViewHolder(holder: PlayerRankHolder?, position: Int) {

        if (position.rem(2) == 0) {
            holder?.itemView?.setBackgroundColor(Color.WHITE)
        } else {
            holder?.itemView?.setBackgroundColor(Color.parseColor("#F6F7F8"))
        }

        when (tabPosition) {
            0 -> {
                when (dataValue) {
                    0 -> {
                        holder?.rank?.text = rankingResponse.result.rankings.odiRanking.odiBatsmenRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.odiRanking.odiBatsmenRanking.data[position].playerName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.odiRanking.odiBatsmenRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.GONE
                    }
                    1 -> {

                        holder?.rank?.text = rankingResponse.result.rankings.odiRanking.odiBowlerRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.odiRanking.odiBowlerRanking.data[position].playerName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.odiRanking.odiBowlerRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.GONE
                    }
                    2 -> {

                        holder?.rank?.text = rankingResponse.result.rankings.odiRanking.odiAllRounderRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.odiRanking.odiAllRounderRanking.data[position].playerName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.odiRanking.odiAllRounderRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.GONE
                    }
                    3 -> {

                        holder?.rank?.text = rankingResponse.result.rankings.odiRanking.odiTeamRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.odiRanking.odiTeamRanking.data[position].teamName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.odiRanking.odiTeamRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.VISIBLE
                        holder?.rating?.text = rankingResponse.result.rankings.odiRanking.odiTeamRanking.data[position].rating.toString()
                    }
                }
            }
            1 -> {
                when (dataValue) {
                    0 -> {

                        holder?.rank?.text = rankingResponse.result.rankings.testRanking.testBatsmenRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.testRanking.testBatsmenRanking.data[position].playerName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.testRanking.testBatsmenRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.GONE
                    }
                    1 -> {

                        holder?.rank?.text = rankingResponse.result.rankings.testRanking.testBowlerRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.testRanking.testBowlerRanking.data[position].playerName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.testRanking.testBowlerRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.GONE
                    }
                    2 -> {
                        holder?.rank?.text = rankingResponse.result.rankings.testRanking.testAllRounderRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.testRanking.testAllRounderRanking.data[position].playerName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.testRanking.testAllRounderRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.GONE
                    }
                    3 -> {

                        holder?.rank?.text = rankingResponse.result.rankings.testRanking.testTeamRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.testRanking.testTeamRanking.data[position].teamName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.testRanking.testTeamRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.VISIBLE
                        holder?.rating?.text = rankingResponse.result.rankings.testRanking.testTeamRanking.data[position].rating.toString()
                    }
                }
            }
            2 -> {
                when (dataValue) {
                    0 -> {

                        holder?.rank?.text = rankingResponse.result.rankings.t20Ranking.t20BatsmenRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.t20Ranking.t20BatsmenRanking.data[position].playerName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.t20Ranking.t20BatsmenRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.GONE

                    }
                    1 -> {

                        holder?.rank?.text = rankingResponse.result.rankings.t20Ranking.t20BowlerRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.t20Ranking.t20BowlerRanking.data[position].playerName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.t20Ranking.t20BowlerRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.GONE
                    }
                    2 -> {

                        holder?.rank?.text = rankingResponse.result.rankings.t20Ranking.t20AllRounderRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.t20Ranking.t20AllRounderRanking.data[position].playerName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.t20Ranking.t20AllRounderRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.GONE
                    }
                    3 -> {

                        holder?.rank?.text = rankingResponse.result.rankings.t20Ranking.t20TeamRanking.data[position].position.toString()
                        holder?.name?.text = rankingResponse.result.rankings.t20Ranking.t20TeamRanking.data[position].teamName.toString()
                        holder?.points?.text = rankingResponse.result.rankings.t20Ranking.t20TeamRanking.data[position].points.toString()
                        holder?.rating?.visibility = View.VISIBLE
                        holder?.rating?.text =  rankingResponse.result.rankings.t20Ranking.t20TeamRanking.data[position].rating.toString()
                    }
                }
            }
        }

    }

    override fun getItemCount(): Int {
        when (tabPosition) {
            0 -> {
                when (dataValue) {
                    0 -> {
                        return rankingResponse.result.rankings.odiRanking.odiBatsmenRanking.data.size
                    }
                    1 -> {
                        return rankingResponse.result.rankings.odiRanking.odiBowlerRanking.data.size
                    }
                    2 -> {
                        return rankingResponse.result.rankings.odiRanking.odiAllRounderRanking.data.size
                    }
                    3 -> {
                        return rankingResponse.result.rankings.odiRanking.odiTeamRanking.data.size
                    }
                }
            }
            1 -> {
                when (dataValue) {
                    0 -> {
                        return rankingResponse.result.rankings.testRanking.testBatsmenRanking.data.size
                    }
                    1 -> {
                        return rankingResponse.result.rankings.testRanking.testBowlerRanking.data.size

                    }
                    2 -> {
                        return rankingResponse.result.rankings.testRanking.testAllRounderRanking.data.size

                    }
                    3 -> {
                        return rankingResponse.result.rankings.testRanking.testTeamRanking.data.size

                    }
                }
            }
            2 -> {
                when (dataValue) {
                    0 -> {
                        return rankingResponse.result.rankings.t20Ranking.t20BatsmenRanking.data.size

                    }
                    1 -> {
                        return rankingResponse.result.rankings.t20Ranking.t20BowlerRanking.data.size

                    }
                    2 -> {
                        return rankingResponse.result.rankings.t20Ranking.t20AllRounderRanking.data.size

                    }
                    3 -> {
                        return rankingResponse.result.rankings.t20Ranking.t20TeamRanking.data.size

                    }
                }
            }
        }
        return 0
    }

    class PlayerRankHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var rank: TextView = itemView.findViewById<TextView>(R.id.tv_player_rank)
        var name: TextView = itemView.findViewById<TextView>(R.id.tv_player_name)
        var points: TextView = itemView.findViewById<TextView>(R.id.tv_player_points)
        var rating: TextView = itemView.findViewById<TextView>(R.id.tv_player_rating)

    }

}
