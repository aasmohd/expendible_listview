package com.app.cricketapp.model.eve;

import com.app.cricketapp.model.matchresponse.MatchRoom;

import java.util.ArrayList;

/**
 * Created by rahulgupta on 22/08/17.
 */

public class RefreshLineScreen {
    public ArrayList<MatchRoom> matchArrayList;

    public RefreshLineScreen(ArrayList<MatchRoom> matchArrayList) {
        this.matchArrayList = matchArrayList;
    }
}
