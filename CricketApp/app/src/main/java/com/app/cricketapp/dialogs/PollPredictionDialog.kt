package com.app.cricketapp.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import android.widget.TextView
import com.akexorcist.roundcornerprogressbar.TextRoundCornerProgressBar
import com.app.cricketapp.R
import com.app.cricketapp.model.pollresponsemodel.PredictionStats
import com.app.cricketapp.utility.Utility


/**
 * Poll prediction dialog
 */
internal class PollPredictionDialog(context: Context, private val predictionStats: PredictionStats?) : Dialog(context) {

    private fun calculatePercentage(totalVotes: Long, optionVotes: Long): Float {
        if (totalVotes.toInt() == 0)
            return 0f
        val l = ((optionVotes) / totalVotes.toFloat()) * 100
        return l
    }

    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        window!!.requestFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.my_prediction_history)
        Utility.modifyDialogBoundsToFill(this)


        findViewById<TextView>(R.id.question_text).text = "You have taken part in polls ${predictionStats?.totalPolls} times. This is how you performed."

        val correctPercentage = calculatePercentage(predictionStats?.totalPolls!!, predictionStats.correctCount)
        val inCorrectPercentage = calculatePercentage(predictionStats.totalPolls, predictionStats.incorrectCount)

        val progress1: TextRoundCornerProgressBar = findViewById(R.id.progress_1)
        val progress2: TextRoundCornerProgressBar = findViewById(R.id.progress_2)

        val ques1Perc: TextView = findViewById(R.id.progress_text_1)
        val ques2Perc: TextView = findViewById(R.id.progress_text_2)

        progress1.progress = correctPercentage
        ques1Perc.text = "$correctPercentage%"
        progress1.radius = 15

        progress2.progress = inCorrectPercentage
        ques2Perc.text = "$inCorrectPercentage%"
        progress2.radius = 15

    }
}
