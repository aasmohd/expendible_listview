package com.app.cricketapp.model.matchresponse

import android.text.TextUtils

import com.app.cricketapp.utility.Lg

import java.io.Serializable

/**
 * Created by bhavya on 17-04-2017.
 */

class MatchBean : Serializable {
    private var ballsRemaining: String? = null
    var battingOver: String? = null
    private var androidCurrentVersion: String? = null
    private var battingScore: String? = null
    var battingWickets: String? = null
    var bowlingOver: String? = null
    private var bowlingScore: String? = null
    var bowlingWickets: String? = null
    private var runs: String? = null
    private var sessionOver: String? = null
    private var currentBalls: String? = null
    private var target: String? = null
    var inning: String? = null
    //    var baseUrl: String? = null
    var battingTeam: String? = null
    var sessionSuspend: String? = null
    var matchStartTime: String? = null
    var matchStatus: String? = null
    var landingText: String? = null
    var lambiShow: String? = null

    var battingTeamImage: String? = null
    var bowlerStatus: String? = null
        get() {
            if (TextUtils.isEmpty(field) || field == "0")
                return "-"
            return field
        }
    var bowlingTeam: String? = null
    var bowlingTeamImage: String? = null
    var favouriteTeam: String? = null
        get() {
            if (TextUtils.isEmpty(field) || field == "0")
                return "-"
            return field
        }
    var localDescription: String? = null
    var markerRateTwo: String? = null
    var previousBall: String? = null
    var battingTeamName: String? = null
    var bowlingTeamName: String? = null
    var lambiN: String? = null
    var lambiY: String? = null

    var sessionStatusOne: String? = null
    var sessionStatusTwo: String? = null
    val sessionStatusTwo2: Int
        get() {
            try {
                return Integer.parseInt(sessionStatusTwo)
            } catch (e: Exception) {
                Lg.i("Error", e.message)
                return 0
            }

        }

    var battingTotalOver: String? = null
    var bowlingTotalOver: String? = null
    var currentStatus: CurrentStatus? = null


    var batsmanOneStatus: String? = null
        get() {
            if (TextUtils.isEmpty(field) || field == "0")
                return "-"
            return field
        }
    var batsmanTwoStatus: String? = null
        get() {
            if (TextUtils.isEmpty(field) || field == "0")
                return "-"
            return field
        }


    fun getCurrentBalls(): Int {
        try {
            return Integer.parseInt(currentBalls)
        } catch (e: Exception) {
            Lg.i("Error", e.message)
            return 0
        }

    }

    fun getSessionOver(): Int {
        try {
            return Integer.parseInt(sessionOver)
        } catch (e: Exception) {
            Lg.i("Error", e.message)
            return 0
        }

    }

    fun getAndroidCurrentVersion(): Int {
        try {
            return Integer.parseInt(androidCurrentVersion)
        } catch (e: Exception) {
            e.printStackTrace()
            return -1
        }

    }


    fun getBallsRemaining(): Int {
        try {
            return Integer.parseInt(ballsRemaining)
        } catch (e: Exception) {
            Lg.i("Error", e.message)
            return 0
        }

    }


    fun getBattingScore(): Int? {
        try {
            return battingScore?.toInt()
        } catch (e: Exception) {
            Lg.i("Error", e.message)
            return 0
        }

    }


    fun getBowlingScore(): Int {
        try {
            return Integer.parseInt(bowlingScore)
        } catch (e: Exception) {
            Lg.i("Error", e.message)
            return 0
        }

    }


    fun getRuns(): Int? {
        try {
            return runs?.toInt()
        } catch (e: Exception) {
            Lg.i("Error", e.message)
            return 0
        }

    }

    fun getTarget(): Int {
        try {
            return Integer.parseInt(target)
        } catch (e: Exception) {
            Lg.i("Error", e.message)
            return 0
        }

    }

    class CurrentStatus : Serializable, IMatch {
        internal var MSG: String = ""
        internal var NB: String = ""
        internal var LBYE: String = ""
        internal var BYE: String = ""
        internal var WK: String = ""
        internal var WB: String = ""
        internal var RUN: String = ""
        var REVERTED: String = ""

        var other: String
            get() {
                if (TextUtils.isEmpty(OTHER))
                    return ""
                return OTHER
            }
            set(OTHER) {
                this.OTHER = OTHER
            }

        internal var OTHER: String = ""

        var lbye: String
            get() {
                if (TextUtils.isEmpty(LBYE))
                    return "0"
                return LBYE
            }
            set(LBYE) {
                this.LBYE = LBYE
            }

        var bye: String
            get() {
                if (TextUtils.isEmpty(BYE))
                    return "0"
                return BYE
            }
            set(BYE) {
                this.BYE = BYE
            }

        val run: Int
            get() {
                try {
                    return Integer.parseInt(RUN)
                } catch (e: Exception) {
                    Lg.i("Error", e.message)
                    return 0
                }

            }

        fun setRUN(RUN: String) {
            this.RUN = RUN
        }

        var msg: String
            get() {
                if (TextUtils.isEmpty(MSG) || MSG == "0")
                    return ""
                return MSG
            }
            set(MSG) {
                this.MSG = MSG
            }

        var nb: String
            get() {
                if (TextUtils.isEmpty(NB))
                    return "0"
                return NB
            }
            set(NB) {
                this.NB = NB
            }

        var wk: String
            get() {
                if (TextUtils.isEmpty(WK))
                    return "0"
                return WK
            }
            set(WK) {
                this.WK = WK
            }

        var wb: String
            get() {
                if (TextUtils.isEmpty(WB))
                    return "0"
                return WB
            }
            set(WB) {
                this.WB = WB
            }
    }
}
