
package com.app.cricketapp.model.seasonmodels.seasonDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SeasonDetailModel implements Serializable
{

    @SerializedName("status")
    @Expose
    private Long status;
    @SerializedName("messages")
    @Expose
    private String messages;
    @SerializedName("response")
    @Expose
    private Result result;


    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

}
