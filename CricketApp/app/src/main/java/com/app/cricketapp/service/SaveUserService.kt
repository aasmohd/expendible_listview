package com.app.cricketapp.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.provider.Settings
import android.text.TextUtils
import com.app.cricketapp.BuildConfig
import com.app.cricketapp.model.RequestBean
import com.app.cricketapp.model.save_user.NodeResponseSignup
import com.app.cricketapp.model.save_user.SaveUserResponse
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import com.google.gson.Gson
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by bhavya on 02-06-2017.
 */
class SaveUserService : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        callSaveUserApi()
        return super.onStartCommand(intent, flags, startId)
    }

    private fun callSaveUserApi() {
        val apiService = AppRetrofit.getInstance().apiServices;
        val requestBean = RequestBean()
        val deviceId = Settings.Secure.getString(this.contentResolver,
                Settings.Secure.ANDROID_ID)
        val token = Utility.getStringSharedPreference(this, AppConstants.PREFS_DEVICE_TOKEN)

        if (TextUtils.isEmpty(token))
            requestBean.deviceToken = "hello"
        else
            requestBean.deviceToken = token

        requestBean.deviceTypeID = "1"
        requestBean.deviceID = deviceId
        requestBean.currentVersion = BuildConfig.VERSION_CODE.toString()
        requestBean.clientID = "1"
        requestBean.clientSecret = "Mi6lhaR10HyWOxjMqITx3ONWHFkTcHuebIZPYNi1"

        val call = apiService.saveUser(requestBean);
        call.enqueue(object : Callback<NodeResponseSignup> {

            override fun onResponse(call: Call<NodeResponseSignup>?, response: Response<NodeResponseSignup>?)
            {
                saveData(response)
                stopSelf()
            }

            override fun onFailure(call: Call<NodeResponseSignup>?, t: Throwable?) {
                stopSelf()
            }
        });
    }

    private fun saveData(response: Response<NodeResponseSignup>?) {
        try {
            val userResponse = response?.body()?.response as SaveUserResponse
            val disclaimer = Gson().toJson(userResponse.result?.texts?.disclaimerParagraphs)
            Utility.putStringValueInSharedPreference(this, AppConstants.PREFS_DISCLAIMER, disclaimer)
            Utility.putStringValueInSharedPreference(this, AppConstants.APP_USERID,
                    userResponse.result?.userInfo?.appUserID?.toString())
            Utility.putStringValueInSharedPreference(this, AppConstants.PREFS_ACCESS_TOKEN,
                    userResponse.result?.tokenInfo?.access_token)
            Utility.putStringValueInSharedPreference(this, AppConstants.MATCH_API_SERVER_TIME,
                    userResponse.result?.apiInfo?.match.toString())
            Utility.putStringValueInSharedPreference(this, AppConstants.RANKING_API_SERVER_TIME,
                    userResponse.result?.apiInfo?.ranking.toString())
            Utility.putStringValueInSharedPreference(this, AppConstants.NEWS_SERVER_TIME,
                    userResponse.result?.apiInfo?.news.toString())

            EventBus.getDefault().post(userResponse)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}