package com.app.cricketapp.activity;

import android.widget.CompoundButton
import com.app.cricketapp.R
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility
import kotlinx.android.synthetic.main.activity_setting.*

/**
 * Created by bhavya on 12-04-2017.
 */

 class SettingActivity : BaseActivity(), CompoundButton.OnCheckedChangeListener {

    override fun initUi() {

        setBackButtonEnabled();
        val speechFlag = Utility.getBooleanSharedPreference(this, AppConstants.SPEECH_KEY)
        speech_cb.isChecked= speechFlag
        speech_cb.setOnCheckedChangeListener(this);
    }


    override fun getLayoutById(): Int {
        return R.layout.activity_setting;
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

        when (buttonView?.id) {
            R.id.speech_cb -> Utility.putBooleanValueInSharedPreference(this, AppConstants.SPEECH_KEY, isChecked);
        }
    }
}
