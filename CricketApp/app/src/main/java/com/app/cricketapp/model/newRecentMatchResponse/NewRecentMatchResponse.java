
package com.app.cricketapp.model.newRecentMatchResponse;

import com.app.cricketapp.db.dbhelper.DBClass;
import com.app.cricketapp.model.upcomingmatchresponse.Match;

import java.io.Serializable;
import java.util.List;

public class NewRecentMatchResponse implements DBClass, Serializable {
    public Integer _id;
    public List<Match> t20Matches ;
    public List<Match> testMatche ;
    public List<Match> odiMatches ;

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public List<Match> getT20Matches() {
        return t20Matches;
    }

    public void setT20Matches(List<Match> t20Matches) {
        this.t20Matches = t20Matches;
    }

    public List<Match> getTestMatche() {
        return testMatche;
    }

    public void setTestMatche(List<Match> testMatche) {
        this.testMatche = testMatche;
    }

    public List<Match> getOdiMatches() {
        return odiMatches;
    }

    public void setOdiMatches(List<Match> odiMatches) {
        this.odiMatches = odiMatches;
    }
}
