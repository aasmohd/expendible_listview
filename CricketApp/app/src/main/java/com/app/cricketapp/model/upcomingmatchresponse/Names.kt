package com.app.cricketapp.model.upcomingmatchresponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Names : Serializable{

    @SerializedName("related_name")
    @Expose
    var relatedName: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("short_name")
    @Expose
    var shortName: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null

}
