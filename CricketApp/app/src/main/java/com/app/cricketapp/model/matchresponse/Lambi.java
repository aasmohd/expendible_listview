package com.app.cricketapp.model.matchresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rahulgupta on 04/08/17.
 */

public class Lambi implements IMatch {
    @SerializedName("lambiN")
    @Expose
    public String lambiN;
    @SerializedName("lambiY")
    @Expose
    public String lambiY;
}
