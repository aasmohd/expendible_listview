package com.app.cricketapp.interfaces;


import com.app.cricketapp.model.scorecard.ScorecardResponse;
import com.app.cricketapp.model.upcomingmatchresponse.Match;

/**
 * Created by Prashant on 25-08-2017.
 */

public interface FragmentDataCallback {
     Match getBean();
    ScorecardResponse getMyResponse();
}
