package com.app.cricketapp.interfaces;

/**
 * Created by rahulgupta on 11/08/17.
 */

public enum NavDrawerEnum {
    DISCLAIMER, RATE_US, RANKING, SHARE, FEEDBACK, SEASONS, PLAYER_STATS, COUNTRIES
}
