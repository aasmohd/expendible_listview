package com.app.cricketapp.fragment

import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import com.app.cricketapp.ApplicationController
import com.app.cricketapp.R
import com.app.cricketapp.adapter.PreviousBallsAdapter
import com.app.cricketapp.customview.tv.MatchLedTvView
import com.app.cricketapp.databinding.FragmentNewMatchLineBinding
import com.app.cricketapp.databinding.LayoutInning1Binding
import com.app.cricketapp.databinding.LayoutInning2Binding
import com.app.cricketapp.databinding.LayoutTestInning2Binding
import com.app.cricketapp.model.matchresponse.*
import com.app.cricketapp.shake.Techniques
import com.app.cricketapp.shake.YoYo
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.FireBaseController
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd


/**
 * Created by bhavya on 13-04-2017.
 */

open class NewMatchLineFragment : BaseFragment(), FireBaseController.MatchListener {
    override fun onLiveMatchUrlRecieved(s: String?) {
       dataBinding?.tv?.showLiveUrlBtn(s)
    }

    override fun onMsgKeyReceived(s: String?) {
        if(s.equals("1")){
            dataBinding?.descriptionTv?.visibility = View.VISIBLE
            dataBinding?.descriptionTv1?.visibility = View.GONE

            dataBinding?.descriptionTv?.text = Utility.getStringSharedPreference(context,AppConstants.RUNNING_MSG)
        }else if (TextUtils.isEmpty(fireBaseController?.getValue("localDescription"))) {
            dataBinding?.descriptionTv?.visibility = View.GONE
            dataBinding?.descriptionTv1?.visibility = View.VISIBLE
            dataBinding?.descriptionTv1?.text = "-"
        } else {
            dataBinding?.descriptionTv?.visibility = View.GONE
            dataBinding?.descriptionTv1?.visibility = View.VISIBLE
            dataBinding?.descriptionTv1?.text = fireBaseController?.getValue("localDescription")
        }
    }

    var dataBinding: FragmentNewMatchLineBinding? = null
    var mInterstitialAd: InterstitialAd? = null

    override fun getLayoutById(): Int {
        return R.layout.fragment_new_match_line
    }

    override fun updateSpeechIcon() {
        super.updateSpeechIcon()
        try {
            dataBinding?.tv?.checkSpeechFlag()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun initUi() {
        mInterstitialAd = InterstitialAd(context)
        dataBinding = viewDataBinding as FragmentNewMatchLineBinding?
        dataBinding?.descriptionTv?.isSelected = true
        mInterstitialAd?.adUnitId = getString(R.string.interstitial_ad_unit_id)

        mInterstitialAd?.adListener = object : AdListener() {

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                Lg.i("Add", "add left application")
            }

            override fun onAdFailedToLoad(i: Int) {
                super.onAdFailedToLoad(i)
                requestNewInterstitial()
                Lg.i("Add", "failed :- " + i)
            }

            override fun onAdClosed() {
                super.onAdClosed()
                requestNewInterstitial()
                Lg.i("Add", "closed")
            }

            override fun onAdOpened() {
                super.onAdOpened()
                Lg.i("Add", "opened")
            }

            override fun onAdLoaded() {
                super.onAdLoaded()
                Lg.i("Add", "loaded")

            }
        }
        requestNewInterstitial()

        dataBinding?.previousBallsRv?.layoutManager = LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL, false)

        dataBinding?.tv?.setTvListener(object : MatchLedTvView.TvListener {
            override fun onActionWicket() {
            }

            override fun isScreenVisible(): Boolean {
                return true
//                return isMenuVisible
            }

        })

        loadBannerAd()

        var match = arguments.getParcelable<MatchRoom>(AppConstants.EXTRA_MATCHES)
        val pos = arguments.getInt(AppConstants.EXTRA_POS)
        Lg.i("Match", match.toString())
        //Intent parsing

        matchRoom = match.match_room
        if (matchRoom == "match-rooms") {
        } else/*else if (match.matchRoom == "match2")*/ {
            fireBaseController = FireBaseController.getInstance(mContext)
            dataBinding?.tv?.setFireBaseController(fireBaseController)
            dataBinding?.tv?.setFragmentPos(pos)
            fireBaseController?.setCallBack(this)
        }
    }

    var matchRoom = ""

    var fireBaseController: FireBaseController? = null
    override fun onDestroy() {
        super.onDestroy()
        fireBaseController?.destroy()
//        fireBaseController?.goOfline()
    }

    override fun onStop() {
        super.onStop()
        fireBaseController?.destroy()

    }

    override fun onStart() {
        super.onStart()

    }





    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .build()

        mInterstitialAd?.loadAd(adRequest)
    }

    override fun onPause() {
        super.onPause()
        if (fireBaseController != null){

            fireBaseController?.destroy()
//            fireBaseController?.goOfline()
        }

    }

    override fun onResume() {
        super.onResume()
        checkSpeechFlag()
        showProgressBar(true)

        if (fireBaseController != null){
//            fireBaseController?.goOnline()
            fireBaseController?.init(matchRoom)
        }
    }


    fun checkSpeechFlag() {
        val speechFlag = Utility.getBooleanSharedPreference(activity, AppConstants.SPEECH_KEY)
        if (speechFlag) {
            dataBinding?.tv?.setAction2Icon(R.drawable.speaker_on)
        } else {
            dataBinding?.tv?.setAction2Icon(R.drawable.speaker_off)
        }
    }


    fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        dataBinding?.adView?.loadAd(adRequest)
    }

    /**
     * Method to set data to target score
     */
    fun setTargetView() {
        dataBinding?.targetContainer?.removeAllViews()
        val currentInningKey = fireBaseController?.getValue("currentInning")
        if (currentInningKey == "inning1") {
            val binding = LayoutInning1Binding.inflate(LayoutInflater.from(mContext))
            //( total runs / total balls ) * 6
            val currentRunRate: Double

            val currentBalls1 = fireBaseController?.currentBalls
            if (currentBalls1 == 0) {
                currentRunRate = 0.0
            } else {
                currentRunRate = fireBaseController?.currentRunRate!!
            }
            if (currentRunRate > 0)
                binding?.runRateValueTv?.text = String.format("%.2f", currentRunRate)
            else
                binding?.runRateValueTv?.text = "-"

            val remainingBalls = fireBaseController?.remainingBalls
            if (fireBaseController?.config?.matchFormat?.toLowerCase() == "test") {
                binding?.ballsRemLabelTv?.text = getString(R.string.day_colon)
                binding?.ballsRemValueTv?.text = fireBaseController?.matchDay
            } else {
                if (remainingBalls.toString() == "0")
                    binding?.ballsRemValueTv?.text = "-"
                else
                    binding?.ballsRemValueTv?.text = remainingBalls.toString()
            }
            dataBinding?.targetContainer?.addView(binding.root)
            //3rd Box ============================================================================

        } else if (currentInningKey == "inning2") {
            if (fireBaseController?.config?.matchFormat?.toLowerCase() == "test") {
                val binding = LayoutTestInning2Binding.inflate(LayoutInflater.from(mContext))

                val currentInningsScore = fireBaseController?.currentInnings?.score?.toInt() as Int
                val firstInningsScore = fireBaseController?.firstInning?.score?.toInt() as Int

                if (currentInningsScore >= firstInningsScore) {
                    binding?.trailingByTv?.text = currentInningsScore.minus(firstInningsScore).toString()
                    binding?.trailingLabelTv?.text = getString(R.string.leading_by_colon)
                } else {
                    binding?.trailingByTv?.text = firstInningsScore.minus(currentInningsScore).toString()
                    binding?.trailingLabelTv?.text = getString(R.string.trailing_by_colon)
                }

                if (fireBaseController?.config?.matchFormat?.toLowerCase() == "test") {
                    binding?.ballsRemLabelTv?.text = getString(R.string.day_colon)
                    binding?.ballsRemValueTv?.text = fireBaseController?.matchDay
                } else {
                    val remainingBalls = fireBaseController?.remainingBalls
                    if (remainingBalls.toString() == "0")
                        binding?.ballsRemValueTv?.text = "-"
                    else
                        binding?.ballsRemValueTv?.text = remainingBalls.toString()
                }
                dataBinding?.targetContainer?.addView(binding.root)

            } else {
                val binding = LayoutInning2Binding.inflate(LayoutInflater.from(mContext))

                val currentInnings = fireBaseController?.currentInnings
                binding?.targetValueTv?.text = currentInnings?.target
                val target = Integer.parseInt(currentInnings?.target)

                val bowlingScore = Integer.parseInt(currentInnings?.score)
                val remainingRuns = target - bowlingScore

                if (remainingRuns < 1)
                    binding?.runsNeededTv?.text = "-"
                else
                    binding?.runsNeededTv?.text = remainingRuns.toString()

                val remainingBalls = fireBaseController?.remainingBalls

                if (remainingBalls == 0) {
                    binding?.reqRunRateValueTv?.text = "-"
                } else {
                    if (remainingRuns < 1) {
                        binding?.reqRunRateValueTv?.text = "-"
                    } else {
                        val curr1 = remainingRuns.div(remainingBalls!!.toDouble())
                        val currentRunRate = (curr1 * 6)
                        binding?.reqRunRateValueTv?.text = String.format("%.2f", currentRunRate)
                    }
                }

                if (remainingBalls.toString() == "0")
                    binding?.ballsRemValueTv?.text = "-"
                else
                    binding?.ballsRemValueTv?.text = remainingBalls.toString()

                dataBinding?.targetContainer?.addView(binding.root)
            }
        } else if (currentInningKey == "inning3") {
            val binding = LayoutTestInning2Binding.inflate(LayoutInflater.from(mContext))

            val currentInningsScore = fireBaseController?.currentInnings?.score?.toInt() as Int

            val firstInning = fireBaseController?.firstInning
            val secondInning = fireBaseController?.secondInning

            var totalScore = 0
            var otherInningsScore = 0
            if (firstInning?.batting == fireBaseController?.currentInnings?.batting) {
                totalScore = currentInningsScore.plus(firstInning?.score?.toInt() as Int)
                //inning3 == inning1
                //inning2
                otherInningsScore = secondInning?.score?.toInt() as Int
            } else {
                totalScore = currentInningsScore.plus(secondInning?.score?.toInt() as Int)
                //inning3 == inning2
                //inning1 other team
                otherInningsScore = firstInning?.score?.toInt() as Int
            }

            if (totalScore >= otherInningsScore) {
                binding?.trailingByTv?.text = totalScore.minus(otherInningsScore).toString()
                binding?.trailingLabelTv?.text = getString(R.string.leading_by_colon)
            } else {
                binding?.trailingByTv?.text = otherInningsScore.minus(totalScore).toString()
                binding?.trailingLabelTv?.text = getString(R.string.trailing_by_colon)
            }
            val remainingBalls = fireBaseController?.remainingBalls
            if (fireBaseController?.config?.matchFormat?.toLowerCase() == "test") {
                binding?.ballsRemLabelTv?.text = getString(R.string.day_colon)
                binding?.ballsRemValueTv?.text = fireBaseController?.matchDay
            } else {
                if (remainingBalls.toString() == "0")
                    binding?.ballsRemValueTv?.text = "-"
                else
                    binding?.ballsRemValueTv?.text = remainingBalls.toString()
            }

//            if (remainingBalls.toString() == "0")
//                binding?.ballsRemValueTv?.text = "-"
//            else
//                binding?.ballsRemValueTv?.text = remainingBalls.toString()

            dataBinding?.targetContainer?.addView(binding.root)
        } else if (currentInningKey == "inning4") {
            val binding = LayoutTestInning2Binding.inflate(LayoutInflater.from(mContext))

            val currentInningsScore = fireBaseController?.currentInnings?.score?.toInt() as Int

            val firstInning = fireBaseController?.firstInning
            val secondInning = fireBaseController?.secondInning
            val thirdInning = fireBaseController?.thirdInning

            var totalScore = 0
            var otherInningsScore = 0
            if (firstInning?.batting == fireBaseController?.currentInnings?.batting) {
                totalScore = currentInningsScore.plus(firstInning?.score?.toInt() as Int)
                //inning4 == inning1
                //inning2 == inning3

                val secondInningScore = secondInning?.score?.toInt() as Int
                val thirdInningScore = thirdInning?.score?.toInt() as Int
                otherInningsScore = secondInningScore.plus(thirdInningScore)
            } else if (secondInning?.batting == fireBaseController?.currentInnings?.batting) {
                totalScore = currentInningsScore.plus(secondInning?.score?.toInt() as Int)
                //inning4 == inning2
                //inning1 == inning3 other team

                val firstInningScore = firstInning?.score?.toInt() as Int
                val thirdInningScore = thirdInning?.score?.toInt() as Int
                otherInningsScore = firstInningScore.plus(thirdInningScore)
            }

            if (totalScore >= otherInningsScore) {
                binding?.trailingByTv?.text = (totalScore.minus(otherInningsScore).plus(1)).toString()
                binding?.trailingLabelTv?.text = getString(R.string.target)
            } else {
                binding?.trailingByTv?.text = (otherInningsScore.minus(totalScore).plus(1)).toString()
                binding?.trailingLabelTv?.text = getString(R.string.target)
            }


            val remainingBalls = fireBaseController?.remainingBalls

            if (fireBaseController?.config?.matchFormat?.toLowerCase() == "test") {
                binding?.ballsRemLabelTv?.text = getString(R.string.day_colon)
                binding?.ballsRemValueTv?.text = fireBaseController?.matchDay
            } else {
                if (remainingBalls.toString() == "0")
                    binding?.ballsRemValueTv?.text = "-"
                else
                    binding?.ballsRemValueTv?.text = remainingBalls.toString()
            }

//            if (remainingBalls.toString() == "0")
//                binding?.ballsRemValueTv?.text = "-"
//            else
//                binding?.ballsRemValueTv?.text = remainingBalls.toString()

            dataBinding?.targetContainer?.addView(binding.root)
        }

        showProgressBar(false)

    }

    override fun onConfigReceived(keyConfig: String?, config: Config?) {
        try {
            if (config != null) {
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onCurrentStatusReceived(currentStatus: CurrentStatus?, isPlaySpeech: Boolean) {
        dataBinding?.tv?.setTvData(currentStatus, isPlaySpeech)
//        setSessionBalls()
    }

    override fun onInningDataReceived(keyConfig: String?, team: Inning?) {
        dataBinding?.tv?.setMatchData()
        refreshTargetView()
        setSessionBalls()
    }

    override fun refreshTargetView() {
        setTargetView()
    }

    override fun onTeamDataReceived(keyConfig: String?, team: Team?) {
    }

    override fun updateMarket(market: Market?) {
        val rate1 = dataBinding?.marketRate1Iv?.text
        val rate2 = dataBinding?.marketRate2Iv?.text

        dataBinding?.marketRate1Iv?.text = market?.marketRateOne
        dataBinding?.marketRate2Iv?.text = market?.marketRateTwo

        if (rate1 != market?.marketRateOne && market?.marketRateOne != "0")
            YoYo.with(Techniques.Pulse).playOn(dataBinding?.marketRate1Iv)

        if (rate2 != market?.marketRateTwo && market?.marketRateTwo != "0")
            YoYo.with(Techniques.Pulse).playOn(dataBinding?.marketRate2Iv)
    }

    override fun updateSession(session: Session?) {
        dataBinding?.runValue1Iv?.text = session?.sessionStatusOne.toString()
        dataBinding?.runValue2Iv?.text = session?.sessionStatusTwo.toString()
        dataBinding?.overValueTv?.text = session?.sessionOver.toString()

        setSessionBalls()
    }

    fun setSessionBalls()
    {
        //Runs
        val ballValue1 = fireBaseController?.sessionRuns
        dataBinding?.ballValue1Iv?.text = ballValue1

        //Balls
        val ballValue2 = fireBaseController?.sessionBalls
        dataBinding?.ballValue2Iv?.text = ballValue2
    }

    override fun updateLambi(session: Lambi?) {
        dataBinding?.lambi1Iv?.text = session?.lambiN
        dataBinding?.lambi2Iv?.text = session?.lambiY
    }

    override fun updateAll() {
        val favouriteTeam = fireBaseController?.getValue("favouriteTeam")
        if (favouriteTeam == "0" || favouriteTeam == "-") {
            dataBinding?.teamNameTv?.text = "-"
        } else {
            dataBinding?.teamNameTv?.text = favouriteTeam
        }

        var batsmen1 = fireBaseController?.getValue("batsmanOneStatus")
        if (TextUtils.isEmpty(batsmen1) || batsmen1.equals("0"))
            batsmen1 = "-"
        dataBinding?.batmenNameTv?.text = Html.fromHtml(batsmen1)


        batsmen1 = fireBaseController?.getValue("batsmanTwoStatus")
        if (TextUtils.isEmpty(batsmen1) || batsmen1.equals("0"))
            batsmen1 = "-"
        dataBinding?.batmenName2Tv?.text = Html.fromHtml(batsmen1)
//        dataBinding?.batmenName2Tv?.text = batsmen1


        batsmen1 = fireBaseController?.getValue("bowlerStatus")

        if (TextUtils.isEmpty(batsmen1) || batsmen1.equals("0"))
            batsmen1 = "-"
        dataBinding?.bowlerNameTv?.text = batsmen1



        setPreviousRuns()

        val suspended = fireBaseController?.getValue("sessionSuspend")
        if (suspended == "1") {
            dataBinding?.suspendedTv?.visibility = View.VISIBLE
        } else {
            dataBinding?.suspendedTv?.visibility = View.GONE
        }

        checkLambiShow(fireBaseController?.getValue("lambiShow"))
    }

    override fun updateData(key: String?, value: String?) {
        when (key) {
            "favouriteTeam" -> {
                if (value == "0" || value == "-") {
                    dataBinding?.teamNameTv?.text = "-"
                } else {
                    dataBinding?.teamNameTv?.text = value
                    YoYo.with(Techniques.Swing).playOn(dataBinding?.teamNameTv)
                }
            }
            "batsmanOneStatus" -> {
                var message = value
                if (TextUtils.isEmpty(value) || value.equals("0"))
                    message = "-"
                dataBinding?.batmenNameTv?.text = Html.fromHtml(message)
            }
            "batsmanTwoStatus" -> {
                var message = value
                if (TextUtils.isEmpty(value) || value.equals("0"))
                    message = "-"
                dataBinding?.batmenName2Tv?.text = Html.fromHtml(message)
            }
            "bowlerStatus" -> {
                var message = value
                if (TextUtils.isEmpty(value) || value.equals("0"))
                    message = "-"
                dataBinding?.bowlerNameTv?.text = message
            }
            "localDescription" -> {
                if (TextUtils.isEmpty(value)) {
                    dataBinding?.descriptionTv1?.text = "-"
                } else
                    dataBinding?.descriptionTv1?.text = value

            }
            "previousBall" -> {
                setPreviousRuns()
            }
            "androidAdFlag" -> {
                if (ApplicationController.getApplicationInstance().isActivityVisible)
                    showFullScreenAdd(value!!)
            }
            "sessionSuspend" -> {
                val suspended = value
                if (suspended == "1") {
                    dataBinding?.suspendedTv?.visibility = View.VISIBLE
                } else {
                    dataBinding?.suspendedTv?.visibility = View.GONE
                }
            }
            "lambiShow" -> {
                checkLambiShow(value)
            }
        }
    }

    fun showFullScreenAdd(androidFlag: String) {
        if (androidFlag == "1") {
            if (mInterstitialAd?.isLoaded!!) {
                mInterstitialAd?.show()
            }
        }
    }

    private fun checkLambiShow(lambiShow: String?) {
        if (!TextUtils.isEmpty(lambiShow)) {
            if (lambiShow == "1") {
                dataBinding?.lambiLv?.visibility = View.VISIBLE
                dataBinding?.lambiSeparator?.visibility = View.VISIBLE
            } else {
                dataBinding?.lambiLv?.visibility = View.GONE
                dataBinding?.lambiSeparator?.visibility = View.GONE
            }
        } else {
            dataBinding?.lambiLv?.visibility = View.GONE
            dataBinding?.lambiSeparator?.visibility = View.GONE
        }

    }

    private fun setPreviousRuns() {
        val recyclerViewState = dataBinding?.previousBallsRv?.layoutManager?.onSaveInstanceState()

        val prevRunsList = ArrayList<String>()
        dataBinding?.previousBallsRv?.adapter = null
        val previousBall = fireBaseController?.getValue("previousBall")
        if (!TextUtils.isEmpty(previousBall)) {
            val split = previousBall?.split(",")

            if (fireBaseController?.config?.matchStatus?.toLowerCase() == "notstarted") {
                prevRunsList.clear()
            } else {
                if (split != null) {
                    for (i in 0..split.size - 1) {
                        if (!TextUtils.isEmpty(split[i])) {
                            prevRunsList.add(split[i])
                        }
                    }
                }
            }
            dataBinding?.previousBallsRv?.adapter = PreviousBallsAdapter(prevRunsList)
        } else {
            dataBinding?.previousBallsRv?.adapter = null
        }
        dataBinding?.previousBallsRv?.layoutManager?.onRestoreInstanceState(recyclerViewState)
        try {
//            dataBinding?.previousBallsRv?.adapter?.notifyItemChanged(dataBinding?.previousBallsRv?.adapter?.itemCount!!)
            fireBaseController?.notifyList(dataBinding?.previousBallsRv)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}