
package com.app.cricketapp.model.pollresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Poll {

    @SerializedName("poll_id")
    @Expose
    private String pollId;
    @SerializedName("poll_question")
    @Expose
    private String pollQuestion;
    @SerializedName("match_date")
    @Expose
    private long matchDate;
    @SerializedName("match_format")
    @Expose
    private String matchFormat;
    @SerializedName("is_poll_submitted")
    @Expose
    private long isPollSubmitted;
    @SerializedName("total_votes")
    @Expose
    private long totalVotes;
    @SerializedName("poll_options")
    @Expose
    private List<PollOption> pollOptions = null;
    @SerializedName("team_1_info")
    @Expose
    private Team1Info team1Info;
    @SerializedName("team_2_info")
    @Expose
    private Team2Info team2Info;

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getPollQuestion() {
        return pollQuestion;
    }

    public void setPollQuestion(String pollQuestion) {
        this.pollQuestion = pollQuestion;
    }

    public long getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(long matchDate) {
        this.matchDate = matchDate;
    }

    public String getMatchFormat() {
        return matchFormat;
    }

    public void setMatchFormat(String matchFormat) {
        this.matchFormat = matchFormat;
    }

    public long getIsPollSubmitted() {
        return isPollSubmitted;
    }

    public void setIsPollSubmitted(long isPollSubmitted) {
        this.isPollSubmitted = isPollSubmitted;
    }

    public long getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(long totalVotes) {
        this.totalVotes = totalVotes;
    }

    public List<PollOption> getPollOptions() {
        return pollOptions;
    }

    public void setPollOptions(List<PollOption> pollOptions) {
        this.pollOptions = pollOptions;
    }

    public Team1Info getTeam1Info() {
        return team1Info;
    }

    public void setTeam1Info(Team1Info team1Info) {
        this.team1Info = team1Info;
    }

    public Team2Info getTeam2Info() {
        return team2Info;
    }

    public void setTeam2Info(Team2Info team2Info) {
        this.team2Info = team2Info;
    }

}