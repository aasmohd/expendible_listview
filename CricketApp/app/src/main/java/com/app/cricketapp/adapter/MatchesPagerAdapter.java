package com.app.cricketapp.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.app.cricketapp.fragment.BaseFragment;
import com.app.cricketapp.fragment.NewMatchLineFragment;
import com.app.cricketapp.model.matchresponse.MatchRoom;
import com.app.cricketapp.utility.AppConstants;

import java.util.ArrayList;

/**
 * Created by rahulgupta on 17/08/17.
 */

public class MatchesPagerAdapter extends FragmentStatePagerAdapter {

    public final SparseArray<BaseFragment> mFragmentList = new SparseArray<>();
    public final ArrayList<MatchRoom> matchArrayList;

    public MatchesPagerAdapter(FragmentManager fm, ArrayList<MatchRoom> matchArrayList) {
        super(fm);
        this.matchArrayList = matchArrayList;
    }

    @Override
    public Fragment getItem(int position) {
        BaseFragment baseFragment = new NewMatchLineFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.EXTRA_MATCHES, matchArrayList.get(position));
        bundle.putInt(AppConstants.EXTRA_POS, position);
        baseFragment.setArguments(bundle);

        mFragmentList.put(position, baseFragment);
        return baseFragment;
    }

    @Override
    public int getItemPosition(Object object) {
        // Causes adapter to reload all Fragments when
        // notifyDataSetChanged is called
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        MatchRoom match = matchArrayList.get(position);
        return match.getTeam1() + " vs " + match.getTeam2();
    }

    @Override
    public int getCount() {
        return matchArrayList.size();
    }

}
