package com.app.cricketapp.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.cricketapp.R
import java.util.*

/**
 * Created by bhavya on 12-04-2017.
 */
class DisclaimerAdapter(var context: Context, var latestNewsList: ArrayList<String>) :
        RecyclerView.Adapter<DisclaimerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.row_disclaimer, parent, false)
        return MyViewHolder(view)

    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        holder?.mTitleTv?.text = latestNewsList[position]

    }

    override fun getItemCount(): Int {
        return latestNewsList.size

    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mTitleTv: TextView? = itemView.findViewById(R.id.disclaimer_tv)

    }
}
