package com.app.cricketapp.utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.app.cricketapp.ApplicationController;
import com.app.cricketapp.R;
import com.app.cricketapp.customview.MaterialProgressDialog;
import com.app.cricketapp.interfaces.OkCancelCallback;
import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import me.zhanghai.android.materialprogressbar.IndeterminateProgressDrawable;

@SuppressLint("SimpleDateFormat")
public class Utility {

    private static AlertDialog mAlertDialog;

    /**
     * Remove the outer dialog padding so that it takes full screen width
     *
     * @param dialog Instance of the dialog
     */
    public static void modifyDialogBoundsToFill(Dialog dialog) {
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.gravity = Gravity.CENTER;
        lp.width = (int) (dialog.getContext().getResources().getDisplayMetrics().widthPixels * 0.95);
//        double height = dialog.getContext().getResources().getDisplayMetrics().heightPixels * 0.6;
//        lp.height = (int) height;
        window.setAttributes(lp);
    }


    /**
     * Method to put string value in shared preference
     *
     * @param context Context of the calling class
     * @param key     Key in which value to store
     * @param value   String value to be stored
     */
    public static void putStringValueInSharedPreference(Context context, String key, String value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public static void putIntValueInSharedPreference(Context context, String key, int value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }


    /**
     * Method to put long value in shared preference
     *
     * @param context Context of the calling class
     * @param key     Key in which value to store
     * @param value   Long value to be stored
     */
    public static void putLongValueInSharedPreference(Context context, String key, long value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    /**
     * Method to put boolean value in shared preference
     *
     * @param context Context of the calling class
     * @param key     Key in which value to store
     * @param value   Boolean value to be stored
     */
    public static void putBooleanValueInSharedPreference(Context context, String key, boolean value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * Method to get string value from shared preference
     *
     * @param context Context of the calling class
     * @param param   Key from which value is retrieved
     */
    public static String getStringSharedPreference(Context context, String param) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(param, "");
    }


    public static int getIntFromSharedPreference(Context context, String param) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(param, 0);
    }

    /**
     * Method to get long value from shared preference
     *
     * @param context Context of the calling class
     * @param param   Key from which value is retrieved
     */
    public static long getLongSharedPreference(Context context, String param) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getLong(param, 0);
    }

    /**
     * Method to get boolean value from shared preference
     *
     * @param context Context of the calling class
     * @param param   Key from which value is retrieved
     */
    public static boolean getBooleanSharedPreference(Context context, String param) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(param, false);
    }

    /**
     * Method to clear shared preference key value
     *
     * @param context Context of the calling class
     * @param key     Key from which value is to be cleared
     */
    public static void clearSharedPrefData(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }

    /**
     * Method to clear all shared preference key value
     *
     * @param context Context of the calling class
     */
    public static void clearAllSharedPrefData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    /**
     * Static method to get an instance of material styled progress dialog
     *
     * @param mContext Context of the calling class
     * @return An instance of MaterialProgressDialog
     */
    public static MaterialProgressDialog getProgressDialogInstance(Context mContext) {
        MaterialProgressDialog mProgressDialog = new MaterialProgressDialog(mContext,
                mContext.getString(R.string.loading));
        mProgressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        mProgressDialog.setCancelable(false);
        return mProgressDialog;
    }

    /**
     * Static method to get an instance of material styled progress bar
     *
     * @param mContext Context of the calling class
     * @param resId    Resource Id of the progress bar
     * @return An instance of MaterialProgressBar
     */
    public static ProgressBar getProgressBarInstance(Context mContext, int resId) {

        ProgressBar nonBlockingProgressBar = (ProgressBar) ((Activity) mContext).getWindow().findViewById(resId);
        if (nonBlockingProgressBar != null)
            nonBlockingProgressBar.setIndeterminateDrawable(new IndeterminateProgressDrawable(mContext));
        return nonBlockingProgressBar;
    }

    /**
     * Static method to show alert dialog
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */
    public static void showAlertDialog(Context mContext, String text) {
        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher).setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog.dismiss();
                    }
                }).create();

        mAlertDialog.show();
        mAlertDialog.show();
        Button button = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        button.setTextColor(mContext.getResources().getColor(R.color.color_accent));
    }

    /**
     * Static method to show alert dialog
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */
    public static void showAlertDialog(Context mContext, String text, final OkCancelCallback okCancelCallback) {
        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher).setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog.dismiss();
                        if (okCancelCallback != null)
                            okCancelCallback.onOkClicked();
                    }
                }).create();

        mAlertDialog.show();
        mAlertDialog.show();
        Button button = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        button.setTextColor(mContext.getResources().getColor(R.color.color_accent));
    }

    /**
     * Static method to show alert dialog with Ok cancel buttons
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */
    public static void showAlertDialogWithCallback(Context mContext, String text,
                                                   final OkCancelCallback okCancelCallback) {

        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher)
                .setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog.dismiss();
                        if (okCancelCallback != null)
                            okCancelCallback.onCancelClicked();
                    }
                }).setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog.dismiss();
                        if (okCancelCallback != null)
                            okCancelCallback.onOkClicked();

                    }
                }).create();
        mAlertDialog.show();
        mAlertDialog.show();
        Button nbutton = mAlertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(Color.GRAY);
        Button pbutton = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(mContext.getResources().getColor(R.color.color_accent));
    }

    /**
     * Static method to show alert dialog with Ok cancel buttons
     *
     * @param mContext     Context of the calling class
     * @param text         Text to show in toast
     * @param positiveText Text to show on OKAY button
     * @param negativeText Text to show on CANCEL button
     */

    public static void showAlertDialogWithCallbackAndText(Context mContext, String text,
                                                          final OkCancelCallback okCancelCallback, String positiveText,
                                                          String negativeText) {

        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher)
                .setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog.dismiss();
                        if (okCancelCallback != null)
                            okCancelCallback.onCancelClicked();
                    }
                }).setCancelable(false)
                .setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog.dismiss();
                        if (okCancelCallback != null)
                            okCancelCallback.onOkClicked();

                    }
                }).create();

        mAlertDialog.show();
        mAlertDialog.show();
        Button nbutton = mAlertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(Color.GRAY);
        Button pbutton = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(mContext.getResources().getColor(R.color.color_accent));
    }

    /**
     * Static method to show alert dialog with Ok cancel buttons
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */

    public static AlertDialog showAlertDialogWithOkButton(Context mContext, String text,
                                                          final OkCancelCallback okCallback) {
        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher)
                .setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog.dismiss();
                        if (okCallback != null)
                            okCallback.onOkClicked();

                    }
                }).create();

        mAlertDialog.show();
        return mAlertDialog;
    }

    /**
     * Static method to show alert dialog with Ok cancel buttons
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */

    public static AlertDialog showAlertDialogWithOkButton(Context mContext, String text,
                                                          final OkCancelCallback okCallback, String positiveText) {
        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher)
                .setCancelable(false)
                .setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog.dismiss();
                        if (okCallback != null)
                            okCallback.onOkClicked();

                    }
                }).create();

        mAlertDialog.show();
        return mAlertDialog;
    }

    /**
     * Static method to show alert dialog with Ok cancel buttons
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */

    public static AlertDialog showAlertDialogWithOkCancelButton(Context mContext, String text,
                                                                final OkCancelCallback okCallback,
                                                                String positiveText, String negativeText) {
        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher)
                .setCancelable(false)
                .setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog.dismiss();
                        if (okCallback != null)
                            okCallback.onOkClicked();

                    }
                }).setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog.dismiss();
                        if (okCallback != null)
                            okCallback.onCancelClicked();

                    }
                })
                .create();

        mAlertDialog.show();
        return mAlertDialog;
    }

    public static boolean isShowFullAdd() {
        int randomNumberForAd = ApplicationController.getApplicationInstance().getRandomNumberForAd();
        int randomNum = new Random().nextInt(100);
        ApplicationController.getApplicationInstance().setRandomNumberForAd(randomNum);
        if (randomNumberForAd % 3 == 0) {
            Lg.i("Advertisement", randomNumberForAd+" : Number True");
            return true;
        } else {
            Lg.i("Advertisement", randomNumberForAd+" : Number False");
            return false;
        }
    }

    /**
     * This method is depreciated
     * use<code>ApplicationController.getApplicationInstance().isNetworkConnected()</code>
     *
     * @see Utility#getNetworkState(Context context)
     */
    /*@Deprecated
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED ||
        connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTING ||
        connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
         connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTING;
    }*/
    public static boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return connMgr.getActiveNetworkInfo().getState() == NetworkInfo.State.CONNECTED
                    || connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTING
                    || connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                    || connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTING;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Static method to check network availability
     *
     * @param context Context of the calling class
     */

    public static boolean getNetworkState(Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }


    public static void showKeyboard(Context context, EditText editText) {
        showKeyboard(context);
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
            imm.showSoftInput(((Activity) context).getCurrentFocus(), InputMethodManager.SHOW_FORCED);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to show keyboard
     *
     * @param context Context of the calling activity
     */
    public static void showKeyboard(Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(((Activity) context).getCurrentFocus(), InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Method to hide keyboard
     *
     * @param mContext Context of the calling class
     */
    public static void hideKeyboard(Context mContext) {
        try {
            InputMethodManager inputManager = (InputMethodManager) mContext
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(((Activity) mContext).getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    /**
     * Method to hide keyboard on view focus
     *
     * @param context    Context of the calling class
     * @param myEditText focussed view
     */
    public static void hideKeyboard(Context context, View myEditText) {
        hideKeyboard(context);
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    public static MaterialProgressDialog getProgressDialogInstanceWithText(Context mContext,
                                                                           String dialogText) {
        MaterialProgressDialog mProgressDialog = new MaterialProgressDialog(mContext,
                dialogText);
        mProgressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        mProgressDialog.setCancelable(false);
        return mProgressDialog;
    }

    public static String getLocalDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        formatter.setTimeZone(TimeZone.getDefault(/*"GMT+5:30"*/));
        String dateString = formatter.format(new Date(milliSeconds));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        calendar.add(Calendar.MILLISECOND, TimeZone.getDefault().getOffset(calendar.getTimeInMillis()));

//        return formatter.format(calendar.getTime());
        return dateString;
    }

    public static String getLocalDate2(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = formatter.format(new Date(milliSeconds));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        calendar.add(Calendar.MILLISECOND, TimeZone.getDefault().getOffset(calendar.getTimeInMillis()));
//        return formatter.format(calendar.getTime());
        return dateString;
    }

    public static String getUtcDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = formatter.format(new Date(milliSeconds));

        return dateString;
    }

    public static String getLocalTimeZone() {
        SimpleDateFormat formatter = new SimpleDateFormat("zzz");
        formatter.setTimeZone(TimeZone.getDefault());
        String dateString = formatter.format(new Date());
        return TimeZoneList.getTimezoneShort(dateString, null);
    }


    /**
     * Method to parse date from server
     *
     * @param date         Date from the server
     * @param sourceFormat KFormatter of the date from server
     * @param targetFormat Target format in which to return the date
     * @return Formatted date
     */
    public static String parseDateTimeUtc(String date, String sourceFormat, String targetFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(sourceFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date strDate = new Date();

        try {
            strDate = sdf.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SimpleDateFormat sdf2;
        sdf2 = new SimpleDateFormat(targetFormat);
        sdf2.setTimeZone(TimeZone.getDefault());
        return sdf2.format(strDate);
    }

    /**
     * Method to parse date from server
     *
     * @param date         Date from the server
     * @param sourceFormat KFormatter of the date from server
     * @return Formatted date
     */
    public static Date parseDateTimeLocal(String date, String sourceFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(sourceFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date strDate = new Date();

        try {
            strDate = sdf.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strDate;
    }

    public static String parseDateTimeUtcToGmt(String stringDate) {
        DateFormat formatter;
        Date date = null;
        formatter = new SimpleDateFormat(AppConstants.SERVER_DATE_FORMAT);
        try {
            date = formatter.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //creating DateFormat for converting time from local timezone to GMT
        DateFormat converter = new SimpleDateFormat(AppConstants.SERVER_DATE_FORMAT1);

        //getting GMT timezone, you can get any timezone e.g. UTC
        converter.setTimeZone(TimeZone.getTimeZone("GMT"));

        System.out.println("local time : " + date);
        System.out.println("time in GMT : " + converter.format(date));
        return converter.format(date);
    }

    public static String getTimeAgo(String timeString, Context ctx, String dateFormat) {

        SimpleDateFormat sourceFormat = new SimpleDateFormat(dateFormat);
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date;
        String timeAgo;
        try {
            date = sourceFormat.parse(timeString);

            TimeZone tz = TimeZone.getDefault();
            SimpleDateFormat destFormat = new SimpleDateFormat(dateFormat);
            destFormat.setTimeZone(tz);

            String result = destFormat.format(date);
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
            Date startDate = formatter.parse(result);

            long time = startDate.getTime();

            Date curDate = currentDate();
            long now = curDate.getTime();
            /*if (time >= now || time <= 0) {
                timeAgo = ctx.getResources().getString(R.string.date_util_term_less)
                + " " + ctx.getResources().getString(R.string.date_util_term_a) +
                 " " + ctx.getResources().getString(R.string.date_util_unit_minute);
			}*/

            int dim = getTimeDistanceInMinutes(time);

            if (dim == 0 || dim == 1) {
                return /*timeAgo =  "1 " + ctx.getResources()
                .getString(R.string.date_util_unit_minute)*/"now";
            } else if (dim >= 2 && dim <= 44) {
                timeAgo = dim + " " + ctx.getResources().getString(R.string.date_util_unit_minutes);
            } else if (dim >= 45 && dim <= 89) {
                timeAgo = "1 " + ctx.getResources().getString(R.string.date_util_unit_hour);
            } else if (dim >= 90 && dim <= 1439) {

                if ((Math.round(dim / 60)) == 1)
                    timeAgo = (Math.round(dim / 60)) + " "
                            + ctx.getResources().getString(R.string.date_util_unit_hour);
                else
                    timeAgo = (Math.round(dim / 60)) + " "
                            + ctx.getResources().getString(R.string.date_util_unit_hours);
            } else if (dim >= 1440 && dim <= 2519) {
                timeAgo = "1 " + ctx.getResources().getString(R.string.date_util_unit_day);
            } else if (dim >= 2520 && dim <= 43199) {
                timeAgo = (Math.round(dim / 1440)) + "";
                if (timeAgo.equals("1")) {
                    timeAgo = timeAgo + " " + ctx.getResources().getString(R.string.date_util_unit_day);
                } else {
                    timeAgo = timeAgo + " " + ctx.getResources().getString(R.string.date_util_unit_days);
                }

            } else if (dim >= 43200 && dim <= 86399) {
                timeAgo = "1 " + ctx.getResources().getString(R.string.date_util_unit_month);
            } else if (dim >= 86400 && dim <= 525599) {
                timeAgo = (Math.round(dim / 43200)) + " "
                        + ctx.getResources().getString(R.string.date_util_unit_months);
            } else if (dim >= 525600 && dim <= 655199) {
                timeAgo = "1 "
                        + ctx.getResources().getString(R.string.date_util_unit_year);
            } else if (dim >= 655200 && dim <= 1051199) {
                timeAgo = "2 "
                        + ctx.getResources()
                        .getString(R.string.date_util_unit_year);
            } else {
                timeAgo = (Math.round(dim / 525600))
                        + " "
                        + ctx.getResources().getString(
                        R.string.date_util_unit_years);
            }
        } catch (Exception e) {
            timeAgo = timeString;
            e.printStackTrace();
        }
        return /*ctx.getResources().getString(R.string.date_util_prefix_about) + " " + */timeAgo
                + " " + ctx.getResources().getString(R.string.date_util_suffix);
    }

    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        return calendar.getTime();
    }

    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }

    public static void setImageWithUrl(String thumbUrl, int newsPlaceholder, ImageView mNewsIv) {
        Picasso.with(mNewsIv.getContext()).load(thumbUrl)
                .placeholder(newsPlaceholder)
                .error(newsPlaceholder)
                .into(mNewsIv);
    }

    /**
     * General Method to generate Hash-key for facebook app.
     */
    public static void generateFBKeyHash(Context mContext) {
        try {
            PackageInfo info = mContext.getPackageManager().getPackageInfo("com.app.cricketapp", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
    /**
     * Method to check app is in background or not
     *
     * @param context Context
     * @return is app in background
     */
    public static boolean isApplicationInBackground(Context context) {
        final ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningTaskInfo> tasks = manager.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            final ComponentName topActivity = tasks.get(0).topActivity;
            return !topActivity.getPackageName().equals(context.getPackageName());
        }
        return false;
    }

    public static int getDayOfWeek(long milliSeconds, String dateFormat) {

    /*    Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        calendar.add(Calendar.MILLISECOND, TimeZone.getDefault().getOffset(calendar.getTimeInMillis()));
        int day = calendar.get(Calendar.DAY_OF_WEEK);*/

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        d.setTime(milliSeconds);
        String day = sdf.format(d);

//        return formatter.format(calendar.getTime());
        switch (day){
            case "Sunday" :
                return R.drawable.sun;
            case "Monday":
                return R.drawable.mon;
            case "Tuesday":
                return R.drawable.tue;
            case "Wednesday":
                return R.drawable.wed;
            case "Thursday":
                return R.drawable.thu;
            case "Friday":
                return R.drawable.fri;
            case "Saturday":
                return R.drawable.sat;
        }
        return 0;
    }

    public static String getDay(long milliseconds){
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        d.setTime(milliseconds);
        String day = sdf.format(d);
        return day;
    }
}