package com.app.cricketapp.activity

import android.os.Handler
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.widget.RadioGroup
import com.app.cricketapp.R
import com.app.cricketapp.adapter.RankingPagerAdapter
import com.app.cricketapp.customview.CustomToast
import com.app.cricketapp.databinding.ActivityRankingBinding
import com.app.cricketapp.db.dao.RankingResponseDAO
import com.app.cricketapp.fragment.PlayerRankingFragment
import com.app.cricketapp.model.ranking.RankingResponse
import com.app.cricketapp.network.ApiServices
import com.app.cricketapp.network.AppRetrofit
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Lg
import com.app.cricketapp.utility.Utility
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Prashant on 21-08-2017.
 */
class RankingActivity : BaseActivity(), ViewPager.OnPageChangeListener {

    var activityRanking: ActivityRankingBinding? = null
    var selectedId = 0
    var dataValue = 0
    var response: RankingResponse? = null
    var viewPager: ViewPager? = null
    var mAdView: AdView? = null

    override fun initUi() {
        activityRanking = viewDataBinding as ActivityRankingBinding?
        adapter = RankingPagerAdapter(supportFragmentManager)
        val tabLayout: TabLayout? = activityRanking?.tabLayout
        viewPager = activityRanking?.viewPager
        viewPager?.addOnPageChangeListener(this)
        mAdView = findViewById(R.id.adView) as AdView?

        val rgRanking: RadioGroup? = activityRanking?.rgRanking
        var selectedId: Int = rgRanking?.checkedRadioButtonId!!
        rgRanking.setOnCheckedChangeListener { radioGroup, i ->
            if (i == activityRanking?.rbBatsman?.id) {
                dataValue = 0
                if (response != null) {

                    setData(dataValue, response, viewPager?.currentItem!!)
                }
            } else if (i == activityRanking?.rbBowler?.id) {
                dataValue = 1
                if (response != null) {

                    setData(dataValue, response, viewPager?.currentItem!!)
                }
            } else if (i == activityRanking?.rbAllRounder?.id) {
                dataValue = 2
                if (response != null) {

                    setData(dataValue, response, viewPager?.currentItem!!)
                }
            } else if (i == activityRanking?.rbTeams?.id) {
                dataValue = 3
                if (response != null) {

                    setData(dataValue, response, viewPager?.currentItem!!)
                }
            }
        }
        setupViewPager(viewPager!!, tabLayout!!)
        showProgressBar(true)
        if (Utility.getLongSharedPreference(this, "ranking_time_stamp") != 0L) {
            // time difference check logic
            if (Utility.getLongSharedPreference(this, "ranking_time_stamp")<Utility.
                    getStringSharedPreference(this,AppConstants.RANKING_API_SERVER_TIME).toLong())
            {
                if (Utility.isNetworkAvailable(this)) {
                    callRankingService()
                } else {
                    showProgressBar(false)
                    CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
                }
            } else {

                response = RankingResponseDAO.getRecentMatches()
                if (response != null) {
                    Handler().postDelayed({
                        setData(dataValue, response, viewPager?.currentItem!!)
                        showProgressBar(false)
                    }, 500)

                }
            }

        } else {
            val time = System.currentTimeMillis()

            Utility.putLongValueInSharedPreference(this, "ranking_time_stamp", time)

            if (Utility.isNetworkAvailable(this)) {
                callRankingService()
            } else {
                showProgressBar(false)
                CustomToast.getInstance(this).showToast(getString(R.string.check_internet_connection))
            }
        }
//        MobileAds.initialize(applicationContext, getString(R.string.banner_ad_unit_id))


        loadBannerAd()
        initFullScreenAdd()


    }

    fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        mAdView?.loadAd(adRequest)
    }

    override fun getLayoutById(): Int = R.layout.activity_ranking

    var adapter: RankingPagerAdapter? = null
    private fun setupViewPager(viewPager: ViewPager, tabLayout: TabLayout) {

        adapter?.addFragment(PlayerRankingFragment(), "ODI")
        adapter?.addFragment(PlayerRankingFragment(), "TEST")
        adapter?.addFragment(PlayerRankingFragment(), "T20")
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        when (position) {
            0 -> {
                if (this@RankingActivity.response != null) {

                    setData(dataValue, this@RankingActivity.response, 0)
                }
            }
            1 -> {
                if (this@RankingActivity.response != null) {

                    setData(dataValue, this@RankingActivity.response, 1)
                }
            }
            2 -> {
                if (this@RankingActivity.response != null) {

                    setData(dataValue, this@RankingActivity.response, 2)
                }
            }
        }
    }

    private fun callRankingService() {

        val apiService: ApiServices = AppRetrofit.getInstance().apiServices
        val call: Call<RankingResponse> = apiService.rankingData()
        call.enqueue(object : Callback<RankingResponse> {
            override fun onFailure(call: Call<RankingResponse>?, t: Throwable?) {
                showProgressBar(false)
            }

            override fun onResponse(call: Call<RankingResponse>?, response: Response<RankingResponse>?) {
               if (response?.body() != null){
                   Utility.putLongValueInSharedPreference(this@RankingActivity, "ranking_time_stamp", System.currentTimeMillis())
                   showProgressBar(false)
                   RankingResponseDAO.delete();
                   RankingResponseDAO.insert(response?.body()!!);

                   this@RankingActivity.response = RankingResponseDAO.getRecentMatches()
                   if (this@RankingActivity.response != null) {
                       setData(dataValue, this@RankingActivity.response, this@RankingActivity.viewPager?.currentItem!!)
                   }
               }
            }

        })
    }

    private fun setData(dataValue: Int, response: RankingResponse?, tabPosition: Int) {
        val playerRankingFragment: PlayerRankingFragment = adapter?.getItem(viewPager?.currentItem!!) as PlayerRankingFragment
        playerRankingFragment.refreshList(dataValue, response!!, tabPosition)
        /* val fr = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.view_pager + ":" + viewPager?.currentItem)
         if(fr is PlayerRankingFragment) {
             fr.refreshList(dataValue, response!!, tabPosition)
         }*/
    }

    val mInterstitialAd: InterstitialAd = InterstitialAd(this)
    private fun initFullScreenAdd() {
        if (Utility.isShowFullAdd()) {

            mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

            mInterstitialAd.adListener = object : AdListener() {

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Lg.i("Add", "add left application")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    requestNewInterstitial()
                    Lg.i("Add", "failed :- " + i)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Lg.i("Add", "closed")
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Lg.i("Add", "opened")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Lg.i("Add", "loaded")
                    mInterstitialAd.show()

                }
            }
            requestNewInterstitial()
        }
    }

    fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("8E490BBD0F00223A022E8AC34F3AC31D")
                .addTestDevice("06CD7F1190497B9ADD39C03F2D21C717")
                .build()

        mInterstitialAd.loadAd(adRequest)
    }


}