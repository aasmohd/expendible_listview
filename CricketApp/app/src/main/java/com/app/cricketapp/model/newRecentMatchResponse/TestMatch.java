package com.app.cricketapp.model.newRecentMatchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Prashant on 12-09-2017.
 */

public class TestMatch {

    @SerializedName("basics_info")
    @Expose
    private BasicsInfo basicsInfo;
    @SerializedName("names")
    @Expose
    private Names names;
    @SerializedName("season")
    @Expose
    private Season season;
    @SerializedName("teams")
    @Expose
    private Teams teams;
    @SerializedName("others")
    @Expose
    private Others others;
    @SerializedName("results")
    @Expose
    private Results results;
    @SerializedName("msgs")
    @Expose
    private Msgs msgs;

    public BasicsInfo getBasicsInfo() {
        return basicsInfo;
    }

    public void setBasicsInfo(BasicsInfo basicsInfo) {
        this.basicsInfo = basicsInfo;
    }

    public Names getNames() {
        return names;
    }

    public void setNames(Names names) {
        this.names = names;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Teams getTeams() {
        return teams;
    }

    public void setTeams(Teams teams) {
        this.teams = teams;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

    public Msgs getMsgs() {
        return msgs;
    }

    public void setMsgs(Msgs msgs) {
        this.msgs = msgs;
    }

}
