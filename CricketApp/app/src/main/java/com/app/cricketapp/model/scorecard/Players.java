package com.app.cricketapp.model.scorecard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by rahulgupta on 18/07/17.
 */

public class Players implements Serializable{

    @SerializedName("fullname")
    @Expose
    public String fullName;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("player_key")
    @Expose
    public String playerKey;
}
