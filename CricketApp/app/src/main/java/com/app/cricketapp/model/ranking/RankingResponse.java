
package com.app.cricketapp.model.ranking;

import com.app.cricketapp.db.dbhelper.DBClass;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RankingResponse implements DBClass,Serializable{

    public Integer _id;
    @SerializedName("status")
    @Expose
    public Long status;
    @SerializedName("messages")
    @Expose
    public String messages;
    @SerializedName("response")
    @Expose
    public Result result;

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

}
