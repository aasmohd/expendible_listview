
package com.app.cricketapp.model.matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatchesResponse {

    @SerializedName("status")
    @Expose
    public long status;
    @SerializedName("messages")
    @Expose
    public String messages;
    @SerializedName("result")
    @Expose
    public Result result;

}
