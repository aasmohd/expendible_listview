package com.app.cricketapp.activity;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.cricketapp.R;
import com.app.cricketapp.model.MyPollAccuracy.Polls;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    private Context context;
    private List<Polls> my_data;
    int colorwhite = Color.parseColor("#f9f5f5");
    int white = Color.parseColor("#ffffff");
    int [] img_res={R.drawable.right,R.drawable.wrong};



    RecyclerAdapter(Context context, List<Polls> my_data) {
        this.context = context;
        this.my_data = my_data;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapterdata, null);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        holder.linearLayoutl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        holder.linearLayoutl.setOrientation(LinearLayout.HORIZONTAL);
        String TimestToDate = getTStoDate(my_data.get(position).getMatch_date());

        holder.date.setText(TimestToDate);
        holder.teams.setText(my_data.get(position).getTeams());
        holder.won.setText(my_data.get(position).getWinTeam());
        holder.mypoll.setText(my_data.get(position).getMyPoll());
       if (position % 2 == 0) {
            holder.linearLayoutl.setBackgroundColor(colorwhite);
        } else {
            holder.linearLayoutl.setBackgroundColor(white);
        }

        if(my_data.get(position).getMyPoll().equals(my_data.get(position).getWinTeam())) {
            holder.mypoll.setVisibility(View.GONE);
           // holder.mypoll.setText("right");
            holder.right_wrong_pic.setVisibility(View.VISIBLE);
            holder.right_wrong_pic.setImageResource(img_res[0]);
        }
        else if((my_data.get(position).getWinTeam().equals("pending"))) {
            holder.mypoll.setText(my_data.get(position).getMyPoll());
        }
        else {
            holder.mypoll.setVisibility(View.GONE);
           // holder.mypoll.setText("wrong");
            holder.right_wrong_pic.setVisibility(View.VISIBLE);
            holder.right_wrong_pic.setImageResource(img_res[1]);
        }

    }

    @Override
    public int getItemCount() {

        return my_data.size();

    }

    private String getTStoDate(String tsValue) {

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        Long num = Long.parseLong(tsValue);
        cal.setTimeInMillis(num);
        return DateFormat.format("dd-MMM-yy", cal).toString();
    }


    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView date, teams, won, mypoll;
        private LinearLayout linearLayoutl;
        private ImageView right_wrong_pic;


        RecyclerViewHolder(View View) {
            super(View);
            linearLayoutl = View.findViewById(R.id.cardview);
            date = (TextView) View.findViewById(R.id.date);
            teams = (TextView) View.findViewById(R.id.teams);
            won = (TextView) View.findViewById(R.id.won);
            mypoll = (TextView) View.findViewById(R.id.mypoll);
            right_wrong_pic=(ImageView)View.findViewById(R.id.right_wrong_pic);
        }
    }


}