package com.app.cricketapp.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.app.cricketapp.R
import com.app.cricketapp.activity.SeasonDetailActivity
import com.app.cricketapp.model.seasonmodels.seasonsList.Season
import com.app.cricketapp.utility.AppConstants
import com.app.cricketapp.utility.Utility

/**
 * Created by Prashant on 04-10-2017.
 */
class SeasonAdapter(var context:Context , public var seasonList:MutableList<Season>):RecyclerView.Adapter<SeasonAdapter.SeasonVH>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SeasonVH {
       val view = LayoutInflater.from(context).inflate(R.layout.row_season,parent,false)
        return SeasonVH(view)
    }

    override fun getItemCount(): Int {
        return seasonList.size
    }

    override fun onBindViewHolder(holder: SeasonVH?, position: Int) {
        holder?.seasonName?.text = seasonList.get(position).name
        holder?.seasondate?.text = Utility.getLocalDate(seasonList.get(position).startDate*1000,"dd MMM")+
                " - "+ Utility.getLocalDate(seasonList.get(position).endDate*1000,"dd MMM yyyy")
        holder?.seasonLayout?.setOnClickListener({
            var intent = Intent(context,SeasonDetailActivity::class.java)
            intent.putExtra(AppConstants.EXTRA_KEY,seasonList.get(position).seasonKey.toString())
            intent.putExtra("title",seasonList.get(position).name.toString())
            context.startActivity(intent)
        })
    }

    class SeasonVH(view:View) : RecyclerView.ViewHolder(view) {
        var seasonName = view.findViewById<TextView>(R.id.tv_seasons)
        var seasondate = view.findViewById<TextView>(R.id.tv_date)
        var seasonLayout = view.findViewById<RelativeLayout>(R.id.rel_lay)

    }

}